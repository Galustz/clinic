function getDetails(id){
$.ajax({					
    type:'get',					
    url:'pharmacy/listdetails' + '/' + id,	
    dataType: 'json',	
    data:'',		
})
.done(function(data) {
    console.log(data);
})
.fail(function(error) {
    console.log(error);
});
}

function newservicecost(id){
    $.ajax({					
        type:'get',					
        url:'getMedCost' + '/' + id,	
        dataType: 'json',	
        data:'',		
    })
    .done(function(data) {
        console.log(data);
        var currentcost = parseInt($('#service_cost').val());
        if($('#medstatus'+id).val()==="Refused"){
            $('#service_cost').val(currentcost-data);
        }else if($('#medstatus'+id).val()==="Taken"){
            $('#service_cost').val(data+currentcost);
        }
    })
    .fail(function(error) {
        console.log(error);
    });
    console.log($('#medstatus'+id).val());
}
function newtestcost(id){
    console.log(id);
    $.ajax({					
        type:'get',					
        url:'getTestCost' + '/' + id,	
        dataType: 'json',	
        data:'',		
    })
    .done(function(data) {
        console.log(data);
        var currentcost = parseInt($('#service_cost').val());
        if($('#medstatus'+id).val()==="Refused"){
            console.log('refused');
            $('#service_cost').val(currentcost-data);
        }else if($('#medstatus'+id).val()==="Testing"){
            console.log('taken');
            $('#service_cost').val(data+currentcost);
        }
    })
    .fail(function(error) {
        console.log(error);
    });
}
function calculateChange(){
    var amountPaid = parseInt($('#amount_paid').val());
    var serviceCost = parseInt($('#service_cost').val());
    if(amountPaid<=serviceCost){
        var amountDue =serviceCost- amountPaid;
        $('#amount_due').val(amountDue);
        $('#change').val("0");
        console.log(amountDue);
    }else{
        var change = amountPaid - serviceCost;
        $('#amount_due').val("0");
        $('#change').val(change);
        console.log( amountPaid + "" + serviceCost);
    }
}

function calculateRemainingChange(){
    var change = parseInt($('#change').val());
    var changeGiven = parseInt($('#change_given').val());
    if(changeGiven<=change){
        var remainingChange = change - changeGiven;
        $('#change_remaining').val(remainingChange);
        console.log(remainingChange);
    }else{
        $('#change_remaining').val("0");
    }
}