
$('#modal-dob').on('change',function(){
    var dob_val = $('#modal-dob').val();
    $.ajax({
        url:  rootUrl + '/getAge',
        type: 'GET',
        dataType: 'json',
        data: {dob:dob_val}
    })
    .done(function(data){
        $('#modal-age-year').val(data.years);
        $('#modal-age-month').val(data.months);
        $('#modal-age-day').val(data.days);
    })
    .fail(function(data){
        console.log(data);
    });
});

$('#modal-age-day').on('change',function(){
    var year = $('#modal-age-year').val();
    var month = $('#modal-age-month').val();
    var day = $('#modal-age-day').val();
    
    var birthDate = moment().subtract(year, 'years').subtract(month, 'months').subtract(day, 'days');
    $('#modal-dob').val(birthDate.format("YYYY-MM-DD"));
});

$('#modal-age-month').on('change',function(){
    var year = $('#modal-age-year').val();
    var month = $('#modal-age-month').val();
    var day = $('#modal-age-day').val();
    
    var birthDate = moment().subtract(year, 'years').subtract(month, 'months').subtract(day, 'days');
    $('#modal-dob').val(birthDate.format("YYYY-MM-DD"));
});

$('#modal-age-year').on('change',function(){
    var year = $('#modal-age-year').val();
    var month = $('#modal-age-month').val();
    var day = $('#modal-age-day').val();
    
    var birthDate = moment().subtract(year, 'years').subtract(month, 'months').subtract(day, 'days');
    $('#modal-dob').val(birthDate.format("YYYY-MM-DD"));
});


var room = 1;

function income_fields() {
    room++;
    var objTo = document.getElementById('medicine_fields');
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass" + room);
    var rdiv = 'removeclass' + room;
    divtest.innerHTML =
        '<div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="Degree" name="variation_values[]"  placeholder="Size"></div></div><div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="Degree" name="variation_codes[]"  placeholder="Code"></div></div><div class="col-sm-4 nopadding"><div class="form-group"><div class="input-group"><input type="text" class="form-control" id="Degree" name="variation_prices[]"  placeholder="Price"><div class="input-group-btn"> <button class="btn btn-danger" type="button" onclick="remove_income_fields(' +
        room +
        ');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';

    objTo.appendChild(divtest);
}

function remove_income_fields(rid) {
    $('.removeclass' + rid).remove();
}
var medIndex = 1; 

function add_medicine_fields_modal(){ 
    medIndex++;
    var medRow = document.createElement("div");
    medRow.setAttribute("class","row");
    medRow.setAttribute("id","medRowModal"+medIndex);
    medRow.setAttribute("style","padding-top:10px;");
    var fields = 
    "<div class='col-md-2' > "+
    "  <select class='select2a' id='medicinesTypeModal"+medIndex+"' onchange='fetchMedicinesModal("+medIndex+");loadUnitsModal("+medIndex+")' form-control'>"+$('#medicinesTypeModal1').html()+"<select/>"+
    "</div>"+
    "<div class='col-md-2' > "+
    "   <select class='select2a form-control' id='medicinesModal"+medIndex+"' name='medicine[]'>"+
    "       <option value='' disabled selected> "+
    "           Please Select"+
    "       </option> "+
    "   <select/>"+
    "</div>"+ 
    "<div class='col-md-2'>    "+
    "    <div class='row'>"+
    "        <div class='col-md-5'>"+ 
    "            <input type='hidden' name='quantityy[]'  id='tabsModal"+medIndex+"'>"+
    "            <input type='text' style='width:100%' name='quantity[]' onkeyup='calculateTabletsModal("+medIndex+")' id='quantityModal"+medIndex+"' placeholder='' class='form-control form-control-sm' required>"+
    "        </div> "+
    "        <div class='col-md-7'>"+ 
    "            <select id='medicineUnit"+medIndex+"' name='medicine_unit[]' onchange='calculateTablets("+medIndex+")' class='form-control form-control-sm select2-tagged'> "+$('#medicineUnits').html()+
    "            </select>"+
    "             </div> "+
    "    </div>"+ 
    "</div>"+
    "<div class='col-md-1'>"+
    "    <input type='text' name='per_day[]' value='' id='perdayModal"+medIndex+"' onkeyup='calculateQuantityModal("+medIndex+")' class='form-control form-control-sm' required>"+
    "</div>"+ 
    "<div class='col-md-1'>"+ 
    "    <div class='row'>"+
    "        <input type='text' name='days[]' value='' id='daysModal"+medIndex+"' placeholder='' onkeyup='calculateQuantityModal("+medIndex+")' class='form-control form-control-sm col-md-4'>"+
    "        <select name='length[]'  onchange='calculateQuantityModal("+medIndex+")' id='lengthModal"+medIndex+"' class='form-control form-control-sm col-md-8' required>"+
    "            <option value='/7'>/7</option>"+
    "            <option value='/12'>/12</option>"+
    "            <option value='/52'>/52</option>"+
    "        </select>"+
    "    </div>                        "+
    "</div>"+
    "<div class='col-md-1'>"+ 
    "    <input type='text' name='total[]' onkeyup='checkStockModal("+medIndex+")' id='totalModal"+medIndex+"' placeholder='' class='form-control form-control-sm' required>"+
    "</div>"+
    "<div class='col-md-1'>"+
    "    <label for='med'></label>   "+
    "    <button style='margin-top:-21px;font-size:13px;' type='button' onclick='show_descripiton_modal("+medIndex+");' class='btn btn-sm btn-outline-success'>Description</button>"+
    "</div>"+
    "<div class='col-md-2'>        "+
    "<button type='button' onclick='remove_medicine_fields_modal("+medIndex+");' class='btn btn-outline-danger btn-sm'><i class='fa fa-minus'></i></button> "+
    "</div>"; 
    medRow.innerHTML = fields;
    $('#medicine_container_modal').append(medRow);
        
    $('.select2a').select2();
    $('.select2-tagged').select2({
        tags:true
    });
}

$('.select2-tagged').select2({
    tags:true
});
function remove_medicine_fields_modal(mid){
    $('#medRowModal'+mid).remove();
}

function add_medicine_fields(){ 
    medIndex++;
    var medRow = document.createElement("div");
    medRow.setAttribute("class","row");
    medRow.setAttribute("id","medRow"+medIndex);
    medRow.setAttribute("style","padding-top:10px;");
    var fields = 
    "<div class='col-md-2' > "+
    "  <select class='select2a' id='medicinesType"+medIndex+"' onchange='fetchMedicines("+medIndex+");loadUnits("+medIndex+")' form-control'>"+$('#medicinesType1').html()+"<select/>"+
    "</div>"+
    "<div class='col-md-2' > "+
    "   <select class='select2a form-control' id='medicines"+medIndex+"' name='medicine[]'>"+
    "       <option value='' disabled selected> "+
    "           Please Select"+
    "       </option> "+
    "   <select/>"+
    "</div>"+ 
    "<div class='col-md-2'>    "+
    "    <div class='row'>"+
    "        <div class='col-md-5'>"+ 
    "            <input type='hidden' name='quantityy[]'  id='tabs"+medIndex+"'>"+
    "            <input type='text' style='width:100%' name='quantity[]' onkeyup='calculateTablets("+medIndex+")' id='quantity1' placeholder='' class='form-control form-control-sm' required>"+
    "        </div> "+
    "        <div class='col-md-7'>"+ 
    "            <select id='medicineUnit"+medIndex+"' name='medicine_unit[]' onchange='calculateTablets("+medIndex+")' class='form-control form-control-sm select2-tagged' required> "+$('#medicineUnits').html()+
    "            </select>"+
    "             </div> "+
    "    </div>"+ 
    "</div>"+
    "<div class='col-md-1'>"+
    "    <input type='text' name='per_day[]' value='' id='perday"+medIndex+"' onkeyup='calculateQuantity("+medIndex+")' class='form-control form-control-sm' required>"+
    "</div>"+ 
    "<div class='col-md-1'>"+ 
    "    <div class='row'>"+
    "        <input type='text' name='days[]' value='' id='days"+medIndex+"' placeholder='' onkeyup='calculateQuantity("+medIndex+")' class='form-control form-control-sm col-md-4'>"+
    "        <select name='length[]'  onchange='calculateQuantity("+medIndex+")' id='length"+medIndex+"' class='form-control form-control-sm col-md-8'>"+
    "            <option value='/7'>/7</option>"+
    "            <option value='/12'>/12</option>"+
    "            <option value='/52'>/52</option>"+
    "        </select>"+
    "    </div>                        "+
    "</div>"+
    "<div class='col-md-1'>"+ 
    "    <input type='text' name='total[]' onkeyup='checkStock("+medIndex+")' id='total"+medIndex+"' placeholder='' class='form-control form-control-sm' required>"+
    "</div>"+
    "<div class='col-md-1'>"+
    "    <label for='med'></label>   "+
    "    <button style='margin-top:-47px;' type='button' onclick='show_descripiton("+medIndex+");' class='btn btn-sm btn-outline-success'>Description</button>"+
    "</div>"+
    "<div class='col-md-2'>        "+
    "<button type='button' onclick='remove_medicine_fields("+medIndex+");' class='btn btn-outline-danger btn-sm'><i class='fa fa-minus'></i></button> "+
    "</div>"; 
    medRow.innerHTML = fields;
    $('#medicine_container').append(medRow);
        
    $('.select2a').select2();
    $('.select2-tagged').select2({
        tags:true
    });
}

function remove_medicine_fields(mid){
    $('#medRow'+mid).remove();
}  

function fetchMedicines(index){
    var medicineTypeField = $('#medicinesType'+index);
    var typeId = medicineTypeField.val();
    var medicineField = $('#medicines'+index);
    var mgsField = $('#mgs_per_tablet_'+index);
    if(typeId==4){  
        mgsField.attr('disabled',false);
    }else{
        mgsField.attr('disabled',true);
    }

    data = { id:typeId };   
    $.ajax({					
        type:'get',					
        url: rootUrl + '/doctor/getMedicines' ,	
        dataType: 'json',	
        data:data,		
    })
    .done(function(data) {
        console.log(data.length); 
        var count = data.length;
        var newOptions = {}; 
        if(medicineField.prop) {
            var options = medicineField.prop('options');
        }
        else {
            var options = medicineField.attr('options');
        }
        $('option', medicineField).remove();
        options[options.length] = new Option("Please select", "");
        for(var i = 0; i<count;i++){
            options[options.length] = new Option(data[i].name, data[i].id);
        }; 
    })
    .fail(function(error) {
        console.log("error: "+error); 
    });
} 
 
function fetchMedicinesModal(index){
    var medicineTypeField = $('#medicinesTypeModal'+index);
    var typeId = medicineTypeField.val();
    var medicineField = $('#medicinesModal'+index);
    var mgsField = $('#mgs_per_tablet_Modal'+index);
    if(typeId==4){  
        mgsField.attr('disabled',false);
    }else{
        mgsField.attr('disabled',true);
    }

    data = { id:typeId };   
    $.ajax({					
        type:'get',					
        url: rootUrl + '/doctor/getMedicines' ,	
        dataType: 'json',	
        data:data,		
    })
    .done(function(data) {
        console.log(data.length); 
        var count = data.length;
        var newOptions = {}; 
        if(medicineField.prop) {
            var options = medicineField.prop('options');
        }
        else {
            var options = medicineField.attr('options');
        }
        $('option', medicineField).remove();
        options[options.length] = new Option("Please select", "");
        for(var i = 0; i<count;i++){
            options[options.length] = new Option(data[i].name, data[i].id);
        }; 
    })
    .fail(function(error) {
        console.log("error: "+error); 
    });
}
 
function show_descripiton(index){
    var medId = $('#medicines'+index).val();
    if(!medId)
        alert('select a medicine first');
    else{
        data = { id:medId };   
        $.ajax({					
            type:'get',					
            url: rootUrl + '/medicineDescription' ,	
            dataType: 'json',	
            data:data,		
        })
        .done(function(data) {
            alert("Description:\n"+data.description); 
        })
        .fail(function(error) {
            console.log(error); 
        });
    } 
}

function show_descripiton_modal(index){
    var medId = $('#medicinesModal'+index).val();
    if(!medId)
        alert('select a medicine first');
    else{
        data = { id:medId };   
        $.ajax({					
            type:'get',					
            url: rootUrl + '/medicineDescription' ,	
            dataType: 'json',	
            data:data,		
        })
        .done(function(data) {
            alert("Description:\n"+data.description); 
        })
        .fail(function(error) {
            console.log(error); 
        });
    } 
}

function checkStock(index){ 
    var total = parseInt($('#total'+index).val());
    var medicinesId = $('#medicines'+index).val();
    var checkMedicineStockField = $('#checking_stock_gif');
      
    var medicinesId = $('#medicines'+index).val(); 
    var submitButton = $('#submit_button'); 
    $('#tabs'+index).val(total)
     
    data = { medicine_id: medicinesId, total_quantity: total};
    if(total>1){
        checkMedicineStockField.fadeIn();
        $.ajax({					
            type:'get',					
            url: rootUrl + '/checkMedicineStock' ,	
            dataType: 'json',	
            data:data,		
        })
        .done(function(data) {
            console.log(data); 
            if(data.available=="false"){ 
                $('#warning_text').text('No enough medicines for the prescribed quantity in medicine '+
                data.medicine+'!'
                +' The remaining quantity is '+
                data.remaining_quantity);

                $('#warning_div').fadeIn(); 
                submitButton.attr('disabled','true');
            }else{ 
                submitButton.removeAttr('disabled');
                $('#warning_div').fadeOut(); 
            }
            checkMedicineStockField.fadeOut();
        })
        .fail(function(error) {
            console.log("error: "+error);
            checkMedicineStockField.fadeOut();
        });
    }else{
        $('#warning_div').fadeOut(); 
    }
}

function checkStockModal(index){ 
    var total = parseInt($('#totalModal'+index).val());
    var medicinesId = $('#medicinesModal'+index).val();
    var checkMedicineStockField = $('#checking_stock_gif_modal');
      
    var medicinesId = $('#medicinesModal'+index).val(); 
    var submitButton = $('#submit_button_modal'); 
    $('#tabsModal'+index).val(total)
     
    data = { medicine_id: medicinesId, total_quantity: total};
    if(total>1){
        checkMedicineStockField.fadeIn();
        $.ajax({					
            type:'get',					
            url: rootUrl + '/checkMedicineStock' ,	
            dataType: 'json',	
            data:data,		
        })
        .done(function(data) {
            console.log(data); 
            if(data.available=="false"){ 
                $('#warning_text_modal').text('No enough medicines for the prescribed quantity in medicine '+
                data.medicine+'!'
                +' The remaining quantity is '+
                data.remaining_quantity);

                $('#warning_div_modal').fadeIn(); 
                submitButton.attr('disabled','true');
            }else{ 
                submitButton.removeAttr('disabled');
                $('#warning_div_modal').fadeOut(); 
            }
            checkMedicineStockField.fadeOut();
        })
        .fail(function(error) {
            console.log("error: "+error);
            checkMedicineStockField.fadeOut();
        });
    }else{
        $('#warning_div_modal').fadeOut(); 
    }
}
