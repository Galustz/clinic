let initial_values =  {
    service_cost: 0,
    service_mode_price: 0,
    service_price: 0,
    medicine_price: 0,
    tests_price: 0,
    consultation_fee: 0,
    should_show: false,
    full_price_required: false
};
let store = {
    root_uri: '/reception-service-details',
    costing : {
        "adult": initial_values,
        "child": initial_values,
        "visit": initial_values,
    }
}

export default store