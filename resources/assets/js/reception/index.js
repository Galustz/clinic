
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

import store from "./store"
import VModal from "vue-js-modal"

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(require('vue-moment'))
Vue.use(VModal) 

Vue.component('r-s-d', require('./components/ReceptionServiceDetails.vue')); 
Vue.component('r-c-c', require('./components/ReceptionCostCalculate.vue'));
Vue.component('r-p-t', require('./components/ReceptionPatientType.vue'));
Vue.component('p-s', require('./components/PatientSearch.vue'));
Vue.component('birthday-calculate', require('./components/BirthDayCalculate.vue'));
Vue.component('investigation-results', require('./components/InvestigationResultsModal.vue'));

try{
    const app = new Vue({
        el: '#v-reception',
        data: store
    });
}catch(ex){

}
