@extends('layouts.admin')
@section('content')
@php 
    use App\Room;
    $rooms = Room::all();
@endphp
<div class="container">
    <div class="card card-body">
        <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/users/rooms/new')}}">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="row form-group">
                    <input type="text" name="id" id="service_id" hidden>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                <input type="text" placeholder="Name" name="name" id="service" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                <input type="text" name="use" placeholder="Use" id="price" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table id="patient-que-table" class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>Number</th>
                            <th>Name</th>
                            <th>Use</th>
                            {{--
                            <th></th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rooms as $room)
                        <tr class="table-row">
                            <td>{{$room->id}}</td>
                            <td>{{$room->name}}</td>
                            <td>{{$room->use}}</td>
                            {{--
                            <td>
                                <a href="{{url('admin/pharmacy/categories/delete',$category->id)}}" class="btn btn-danger">Delete</a>
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE                  -->
        </div>
    </div>
</div>
@endsection