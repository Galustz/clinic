@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            'header'=>"Employee attendance",
            'button'=>[
                "name"=>"Take attendance",
                "color"=>"primary",
                "url"=>$take_attendance_url
            ]
        ])
        <div class="row" style="padding-right:16px;padding-left:14px">
            <div class="col-md-12">
                <p><b>NOTE: <i class="fa fa-check present"></i> => present | <i class="fa fa-times absent"></i> => absent | <i class="fa fa-clock late"></i> => late</b></p>
                <table class="table attendance-table">
                    <thead>
                        <tr>
                            <th>
                                Employee Name:
                            </th>
                            @foreach ($dates_in_month as $date)
                                <th>{{$date->day}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($employees as $employee)
                            <tr>
                                <td>
                                    {{$employee->name}} <br>
                                    @include('components.attendance-stats')
                                </td>
                                @foreach ($dates_in_month as $date)
                                    <td>@include('components.attendance-status',["status"=>$employee->attendance($date)])</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>    
                <hr>            
            </div>
        </div>
    </div>
@endsection