@extends('components.add-form')

@section('form')
    <div class="col-md-12 form-group">
        <label for=""><strong>Date</strong></label>
        <input type="date" name="date" class="form-control" value="{{$date}}" id="" required>
    </div>
    @foreach ($employees as $employee)
        <div class="col-md-4 form-group">
            <label for=""><strong>{{$employee->name}}</strong></label>
            <input type="hidden" name="employee_id[]" value="{{$employee->id}}">
            <hr>
            <input type="radio" name="status[{{$employee->id}}]" id="" value="present" required checked> Present
            <input type="radio" name="status[{{$employee->id}}]" id="" value="late" required> Late
            <input type="radio" name="status[{{$employee->id}}]" id="" value="absent" required> Absent
            <hr>            
        </div>
    @endforeach
@endsection