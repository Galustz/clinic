@extends('components.add-form')

@section('form')
    <div class="col-md-6 form-group">
        <label for="">Select employee</label>
        <select name="employee_id" id="" class="form-control">
            @foreach ($employees as $employee)
                <option value="{{$employee->id}}" {{$model->employee_id==$employee->id?'selected':''}}>{{$employee->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-6 form-group">
        <label for="">Reason</label>
        <textarea name="reason" id="" cols="30" rows="3" class="form-control">{{$model->reason??null}}</textarea>
    </div>
    <div class="col-md-6 form-group">
        <label for="">Date/dates</label>
        <input type="text" name="date" id="" value="{{$model->date??null}}" class="form-control d-picker-multi">
    </div>
@endsection