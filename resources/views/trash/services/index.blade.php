@extends('layouts.admin')
@section('content')
@php
    use App\Service;
    $services = Service::where('trashed','true')->get();
    $i=1;
@endphp
<div class="container">
   
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table id="patient-que-table" class="table datatable table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>Number</th>
                            <th>Name</th>
                            <th>Price</th>
                            {{-- <th></th>
                            
                            <th></th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($services as $service)
                        <tr class="table-row">
                            <td>{{$i}}</td>
                            <td>{{$service->name}}</td>
                            <td>{{$service->price}}</td>
                            {{-- <td><a class="btn btn-primary" href="{{url('admin/reception/services/edit',$service->id)}}">Edit</a></td>
                            <td>
                                <a href="{{url('admin/reception/services/delete',$service->id)}}" class="btn btn-danger">Delete</a>
                            </td> --}}
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE                  -->
        </div>
    </div>
</div>
@endsection