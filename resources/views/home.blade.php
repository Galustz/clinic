@extends('layouts.app')
@section('content') 
  @yield('section')
  @include('components.announcements')
@endsection
@section('other-script')
  @yield('scripts')
@endsection