@extends('employees.view')
@section('tab-content')
 <div class="row">
    <div class="col-md-12" style="padding-left:30px; padding-right:30px;">
        <table class="table borderless m-t-10">
            <tr>
                <td col="20%">Name:</td>
                <td>{{$model->name}}</td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>{!!$model->email!!}</td>
            </tr>  
            <tr>
                <td>Phone Number:</td>
                <td>{!!$model->phone_no!!}</td>
            </tr>  
            <tr>
                <td>Gender:</td>
                <td>{!!$model->gender!!}</td>
            </tr>  
            <tr>
                <td>Designation:</td>
                <td>{!!$model->department_name!!}</td>
            </tr>  
            <tr>
                <td>Joining date:</td>
                <td>{!!$model->joining_date!!}</td>
            </tr>  
            <tr>
                <td>Leaves taken:</td>
                <td>{!!$model->leaves()!!}/28</td>
            </tr>  
        </table>                
    </div>
</div>   
@endsection
