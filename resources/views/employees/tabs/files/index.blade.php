@extends('employees.view')
@section('tab-content')
    <div class="container"> 
        @include('components.content-header',[
            "header"=>"Employee documents",
            "button"=>[
                "name"=>"Add new employee file",
                "color"=>"primary",
                "url"=>$new_employee_file_url
            ]
        ])
        @include('components.table',[
            "columns"=> ["No","File","Description","Action"],
            "collection"=>$employee_files,
            "data_columns"=>[
                "iteration",
                "name",
                "description", 
                "buttons"=>[ 
                    ["name"=>"Download","url"=>$download_employee_file_url,"id"=>true,"color"=>"success"],
                    ["name"=>"Delete","url"=>$remove_employee_file_url,"id"=>true,"color"=>"danger","should_confirm"=>true,"action"=>"delete"] 
                ]
            ]
        ])
    </div> 
@endsection