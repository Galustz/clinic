@extends('layouts.admin')

@section('content')
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link {{Request::is('employees/view*')?'active':''}}" href="{{url('employees/view',$id)}}">Details</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Request::is('employee-files*')?'active':''}}" href="{{url('employee-files/files',$id)}}">Documents</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Request::is('employee-appraisal*')?'active':''}}" href="{{url('/employee-appraisals/files',$id)}}">Performance appraisal</a>
            </li> 
        </ul> 
        <div class="pt-4">
          @yield('tab-content')        
        </div>

        {{-- @if (Request::is('employee-files*'))
            @include($view_root.'.tabs.files.index')
        @endif
        @if ($link=='performance')
            @include($view_root.'.tabs.performance')
        @endif  --}}
    </div>
@endsection