<header class="header-desktop3 d-none d-lg-block">
     <div class="section__content section__content--p35">
         <div class="header3-wrap">
            
             <div class="header__navbar">
                 <ul class="list-unstyled">
                     
                 </ul>
             </div>
             <a class="js-acc-btn" href="{{ url('logout') }}">Logout</a>
             @if(Auth::user()->hasRole('admin'))
             <a class="js-acc-btn top-links" href="{{ url('reception') }}">Reception</a>
             <a class="js-acc-btn top-links" href="{{ url('billing') }}">Billing</a>
             <a class="js-acc-btn top-links" href="{{ url('doctor') }}">Doctor</a>
             <a class="js-acc-btn top-links" href="{{ url('laboratory') }}">Laboratory</a>
             <a class="js-acc-btn top-links" href="{{ url('pharmacy') }}">Pharmacy</a>
             <a class="js-acc-btn top-links" href="{{ url('admin') }}">Admin</a>
             @endif
             <div class="header__tool">
                 <div class="account-wrap">
                     <div class="account-item account-item--style2 clearfix js-item-menu">
                         
                         <div class="content">
                             {{-- <a class="js-acc-btn" href="{{ url('logout') }}">Logout</a> --}}
                         </div> 
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </header>
 <style>
     .top-links{
         color:white !important;
     }
 </style>
