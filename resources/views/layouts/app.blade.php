<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ElWogha Yoga">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title Page-->
    <title>{{!empty($title)?$title:'Clinic'}}</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('assets/css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('assets/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('assets/vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('assets/css/theme.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/css/datatables.min.css')}}" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{asset('assets/js/datatables/keytable.min.js')}}">

    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/af-2.2.2/b-1.5.1/datatables.min.css"/>
     --}}
    <link href="{{ asset('assets/css/side-style.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/css/lobibox.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/css/jquery-ui.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/css/styles.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/css/mystyles.css')}}" rel="stylesheet" media="all">
    
    <!-- Select 2-->
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}"> --}}

    {{-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
     --}}
    <!-- Jquery JS-->
    <script src="{{ asset('assets/vendor/jquery-3.2.1.min.js')}}"></script>  
    <!-- Webcam JS-->
    <script type="text/javascript" src="{{ asset('assets/js/packages/webcam.min.js') }}"></script> 
    <script>
            var rootUrl = "{{url('/')}}";
        </script>
    <link href="{{ asset('css/custom.css')}}" rel="stylesheet" media="all">

</head>

<body class="">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        @include('layouts.header')
        <!--END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->

        <!--END HEADER MOBILE-->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7"> 
            <div style="min-height:700px;" id="app">
                @yield('content') 
            </div>
            

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright &copy; {{date('Y')}} <a href="http://legendaryits.com">Legendary IT Solutions</a>. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>
        <!-- END PAGE CONTENT-->

    </div>

    
    {{-- <script src="{{asset('js/app.js')}}"></script> --}}
    <!-- Bootstrap JS-->
    {{-- <script src="{{ asset('js/app.js')}}"></script> --}}
    <script src="{{ asset('assets/vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>  
    <!-- Vendor JS       -->
    <script src="{{ asset('assets/vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{ asset('assets/vendor/wow/wow.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{ asset('assets/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{ asset('assets/vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{ asset('assets/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/select2.min.js')}}"></script>
    <script src="{{ asset('assets/js/packages/lobibox.min.js')}}"></script>
    <!--<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>-->

    <script src="{{ asset('assets/js/datatables/datatables.min.js')}}"></script>
    <script src="{{ asset('assets/js/datatables/keytable.min.js')}}"></script>
    <script src="{{ asset('assets/js/packages/jquery-ui.js')}}"></script>
    <script src="{{ asset('assets/js/packages/moment.min.js')}}"></script>
    <script src="{{ asset('assets/js/packages/sw.js')}}"></script>

    <!-- Mousetrap -->
    <script src="{{ asset('assets/js/packages/mousetrap.min.js') }}"></script>
    {{-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/af-2.2.2/b-1.5.1/datatables.min.js"></script>
    
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/sw.js"></script> --}}
    
    <!-- Select 2 -->
    <script src="{{ asset('assets/js/packages/select2.full.min.js') }}"></script>

    <!-- Main JS-->
    <script src=""></script>
    <script src="{{ asset('assets/js/dev/main.js')}}"></script>
    <script src="{{ asset('assets/js/dev/clinic.js')}}"></script>
    <script src="{{ asset('assets/js/dev/pharmacy.js')}}"></script>
    <script src="{{ asset('assets/js/dev/billing.js')}}"></script>
    <script src="{{ asset('assets/js/dev/doctor.js')}}"></script>
    <script src="{{ asset('assets/js/dev/announcements.js')}}"></script>
    {{-- <script src="{{ asset('assets/js/tables.js')}}"></script> --}}

    <script>  
        $(document).ready(function(){  
            
            // $('.datatable').dataTable();
            $('.mytable').dataTable();

            function check_session()
            {
                $.ajax({
                    url:"{{ url('/checkSession') }}",
                    type:"GET"
                }).done(function(data){
                    if(data.data == '1')
                    {
                        //alert('Your session has been expired!');  
                        location.reload();
                    }
                }).fail(function(error){
                    var error_code = $.parseJSON(error);
                    console.log(JSON.stringify(error_code));
                });
            }
            setInterval(function(){
                check_session();
            }, 10000);
        });  
    </script>
      
    @stack('script')
    @yield('other-script')
</body>

</html>
<!-- end document-->
