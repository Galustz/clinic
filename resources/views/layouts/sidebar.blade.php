<!-- MENU SIDEBAR-->
@php 
    $open = "style=\"display:block\"";
@endphp
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="#">
                <img src="{{asset('assets/images/clinic-logo.png')}}" alt="">
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            <nav class="navbar-sidebar">
                <ul class="list-unstyled navbar__list">
                    <li class="{{Request::is('admin')?'active':''}}">
                        <a href="{{url('/')}}">
                            <i class="fas fa-hospital-o"></i>Home</a>
                    </li>
                    {{-- <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-table"></i>Reception</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  {!!Request::is('admin/reception/*')?$open:''!!}>
                            <li class="{{Request::is('admin/reception/services')?'active':''}}" >
                                <a href="{{url('/admin/reception/services')}}">Services</a>
                            </li>
                            <li class="{{Request::is('admin/reception/patienttypes')?'active':''}}">
                                <a href="{{url('/admin/reception/patienttypes')}}">Patient Types</a>
                            </li>
                            <li class="{{Request::is('admin/reception/servicemodes')?'active':''}}">
                                <a  href="{{url('/admin/reception/servicemodes')}}">Service Modes</a>
                            </li>
                        </ul>
                    </li> --}}
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-user"></i>Patients</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  {!!Request::is('admin/patients/*')?$open:''!!}>
                            <li class="{{Request::is('admin/patients/list')?'active':''}}">
                                <a href="{{url('/admin/patients/list')}}">List</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-archive"></i>Stock</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  
                        {!!Request::is('admin/suppliers*','admin/pharmacy/medicines*','equipments*')?$open:''!!}>  
                            <li class="{{Request::is('admin/pharmacy/medicines')?'active':''}}">
                                <a href="{{url('/admin/pharmacy/medicines')}}">Medicines</a>
                            </li>
                            <li class="{{Request::is('equipments')?'active':''}}">
                                <a href="{{url('/equipments')}}">Equipments</a>
                            </li>
                            {{-- <li class="{{Request::is('admin/suppliers/add')?'active':''}}">
                                <a href="{{url('/admin/suppliers/add')}}">New supplier</a>
                            </li> --}}
                            <li class="{{Request::is('admin/suppliers')?'active':''}}">
                                <a href="{{url('/admin/suppliers')}}">Suppliers</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{Request::is('inventory*')?'active':''}}">
                        <a href="{{url('inventory')}}"><i class="fa fa-box"></i>Inventory</a>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-calculator"></i>Cash control</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  
                        {!!Request::is('cash-control*')||Request::is('credit*')||Request::is('payment*')?$open:''!!}>  
                            <li class="{{Request::is('cash-control/cash-in')?'active':''}}">
                                <a href="{{url('cash-control/cash-in')}}">Cash In</a>
                            </li> 
                            {{-- <li class="{{Request::is('cash-control/cash-out')?'active':''}}">
                                <a href="{{url('cash-control/cash-out')}}">Cash Out</a>
                            </li>  --}}
                            <li class="{{Request::is('credit*')?'active':''}}">
                                <a href="{{url('credit')}}">Credit</a>
                            </li> 
                            <li class="{{Request::is('payments*')?'active':''}}">
                                <a href="{{url('payments')}}">Payments</a>
                            </li> 
                        </ul>
                    </li>
                    <li class="{{Request::is('consultants*')?'active':''}}">
                        <a href="{{url('consultants')}}"><i class="fa fa-id-card"></i>Consultants</a>
                    </li>
                    <li class="{{Request::is('appointments*')?'active':''}}">
                        <a href="{{url('appointments')}}"><i class="fa fa-calendar-alt"></i>Appointments</a>
                    </li>
                    <li class="{{Request::is('announcements*')?'active':''}}">
                        <a href="{{url('announcements')}}"><i class="fa fa-flask"></i>Announcements</a>
                    </li>
                    {{-- <li class="{{Request::is('organisation-structure*')?'active':''}}">
                        <a href="{{url('organisation-structure')}}"><i class="fa fa-hospital"></i>Organisation structure</a>
                    </li> --}}
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-list-alt"></i>Organisation structure</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  
                        {!!Request::is('organisation-structure*','og-meeting-minutes*','og-schedules*')?$open:''!!}>  
                            <li class="{{Request::is('organisation-structure*')?'active':''}}">
                                <a href="{{url('organisation-structure')}}">File</a>
                            </li> 
                            <li class="{{Request::is('og-meeting-minutes*')?'active':''}}">
                                <a href="{{url('og-meeting-minutes')}}">Meeting minutes</a>
                            </li> 
                            <li class="{{Request::is('og-schedules*')?'active':''}}">
                                <a href="{{url('og-schedules')}}">Schedules</a>
                            </li>  
                        </ul>
                    </li>
                    <li class="{{Request::is('standard-operating-procedures*')?'active':''}}">
                        <a href="{{url('standard-operating-procedures')}}"><i class="fa fa-clipboard"></i>SOPs</a>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-list-alt"></i>Quality control</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  
                        {!!Request::is('quality-control*')||Request::is('meeting-minutes*')||Request::is('schedules*')?$open:''!!}>  
                            <li class="{{Request::is('quality-control/members')?'active':''}}">
                                <a href="{{url('quality-control/members')}}">Members</a>
                            </li> 
                            <li class="{{Request::is('meeting-minutes')?'active':''}}">
                                <a href="{{url('meeting-minutes')}}">Meeting minutes</a>
                            </li> 
                            <li class="{{Request::is('schedules*')?'active':''}}">
                                <a href="{{url('schedules')}}">Schedules</a>
                            </li> 
                            <li class="{{Request::is('quality-control/members/appointment-letter*')?'active':''}}">
                                <a href="{{url('quality-control/members/appointment-letter')}}">Appoinment Letter</a>
                            </li> 
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-laptop"></i>HR and Payroll</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  
                        {!!Request::is('employees*','attendance*','leaves*','payroll*')?$open:''!!}>  
                            <li class="{{Request::is('employees*')?'active':''}}">
                                <a href="{{url('employees')}}">Employee list</a>
                            </li> 
                            <li class="{{Request::is('attendance')?'active':''}}">
                                <a href="{{url('attendance')}}">Attendance</a>
                            </li> 
                            <li class="{{Request::is('leaves*')?'active':''}}">
                                <a href="{{url('leaves')}}">Leaves</a>
                            </li>  
                            <li class="{{Request::is('payroll*')?'active':''}}">
                                <a href="{{url('payroll')}}">Payroll</a>
                            </li>  
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-credit-card"></i>Insurance/Corporate</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  
                        {!!Request::is('insurance-companies*')||Request::is('corporate-companies*')?$open:''!!}>  
                            <li class="{{Request::is('insurance-companies*')?'active':''}}">
                                <a href="{{url('insurance-companies')}}">Insurance companies</a>
                            </li> 
                            <li class="{{Request::is('corporate-companies')?'active':''}}">
                                <a href="{{url('corporate-companies')}}">Corporate companies</a>
                            </li>  
                        </ul>
                    </li> 
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-user"></i>Users</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  {!!Request::is('admin/users/*')?$open:''!!}>
                            <li class="{{Request::is('admin/users')?'active':''}}">
                                <a href="{{ url('/admin/users/') }}">Users</a>
                            </li>
                            <li class="{{Request::is('admin/users/rooms')?'active':''}}">
                                <a href="{{ url('/admin/users/rooms') }}">Rooms</a>
                            </li>
                            <li class="{{Request::is('admin/users/units')?'active':''}}" class="{{Request::is('/admin/laboratory/tests')?'active':''}}">
                                <a href="{{ url('/admin/users/units') }}">Units</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-book"></i>Reports</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  {!!Request::is('admin/reports/*')?$open:''!!}>
                            {{-- <li class="{{Request::is('admin/reports/reception')?'active':''}}">
                                <a href="{{ url('/admin/reports/reception') }}">Reception</a>
                            </li>
                            <li class="{{Request::is('admin/reports/doctors')?'active':''}}">
                                <a href="{{ url('/admin/reports/doctors') }}">Doctors</a>
                            </li>
                            <li class="{{Request::is('admin/reports/billing')?'active':''}}">
                                <a href="{{ url('/admin/reports/billing') }}">Billing</a>
                            </li class="{{Request::is('admin/reports/laboratory')?'active':''}}">
                                <a href="{{ url('/admin/reports/laboratory') }}">Laboratory</a>
                            </li class="{{Request::is('admin/reports/pharmacy')?'active':''}}">
                                <a href="{{ url('/admin/reports/pharmacy') }}">Pharmacy</a>
                            </li> --}}
                            <li class="{{Request::is('admin/reports/new-patients')||Request::is('admin/reports/new-patients/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/new-patients') }}">New patients</a>
                            </li>
                            <li class="{{Request::is('admin/reports/express-patients')||Request::is('admin/reports/express-patients/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/express-patients') }}">Express patients</a>
                            </li>
                            <li class="{{Request::is('admin/reports/equipments')||Request::is('admin/reports/equipments/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/equipments') }}">Equpments</a>
                            </li>
                            <li class="{{Request::is('admin/reports/patients-per-disease')||Request::is('admin/reports/patients-per-disease/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/patients-per-disease') }}">Patients per disease</a>
                            </li>
                            <li class="{{Request::is('admin/reports/medicine-sold')||Request::is('admin/reports/medicine-sold/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/medicine-sold') }}">Medicines sold</a>
                            </li>
                            <li class="{{Request::is('admin/reports/consultations')||Request::is('admin/reports/consultations/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/consultations') }}">Consultations</a>
                            </li>
                            <li class="{{Request::is('admin/reports/mtuha')||Request::is('admin/reports/mtuha/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/mtuha') }}">Mtuha</a>
                            </li>
                            <li class="{{Request::is('/reports/graphs')||Request::is('/reports/graphs/*')?'active':''}}">
                                <a href="{{ url('/reports/graphs') }}">Graph</a>
                            </li>
                            <li class="{{Request::is('admin/reports/stock')||Request::is('admin/reports/stock/*')?'active':''}}">
                                <a href="{{ url('/admin/reports/stock') }}">Stock</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-desktop"></i>Screens</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{ url('reception') }}">Reception</a>
                            </li>
                            <li>
                                <a href="{{ url('doctor') }}">Doctor</a>
                            </li>
                            <li>
                                <a href="{{ url('billing') }}">Billing</a>
                            </li>
                                <a href="{{ url('laboratory') }}">Laboratory</a>
                            </li>
                                <a href="{{ url('pharmacy') }}">Pharmacy</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-trash"></i>Trash</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list"  {!!Request::is('admin/trash/*')?$open:''!!}>
                            <li class="{{Request::is('admin/trash/services')?'active':''}}">
                                <a href="{{ url('/admin/trash/services') }}">Services</a>
                            </li>
                            <li class="{{Request::is('admin/trash/patienttypes')?'active':''}}">
                                <a href="{{ url('/admin/trash/patienttypes') }}">Patient Types</a>
                            </li>
                            <li class="{{Request::is('admin/trash/servicemodes')?'active':''}}">
                                <a href="{{ url('/admin/trash/servicemodes') }}">Service Modes</a>
                            </li class="{{Request::is('admin/trash/tests')?'active':''}}">
                                <a href="{{ url('/admin/trash/tests') }}">Tests/Examinations</a>
                            </li class="{{Request::is('admin/trash/medicines')?'active':''}}">
                                <a href="{{ url('/admin/trash/medicines') }}">Medicines</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-flask"></i>Settings</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list" 
                            {!!Request::is('admin/laboratory/tests*','admin/settings*','admin/reception/*','settings/medicine-categories*')?$open:''!!}> 
                            <li class="{{Request::is('admin/laboratory/tests')?'active':''}}">
                                <a href="{{url('/admin/laboratory/tests')}}">Tests/Examinations</a>
                            </li>                            
                            <li class="{{Request::is('settings/medicine-categories')?'active':''}}">
                                <a href="{{url('settings/medicine-categories')}}">Medicine Types</a>
                            </li>                            
                            <li class="{{Request::is('admin/reception/services')?'active':''}}" >
                                <a href="{{url('/admin/reception/services')}}">Services</a>
                            </li>
                            <li class="{{Request::is('admin/reception/patienttypes')?'active':''}}">
                                <a href="{{url('/admin/reception/patienttypes')}}">Patient Types</a>
                            </li>
                            <li class="{{Request::is('admin/reception/servicemodes')?'active':''}}">
                                <a  href="{{url('/admin/reception/servicemodes')}}">Service Modes</a>
                            </li>
                            <li class="{{Request::is('admin/settings')?'active':''}}">
                                <a href="{{url('admin/settings')}}">General</a>
                            </li>
                        </ul>
                    </li> 
                </ul>
            </nav><br>
            <br>
            <br>
            <span style="margin-left:20px;">Version: 2.0.0.0</span>
        </div>
    </aside>
<!-- END MENU SIDEBAR-->