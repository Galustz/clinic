@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            'header'=>'Equipment',
            "button"=>[
                'name'=>'Add equipment',
                'url'=>$add_equipment_url,
                'color'=>'primary',
                ],
            ])

        @include('components.table',[
            "columns"=>['No','Name',"Code","Purchasing Unit Name","Selling Unit Name","Unit Purchasing Price","Quantity per unit","Quantity in stock","Action"],
            "collection"=>$equipments,
            "data_columns"=>[
                "iteration",
                "name",
                "code",
                "purchasing_unit_name",
                "selling_unit_name",
                "unit_purchasing_price",
                "quantity_per_unit",
                "quantity_in_stock",
                "buttons"=>[
                    ["url"=>$view_url,"id"=>true,"name"=>"View","color"=>"success"],
                    ["url"=>$restock_url,"id"=>true,"name"=>"Restock","color"=>"warning"],
                    ["url"=>$edit_url,"id"=>true,"name"=>"Edit","color"=>"primary"],
                    ["url"=>$delete_url,"id"=>true,"name"=>"Delete","color"=>"danger","should_confirm"=>true,"action"=>"delete"],
                ]
             ],
        ])
    </div>
@endsection