@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Equipment: ".$model->name
        ])
        <div class="row">
            <div class="col-md-12" style="padding-left:30px; padding-right:30px;">
                <table class="table borderless m-t-10"> 
                    <tr>
                        <td width="20%">Name:</td>
                        <td>{{$model->name}}</td>
                    </tr>
                    <tr>
                        <td width="20%">Equipment code:</td>
                        <td>{{$model->code}}</td>
                    </tr>
                    <tr>
                        <td width="20%">Equipment code:</td>
                        <td>{{$model->code}}</td>
                    </tr>
                    <tr>
                        <td width="20%">Purchasing unit name:</td>
                        <td>{{$model->purchasing_unit_name}}</td>
                    </tr>
                    <tr>
                        <td width="20%">Selling unit name:</td>
                        <td>{{$model->selling_unit_name}}</td>
                    </tr>
                    <tr>
                        <td width="20%">Unit purchasing price:</td>
                        <td>{{$model->unit_purchasing_price}}</td>
                    </tr>
                    <tr>
                        <td width="20%">Quantity per unit:</td>
                        <td>{{$model->quantity_per_unit}}</td>
                    </tr>
                    <tr>
                        <td width="20%">Total remaining quantity:</td>
                        <td>{{$model->quantity_in_stock}}</td>
                    </tr>   
                </table>                
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12" style="padding-left:43px; padding-bottom: 40px;">
                @include('components.button',["url"=>$restock_url,"color"=>'warning',"text"=>'Restock'])
                @include('components.button',["url"=>$restock_url,"color"=>'primary',"text"=>'Edit'])
                @include('components.button',["url"=>"back","color"=>'success',"text"=>'Back'])
            </div>
        </div>
        @include('components.content-header',[
            "header"=>"Equipment Restocking History: ",
        ]) 
        @include('components.table',[
            "columns"=>['No','Date',"Code","Quantity Added","Quantity remaining","Action"],
            "collection"=>$model->batches,
            "data_columns"=>[
                "iteration",
                "date", 
                "code",
                "quantity",
                "remaining_quantity", 
                "buttons"=>[ 
                    ["url"=>$batch_delete_url,"id"=>true,"name"=>"Remove","color"=>"danger","should_confirm"=>true,"action"=>"delete"],
                ]
             ],
        ])
    </div>
@endsection