
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong><i class="fa fa-calendar"></i>   New Appointment</strong>
					</div>
					<form enctype="multipart/form-data" method="post" action="{{ route('appointments.store') }}" id="new-appointment" >
                        {{ csrf_field() }}
                        <input type="text" name="check-id-id" id="check-id" hidden>
                        <div class="card-body card-block">

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="patient_reg_no" class=" form-control-label">Patient Reg. No.</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-id-card"></i>
                                            </div>
                                            <input type="text" name="patient_reg_no" id="reg_no" class="form-control reg_no" required disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name" class=" form-control-label">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" name="name" id="check-name" class="form-control" required autofocus>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="gender" class=" form-control-label">Gender</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-intersex"></i>
                                            </div>
                                            <input type="text" id="gender" name="gender" class="form-control gender" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email" class=" form-control-label">Email</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-at"></i>
                                            </div>
                                            <input type="email" name="email" id="email" class="form-control email">
                                        </div>
                                    </div>
                                </div>
                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address" class=" form-control-label">Address</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-map-marker"></i>
                                            </div>
                                            <input type="text" name="address" id="address" class="form-control address">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="phone" class=" form-control-label">Mobile Phone No.</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-mobile"></i>
                                            </div>
                                            <input type="text" name="phone" id="phone" class="form-control phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="type" class=" form-control-label">Type of Patient</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-hospital"></i>
                                            </div>
                                            <select name="type" id="type" class="form-control">
                                                <option value="">Please select</option>
                                                <option value="General">General</option>
                                                <option value="Corporate">Corporate</option>
                                                <option value="Insurance">Insurance</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="dob" class=" form-control-label">Date &amp; Time of Appointment</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="date" id="dob" name="dob" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="service_id" class=" form-control-label">Requested Service</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-hand-stop-o"></i>
                                            </div>
                                            {{ Form::select('service_id', $services, null, ['class' => 'form-control','placeholder'=>'Please select']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="doc_id" class=" form-control-label">Assign Doctor</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user-md"></i>
                                            </div>
                                            {{ Form::select('doc_id', $doctors, null, ['class' => 'form-control','placeholder'=>'Please select']) }}
                                        </div>
                                    </div>
                                </div>                
                            </div>
                            <strong class="pull-right" id="display-amount">Amount Due: TZS 7900</strong><br />
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>
                        </div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script>
        $('#service_id').on('change',function(){
            var id = $(this).val();
            
            $.ajax({
                url: "{{ url('getCharges') }}/" + id,
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            })
            .done(function(data) {
                $('#dispaly-amount').html('Amount Due: TZS ' + data.amount);
            })
            .fail(function() {
                console.log("error");
            });
        });
    </script>
    
@endpush