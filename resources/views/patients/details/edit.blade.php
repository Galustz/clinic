@extends('home') 
    @php use App\Patient; 
    $patient = Patient::find($id); 
    $dob = $patient->DOB;
    $date = date_create_from_format('Y-m-d', $dob);       

    $reference = new DateTime;

    $diff = $reference->diff($date);

    $years = $diff->y;
    $months = $diff->m;
    $days = $diff->d;

@endphp 
@section('section')
<div class="container"><br><br><br>
    <h4>Edit Patient Details</h4>
    <div class="card">
        <div class="card-body">
            <form enctype="multipart/form-data" method="post" action="{{ url('admin/patients/edit') }}" id="register-child-patient">
                {{ csrf_field() }}
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="reg_no" class=" form-control-label">Patient Reg No.</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </div>
                                    <input type="text" name="reg_no" value="{{$patient->reg_no}}" class="form-control" required readonly>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$patient->id}}">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name" class=" form-control-label">Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" value="{{$patient->name}}" name="name" id="name" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="gender" class=" form-control-label">Gender</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-intersex"></i>
                                    </div>
                                    <select name="gender" value="{{$patient->gender}}" id="gender" class="form-control" required>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="blood_group" class=" form-control-label">Blood Group</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-tint"></i>
                                    </div>
                                    <input type="text" value="{{$patient->blood_group}}" id="blood_group" name="blood_group" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="type" class=" form-control-label">Type of Patient</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-hospital"></i>
                                    </div>
                                    <select name="patient_type" value="{{$patient->patient_type}}" id="patient_type" class="form-control">
                                        <option value="General">General</option>
                                        <option value="Corporate">Corporate</option>
                                        <option value="Insurance">Insurance</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nationality" class=" form-control-label">Nationality</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <input type="text" value="{{$patient->nationality}}" name="nationality" id="nationality" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($patient->type == "child")
                    <p>
                        <span>Parents Details</span>
                    </p>
                    <hr />

                    <div class="row form-group">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="father_name" class=" form-control-label">Father's Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" value="{{$patient->father_name}}" name="father_name" id="father_name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="father-occupation" class=" form-control-label">Father's Occupation</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-briefcase"></i>
                                    </div>
                                    <input type="text" value="{{$patient->father_occupation}}" id="father_occupation" name="father_occupation" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mother-name" class=" form-control-label">Mother's Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" value="{{$patient->mother_name}}" id="mother_name" name="mother_name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mother-occupation" class=" form-control-label">Mother's Occupation</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-briefcase"></i>
                                    </div>
                                    <input type="text" value="{{$patient->mother_occupation}}" id="mother_occupation" name="mother_occupation" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <p>
                        <span>Contacts</span>
                    </p>
                    <hr />

                    <div class="row form-group">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email" class=" form-control-label">Email</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-at"></i>
                                    </div>
                                    <input type="email" value="{{$patient->email}}" name="email" id="email" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="address" class=" form-control-label">Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" value="{{$patient->address}}" name="address" id="address" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="phone" class=" form-control-label">Mobile Phone No.</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-mobile"></i>
                                    </div>
                                    <input type="text" value="{{$patient->phone}}" name="phone" id="phone" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="landline" class=" form-control-label">Landline No.</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" value="{{$patient->telephone}}" name="landline" id="landline" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>
                        <span>Date of Birth &amp; Age</span>
                    </p>
                    <hr />

                    <div class="row form-group">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dob" class=" form-control-label">Date of Birth</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input value="{{$patient->DOB}}" type="date" id="dob" name="dob" class="form-control" required>
                                </div>
                            </div>
                        </div>
            </div>
            <div class="card-footer">
                <button type= "submit" class="btn btn-primary">
                    Edit</button>
                <a onclick="window.history.back()" class="btn btn-danger">
                    Cancel
                </a>
            </div>
        </div>
                        
            </form>
    </div>

    @endsection