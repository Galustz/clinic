@extends('home')
@section('section')
<section class="p-t-20">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35">{{ $patient->reg_no }}:<strong> {{ $patient->name }}'s</strong> History</h3>
    
            <div class="row">
                <div class="col-md-4">
                    <img src="assets/images/patients/{{ $patient->photo }}" alt="" width="200" height="auto">
                </div>
        
                <div class="row col-md-4">
                    <div class="col-md-6">
                        <p>Date of Visit #</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $date }}</p>
                    </div>
        
                    <div class="col-md-6">
                        <p>Doctor Alloccated</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $doctor }}</p>
                    </div>
        
                    <div class="col-md-6">
                        <p>Service Requested</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $service }}</p>
                    </div>

                    <div class="col-md-6">
                        <p>Service Mode</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $service_mode }}</p>
                    </div>
                </div>
            </div>
            
            <br/><br/>
            <h4><strong>Vitals</strong></h4><br/>
            <hr />
    
            <div class="row">            
                <div class="row col-md-6">
                    <div class="col-md-6">
                    <p>Weight</p>
                    </div>
                    <div class="col-md-6">
                    <p>{{ $vitals->weight }}kg</p>
                    </div>
        
                    <div class="col-md-6">
                    <p>Height</p>
                    </div>
                    <div class="col-md-6">
                    <p>{{ $vitals->height }}m</p>
                    </div>
        
                    <div class="col-md-6">
                    <p>Blood Pressure</p>
                    </div>
                    <div class="col-md-6">
                    <p>{{ $vitals->blood_pressure }}</p>
                    </div>
        
                    <div class="col-md-6">
                    <p>Pulse Rate</p>
                    </div>
                    <div class="col-md-6">
                    <p>{{ $vitals->pulse_rate }}</p>
                    </div>
                </div>
                <div class="row col-md-6">
                    <div class="col-md-6">
                        <p>Respiratory Rate</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $vitals->respiratory_rate }}</p>
                    </div>
        
                    <div class="col-md-6">
                        <p>Temperature</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $vitals->temperature }}&#8451;</p>
                    </div>
        
                    <div class="col-md-6">
                        <p>Other</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $vitals->other }}</p>
                    </div>
        
                    <div class="col-md-6">
                        <p>Name of Nurse</p>
                    </div>
                    <div class="col-md-6">
                        <p>{{ $vitals->nurse }}</p>
                    </div>
                </div>
            </div>

            <br/><br/>
            <h4><strong>Diagnosis</strong></h4><br/>
            <hr />
    
            <div class="row">
                <h5><strong>Description</strong></h5>            
                <p> {{ $diagnosis->description }} </p><br><br>

                <h5><strong>Examination</strong></h5>
                <p> {{ $diagnosis->examination }} </p><br><br>

                <h5><strong>Differential Diagnosis</strong></h5>
                <p> {{ $diagnosis->diff_diagnosis }} </p>
            </div>

            <br/><br/>
            <h4><strong>Prescriptions</strong></h4><br/>
            <hr />
    
            <div class="row">
                <h5><strong>Case History</strong></h5>
                <p> {{ $prescription->case_history }} </p><br><br>

                <h5><strong>Description</strong></h5>
                <p> {{ $prescription->description }} </p><br><br>

                <h5><strong>Medication prescribed</strong></h5>
                <p> {{ $prescription->medication }} </p><br><br>

                <h5><strong>Medication from Pharmacy</strong></h5>
                <p> {{ $prescription->medication_from_pharmacist }} </p><br>
            </div>
            <br/><br/>
			
            <h4><strong>Laboratory Results</strong></h4><br/>
            <hr />
    
            <div class="row">
                <h5><strong>Investigation Requested</strong></h5>
                <p> {{ $lab_result->inv_resquested }} </p><br><br>

                <h5><strong>Laboratory Results</strong></h5>
                <p> {{ $lab_result->results }} </p><br>
            </div>
            <br/><br/>
            <hr />
        </div>
        </div>
    </div>
</section>

@include('patients.details.payments')
@endsection

