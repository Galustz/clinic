@extends('home')
@php
  use App\Patient;
  use Carbon\Carbon;
  use App\PatientQueueTemporary;

  $patient = Patient::find($id);
  $date = date_create_from_format('Y-m-d', $patient->DOB);   
  $reference = Carbon::now();
  $diff = $reference->diff($date);
  $years = $diff->y;

  $patient_visits = PatientQueueTemporary::where('patient_reg_no',$patient->reg_no)->where('status','attended')->orderBy('visit_no','asc')->get();
@endphp
@section('section')
<div style="padding:40px;">
  <a onclick="window.history.back()" class="btn btn-link">Back</a>
  <div class="card card-body">
      <div class="row">
        <h4 class="col-lg-10 col-md-9">Patient Details</h4>
        <div class="col-lg-2 col-md-3">
            <div class="row">               
            </div>
        </div>
        

      </div>
      <hr>
      <div class="row">
          <div class="col-lg-2">
              Date: <br> <strong>{{$patient->created_at->format('d/m/Y')}}</strong> 
          </div> 
          <div class="col-lg-1">
              C/I:<br> <strong>Cash</strong>
          </div>
          <div class="col-lg-2">
              Name:<br> <strong>{{$patient->name}}</strong>
          </div>
          <div class="col-lg-1">
              Age:<br> <strong>{{$years}}</strong>
          </div>
          <div class="col-lg-1">
              Gender:<br> <strong>{{$patient->gender}}</strong>
          </div>
          <div class="col-lg-3">
              Occupation:<br> <strong>{{$patient->occupation}}</strong>
          </div>
      </div>
      <hr>
      <div class="row">
        
        <div class="col-lg-3">
         Email: <br> <strong>{{$patient->email}}</strong> 
        </div> 
        <div class="col-lg-3">
          Address: <br> <strong>{{$patient->address}}</strong> 
         </div> 
         <div class="col-lg-3">
          Mobile No: <br> <strong>{{$patient->phone}}</strong> 
         </div> 
         <div class="col-lg-3">
          Landline No: <br> <strong>{{$patient->telephone}}</strong> 
         </div> 
      </div>
  </div>
  {{-- <div class="card card-body">
      <br>   
      <h4 class="">Patient Visits</h4>
      <div class="table-responsive">
        <table class="table col-lg-12 col-md-12 col-sm-12">
            <tbody>
                <tr>
                  <td><strong>Visit No</strong></td>
                  <td><strong>Date</strong></td>
                  <td><strong>Time</strong></td>
                  <td></td>
                </tr>
                @foreach($patient_visits as $pv)
                <tr>
                <td>{{$pv->visit_no}}</td>
                <td>{{$pv->created_at->format('d/m/y')}}</td>
                <td>{{$pv->created_at->format('h:i a')}}</td>
                <td><a href="{{route('patient-visit',$pv->visit_no)}}" class="btn btn-primary">View</a></td> 
                @endforeach
            </tbody>
        </table>
    </div>  
  </div> --}}
</div>

  
@endsection