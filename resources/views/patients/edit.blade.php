@extends('home')
@section('section')
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong><i class="fa fa-user"></i>   Edit Patient Details</strong>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="{{ route('patients.update') }}/{{ $patient->id }}" id="update-patient">
                            {{ csrf_field() }}
                        <input type="text" name="p-id" id="p-id" hidden>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="reg_no" class=" form-control-label">Patient Reg No.</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-id-card"></i>
                                            </div>
                                            <input type="text" name="reg_no" id="reg_no" class="form-control" value="{{ $patient->reg_no }}" required disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name" class=" form-control-label">Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" name="name" id="name" class="form-control" value="{{ $patient->name }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="gender" class=" form-control-label">Gender</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-intersex"></i>
                                            </div>
                                            <input type="text" id="gender" name="gender" class="form-control" value="{{ $patient->gender }}"  required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="blood-group" class=" form-control-label">Blood Group</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-tint"></i>
                                            </div>
                                            <input type="text" id="blood-group" name="blood_group" class="form-control" value="{{ $patient->blood_group }}" >
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="type" class=" form-control-label">Type of Patient</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-hospital"></i>
                                            </div>
                                            <select name="type" id="type" class="form-control" required>
                                                <option value="">Please select</option>
                                                <option value="General" @if($patient->type == 'General')selected @endif>General</option>
                                                <option value="Corporate" @if($patient->type == 'Corporate')selected @endif>Corporate</option>
                                                <option value="Insurance" @if($patient->type == 'Insurance')selected @endif>Insurance</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="nationality" class=" form-control-label">Nationality</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-globe"></i>
                                            </div>
                                            <input type="text" name="nationlity" id="nationality" class="form-control" value="{{ $patient->nationality }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="photo" class=" form-control-label">Patient's Photo</label>
                                        <div id="img-display"><img src="assets/images/patient/{{ $patient->photo }}" alt="{{ $patient->photo }}" width="100" height="100"></div>                                
                                        <button id="capturePhoto" class="btn btn-primary"><i class="fa fa-camera"></i> Take a photo</button>
                                        <p>OR Upload</p>
                                        <input type="file" id="photo" name="photo" class="form-control-file">
                                    </div>
                                </div>
                            </div>

                            <p><span>Parents Details</span></p>
                            <hr />

                            <div class="row form-group">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="father-name" class=" form-control-label">Father's Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" name="father-name" id="father-name" class="form-control" value="{{ $patient->father_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="father-occupation" class=" form-control-label">Father's Occupation</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                            <input type="text" id="father-occupation" name="father-occupation" class="form-control" value="{{ $patient->father_occupation }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="mother-name" class=" form-control-label">Mother's Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" id="mother-name" name="mother-name" class="form-control" value="{{ $patient->mother_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="mother-occupation" class=" form-control-label">Mother's Occupation</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                            <input type="text" id="mother-occupation" name="mother-occupation" class="form-control" value="{{ $patient->mother_occupation }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <p><span>Contacts</span></p>
                            <hr />

                            <div class="row form-group">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="email" class=" form-control-label">Email</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-at"></i>
                                            </div>
                                            <input type="email" name="email" id="email" class="form-control" value="{{ $patient->email }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="address" class=" form-control-label">Address</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-map-marker"></i>
                                            </div>
                                            <input type="text" name="address" id="address" class="form-control" value="{{ $patient->address }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone" class=" form-control-label">Mobile Phone No.</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-mobile"></i>
                                            </div>
                                            <input type="text" name="phone" id="phone" class="form-control" value="{{ $patient->phone }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="landline" class=" form-control-label">Landline No.</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" name="telephone" id="telephone" class="form-control" value="{{ $patient->telephone }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <p><span>Date of Birth &amp; Age</span></p>
                            <hr />

                            <div class="row form-group">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="dob" class=" form-control-label">Date of Birth</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="date" id="@if($patient->patient_type == 'adult') adult-dob @else dob @endif" name="dob" class="form-control" required value="{{ $patient->DOB->format('d/m/Y') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="age-year" class=" form-control-label">Age</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="@if($patient->patient_type == 'adult') adult-year @else age-year @endif" name="age-year" class="form-control">
                                        </div>
                                        <small class="form-text text-muted">Year</small>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="age-year" class=" form-control-label"></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="@if($patient->patient_type == 'adult') adult-month @else age-month @endif" name="age-month" class="form-control">
                                        </div>
                                        <small class="form-text text-muted">Month</small>
                                    </div>
                                </div>
                                @if($patient->patient_type == 'child')
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="age-year" class=" form-control-label"></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="age-day" name="age-day" class="form-control">
                                        </div>
                                        <small class="form-text text-muted">Day</small>
                                    </div>
                                </div>
                                @endif
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Edit
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    @if($patient->patient_type == 'child')
    <script>
        $(window).on('load',function(){
            var dob_val = $('#dob').val();
            $.ajax({
                url: 'getAge',
                type: 'GET',
                dataType: 'json',
                data: {dob:dob_val}
            })
            .done(function(data){
                $('#age-year').val(data.years);
                $('#age-month').val(data.months);
                $('#age-day').val(data.days);
            })
            .fail(function(data){
                console.log(data);
            });
        });
    </script>
    @endif 
    @if($patient->patient_type == 'adult')
    <script>
        $(window).on('load',function(){
            var dob_val = $('#adult-dob').val();
            $.ajax({
                url: 'getAge',
                type: 'GET',
                dataType: 'json',
                data: {dob:dob_val}
            })
            .done(function(data){
                $('#adult-year').val(data.years);
                $('#adult-month').val(data.months);
            })
            .fail(function(data){
                console.log(data);
            });
        });
    </script>
    @endif
@endpush