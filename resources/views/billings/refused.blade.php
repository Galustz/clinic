@php
//use App\PharmacyList;
use App\BillingList;
use App\Patient;
use App\MedicineRecord;
use App\LabTestRecord;  
$billinglist = BillingList::where('billing_id',0)->where('status','refused')->where( 'created_at', '>', Carbon\Carbon::now()->subDays(30))->get();
$i = 0;
//for use in the table for alternating columns.
$previous_registration_number = 0;
@endphp
@if(!empty(session('success')))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
    <span class="badge badge-pill badge-success">Success</span>
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
@endif
<section class="p-t-20">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="">Billing List</h3>
            <h5>{{Carbon\Carbon::today()->subdays(30)->format('d/m/Y').' to '.Carbon\Carbon::today()->format('d/m/Y')}}</h5>
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE-->
                    <div class="table-responsive m-b-40">
                        <table id="patient-que-table" class="datatable mytable">
                            <thead>
                                <tr>
                                    {{-- <th>Number</th> --}}
                                    <th>Reg No</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Doctor</th>
                                    <th>Test/Medicine</th>
                                    <th style="width:100px !important;">Previous change</th>
                                    <th style="width:100px !important;">Total Amount</th>
                                    <th style="width:100px !important;">Amount due</th>
                                    <th style="width:100px !important;">Cash Paid</th>
                                    <th style="width:100px !important;">Change remaining</th>
                                    <th style="width:100px !important;">Change returned</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($billinglist as $billingitem)
                                    @php
                                        $prn = $billingitem->patient_registration_no;
                                        $pvn = $billingitem->visit_no;
                                        $patient = Patient::find($prn);
                                        $billing = \App\Billing::where('patient_registration_no',$prn)->where('visit_no',$pvn)->orderBy('created_at','desc')->first();
                                        $change = $billing->change->change_due;
                                        if($billingitem->type == "Pharmacy"){
                                            $servs = MedicineRecord::where('patient_registration_no',$prn)
                                            ->where('family_help_id',$billingitem->family_help_id)
                                            ->where('visit_no',$pvn)
                                            ->where('status','Not Taken')
                                            ->get();
                                        }else if($billingitem->type == "Laboratory"){
                                            $servs = LabTestRecord::where('patient_registration_no',$prn)
                                            ->where('family_help_id',$billingitem->family_help_id)
                                            ->where('visit_no',$pvn)
                                            ->where('status','given')
                                            ->get();
                                        }
                                        $sum=0;
                                        $laburl = url('/billing/payTestBill');
                                        $medurl = url('/billing/payMedicineBill');
                                        $url = $billingitem->type=="Laboratory"?$laburl:$medurl;
                                        //store the current registration number
                                        //check if the current registration number is equal to the previous one then increment or not.
                                        if($previous_registration_number==$prn){
                                            $previous_registration_number = $prn;
                                        }else{
                                            $previous_registration_number = $prn;
                                            $i++;
                                        }
                                        $name = $patient->name;
                                        if($billingitem->family_help_id!=0)
                                            $name = $name.'\'s '.$billingitem->familyHelp->relationship;
                                    @endphp
                                    <tr style="background-color: {{$i%2==0?'white':'#eeeeee'}} !important;">
                                        {{-- <td>{{$billingitem->id}}</td> --}}
                                        <td>{{$patient->reg_no}}</td>
                                        <td>{{$name}}</td>
                                        <td>{{$billingitem->type}}</td>
                                        <td>Doctor 1</td>
                                        <form action="{{$url}}" method="post">
                                            {{csrf_field()}}
                                            <td>
                                                @if(!empty($servs)) 
                                                    @foreach($servs as $serv)
                                                    @php
                                                        $price = $serv->item->price?$serv->item->price:$serv->item->selling_price*$serv->total;
                                                        $sum = $sum + $price;
                                                        $amount_due = $change>=$sum?0:$sum-$change; 
                                                        $change_remaining = $amount_due==0?$change-$sum:0;
                                                    @endphp
                                                    <input type="hidden" value="{{$serv->id}}" name="{{$billingitem->type=='Laboratory'?'lab_record_id[]':'medicine_record_id[]'}}">
                                                    <input type="checkbox" name="status{{$serv->id}}" value="Taken" onchange="newSum({{$billingitem->id}},{{$serv->id}})"
                                                        id="checkbox{{$billingitem->id.$serv->id}}" checked><span>{{$serv->item->name}}</span>
                                                    <input type="hidden" name="" id="price{{$billingitem->id.$serv->id}}" value="{{$price}}"><br>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td> 
                                                <input type="text" name="previous_change" value="{{number_format($change,0)}}" id="{{'previous_change'.$billingitem->id}}" class="form-control" readonly>
                                            </td>
                                            <td> 
                                                <input type="text" name="total_amount" value="{{number_format($sum,0)}}" id="{{'sum'.$billingitem->id}}" class="form-control" readonly>
                                            </td>
                                            <td>  
                                                <input type="text" name="" value="{{number_format($amount_due,0)}}" id="{{'amount_required_to_pay'.$billingitem->id}}" class="form-control" readonly>
                                            </td>
                                            <td>                              
                                                <input type="text" name="amount_recieved" id="{{'amount_recieved'.$billingitem->id}}" value="0" class="form-control" {{$amount_due==0?'readonly':''}} onkeyup="newChange({{$billingitem->id}})">
                                            </td>
                                            <td>                              
                                                <input type="text" name="change_remaining" id="{{'change_remaining'.$billingitem->id}}" value="{{number_format($change_remaining,0)}}" class="form-control" readonly>
                                            </td>
                                            <td> 
                                                <input type="text" name="change_returned" id="{{'change_returned'.$billingitem->id}}" value="0" class="form-control">
                                            </td>
                                            <input type="hidden" name="amount_due" value="0" id="amount_due{{$billingitem->id}}">
                                            <input type="hidden" name="list_id" value="{{$billingitem->id}}">
                                            <input type="hidden" name="prn" value="{{$prn}}">
                                            <input type="hidden" name="pvn" value="{{$pvn}}">
                                            {{-- <td><br><button type="submit" href="" class="btn btn-primary">Bill</button></td> --}}
                                        </form>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE                  -->
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .table-row:hover td {
        cursor: pointer;
        background-color: grey;
        color: white;
    }

    .active-row td {
        background-color: grey;
        color: white;
    }
</style>