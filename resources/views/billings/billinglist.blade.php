@php
//use App\PharmacyList;
use App\BillingList;
use App\Patient;
use App\MedicineRecord;
use App\LabTestRecord;
$billinglist = BillingList::where('billing_id',0)
                    ->where('created_at','>=',Carbon\Carbon::today())
                    ->where(function($query){
                          $query->orWhere('status','!=','refused')
                                ->orWhereNull('status');
                    })                   
                    ->get();
//dd($billinglist);
$i = 0;
//for use in the table for alternating columns.
$previous_registration_number = 0;
@endphp
@if(!empty(session('success')))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
    <span class="badge badge-pill badge-success">Success</span>
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
@endif
<section class="p-t-20">
    <div class="row">
        <div class="col-lg-12">
            @php
                // xdebug_break();
            @endphp
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <h3 class="">Patients List</h3>      
                    <h5>{{Carbon\Carbon::today()->format('d/m/Y')}}</h5>              
                </div>
                <div class="col-lg-7 col-md-7 pull-right text-right">
                    @php
                        $requests = \App\GloveRequest::where('created_at','>=',Carbon\Carbon::today())->orderBy('created_at','asc')->get();
                        $sum = 0; 
                        foreach($requests as $request){
                            $sum += $request->no_of_gloves;
                        }
                    @endphp
                    <h3>Gloves: {{$sum}}</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE-->
                    <div class="table-responsive m-b-40">
                        <table id="patient-que-table" class="datatabe mytable" data-row-count="{{$billinglist->count()}}">
                            <thead>
                                <tr>
                                    {{-- <th>Number</th> --}}
                                    <th>Reg No</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Doctor</th>
                                    <th>Test/Medicine</th>
                                    <th style="width:100px !important;">Total Amount</th>
                                    <th style="width:100px !important;">Previous change</th>
                                    <th style="width:100px !important;">Payment Remaining</th>
                                    <th style="width:100px !important;">Cash Paid</th>
                                    <th style="width:100px !important;">Change remaining</th>
                                    <th style="width:100px !important;">Change returned</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php 
                                    $row_no = 1;
                                @endphp
                                @foreach ($billinglist as $billingitem)
                                    @php
                                        $additional_charge = 0;
                                        $prn = $billingitem->patient_registration_no;
                                        $pvn = $billingitem->visit_no;
                                        $patient = Patient::find($prn);
                                        $should_not_pay = $patient->patient_type == "Insurance"||$patient->patient_type=="Corporate";
                                        $billing = \App\Billing::where('patient_registration_no',$prn)->where('visit_no',$pvn)->orderBy('created_at','desc')->first();
                                        $change = $billingitem->family_help_id!=0?0:$billing->change->change_due;
                                        if($billingitem->type == "Pharmacy"){
                                            $servs = MedicineRecord::where('patient_registration_no',$prn)
                                            ->where('family_help_id',$billingitem->family_help_id)
                                            ->where('visit_no',$pvn)
                                            ->where('status','Not Taken')
                                            ->whereIn('billing_id',[0,$billingitem->id])
                                            ->get();
                                            $additional_charge = \App\Setting::where('name','service_charge')->first();
                                            $additional_charge = $additional_charge!=null?($additional_charge->value!=""?$additional_charge->value:0):0;
                                            $fields_count =  $servs->count() + 5;
                                        }else if($billingitem->type == "Laboratory"){
                                            $servs = LabTestRecord::where('patient_registration_no',$prn)
                                            ->where('family_help_id',$billingitem->family_help_id)
                                            ->where('visit_no',$pvn)
                                            ->where('status','pending')
                                            ->whereIn('billing_id',[0,$billingitem->id])
                                            ->get();  
                                            $fields_count =  $servs->count() + 4;
                                        }
                                        $sum=0;
                                        $laburl = url('/billing/payTestBill');
                                        $medurl = url('/billing/payMedicineBill');
                                        $url = $billingitem->type=="Laboratory"?$laburl:$medurl;
                                        //store the current registration number
                                        //check if the current registration number is equal to the previous one then increment or not.
                                        if($previous_registration_number==$prn){
                                            $previous_registration_number = $prn;
                                        }else{
                                            $previous_registration_number = $prn;
                                            $i++;
                                        }
                                        $name = $patient->name;
                                        if($billingitem->family_help_id!=0)
                                            $name = $name.'\'s '.$billingitem->familyHelp->relationship;
                                        $field_no = 1;
                                        //dd($fields_count);
                                        $price = 0;
                                        $sum = $additional_charge;
                                        $amount_due = 0;
                                        $change_remaining = 0; 
                                    @endphp
                                    <tr style="background-color: {{$i%2==0?'white':'#eeeeee'}} !important;" id="row{{$row_no}}" data-fields-count="{{$fields_count}}">
                                        {{-- <td>{{$billingitem->id}}</td> --}}
                                        <td>{{$patient->reg_no}}</td>
                                        <td>{{$name}}</td>
                                        <td>{{$billingitem->type}}</td>
                                        <td>Doctor 1</td>
                                        <form action="{{$url}}" method="post">
                                            {{csrf_field()}}
                                            <td>
                                                @if(!empty($servs)) 
                                                    @foreach($servs as $serv)
                                                    @php
                                                        $price = $serv->item->price?$serv->item->price:$serv->item->selling_price*$serv->total;
                                                        $sum = $sum + $price;
                                                        $amount_due = $change>=$sum?0:$sum-$change; 
                                                        $change_remaining = $billingitem->family_help_id!=0?0:($amount_due==0?$change-$sum:0);
                                                        if($should_not_pay){
                                                            $amount_due = 0; 
                                                        }
                                                    @endphp
                                                    <div style="padding:0px;" id="field{{$row_no.$field_no}}" data-field-id="checkbox{{$billingitem->id.$serv->id}}">
                                                    <input type="hidden" value="{{$serv->id}}" name="{{$billingitem->type=='Laboratory'?'lab_record_id[]':'medicine_record_id[]'}}">
                                                    <input type="checkbox" name="status{{$serv->id}}" value="Taken" onchange="newSum({{$billingitem->id}},{{$serv->id}})"
                                                        id="checkbox{{$billingitem->id.$serv->id}}" class="mousetrap" checked><span>{{$serv->item->name}}</span>
                                                    <input type="hidden" name="" id="price{{$billingitem->id.$serv->id}}" value="{{$price}}"></div>
                                                    @php
                                                        $field_no++;
                                                    @endphp
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                <input type="text" name="total_amount" value="{{number_format($sum,0)}}"
                                                    id="{{'sum'.$billingitem->id}}" class="form-control" readonly>
                                            </td>
                                            @if ($should_not_pay)
                                                <td colspan="5" class="text-center">{{$patient->patient_type." patient"}}</td>
                                            @endif
                                            @if ($should_not_pay) 
                                            @else
                                            <td>
                                                <input type="text" name="previous_change"
                                                    value="{{number_format($change,0)}}"
                                                    id="{{'previous_change'.$billingitem->id}}" class="form-control"
                                                    readonly>

                                            </td>@endif
                                            @if ($should_not_pay) 
                                            @else
                                            <td>
                                                <input type="text" name="" value="{{number_format($amount_due,0)}}"
                                                    id="{{'amount_required_to_pay'.$billingitem->id}}"
                                                    class="form-control" readonly>

                                            </td> @endif
                                            @if ($should_not_pay) 
                                            @else
                                            <td>
                                                <div id="field{{$row_no.$field_no}}"
                                                    data-field-id="{{'amount_recieved'.$billingitem->id}}">
                                                    <input type="text" name="amount_recieved"
                                                        id="{{'amount_recieved'.$billingitem->id}}"
                                                        value="{{$amount_due==0?'0':''}}" class="form-control mousetrap"
                                                        {{$amount_due==0?'readonly':'required'}}
                                                        onkeyup="newChange({{$billingitem->id}})">
                                                </div>
                                            </td> @endif
                                            @if ($should_not_pay) 
                                            @else 
                                            <td>
                                                <input type="text" name="change_remaining"
                                                    id="{{'change_remaining'.$billingitem->id}}"
                                                    value="{{number_format($change_remaining,0)}}" class="form-control"
                                                    readonly>
                                            </td> @endif
                                            @if ($should_not_pay) 
                                            @else
                                            <td>
                                                <div id="field{{$row_no.($field_no+1)}}"
                                                    data-field-id="{{'change_returned'.$billingitem->id}}"> 
                                                    <input type="text" name="change_returned"
                                                        id="{{'change_returned'.$billingitem->id}}"
                                                        {!!$should_not_pay?"value='0' readonly":""!!}
                                                        class="form-control mousetrap" required> 
                                                </div>
                                            </td> @endif
                                            <input type="hidden" name="amount_due" value="0" id="amount_due{{$billingitem->id}}">
                                            <input type="hidden" name="list_id" value="{{$billingitem->id}}">
                                            <input type="hidden" name="prn" value="{{$prn}}">
                                            <input type="hidden" name="pvn" value="{{$pvn}}">
                                            <input type="hidden" name="service_charge" value="{{$additional_charge}}">
                                            <td>
                                                <br>
                                                @if ($should_not_pay)
                                                    <button type="submit" class="btn btn-primary mousetrap btn-sm" formaction="{{$url.'?action=cashless'}}">Pay</button>
                                                    <div id="field{{$row_no.($field_no+4)}}" data-field-id="{{'refused-btn'.$billingitem->id}}">
                                                        <button id="{{'refused-btn'.$billingitem->id}}" type="submit" href="" class="btn btn-primary mousetrap btn-sm"  formaction="{{$url.'?action=refused'}}">Refused</button></button>
                                                    </div> 
                                                @else
                                                    <div id="field{{$row_no.($field_no+2)}}" data-field-id="{{'bill-btn'.$billingitem->id}}">
                                                        <button id="{{'bill-btn'.$billingitem->id}}" type="submit" href="" class="btn btn-primary mousetrap btn-sm" formaction="{{$url.'?action=bill'}}">Bill</button>
                                                    </div>
                                                    @if($billingitem->type=="Pharmacy")
                                                    <div id="field{{$row_no.($field_no+3)}}" data-field-id="{{'bill-discount-btn'.$billingitem->id}}">
                                                        <button id="{{'bill-discount-btn'.$billingitem->id}}" type="submit" href="" class="btn btn-primary mousetrap btn-sm" formaction="{{$url.'?action=discount'}}">Discount</button>
                                                    </div>
                                                    @else
                                                        @php
                                                            $field_no--;
                                                        @endphp
                                                    @endif
                                                    <div id="field{{$row_no.($field_no+4)}}" data-field-id="{{'refused-btn'.$billingitem->id}}">
                                                        <button id="{{'refused-btn'.$billingitem->id}}" type="submit" href="" class="btn btn-primary mousetrap btn-sm"  formaction="{{$url.'?action=refused'}}">Refused</button></button>
                                                    </div> 
                                                @endif
                                            </td>
                                        </form>
                                    @php
                                        $row_no++;
                                    @endphp
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE                  -->
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .table-row:hover td {
        cursor: pointer;
        background-color: grey;
        color: white;
    }

    .active-row td {
        background-color: grey;
        color: white;
    }
</style>