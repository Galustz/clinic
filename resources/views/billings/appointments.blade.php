@php
//use App\PharmacyList;
use App\BillingList;
use App\Patient;
use App\MedicineRecord;
use App\LabTestRecord;
use App\Billing;
$billinglist = BillingList::where('type','Appointment')
                    ->where('created_at','>=',Carbon\Carbon::today())
                    ->where(function($query){
                          $query->orWhere('status','!=','paid')
                                ->orWhereNull('status');
                    })                   
                    ->get(); 
$i = 0;
//for use in the table for alternating columns.
$previous_registration_number = 0;
@endphp
@if(!empty(session('success')))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
    <span class="badge badge-pill badge-success">Success</span>
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
@endif
<section class="p-t-20">
    <div class="row">
        <div class="col-lg-12">
            @php
                // xdebug_break();
            @endphp
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <h3 class="">Patients List</h3>      
                    <h5>{{Carbon\Carbon::today()->format('d/m/Y')}}</h5>              
                </div>
                {{-- <div class="col-lg-7 col-md-7 pull-right text-right">
                    @php
                         $requests = \App\GloveRequest::where('created_at','>=',Carbon\Carbon::today())->orderBy('created_at','asc')->get();
                        $sum = 0; 
                        foreach($requests as $request){
                            $sum += $request->no_of_gloves;
                        }
                    @endphp
                    <h3>Gloves: {{$sum}}</h3>
                </div> --}}
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE-->
                    <div class="table-responsive m-b-40">
                        <table id="patient-que-table" class="datatabe mytable" data-row-count="{{$billinglist->count()}}">
                            <thead>
                                <tr>
                                    {{-- <th>Number</th> --}}
                                    <th>Reg No</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Doctor</th>
                                    <th>Test/Medicine</th>
                                    <th style="width:100px !important;">Total Amount</th>
                                    <th style="width:100px !important;">Previous change</th>
                                    <th style="width:100px !important;">Payment Remaining</th>
                                    <th style="width:100px !important;">Cash Paid</th>
                                    <th style="width:100px !important;">Change remaining</th>
                                    <th style="width:100px !important;">Change returned</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php 
                                    $row_no = 1;
                                @endphp
                                @foreach ($billinglist as $billingitem)
                                    @php
                                        $additional_charge = 0;
                                        $prn = $billingitem->patient_registration_no;
                                        $pvn = $billingitem->visit_no;
                                        $patient = Patient::find($prn);
                                        $billing = \App\Billing::where('patient_registration_no',$prn)->where('visit_no',$pvn)->orderBy('created_at','desc')->first();
                                        $change = $billingitem->family_help_id!=0?0:$billing->change->change_due;
                                        $fields_count = 4;
                                        $sum=0;
                                        $url = url('billing/pay-for-appointment'); 
                                        //store the current registration number
                                        //check if the current registration number is equal to the previous one then increment or not.
                                        if($previous_registration_number==$prn){
                                            $previous_registration_number = $prn;
                                        }else{
                                            $previous_registration_number = $prn;
                                            $i++;
                                        }
                                        $name = $patient->name; 
                                        $field_no = 1;
                                        //dd($fields_count);
                                        $price = 0;
                                        $sum = $additional_charge;
                                        $amount_due = 0;
                                        $change_remaining = 0;
                                        $min = "";
                                    @endphp
                                    <tr style="background-color: {{$i%2==0?'white':'#eeeeee'}} !important;" id="row{{$row_no}}" data-fields-count="{{$fields_count}}">
                                        {{-- <td>{{$billingitem->id}}</td> --}}
                                        <td>{{$patient->reg_no}}</td>
                                        <td>{{$name}}</td>
                                        <td>{{$billingitem->type}}</td>
                                        <td>{{$billingitem->patient_que->doctor->name}}</td>
                                        <form action="{{$url}}" method="post">
                                            {{csrf_field()}}
                                            <td> 
                                                @php 
                                                    $sum = $billing->amount_due;
                                                    $amount_due = $change>=$sum?0:$sum-$change; 
                                                    $change_remaining = $billingitem->family_help_id!=0?0:($amount_due==0?$change-$sum:0);
                                                    $min = "min='$amount_due'";
                                                @endphp
                                                <h6>Consultation fee</h6>                                                    
                                            </td>
                                            <td>
                                                <input type="text" name="total_amount" value="{{number_format($sum,0)}}" id="{{'sum'.$billingitem->id}}" class="form-control" readonly>
                                            </td>
                                            <td> 
                                                <input type="text" name="previous_change" value="{{number_format($change,0)}}" id="{{'previous_change'.$billingitem->id}}" class="form-control" readonly>
                                            </td>
                                            <td>  
                                                <input type="text" name="" value="{{number_format($amount_due,0)}}" id="{{'amount_required_to_pay'.$billingitem->id}}" class="form-control" readonly>
                                            </td>
                                            <td>          
                                                <div id="field{{$row_no.$field_no}}" data-field-id="{{'amount_recieved'.$billingitem->id}}">                   
                                                <input type="number" name="amount_recieved" id="{{'amount_recieved'.$billingitem->id}}" {!!$min!!} value="{{$amount_due==0?'0':''}}" class="form-control mousetrap px-0" {{$amount_due==0?'readonly':'required'}} onkeyup="newChange({{$billingitem->id}})">
                                                </div> 
                                            </td>
                                            <td>                              
                                                <input type="text" name="change_remaining" id="{{'change_remaining'.$billingitem->id}}" value="{{number_format($change_remaining,0)}}" class="form-control" readonly>
                                            </td>
                                            <td> 
                                                <div id="field{{$row_no.($field_no+1)}}" data-field-id="{{'change_returned'.$billingitem->id}}">
                                                <input type="number" name="change_returned" id="{{'change_returned'.$billingitem->id}}" value="" class="form-control mousetrap px-0" required>
                                                </div>
                                            </td>
                                            <input type="hidden" name="amount_due" value="0" id="amount_due{{$billingitem->id}}">
                                            <input type="hidden" name="list_id" value="{{$billingitem->id}}">
                                            <input type="hidden" name="prn" value="{{$prn}}">
                                            <input type="hidden" name="pvn" value="{{$pvn}}">
                                            <input type="hidden" name="service_charge" value="{{$additional_charge}}">
                                            <td>
                                                <br>
                                                <div id="field{{$row_no.($field_no+2)}}" data-field-id="{{'bill-btn'.$billingitem->id}}">
                                                    <button id="{{'bill-btn'.$billingitem->id}}" type="submit" href="" class="btn btn-primary mousetrap btn-sm" formaction="{{$url}}">Pay</button>
                                                </div> 
                                                @php
                                                    $field_no--;
                                                @endphp   
                                            </td>
                                        </form>
                                    @php
                                        $row_no++;
                                    @endphp
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE                  -->
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .table-row:hover td {
        cursor: pointer;
        background-color: grey;
        color: white;
    }

    .active-row td {
        background-color: grey;
        color: white;
    }
</style>