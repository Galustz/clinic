  @php
    use App\PatientQueueTemporary;
    use App\Patient;
    use App\BillingList;
    $patients = PatientQueueTemporary::whereDate('created_at',Carbon\Carbon::today())
                                        ->whereIn('type',PatientQueueTemporary::PATIENT_QUEUE_TYPES)
                                        ->whereIn('status',['waiting','out'])
                                        ->orderBy('service_mode','asc')
                                        ->orderBy('created_at','asc')
                                        ->get();
    $results = BillingList::whereIn('status',['done','Left'])->orderBy('updated_at','asc')->orderBy('status','asc')->get();

  @endphp

  <!-- DATA TABLE-->
  <section class="p-t-20">
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                  <h4 class="">Patients Queue</h4>
                    <h5>Date:{{Carbon\Carbon::now()->format('d/m/y')}}</h5>
                  <div class="row">
                      <div class="col-md-12">
                          <!-- DATA TABLE-->
                          <div class="table-responsive m-b-40">
                              <table id="patient-que-table" class="datatable mytable">
                                  <thead>
                                      <tr>
                                          <th>Que <br> #</th>  
                                          <th>reg. <br> #</th>
                                          <th>name</th> 
                                          <th>Service <br> Mode</th> 
                                          <th>Alloc. <br> Doctor</th> 
                                          <th>Status</th>
                                          <th></th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                        @foreach($patients as $pt)
                                            @php
                                                $patient = Patient::find($pt->patient_reg_no); 
                                            @endphp
                                        <tr {!!$pt->status=="Out"?'style="background-color:#ffaaaa !important;"':''!!}>
                                            <td>{{$pt->patient_que_no}}</td>  
                                            <td>{{$pt->patient_reg_no}}</td>
                                            <td>{{$patient->name}}</td> 
                                            <td>{{$pt->serviceMode->name}}</td> 
                                            <td>{{$pt->doctor->name}}</td> 
                                            <td>{{$pt->status}}</td>
                                            <td><a href="{{url('reception/patientEntered',$pt->id)}}" class="btn btn-primary">Enter</a></td>
                                            <td><a href="{{url('reception/patientOut',$pt->id)}}" class="btn btn-secondary">Out</a></td>
                                        </tr>
                                        @endforeach
                                  </tbody>
                              </table>
                          </div>
                          <!-- END DATA TABLE                  -->
                      </div>
                  </div> 
              </div>
              <div class="col-md-6">
                <h4 class="">Lab Queue</h4>
                <h5>Date:{{Carbon\Carbon::now()->format('d/m/y')}}</h5>
                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="" class="datatable mytable">
                                <thead>
                                    <tr>
                                            <th>Queue <br> #</th>  
                                            <th>reg. <br> #</th>
                                            <th>name</th> 
                                            <th>Service <br> Mode</th> 
                                            <th>Alloc. <br> Doctor</th>  
                                            <th>Status</th>
                                            <th></th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($results as $result)
                                      <tr {!!$result->status == 'Left'?'style="background-color:#faa;"':''!!}>
                                          <td>{{$i}}</td>
                                          @php 
                                              $i++;
                                              $patient = App\Patient::find($result->patient_registration_no);
                                              $patientQ = App\PatientQueueTemporary::where('patient_reg_no',$result->patient_registration_no)->where('visit_no',$result->visit_no)->first();
                                              $name = $patient->name; 
                                              if($result->family_help_id!=0)
                                                    $name = $name.'\'s '.$result->familyHelp->relationship;
                                          @endphp 
                                          <td>{{$result->patient_registration_no}}</td>
                                          <td>{{$name}}</td> 
                                          <td>{{$patientQ->service_mode??'Normal'}}</td>
                                          <td>{{$patientQ->doctor??'Reception'}}</td>  
                                          <td>{{$result->status}}</td>
                                          <td>
                                            @if ($patientQ)
                                              <a href="{{url('reception/labPatientEntered',$result->id)}}" class="btn btn-primary btn-sm">Enter</a>
                                              <a href="{{url('reception/labPatientOut',$result->id)}}" class="btn btn-secondary btn-sm">Out</a></td>   
                                            @else
                                                <investigation-results billing_list_id="{{$result->id}}"></investigation-results>
                                            @endif                                  
                                      </tr>   
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
              </div>
          </div>
      </div>
  </section>
  <!-- END DATA TABLE--> 