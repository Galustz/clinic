@php
use App\Service;
use App\PatientType;
use App\ServiceMode;
$services = Service::where('trashed','false')->get();
$patienttypes = PatientType::where('trashed','false')->get();
$servicemodes = ServiceMode::where('trashed','false')->get();
@endphp
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <form enctype="multipart/form-data" method="post" action="{{ url('/patient/visit')  }}" id="register-adult-patient">
                    {{ csrf_field() }} 
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="card"> 
                                <input type="text" name="type" id="type" value="adult" hidden>
                                <input type="text" class="check-id" id="check-id" name="id" hidden> 
                                <div class="card-body card-block"> 
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <h5>Patient details</h5>
                                            <hr>
                                        </div>
                                        <p-s></p-s>
                                        <div class="col-md-12"><br> 
                                            <h5>Service details</h5>
                                            <hr>
                                        </div>
                                        <r-s-d id="visit"/>
                                        <div class="col-md-3">
                                        </div>
                                        <input id="imagedata" type="hidden" name="imagedata" value=""/> 
                                    </div>
                                    {{-- <button class="btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#demo">Other</button> --}}
                                    @php
                                        $index = 1;
                                    @endphp
                                </div>
                            </div>
                            
                    <div class="row">
                        <div class="card col-lg-12">
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                        </div>
                    </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card"> 
                                <div class="card-body">
                                   <r-c-c id="visit"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>