@extends('layouts.admin')
@section('content')
@php
    use App\ServiceMode;
    $servicemode = ServiceMode::find($id);
@endphp
<div class="container">
    <div class="card card-body">
        <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/reception/servicemodes/edit')}}">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="row form-group"> 
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                            <input type="text" value="{{$servicemode->name}}" placeholder="Name" name="name" id="servicemode" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                <input type="hidden" name="servicemode_id" value="{{ $servicemode->id}}">
                                <input type="number" value="{{$servicemode->price}}" name="price" placeholder="Price" id="price" class="form-control">
                                <span style="padding-top:10px;padding-left:10px;">. Tsh</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </div>
            </div>
        </form>
    </div> 
</div>
@endsection