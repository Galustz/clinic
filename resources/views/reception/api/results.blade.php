<div class="row">
    <div class="col-md-12">
        <h4 class="">Results</h4>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h6>Details</h6> <br>
                <table class="col-md-12 table table-striped table-borderless">
                    <tr>
                        <th>Date</th>
                        <th>Reg no</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                    </tr>
                    <tr>
                        <td>{{$billing_list->created_at->format('d/m/Y')}}</td>
                        <td>{{$patient->reg_no}}</td>
                        <td>{{$patient->name}}</td>
                        <td>{{$patient->age}}</td>
                        <td>{{$patient->gender}}</td>
                    </tr>
                </table> <br>
                @if(!empty($tests))     
                    @foreach($tests as $test)
                        @if($test->results->type == "urinal")
                        <h6>Urine test</h6>  <br>
                        <table class="col-md-12 table table-striped table-borderless">
                            <tr>
                                <th rowspan="2">Macro</th>
                                <th colspan="5">Micro</th>
                            </tr>
                            <tr>
                                <th>Color</th>
                                <th>Blood</th>
                                <th>Pus</th>
                                <th>Ova</th>
                                <th>Other</th>
                            </tr>
                            <tr>
                                <td>{{$test->results->object->macro}}</td> 
                                <td>{{$test->results->object->color}}</td> 
                                <td>{{$test->results->object->blood}}</td> 
                                <td>{{$test->results->object->pus}}</td> 
                                <td>{{$test->results->object->ova}}</td> 
                                <td>{{$test->results->object->other}}</td> 
                            </tr>
                        </table> <br>
                        @elseif($test->results->type == "stool") 
                        <h6>Stool test</h6> <br>
                        <table class="col-md-12 table table-striped table-borderless">
                            <tr>
                                <th rowspan="2">Macro</th>
                                <th colspan="4">Micro</th>
                            </tr>
                            <tr> 
                                <th>Blood</th>
                                <th>Pus</th>
                                <th>Ova</th>
                                <th>Other</th>
                            </tr>
                            <tr>  
                                <td>{{$test->results->object->macro}}</td> 
                                <td>{{$test->results->object->blood}}</td> 
                                <td>{{$test->results->object->pus}}</td> 
                                <td>{{$test->results->object->ova}}</td> 
                                <td>{{$test->results->object->other}}</td> 
                            </tr>
                        </table> <br>
                        @elseif($test->results->type == "fbp")  
                        <h6>FBP</h6> <br>
                        <table class="col-md-12 table table-striped table-borderless"> 
                            <tr>
                                <th>wbc</th>
                                <th>gra</th>
                                <th>Hb</th> 
                            </tr>
                            <tr>
                                <td>{{$test->results->object->wbc}}</td> 
                                <td>{{$test->results->object->gra}}</td> 
                                <td>{{$test->results->object->Hb}}</td>  
                            </tr>
                        </table> <br>
                        @elseif($test->results->type == "default")
                        <h6>{{$test->lab_test->name}}</h6> <br>
                        <table class="col-md-12 table table-striped table-borderless"> 
                            <tr>
                                <th>result</th> 
                            </tr>
                            <tr>
                                <td>{{$test->result}}</td>  
                            </tr>
                        </table> <br>
                        @endif
                    @endforeach   
                @endif        
            </div>
        </div> 
    </div>
</div>