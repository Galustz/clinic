@extends('layouts.admin')
@section('content')
@php
    use App\PatientType;
    $patienttypes = PatientType::where('trashed','false')->get();
    $i=1;
@endphp
<div class="container">
    <div class="card card-body">
        <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/reception/patienttypes/new')}}">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="row form-group">
                    <input type="text" name="id" id="service_id" hidden>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                <input type="text" placeholder="Name" name="name" id="service" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                <input type="number" name="price" placeholder="Price" id="price" class="form-control">
                                <span style="padding-top:10px;padding-left:10px;">. Tsh</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table id="patient-que-table" class="table datatable table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>Number</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th></th>
                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($patienttypes as $patienttype)
                        <tr class="table-row">
                            <td>{{$i}}</td>
                            <td>{{$patienttype->name}}</td>
                            <td>{{$patienttype->price}}</td>
                            <td><a class="btn btn-primary" href="{{url('admin/reception/patienttypes/edit',$patienttype->id)}}">Edit</a></td>
                            <td>
                                <a href="{{url('admin/reception/patienttypes/delete',$patienttype->id)}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE                  -->
        </div>
    </div>
</div>
@endsection