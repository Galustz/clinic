 {{-- @if(session('result')) {{session('result')}} @endif --}}

@php
    use App\Symptom;
    use App\LabTest;
    use App\Medicine;

    $symptoms = Symptom::all();
    $labtests = LabTest::where('trashed','false')->get();
    $medicines = Medicine::where('trashed','false')->get();
@endphp

    {{ csrf_field() }} 
    <br>
        <div class="row">            
            <div class="form-group col-md-6">
                <label for="symptoms">Symptoms/Cc:</label>
                {{-- <input type="text"  name="symptoms" id="symptoms" /> --}}

                {{-- <select name="select" id="symptomsselect" class="form-control"  multiple data-role="tagsinput">
                </select> --}}
                <select class="select2tags" name="symptoms[]" id="symptoms" multiple="multiple">
                    @foreach($symptoms as $symptom)
                    <option value="{{$symptom->name}}">{{$symptom->name}}</option>
                    @endforeach
                </select>
            </div>  
            <div class="form-group col-md-6">
                <label for="history">History:</label>
                <textarea class="form-control" placeholder="Patient History..." name="history" id="history" rows="3"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-5">
                <label for="mah">Medicine at home:</label>
                <div class="row">
                    <div class="col-md-8">
                        <textarea class="form-control" placeholder="" name="medicine_at_home" id="mah" rows="3"></textarea>
                    </div>
                    <div class="col-md-4">
                        <div class="form-check">
                            <div class="radio">
                                <label for="radio1" class="form-check-label ">
                                    <input type="radio" id="radio1" name="dose_status" value="on_progress" class="form-check-input">On Progress
                                </label>
                            </div>
                            <div class="radio">
                                <label for="radio2" class="form-check-label ">
                                    <input type="radio" id="radio2" name="dose_status" value="finished" class="form-check-input">Finished
                                </label>
                            </div>
                            <div class="radio">
                                <label for="radio3" class="form-check-label ">
                                    <input type="radio" id="radio3" name="dose_status" value="none" class="form-check-input">None
                                </label>
                            </div>
                            <div class="radio">
                                <label for="radio4" class="form-check-label">
                                    <input type="radio" id="radio4" name="dose_status" value="stopped" class="form-check-input">Stopped
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-7">
                <label for="examinations">Examinations</label>
                <div id="examinations" class="row">
                    <div class="form-group col-md-4">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="ge">G/E:</label>
                            </div>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="ge" id="ge" value="Stable">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="ge">RES:</label>
                            </div>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="res" id="res" value="NAD">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="res">cns:</label>
                            </div>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="cns" id="cns" value="NAD">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="cvs">CVS:</label>
                            </div>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="cvs" id="cvs" value="NAD">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="pa">PA:</label>
                            </div>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="pa" id="pa" value="NAD">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="other">Other:</label>
                            </div>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="other" id="other" value="NAD">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('doctor.components.lab-test-form')
    </div>
    <div class="card card-body">
        @include('doctor.components.medicine-form')
    </div>

    <div class="card card-body">
        <div class="row"> 
                <div class="form-group col-md-12">
                        <label for="symptoms">Diagnosis:</label>
                        @php
                            $diagnoses = \App\Diagnosis::all();
                        @endphp
                        <select class="select2" name="diagnosis[]" id="diagnosis" multiple="multiple">
                            @foreach($diagnoses as $diagnosis)
                                <option value="{{$diagnosis->id}}">{{$diagnosis->name}}</option>
                            @endforeach
                        </select>
                        @if (!empty($isHistory))
                            <button type="submit" class="btn btn-primary" style="margin-top:10px;">Submit</button>
                        @endif
                    </div>
        </div> 
    </div>
    <div class="card card-body">
        <div class="row">
            <div class="col-lg-6"> 
                <textarea class="form-control" placeholder="Comment from doctor..." name="comment_from_doctor" id="cfd" rows="3"></textarea>
            </div>
            <div class="col-lg-3">                
                    <label for="symptoms">Follow up date:</label>
                    <input value="{{Carbon\Carbon::now()->addDays(7)->format('Y-m-d')}}" type="date" id="adult-dob" name="follow_up_date" class="form-control" required>
            </div>
            <div class="col-lg-3">                
                    <label for="symptoms">Take Photo:</label><br>
                    <button class="btn btn-warning" type="button">Camera</button>
              </div>
        </div> 
    </div>
    <input type="hidden" name="patient_que_id" value="{{$pts->id}}"/>
    <button type="submit" id="submit_button" class="btn btn-primary">Submit</button>
    <button type="reset" class="btn btn-danger">Reset</button>
</form>
@include('doctor.familyhelp.index')