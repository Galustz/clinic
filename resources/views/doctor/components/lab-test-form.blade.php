<div class="row">            
        <div class="form-group col-md-6">
            <label for="symptoms">Lab tests:</label>
            {{-- <input type="text"  name="symptoms" id="symptoms" /> --}}

            {{-- <select name="select" id="symptomsselect" class="form-control"  multiple data-role="tagsinput">
            </select> --}}
            <select class="select2" name="lab_tests[]" id="lab_tests" multiple="multiple">
                @foreach($labtests as $labtest)
                    <option value="{{$labtest->id}}">{{$labtest->name}} test</option>
                @endforeach
            </select>
            @if (!empty($isHistory))
                <button type="submit" class="btn btn-primary" style="margin-top:10px;">Give tests</button>
            @endif
        </div>
        <div class="form-group col-md-6">
            <label for="nfd">Note to the lab:</label>
            <textarea class="form-control" name="note_from_doctor" id="nfd" rows="3"></textarea> 
        </div>
</div>