@php
    $pts = App\PatientQueueTemporary::find($id);
    $vital = App\Vital::where('patient_reg_no',$pts->patient_reg_no)->where('visit_no',$pts->visit_no)->first();
    $patient = App\Patient::where('reg_no',$pts->patient_reg_no)->first();
    $dob = $patient->DOB;
    $date = date_create_from_format('Y-m-d', $dob);       

    $reference = new DateTime;

    $diff = $reference->diff($date);

    $years = $diff->y;
    $months = $diff->m;
    $days = $diff->d;
@endphp  
<form enctype="multipart/form-data" method="post" action="{{ url('doctor/patient/edit') }}" id="register-adult-patient" style="padding-bottom:-20px;">
    {{ csrf_field() }} 
    <div class="row">
        <div class="col-lg-12"> 
                    <input type="hidden" name="url" value="{{url()->full()}}">
                    <input type="text" name="type" id="type" value="adult" hidden>
                    <div class="card-body card-block" style="margin-bottom:-70px;">
                    <div class="row form-group">  
                    <div class="row form-group">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dob" class=" form-control-label">Date of Birth</label>
                                <div class="input-group"> 
                                    <input type="date" id="dob" name="dob" class="form-control" required value="{{$patient->DOB}}">
                                    @php
                                        xdebug_break();
                                    @endphp
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="age-year" class=" form-control-label">Age</label>
                            <div class="row">
                                <div class="col-md-4">
                                     <div class="form-group">
                                        <div class="input-group"> 
                                            <input type="text" id="age-year" name="age-year" class="form-control" value="{{$years}}">
                                        </div>
                                        <small class="form-text text-muted">Year</small>
                                    </div>
                                </div>                                   
                                <div class="col-md-4">
                                    <div class="form-group"> 
                                        <div class="input-group"> 
                                            <input type="text" id="age-month" name="age-month" class="form-control" value="{{$months}}">
                                        </div>
                                        <small class="form-text text-muted">Month</small>
                                    </div>
                                </div> 
                                <div class="col-md-4">
                                    <div class="form-group"> 
                                        <div class="input-group"> 
                                            <input type="text" id="age-day" name="age-day" class="form-control" value="{{$days}}">
                                        </div>
                                        <small class="form-text text-muted">Day</small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-1">
                                        <label for="weight" class=" form-control-label">Weight</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="weight" id="weight" class="form-control" value="{{$vital->weight}}">
                                        </div>
                                    </div>
                                </div>  
                            </div>                            
                        </div>  
                        <div class="col-md-1">
                                        <label for="weight" class=" form-control-label">Height</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="height" class="form-control" value="{{$vital->height}}">
                                        </div>
                                    </div>
                                </div>  
                            </div>                            
                        </div>  
                        <div class="col-md-1">
                                        <label for="weight" class=" form-control-label">Temp</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="temperature" class="form-control" value="{{$vital->temperature}}">
                                        </div>
                                    </div>
                                </div>  
                            </div>                            
                        </div>  
                        <div class="col-md-1">
                                        <label for="weight" class=" form-control-label">BP</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="blood_pressure" class="form-control" value="{{$vital->blood_pressure}}">
                                        </div>
                                    </div>
                                </div>  
                            </div>                            
                        </div>  
                        <div class="col-md-2">
                             <label for="weight" class=" form-control-label">PR</label>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="pulse_rate"  class="form-control" value="{{$vital->pulse_rate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button> 
                                </div>
                            </div>                            
                        </div>  
                        <input type="hidden" name="id" value="{{$id}}">
                         
                    </div> 
                     @php
                        $index = 2;
                    @endphp
                </div> 
        </div> 
    </div> 
    </div>
</form> 