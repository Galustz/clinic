@php
use App\PatientQueueTemporary;
use App\Patient;
$appointments = PatientQueueTemporary::whereIn('status',['waiting','out','entered'])
                                //   ->where('created_at',Carbon\Carbon::today())                                  
                                  ->whereDoctor(Auth::id())
                                //   ->orderBy('service_mode','asc')
                                  ->orderBy('created_at','desc')
                                  ->get();
$results = App\BillingList::whereIn('status',['done','Left','Seen'])->orderBy('updated_at','asc')->get();
// dd($results);
@endphp
  <!-- DATA TABLE-->
  <section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-doc-que" class="mytable datatable">
                                <thead>
                                    <tr>
                                        {{-- <th>Que. #</th> --}}
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Time/<br>Appointment</th>
                                        <th>Status</th>
                                        <th>Reg #</th>
                                        <th>name</th>
                                        <th>Type</th>
                                        <th>Service Mode</th>
                                        {{-- <th>Waiting Time</th> --}}
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($appointments as $pt)
                                        @php
                                            $patient = Patient::find($pt->patient_reg_no);
                                        @endphp
                                    <tr>
                                        {{-- <td>{{$pt->patient_que_no}}</td> --}}
                                        <th>{{$loop->iteration}}</th>
                                        <td>{{$pt->created_at->format('d/m/Y')}}</td> 
                                        <td>{{$pt->created_at->format('h:i a')}}</td> 
                                        <td>{{$pt->status}}</td>
                                        <td>{{$pt->patient_reg_no}}</td>
                                        <td>{{$patient->name}}</td>
                                        <td>{{$patient->patient_type}}</td>
                                        <td>{{$pt->serviceMode->name}}</td>
                                        <td><a href="{{route('patient-details',$pt->id)}}" class="btn btn-primary">Attend to</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->