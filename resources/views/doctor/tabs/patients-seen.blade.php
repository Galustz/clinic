@php
use App\PatientQueueTemporary;
use App\Patient;
use App\PatientVisit;
$patients = PatientQueueTemporary::where('status','=','attended')
                    ->where('created_at','>=',Carbon\Carbon::today())
                    ->orderBy('created_at','desc')->get();
@endphp
  <!-- DATA TABLE-->
  <section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-doc-que" class="mytable datatable">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reg. Number</th>
                                        <th>Que. #</th>
                                        <th>name</th>
                                        <th>Type</th>
                                        <th>Service Mode</th>
                                        {{-- <th>Waiting Time</th> --}}
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($patients as $pt)
                                        @php
                                            $patient = Patient::find($pt->patient_reg_no);
                                            $pv = PatientVisit::where('patient_registration_no','=',$pt->patient_reg_no)->where('visit_no','=',$pt->visit_no)->first();
                                            
                                        @endphp
                                    <tr>
                                        <td>{{$pt->created_at->format('d/m/Y')}}</td>
                                        <td>{{$pt->patient_reg_no}}</td>
                                        <td>{{$pt->patient_que_no}}</td>
                                        <td>{{$patient->name}}</td>
                                        <td>{{$patient->type}}</td>
                                        <td>{{$pt->service_mode}}</td>
                                        <td><a href="{{url('doctor/history?id='.$patient->reg_no.'&url='.url()->full())}}" class="btn btn-default">View</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->