@php
    use App\PatientQueueTemporary;
    use App\Patient;
    use Carbon\Carbon;
    use App\Vital;
    use App\KnownPatientOf;
    use App\FamilyHelp;

    $pts = PatientQueueTemporary::find($id);
    $patient = Patient::find($pts->patient_reg_no);
    $vitals = Vital::where('patient_reg_no','=',$pts->patient_reg_no)->where('visit_no','=',$pts->visit_no)->first();
    if(!$vitals)
        $vitals = Vital::create(["patient_reg_no"=>$pts->patient_reg_no,"visit_no"=>$pts->visit_no]);
    $pvn = $pts->visit_no;
    $prn = $pts->patient_reg_no;
    if($pvn>1)
        $ppvn = $pvn-1;
    else
        $ppvn = $pvn;

    $disable_history = $ppvn==1?'disabled':'';
    $date = date_create_from_format('Y-m-d', $patient->DOB);   
    $reference = Carbon::now();
    $diff = $reference->diff($date);
    $years = $diff->y;
    $kpf = KnownPatientOf::where('patient_registration_no',$pts->patient_reg_no)->first(); 

    $family_helps = FamilyHelp::where('patient_registration_no','=',$pts->patient_reg_no)->where('visit_no','=',$pts->visit_no)->get();
@endphp

@extends('home')
@section('section')
<div class="container" style="padding:10px;">
    <a onclick="window.history.back()" class="btn btn-link">Back</a>
    <div class="card card-body">
        <h4 class="">Patient Details</h4>
        <hr>
        <div class="row">
            <div class="col-lg-1">
                Date: <br> <strong>{{$pts->created_at->format('d/m/y')}}</strong> 
            </div>
            <div class="col-lg-1">
                PQ No:<br> <strong>{{$pts->patient_que_no}}</strong>
            </div>
            <div class="col-lg-2">
                Patient Type:<br> <strong>{{$patient->patient_type}}</strong>
            </div>
            <div class="col-lg-2">
                Name:<br> <strong>{{$patient->name}}</strong>
            </div> 
            <div class="col-lg-1">
                Gender:<br> <strong>{{$patient->gender}}</strong>
            </div>
            {{-- <div class="col-lg-2">
                Occupation:<br> <strong>{{$patient->Occupation}}</strong>
            </div> --}}  
            {{-- <div class="col-lg-1">
                Height:<br> <strong>{{$vitals->height}}</strong>
            </div> 
            <div class="col-lg-1">
                Temp:<br> <strong>{{$vitals->temperature}}</strong>
            </div>  --}}
        </div>
        <div class="row">
            @include('doctor.edit-patient')
        </div>
    </div>
    <div class="card card-body">
        <div class="row" style="margin-bottom:20px;">
            <div class="col-md-3"> 
                <h5>Family help</h5>
            </div>
            <div class="col-md-9 text-right">
                <a href="{{url('doctor/familyhelp/add?prn='.$prn.'&pvn='.$pvn)}}" class="btn btn-primary btn-sm text-right">Add</a>
            </div>
        </div>
        <table class="mytable">
            <tr>
                <th>Name</th>
                <th>Relationship</th>
                <th></th>
            </tr>
            @foreach ($family_helps as $fh)
                <tr>
                    <td>{{$fh->name}}</td>
                    <td>{{$fh->relationship}}</td>
                    <td>
                        <a href="{{url('doctor/familyhelp/view?prn='.$prn.'&pvn='.$pvn.'&fhid='.$fh->id.'&url='.url()->full())}}" class="btn btn-primary btn-sm">view</a>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
    <form action="{{route('patient-diagnosis')}}" method="POST">
    <div class="card card-body">
        <div class="row">
            <div class="col-lg-1">Visit No: <strong>{{$pts->visit_no}}</strong></div>
            <div class="col-lg-4">Known patient of:<input class=" form-control" placeholder="Known patient of" type="text" name="known_patient_of" value="{{$kpf?$kpf->content:''}}"></div>
            <div class="col-lg-2"><a href="">Child growth development charts</a></div>
            <div class="col-lg-2"><a href="">Child immunisation schedule</a></div> 
            <a style="height:40px !important;" href="{{url('doctor/history?id='.$patient->reg_no.'&url='.url()->full())}}" {{$disable_history}} class="col-lg-1 btn btn-primary">History</a>           
            {{-- <button style="height:40px !important;margin-left:10px;" type="button" class="col-lg-1 btn btn-secondary mb-1" data-toggle="modal" data-target="#mediumModal">
                Family Help
            </button> --}}
        </div>    
    @include('doctor.patient-diagnosis')
</div>

@endsection