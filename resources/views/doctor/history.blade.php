@php
    use App\PatientQueueTemporary;
    use App\PatientVisit;
    use App\Patient;
    use Carbon\Carbon;
    use App\Vital;
    use App\KnownPatientOf;
    use App\Symptom;
    use App\LabTest;
    use App\Medicine;
    use App\SymptomRecord;
    use App\History;
    use App\LabTestRecord;
    use App\MedicineRecord;
    use App\FollowUpDate;
    use App\HomeMedicine;
    use App\CommentFromDoctor;
    use App\Examination;
    use App\BillingList;
    use App\NoteFromDoctor;
    use App\FamilyHelp;


    $visits = PatientVisit::where('patient_registration_no',$id)->orderBy('visit_no','desc')->get();
    if(empty($visit_no)){        
        $visit_no = $visits[0]->visit_no;
    }
    
    $pts = PatientQueueTemporary::where('patient_reg_no',$id)->where('visit_no',$visit_no)->first();
    $patient = Patient::find($pts->patient_reg_no);
    $vitals = Vital::where('patient_reg_no','=',$pts->patient_reg_no)->where('visit_no','=',$pts->visit_no)->first();
    $bl = BillingList::where('patient_registration_no','=',$pts->patient_reg_no)->where('visit_no','=',$pts->visit_no)->where('family_help_id',0)->first(); 
    $pvn = $pts->visit_no; 
    $prn = $pts->patient_reg_no;
    $registration_no = $id;
    if($pvn>1)
        $ppvn = $pvn-1;
    else
        $ppvn = $pvn;
    $date = date_create_from_format('Y-m-d', $patient->DOB);   
    $reference = Carbon::now();
    $diff = $reference->diff($date);
    $years = $diff->y;
    $months = $diff->m;
    $days = $diff->d;
    $kpf = KnownPatientOf::where('patient_registration_no',$pts->patient_reg_no)->first(); 
    $symptoms = Symptom::all();
    $labtests = LabTest::all();
    $medicines = Medicine::all();

    $symptomrecs = SymptomRecord::where('visit_no',$visit_no)->where('patient_registration_no',$id)->where('family_help_id',0)->get();
    $labtestrecs = LabTestRecord::where('visit_no',$visit_no)->where('patient_registration_no',$id)->where('family_help_id',0)->get();

    $diagnosisrecs = \App\DiagnosisRecord::where('visit_no',$visit_no)->where('patient_registration_no',$id)->get();
    $history = History::where('visit_no',$visit_no)->where('patient_registration_no',$id)->first();
    $fud = FollowUpDate::where('visit_no',$visit_no)->where('patient_registration_no',$id)->first();
    $mah = HomeMedicine::where('visit_no',$visit_no)->where('patient_registration_no',$id)->first();
    $cfd = CommentFromDoctor::where('visit_no',$visit_no)->where('patient_registration_no',$id)->first();
    $examinations = Examination::where('visit_no',$visit_no)->where('patient_registration_no',$id)->first();
    $medrecords = MedicineRecord::where('visit_no',$visit_no)->where('patient_registration_no',$id)->where('family_help_id',0)->get(); 
    $no = 1; 
    $isHistory = true;    
    $family_helps = FamilyHelp::where('patient_registration_no','=',$pts->patient_reg_no)->where('visit_no','=',$pts->visit_no)->get();

@endphp

@extends('home')
@section('section')
<div style="padding:40px;">
    <a href="{{$url}}" class="btn btn-link">Back</a>
    <div class="card card-body">
        <div class="row">
            <form class="col-lg-12" method="get" action="{{url('/doctor/history',$id)}}" method="post">
                {{csrf_field()}}
                <div class="form-group col-lg-12">
                    <div class="row">
                        <input type="hidden" name="id" value="{{$id}}">
                        <input type="hidden" name="url" value="{{$url}}">
                        <div class="col-md-1">Visit No:</div>
                        <div class="col-md-3"> <select class="form-control" name="visit_no" id="vn">
                        @foreach($visits as $visit)
                            <option value="{{$visit->visit_no}}" {{$visit_no == $visit->visit_no?'selected':''}}>{{$visit->visit_no}}</option>
                        @endforeach
                        </select></div>
                        <div class="col-md-2">
                            <button class="btn btn-primary">Select</button>
                        </div>
                    </div>                          
                </div>
            </form>
        </div>
        <hr>
        <h4 class="">Patient Details</h4>
        <hr>
        <div class="row">
            <div class="col-lg-1">
                Date: <br> <strong>{{$pts->created_at->format('d/m/Y')}}</strong> 
            </div>
            <div class="col-lg-1">
                PQ No:<br> <strong>{{$pts->patient_que_no}}</strong>
            </div>
            <div class="col-lg-2">
                Patient Type:<br> <strong>{{$patient->patient_type}}</strong>
            </div>
            <div class="col-lg-2">
                Name:<br> <strong>{{$patient->name}}</strong>
            </div>
            <div class="col-lg-2">
                Age:<br> <strong>{{$years.'y '.$months.'m '.$days.'d'}}</strong>
            </div>
            <div class="col-lg-1">
                Gender:<br> <strong>{{$patient->gender}}</strong>
            </div>
            <div class="col-lg-1">
                Temp:<br> <strong>{{$vitals->temperature}}</strong>
            </div>
            <div class="col-lg-1">
                Height:<br> <strong>{{$vitals->height}}</strong>
            </div>
            {{-- <div class="col-lg-2">
                Occupation:<br> <strong>{{$patient->Occupation}}</strong>
            </div> --}}
        </div>
        <br>
        <h4 class="">Vital Signs</h4>
        <hr>
        @include('doctor.edit-patient') 
    </div>
    <div class="card card-body">
       <div class="row" style="margin-bottom:20px;">
           <div class="col-md-3"> 
               <h5>Family help</h5>
           </div>
           <div class="col-md-9 text-right">
               <a href="{{url('doctor/familyhelp/add?prn='.$prn.'&pvn='.$pvn)}}" class="btn btn-primary btn-sm text-right">Add</a>
           </div>
       </div>
       <table class="mytable">
           <tr>
               <th>Name</th>
               <th>Relationship</th>
               <th></th>
           </tr>
           @foreach ($family_helps as $fh)
               <tr>
                   <td>{{$fh->name}}</td>
                   <td>{{$fh->relationship}}</td>
                   <td>
                       <a href="{{url('doctor/familyhelp/view?prn='.$prn.'&pvn='.$pvn.'&fhid='.$fh->id.'&url2='.url()->full())}}" class="btn btn-primary btn-sm">view</a>
                   </td>
               </tr>
           @endforeach
   
       </table>
   </div>
    <form action="{{url('doctor/updateVisit')}}" method="GET"></form>
    <div class="card card-body">
        <div class="row">
            <div class="col-lg-4">Visit No: <strong>{{$pts->visit_no}}</strong></div>
            <input class="col-lg-4 form-control" placeholder="Known patient of" type="text" name="known_patient_of" value="{{$kpf?$kpf->content:''}}">
            
        </div>   <br>
        <div class="row">  
            <div class="col-lg-4"><a href="">Child growth development charts</a></div>
            <div class="col-lg-4"><a href="">Child immunisation schedule</a></div> 
        </div>     
    </div>
 {{-- @if(session('result')) {{session('result')}} @endif --}}
 
 <div class="card card-body">
     <div class="row">            
         <div class="form-group col-md-12">
             <label for="symptoms">Symptoms/Cc:</label>
             {{-- <input type="text"  name="symptoms" id="symptoms" /> --}}

             {{-- <select name="select" id="symptomsselect" class="form-control"  multiple data-role="tagsinput">
             </select> --}}
             <select class="select2tags" name="symptoms[]" id="symptoms" multiple="multiple">
                 @foreach($symptoms as $symptom)
                    @php
                        $selected = $symptomrecs->contains('symptom_id',$symptom->id)?'selected':'';                        
                    @endphp
                    <option value="{{$symptom->name}}" {{$selected}}>{{$symptom->name}}</option>
                 @endforeach
             </select>
         </div>
     </div>
     <div class="row"> 
         <div class="form-group col-md-12">
             <label for="history">History:</label>
             <textarea class="form-control" value="" placeholder="Patient History..." name="history" id="history" rows="3">{{!empty($history)?$history->content:''}}</textarea>
         </div>
     </div>
     <div class="row">
         <div class="form-group col-md-5">
             <label for="mah">Medicine at home:</label>
             <div class="row">
                 <div class="col-md-8">
                     <textarea class="form-control" placeholder="" name="medicine_at_home" id="mah" rows="3">{{!empty($mah->content)?$mah->content:''}}</textarea>
                 </div>
                 <div class="col-md-4">
                     <div class="form-check">
                         <div class="radio">
                             <label for="radio1" class="form-check-label ">
                                 <input type="radio" id="radio1" name="dose_status" value="on_progress" class="form-check-input">On Progress
                             </label>
                         </div>
                         <div class="radio">
                             <label for="radio2" class="form-check-label ">
                                 <input type="radio" id="radio2" name="dose_status" value="finished" class="form-check-input">Finished
                             </label>
                         </div>
                         <div class="radio">
                             <label for="radio3" class="form-check-label ">
                                 <input type="radio" id="radio3" name="dose_status" value="none" class="form-check-input">None
                             </label>
                         </div>
                         <div class="radio">
                             <label for="radio4" class="form-check-label">
                                 <input type="radio" id="radio4" name="dose_status" value="stopped" class="form-check-input">Stopped
                             </label>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="form-group col-md-7">
             <label for="examinations">Examinations</label>
             <div id="examinations" class="row">
                 <div class="form-group col-md-4">
                     <div class="row">
                         <div class="col-sm-2">
                             <label for="ge">G/E:</label>
                         </div>
                         <div class="col-sm-10">
                             <input class="form-control" type="text" name="ge" id="ge" value="{{!empty($examinations->ge)?$examinations->ge:''}}">
                         </div>
                     </div>
                 </div>
                 <div class="form-group col-md-4">
                     <div class="row">
                         <div class="col-sm-2">
                             <label for="ge">RES:</label>
                         </div>
                         <div class="col-sm-10">
                             <input class="form-control" type="text" name="res" id="res" value="NAD">
                         </div>
                     </div>
                 </div>
                 <div class="form-group col-md-4">
                     <div class="row">
                         <div class="col-sm-2">
                             <label for="res">cns:</label>
                         </div>
                         <div class="col-sm-10">
                             <input class="form-control" type="text" name="cns" id="cns" value="{{!empty($examinations->cns)?$examinations->cns:''}}">
                         </div>
                     </div>
                 </div>
                 <div class="form-group col-md-4">
                     <div class="row">
                         <div class="col-sm-2">
                             <label for="cvs">CVS:</label>
                         </div>
                         <div class="col-sm-10">
                             <input class="form-control" type="text" name="cvs" id="cvs" value="{{!empty($examinations->cvs)?$examinations->cvs:''}}">
                         </div>
                     </div>
                 </div>
                 <div class="form-group col-md-4">
                     <div class="row">
                         <div class="col-sm-2">
                             <label for="pa">PA:</label>
                         </div>
                         <div class="col-sm-10">
                             <input class="form-control" type="text" name="pa" id="pa" value="{{!empty($examinations->pa)?$examinations->pa:''}}">
                         </div>
                     </div>
                 </div>
                 <div class="form-group col-md-4">
                     <div class="row">
                         <div class="col-sm-2">
                             <label for="other">Other:</label>
                         </div>
                         <div class="col-sm-10">
                             <input class="form-control" type="text" name="other" id="other" value="{{!empty($examinations->other)?$examinations->other:''}}">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div> 
 
 <div class="card card-body">
        <div class="row"> 
                <div class="form-group col-md-12">
                        <label for="symptoms">Diagnosis:</label>
                        @php
                            $diagnoses = \App\Diagnosis::all();
                        @endphp

                        @if (!empty($isHistory))
                        <form action="{{url('doctor/add-diagnosis')}}" method="POST">
                            {{ csrf_field() }}
                            <select class="select2" name="diagnosis[]" id="diagnosis" multiple="multiple">
                                @foreach($diagnoses as $diagnosis)
                                    <option value="{{$diagnosis->id}}"  {{$diagnosisrecs->contains('diagnosis_id',$diagnosis->id)?'selected':''}}>{{$diagnosis->name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="prn" value="{{$registration_no}}">
                            <input type="hidden" name="pvn" value="{{$visit_no}}">
                            <button type="submit" class="btn btn-primary" style="margin-top:10px;">Submit</button>
                        </form>
                        @else
                        <select class="select2" name="diagnosis[]" id="diagnosis" multiple="multiple">
                            @foreach($diagnoses as $diagnosis)
                                <option value="{{$diagnosis->id}}"  {{$diagnosisrecs->contains('diagnosis_id',$diagnosis->id)?'selected':''}}>{{$diagnosis->name}}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
        </div> 
    </div>
 <div class="card card-body">
     @if (!empty($labtestrecs)&&!empty($bl))
     <div class="row">
        <div class="col-lg-12">
            <p><strong>Previous Tests:</strong></p>
            <p>
                @forelse ($labtestrecs as $labtestrec)
                    {{$labtestrec->lab_test->name." test,"}}
                @empty 
                @endforelse
                <a href="{{url('doctor/results',!empty($bl)?$bl->id:0)}}">View results</a></p>
        </div>  
    </div> 
    <br>   
     @endif
     <form action="{{url('doctor/giveLabTest')}}" method="post">
        {{csrf_field()}}
     @include('doctor.components.lab-test-form')  
     <input type="hidden" name="url" value="{{$url}}">
     <input type="hidden" name="prn" value="{{$registration_no}}">
     <input type="hidden" name="pvn" value="{{$visit_no}}">
    </form>     
 </div>
 <div class="card card-body">
 <form action="{{url('doctor/prescribeMedicine')}}" method="POST">
    {{ csrf_field() }}
    @include('doctor.components.medicine-form')
    <input type="hidden" name="url" value="{{$url}}">
    <input type="hidden" name="prn" value="{{$registration_no}}">
    <input type="hidden" name="pvn" value="{{$visit_no}}">
    <div class="row">
        <div class="col-md-12">
        <button class="btn btn-primary">Prescribe medicines</button></div>
    </div>    
 </form>
    <br><br> 
 
 @if(!$medrecords->isEmpty())     
    <div class="row">
        <div class="col-md-12">
            <p><strong>Previously prescribed medicines</strong></p>
        </div>
        <div class="col-md-10">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table id="patient-que-table" class="mytable">
                    <thead>
                        <tr>
                            {{-- <th>Number</th> --}}
                            <th>No</th>   
                            <th>Medicine Name</th>  
                            <th>Quantity</th>  
                            <th>Status</th> 
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($medrecords as $medrecord)    
                          <tr>                                          
                              <td>{{$no}}</td>
                              <td>{{$medrecord->medicine->name}}</td>
                              <td>{{' '. $medrecord->quantity.' X'.$medrecord->perday.' X '.$medrecord->days}}</td>
                              <td>{{$medrecord->status}}</td>
                              @php $no++; @endphp   
                          </tr> 
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE                  -->
        </div>
    </div>
    @endif
</div>
 <div class="card card-body">
     <div class="row">
         <div class="col-lg-6"> 
             <textarea class="form-control" placeholder="Comment from doctor..." name="comment_from_doctor" id="cfd" rows="3">{{!empty($cfd)?$cfd->content:''}}</textarea>
         </div>
         <div class="col-lg-3">                
                 <label for="symptoms">Follow up date:</label>
                 <input value="{{!empty($fud)?$fud->content:Carbon::today()->subDays(14)->format('Y-m-d')}}" type="date" id="adult-dob" name="follow_up_date" class="form-control" required>
         </div> 
     </div> 
 </div>
<a href="{{!empty($bl)?url('doctor/closeFile',$bl->id):''}}" class="btn btn-primary">Close File</a> 
</div>

@endsection