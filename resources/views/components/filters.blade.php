@if (count($filters)>0)
    <div class="row" style="padding-right:16px;padding-left:14px">
        <div class="col-md-12">
            <form action="{{url()->current()}}" method="get">
                <div class="row">
                    @if (in_array('from_date',$filters))
                        <div class="form-group col-md-3">
                            <label for="">Date from</label>
                            <input type="date" name="from_date" value="{{request()->from_date}}" class="form-control" id="">
                        </div>                    
                    @endif
                    @if (in_array('to_date',$filters))
                        <div class="form-group col-md-3">
                            <label for="">Date to</label>
                            <input type="date" name="to_date" value="{{request()->to_date}}" class="form-control" id="">
                        </div>                    
                    @endif
                    @if (in_array('from_created',$filters))
                        <div class="form-group col-md-3">
                            <label for="">Created from</label>
                            <input type="date" name="from_created" value="{{request()->from_created}}" class="form-control" id="">
                        </div>                    
                    @endif
                    @if (in_array('to_created',$filters))
                        <div class="form-group col-md-3">
                            <label for="">Created to</label>
                            <input type="date" name="to_created" value="{{request()->to_created}}" class="form-control" id="">
                        </div>                    
                    @endif
                    @if (in_array('month',$filters))
                        @php
                            $selected_month = request()->has('month')?request()->month:\Carbon\Carbon::today()->month;
                        @endphp
                        <div class="form-group col-md-3">
                            <label for="">Month</label>
                            <select name="month" id="" class="form-control">
                                @foreach (\App\Filters\MonthFilter::getMonths() as $key=>$month)
                                    <option value="{{$key}}" {{$selected_month==$key?'selected':''}}>{{$month}}</option>
                                @endforeach
                            </select>
                        </div>                    
                    @endif
                    @if (in_array('year',$filters))
                        <div class="form-group col-md-3">
                            <label for="">Year</label>
                            <select name="year" id="" class="form-control">
                                @foreach (\App\Filters\YearFilter::getYears() as $year)
                                    <option value="{{$year}}" {{request()->year==$year?'selected':''}}>{{$year}}</option>
                                @endforeach
                            </select>
                        </div>                    
                    @endif
                    <div class="col-md-3" style="padding-top:33px">
                        <button class="btn btn-outline-success"><i class="fa fa-filter"></i> Filter</button>
                        <a class="btn btn-outline-primary" href="{{url()->current()}}"><i class="fa fa-clock"></i> Reset</a>
                    </div>
                </div>
            </form> 
            <hr>
        </div> 
    </div>    
@endif