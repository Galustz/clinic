<div class="modal fade" id="action-modal-{{$action}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            {{-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Please confirm action!</h4>
            </div> --}}
            <div class="modal-body">
                <p>Are you sure you want to {{strtolower($action)}} this item? </p>
                <p><strong>{{$message??null}}</strong></p>
                @if ($action == "reverse") 
                    <p>Enter a reverse message <span style="color:red">(*required)</span></p>
                    <textarea name="reverse-message" id="reverse-message" cols="30" rows="3" class="form-control" style="resize:none"></textarea>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="confirm-action-button-{{$action}}">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>