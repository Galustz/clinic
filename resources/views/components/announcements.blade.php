@php
    $announcements = \App\Announcement::getUserAnnouncements(); 
    $new_announcements_count = count(\App\Announcement::getUserNewAnnouncements());
@endphp 
<button id="message-notify" class="btn btn-primary"  data-toggle="modal" data-target="#announcement-modal">
    Announcements
    @if ($new_announcements_count>0)
        <span class="badge badge-danger" id="announcement-badge">{{$new_announcements_count}}</span>
    @endif
</button>
<div class="modal fade" id="announcement-modal" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content"> 
            <div class="modal-body" style="padding:0px">  
                <table class="table">
                    @foreach ($announcements as $announcement)
                        <tr class="{{$announcement->pivot->status=='created'?'table-success':''}}">
                            <td>{{$announcement->created_at}}</td>
                            <td>{{$announcement->description}}</td> 
                        </tr>                        
                    @endforeach
                </table>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" onclick="seen()">Seen</button> 
            </div>
        </div> 
    </div> 
</div>     
