@php
    $standard_operating_procedures = \App\StandardOperatingProcedure::whereIn("designation_id",[Auth::user()->role->id,0])->get();
    $download_standard_operating_procedure_url = url('standard-operating-procedures/download');
@endphp
<div class="container">  
    @include('components.table',[
        "columns"=> ["No","File","Description","Designation","Action"],
        "collection"=>$standard_operating_procedures,
        "data_columns"=>[
            "iteration",
            "file_name",
            "description",
            "designation_name",
            "buttons"=>[ 
                ["name"=>"Download","url"=>$download_standard_operating_procedure_url,"id"=>true,"color"=>"success"],
            ]
        ]
    ])
</div> 