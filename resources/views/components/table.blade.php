<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <table class="mytable">
            <thead>
            <tr>
                @foreach ($columns as $column)
                <th>{{$column}}</th>
                @endforeach
            </tr>                        
            </thead>
            <tbody>
            @foreach ($collection as $item)
                <tr>  
                    @foreach ($data_columns as $key=>$data_column)
                        @if ($data_column=="iteration")
                            <td>{{$loop->parent->iteration}}</td>
                        @else
                            @if (is_array($data_column))
                            <td> 
                                @foreach ($data_column as $button)
                                    @php
                                        if(array_key_exists('show_if',$button)){ 
                                                $item_key = $button['show_if'][0];

                                                $comparison_condition = $button['show_if'][1];
                                                $compared_value = $button['show_if'][2];
                                                $item_value = $item[$item_key];
                                                $eval_string = "return '$item_value' $comparison_condition '$compared_value';";
                                                $should_show = eval($eval_string); 
                                        }else{
                                            $should_show = true;
                                        }
                                    @endphp
                                     @if ($button['id'])
                                        @if (isset($button['should_confirm']))
                                            @if ($should_show)
                                                <button data-href="{{$button['url'].'/'.$item['id']}}"  onclick="openModal(this,'{{$button['action']}}')"  class="btn btn-sm btn-{{$button['color']}}">{{$button['name']}}</button>
                                                @include('components.confirm-action',["action"=>$button['action'],"message"=>$button["additional_confirm_message"]??null])                                                
                                            @endif
                                        @else
                                            @if ($should_show)
                                                <a href="{{$button['url'].'/'.$item['id']}}" class="btn btn-sm btn-{{$button['color']}}">{{$button['name']}}</a>
                                            @endif
                                        @endif
                                     @else
                                        @if ($should_show)
                                            <a href="{{$button['url']}}" class="btn btn-sm btn-{{$button['color']}}">{{$button['name']}}</a>                                            
                                        @endif
                                     @endif
                                 @endforeach 
                            </td>
                            @else
                                <td>{{$item[$data_column]}}</td>
                            @endif
                        @endif
                    @endforeach 
                </tr>                     
            @endforeach                         
            </tbody>  
        </table>                
    </div>
</div>