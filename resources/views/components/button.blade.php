@if ($url=="back")
    <a onclick="window.history.back()" class="btn btn-{{$color}}">{{$text}}</a>
@else
    <a href="{{$url}}" class="btn btn-{{$color}}">{{$text}}</a>
@endif