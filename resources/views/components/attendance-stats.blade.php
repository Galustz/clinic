@php
    $stats = $employee->attendanceStats($date);
@endphp

@if ($stats->present>0)
    <i class="fa fa-check present"></i> {{$stats->present}}
@endif
@if ($stats->absent>0)
    <i class="fa fa-times absent"></i> {{$stats->absent}}
@endif
@if ($stats->late>0)
    <i class="fa fa-clock late"></i> {{$stats->late}}
@endif 