@extends('components.add-form')

@section('form')
    <div class="col-md-12 form-group">
        <label for="">Brief</label>
        <textarea name="content" id="" cols="30" rows="1" class="editor">{!!$model->content??null!!}</textarea>
    </div>   
    <div class="col-md-6">
        <label for="">Date</label>
        <input type="date" name="date" class="form-control" value="{{$model->form_date??\Carbon\Carbon::today()->format('Y-m-d')}}">
    </div>
    <div class="col-md-6">
        <label for="">Time</label>
        <input type="time" name="time" class="form-control" value="{{$model->time??\Carbon\Carbon::now()->format('H:i:s')}}" required>
    </div>
    <div class="col-md-12">
        <label for="">Upload <small>{{$file_edit_message}}</small></label>
        <input type="file" name="document" class="form-control">
    </div>
 
    @if ($is_editing&&$model->hasFile())
        <div class="col-md-6 form-group" style="margin-top:10px;"> 
            <input type="checkbox" name="should_remove_file" value="true" id="" class=""> Remove {{$model->file_name}}
        </div>
    @endif
@endsection