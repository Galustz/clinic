@extends('layouts.admin')
@section('content')
<div class="container">
    @include('components.content-header',[
        'header'=>'Consultants',
        "button"=>[
            'name'=>'Add consultant',
            'url'=>$add_consultant_url,
            'color'=>'primary',
            ],
        ])

    @include('components.table',[
        "columns"=>['No','Name',"Email","Action"],
        "collection"=>$consultants,
        "data_columns"=>[
            "iteration",
            "name",
            "email", 
            "buttons"=>[
                ["url"=>$edit_url,"id"=>true,"name"=>"Edit","color"=>"primary"],
                ["url"=>$delete_url,"id"=>true,"name"=>"Delete","color"=>"danger","should_confirm"=>true,"action"=>"delete"],
            ]
        ]
    ])
</div>    
@endsection