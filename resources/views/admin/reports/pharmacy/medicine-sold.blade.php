@extends('layouts.admin') 
@section('content')
<div class="container card card-body" style="padding:20px;margin:25px; ">
    <div class="row" style="margin-bottom:40px">
        <div class="col-md-6 text-center text-capitalize bg-danger"  style="color:white; padding:10px;">
             Total Medicine Sold: {{ $medicines->count() }}
        </div>
        <div class="col-md-6 text-center text-capitalize bg-success" style="color:black; padding:10px;">
            Total Amount: TSH {{ $total_amount_sold }}
        </div>
    </div>
    <div class="row"> 
        <form action="{{url('admin/reports/medicine-sold/filter')}}" method="post">
            {{ csrf_field() }}
            <div class="row col-lg-12">
                <div class="col-lg-4 form-group">
                    <label for="">From:</label>
                    <input type="date" value="" name="from" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for="">To:</label>
                    <input type="date" value="" name="to" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for=""></label><br>
                    <button type="submit" class="form-control btn btn-md btn-primary" style="margin-top:10px;">Update</button>
                </div>
            </div>
        </form>
        <div class="table-responsive m-b-40">
            <table id="patient-list-table" class="mytable datatable">
                <thead>
                    <tr>
                      <th>no</th>
                      <th>name</th>
                      <th>Selling Price</th> 
                      <th>Selling Unit Name</th> 
                      <th>Purchasing Unit Name</th>
                      <th>Quantity/Unit</th>
                      <th>Unit Purchasing Price</th>
                      <th>Code</th>
                      <th>Code 2</th>
                      <th>Mgs/tab</th>
                      <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                      @foreach($medicines as $medicine)
                          <tr>
                              <td>{{$medicine->id}}</td>
                              <td>{{$medicine->name}}</td>
                              <td>{{$medicine->selling_price}}</td>
                              <td>{{$medicine->selling_unit_name}}</td> 
                              <td>{{$medicine->purchasing_unit_name}}</td> 
                              <td>{{$medicine->quantity_per_unit}}</td> 
                              <td>{{$medicine->unit_purchasing_price}}</td> 
                              <td>{{$medicine->code}}</td> 
                              <td>{{$medicine->code2}}</td> 
                              <td>{{$medicine->mgs_per_tablet}}</td> 
                              <td>{{$medicine->created_at->format('d/m/Y')}}</td>
                          </tr>
                      @endforeach
                </tbody>
            </table>
        </div>
    </div> 
</div>


@endsection