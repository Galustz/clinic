@php
    use App\RequestEquipment;
    $to =!empty($to)?$to:\Carbon\Carbon::today()->format('Y-m-d');
    $from = !empty($from)?$from:\Carbon\Carbon::today()->subDays(7)->format('Y-m-d'); 
     
    $no = 1;

    $requests = RequestEquipment::all();//whereBetween('created_at',[$from,$to])->orderBy('created_at','asc')->get();
@endphp
@extends('layouts.admin') 
@section('content')
<div class="container card card-body" style="padding:20px;margin:25px;">
    <div class="row"> 
        <form action="{{url('admin/reports/equipments/filter')}}" method="get">
            {{ csrf_field() }}
            <div class="row col-lg-12">
                <div class="col-lg-4 form-group">
                    <label for="">From:</label>
                    <input type="date" value="{{$from}}" name="from" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for="">To:</label>
                    <input type="date" value="{{$to}}" name="to" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for=""></label><br>
                    <button class="form-control btn btn-md btn-primary" style="margin-top:10px;">Update</button>
                </div>
            </div>
        </form>
        <div class="table-responsive m-b-40">
                <table id="patient-que-table" class="mytable datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Date</th> 
                                <th>Name</th>   
                                <th>Status</th>   
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($requests as $request)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$request->created_at->format('d/m/Y')}}</td>
                                    <td>{{$request->name}}</td>
                                    <td>{{$request->status}}</td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
        </div>
    </div> 
</div>


@endsection