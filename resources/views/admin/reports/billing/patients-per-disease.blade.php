@extends('layouts.admin') 
@section('content')
<div class="container card card-body" style="padding:20px;margin:25px;">
    <div class="row"> 
        <form action="{{url('admin/reports/filter-patients-per-disease')}}" method="post">
            {{ csrf_field() }}
            <div class="row col-lg-12">
                <div class="col-lg-4 form-group">
                    <label for="">From:</label>
                    <input type="date" value="" name="from" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for="">To:</label>
                    <input type="date" value="" name="to" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for=""></label><br>
                    <button class="form-control btn btn-md btn-primary" style="margin-top:10px;">Update</button>
                </div>
            </div>
        </form>
        <a href="{{ url('admin/reports/patients-per-disease') }}" class="btn btn-md btn-danger" style="margin-top:34px; height:40px;">Clear Filter</a>
        <div class="table-responsive m-b-40">
            <table id="patient-list-table" class="mytable datatable">
                <thead>
                    <tr>
                      <th>no</th>
                      <th>Disease</th>
                      <th>Patients #</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $count = 1;
                    @endphp
                    @php
                        // dd($diseases->count());
                    @endphp
                      @foreach($diseases as $disease)
                          <tr>
                              <td>{{ $count }}</td>
                              <td>{{$disease[0]->diagnosis->name}}</td>
                              <td>{{$disease->count()}}</td>
                          </tr>
                          @php
                              $count++;
                          @endphp
                      @endforeach
                </tbody>
            </table>
        </div>
    </div> 
</div>


@endsection