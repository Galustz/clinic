@php
    $to =!empty($to)?$to:\Carbon\Carbon::today()->format('Y-m-d');
    $from = !empty($from)?$from:\Carbon\Carbon::today()->subDays(7)->format('Y-m-d'); 
    
    $patients= \App\Patient::whereBetween('created_at',[$from,$to])->orderBy('created_at','asc')->get();
    $no = 1;
@endphp
@extends('layouts.admin') 
@section('content')
<div class="container card card-body" style="padding:20px;margin:25px;">
    <div class="row"> 
        <form action="{{url('admin/reports/new-patients/filter')}}" method="get">
            {{ csrf_field() }}
            <div class="row col-lg-12">
                <div class="col-lg-4 form-group">
                    <label for="">From:</label>
                    <input type="date" value="{{$from}}" name="from" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for="">To:</label>
                    <input type="date" value="{{$to}}" name="to" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for=""></label><br>
                    <button class="form-control btn btn-md btn-primary" style="margin-top:10px;">Update</button>
                </div>
            </div>
        </form>
        <div class="table-responsive m-b-40">
            <table id="patient-list-table" class="mytable datatable">
                <thead>
                    <tr>
                      <th>no</th>
                      <th>reg. #</th>
                      <th>name</th>
                      <th>mobile #</th> 
                      <th>type</th> 
                      <th>date</th>
                    </tr>
                </thead>
                <tbody>
                      @foreach($patients as $patient)
                          <tr>
                              <td>{{$no}}</td>
                              <td>{{$patient->reg_no}}</td>
                              <td>{{$patient->name}}</td>
                              <td>{{$patient->phone}}</td>
                              <td>{{$patient->type}}</td> 
                              <td>{{$patient->created_at->format('d/m/Y')}}</td>
                          </tr>
                          @php
                              $no++;
                          @endphp
                      @endforeach
                </tbody>
            </table>
        </div>
    </div> 
</div>
@endsection