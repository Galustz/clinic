@php
    $diagnoses = \App\Diagnosis::all();
@endphp
<div class="report-form-header text-center">
        <p>Taarifa ya Mwezi kutoka OPD</p>
    </div>
    <div class="report-form-subheader text-center row">
        <div class="col-md-4 text-center">
            Jina la kituo: <strong style="text-decoration:underline;">{{$kituo}}</strong>
        </div>
        <div class="col-md-4 text-center">
            Wilaya:  <strong style="text-decoration:underline;">{{$wilaya}}</strong>
        </div>
        <div class="col-md-4 text-center"> 
            Mkoa:  <strong style="text-decoration:underline;">{{$mkoa}}</strong>
        </div>
        <div class="col-md-6 text-center mt-2">
            Mwezi:   <strong style="text-decoration:underline;">{{$month_formatted}}</strong>
        </div>
        <div class="col-md-6 text-center mt-2">
            Mwaka:   <strong style="text-decoration:underline;">{{$year}}</strong>
        </div> 
    </div>
<table class="reporttable" style="width:90%; margin:auto;font-size:15px">
    <tr class="gray-row">
        <th rowspan="2">No</th>
        <th rowspan="2">Maelezo</th>
        <th colspan="3">Umri chini <br> chini ya mwezi 1</th>
        <th colspan="3">Umri <br> mwezi 1 <br> hadi umri <br>chini ya <br> mwaka 1</th>
        <th colspan="3">Umri <br> mwaka 1 <br> hadi umri <br>chini ya <br> miaka 5</th>
        <th colspan="3">Umri <br> miaka 5 <br> hadi umri <br>chini ya <br> miaka 60</th>
        <th colspan="3">Umri miaka 60 na kuendelea</th>
        <th colspan="3">Jumla <br>kuu</th> 
    </tr>
    <tr class="gray-row"> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
    </tr>
    <tr>
        <th>1</th>
        <th>Wagonjwa waliohudhuria kwa mara ya kwanza mwaka huo (*) kituo chochote nchini</th>
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr>
    <tr>
        <th>2</th>
        <th>Mahughurio ya kwanza/wagonjwa wapya (kwenye kituo husika kwa tatizo fulani la kiafya)</th>
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
    </tr>
    <tr>
        <th>3</th>
        <th>Mauhudhurio ya marudio</th>
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
    </tr>
    <tr>
        <th></th>
        <th>Mahudhurio ya OPD (2+3)</th>
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
    </tr>
    <tr class="gray-row">
        <td></td>
        <th>Diagnoses za OPD</th>
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td>
        <td></td>
        <td></td> 
    </tr>
    @foreach ($diagnoses as $diagnosis)
        @php
            $data = $diagnosis->getSummary();
        @endphp
        <tr>
            <th>{{$diagnosis->id+3}}</th>
            <th>{{$diagnosis->name}}</th>
            <td>{{$data->under_one_month->men}}</td>
            <td>{{$data->under_one_month->women}}</td>
            <td>{{$data->under_one_month->total}}</td>
            <td>{{$data->one_month_to_one_year->men}}</td>
            <td>{{$data->one_month_to_one_year->women}}</td>
            <td>{{$data->one_month_to_one_year->total}}</td>
            <td>{{$data->one_year_to_five_years->men}}</td>
            <td>{{$data->one_year_to_five_years->women}}</td>
            <td>{{$data->one_year_to_five_years->total}}</td>
            <td>{{$data->five_years_to_sixty_years->men}}</td>
            <td>{{$data->five_years_to_sixty_years->women}}</td>
            <td>{{$data->five_years_to_sixty_years->total}}</td>
            <td>{{$data->sixty_years_and_above->men}}</td>
            <td>{{$data->sixty_years_and_above->women}}</td>
            <td>{{$data->sixty_years_and_above->total}}</td>
            <td>{{$data->total->men}}</td>
            <td>{{$data->total->women}}</td>
            <td>{{$data->total->total}}</td>
        </tr>
    @endforeach
</table>

<div class="container p-5 m-1">
    <div class="row">
        <div class="col-md-4 mt-2">
            Jina la Mtayarishaji wa taarifa: <strong>{{Auth::user()->name}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Kada: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Wadhifa: ...........................................................
        </div>
        <div class="col-md-4 mt-2">
            Sahihi: ..........................................................
        </div>
        <div class="col-md-4 mt-2">
            Tarehe: <strong>{{\Carbon\Carbon::now()->format('d/m/Y')}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Imepitiwa na: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Namba ya simu ya Kituo/Wilaya/Mkoa: .....................................................
        </div>
        <div class="col-md-4 mt-2">
            Taarifa imepokelewa wilayani tarehe: ........................................................
        </div>
    </div>
</div>