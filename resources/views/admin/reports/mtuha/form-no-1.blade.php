<div class="report-form-header text-center">
    <p>WIZARA YA AFYA, MAENDELEO YA JAMII, JINSIA, WAZEE NA WATOTO <br> FOMU YA TAARIFA YA KUTOLEA DAWA</p>
</div>
<div class="report-form-subheader text-center row">
    <div class="col-md-4 text-center">
        Jina la kituo: <strong style="text-decoration:underline;">{{$kituo}}</strong>
    </div>
    <div class="col-md-4 text-center">
        Wilaya:  <strong style="text-decoration:underline;">{{$wilaya}}</strong>
    </div>
    <div class="col-md-4 text-center"> 
        Mkoa:  <strong style="text-decoration:underline;">{{$mkoa}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwezi:   <strong style="text-decoration:underline;">{{$month_formatted}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwaka:   <strong style="text-decoration:underline;">{{$year}}</strong>
    </div> 
</div>
<table class="reporttable1" style="width:80%; margin:auto;font-size:15px">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Dawa</th>
        <th rowspan="2" colspan="1">Kipimo cha kugawa</th>
        <th colspan="3" class="text-center">Kiasi cha dawa kilichotolewa kwa wagonjwa</th>
        <th rowspan="2" colspan="3">Jumla</th> 
    </tr>
    <tr> 
        <th>Umri chini ya miaka 5</th>
        <th>Umri miaka 5 <br> hadi miaka 59</th> 
        <th>Umiri miaka 60 na zaid</th> 
    </tr> 
    <tr> 
        <td rowspan="4">1</td>
        <td>ALu ya 1 x 6</td>
        <td>Strip</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr>
    <tr>  
        <td>ALu ya 2 x 6</td>
        <td>Strip</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr>
    <tr>  
        <td>ALu ya 3 x 6</td>
        <td>Strip</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr>
    <tr>  
        <td>ALu ya 4 x 6</td>
        <td>Strip</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>2</td>
        <td>Contrimozoxazole ya maji</td> 
        <td>Chupa</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>3(a)</td>
        <td>Amoxycillin DT(250mg)x10</td> 
        <td>Strip</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>3(a)</td>
        <td>Amoxycillin DT(250mg)x5</td> 
        <td>Strip</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>4</td>
        <td>ORS</td> 
        <td>Sachet</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>5</td>
        <td>Zinc Sulphate</td> 
        <td>Vidonge</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td rowspan="2">6</td>
        <td rowspan="2">Mebendazole</td> 
        <td>Vidonge 100mg</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>   
        <td>Vidonge 500mg</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr>  
    <tr>  
        <td rowspan="2">7</td>
        <td rowspan="2">Albendazole</td> 
        <td>Vidonge 200mg</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>   
        <td>Vidonge 400mg</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr>  
    <tr>  
        <td>8</td>
        <td>FEFO</td> 
        <td>Vidonge</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>9</td>
        <td>Folic Acid</td> 
        <td>Vidonge</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>10</td>
        <td>TLE</td> 
        <td>Vidonge</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>11</td>
        <td>Oxytocin</td> 
        <td>Sindano</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>12</td>
        <td>Depoprovera</td> 
        <td>Sindano</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>13</td>
        <td>SP</td> 
        <td>Vidonge</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>14</td>
        <td>Magnesium Sulphate</td> 
        <td>Sindano</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
    <tr>  
        <td>15</td>
        <td>RHZ Rifampicin 150mg/ <br> isoniazide 75mg/ <br> pyrampicin 150mg/isoniazide</td> 
        <td>Vidonge</td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
    </tr> 
</table>

<div class="container p-5 m-1">
    <div class="row">
        <div class="col-md-4 mt-2">
            Jina la Mtayarishaji wa taarifa: <strong>{{Auth::user()->name}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Kada: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Wadhifa: ...........................................................
        </div>
        <div class="col-md-4 mt-2">
            Sahihi: ..........................................................
        </div>
        <div class="col-md-4 mt-2">
            Tarehe: <strong>{{\Carbon\Carbon::now()->format('d/m/Y')}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Imepitiwa na: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Namba ya simu ya Kituo/Wilaya/Mkoa: .....................................................
        </div>
        <div class="col-md-4 mt-2">
            Taarifa imepokelewa wilayani tarehe: ........................................................
        </div>
    </div>
</div>