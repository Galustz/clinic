<table class="reporttable2" style="margin:auto; width:90%;font-size:15px">
    <tr class="text-center">
        <th>1</th>
        <th>2</th>
        <th colspan="2">3</th>
        <th>4</th>
        <th>5</th>
        <th>6</th>
        <th>7</th>
        <th>8</th>
        <th colspan="3">9</th>
        <th>10</th>
        <th colspan="2">11</th>
        <th colspan="3">12</th>
    </tr>
    <tr class="text-center">
        <th rowspan="3">Na.</th>
        <th rowspan="3">Tarehe</th>
        <th rowspan="2" colspan="2">Namba ya utambulisho</th>
        <th rowspan="3">Namba ya usajili wa vizazi (Birth registration)</th>
        <th rowspan="3">Jina la mtoto</th>
        <th rowspan="3">Tarehe ya kuzaliwa</th>
        <th rowspan="3">Mahali anapoishi (Kitongoji/Mtaa) au Jina la mwenyekiti wa Kitongoji</th>
        <th rowspan="3">Jinsi (KE/ME)</th>
        <th colspan="3">Taarifa za mama</th>
        <th rowspan="3">(HEID no)</th>
        <th colspan="2">Tarehe ya chanjo</th>
        <th rowspan="3" colspan="3">Tarehe ya chanjo ya PENTA</th>
    </tr>
    <tr class="text-center">
        <th rowspan="2">Jina la mama</th>
        <th>Ana kinga ya pepopunda a (TT2+)?</th>
        <th>Hali ya VVU</th>
        <th rowspan="2">BCG</th>
        <th rowspan="2">OPVO</th>
    </tr>
    <tr class="text-center">
        <th>Mwaka</th>
        <th>Namba</th>
        <th>N/H/U</th>
        <th>12U-</th>
    </tr>
    <tr class="text-center">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
    </tr>
    @foreach (range(1,15) as $rows)
    <tr class="text-center">
        @foreach (range(1,18) as $columns)
        <td style="height:15px;"></td>
        @endforeach
    </tr>
    @endforeach
</table>

<table class="reporttable2" style="margin:auto; width:90%;font-size:15px;margin: 50px 13px;">
    <tr class="text-center">
        <th colspan="3">13</th>
        <th colspan="3">14</th>
        <th colspan="2">15</th>
        <th colspan="2">16</th>
        <th colspan="3">17</th>
        <th colspan="12">18</th>
        <th colspan="4">19</th>
        <th colspan="1">20</th>
        <th colspan="2">21</th>
        <th colspan="3">22</th>
        <th>23</th>
    </tr>
    <tr class="text-center">
        <th rowspan="2" colspan="3">Tarehe ya Chanjo ya polio</th>
        <th rowspan="2" colspan="3">Tarehe ya Chanjo Pneumonococcal(PCV13)</th>
        <th rowspan="2" colspan="2">Tarehe ya chanjo ya rota</th>
        <th rowspan="2" colspan="2">Tarehe ya chanjo ya surua/rubella</th>
        <th rowspan="1" colspan="3">Vitamin A</th>
        <th rowspan="1" colspan="12">Ukuaji wa mtoto (1=&gt;80% au &gt; -SD, 2=80% <br>au -2SD--3 SD; 3 =&lt;60% au &lt;-3SD)</th>
        <th rowspan="1" colspan="4">Mebendazole/Albendazole kila miezi 6</th>
        <th rowspan="3">LLIN NH</th>
        <th rowspan="1" colspan="2">Ulishaji wa mtoto</th>
        <th rowspan="1" colspan="3">Rufaa</th>
        <th rowspan="1">Maelezo mengineyo/maoni</th>
    </tr>
    <tr class="text-center">
        <th>Miezi 6</th>
        <th>chini ya mwaka</th>
        <th>mwaka 1-5</th>
        <th colspan="3">Miezi 9</th>
        <th colspan="3">Miezi 18</th>
        <th colspan="3">Miezi 36</th>
        <th colspan="3">Miezi 48</th>
        <th colspan="4">N H</th>
        <th colspan="1" rowspan="2">Maziwa ya mama pekee(EBF) N H (Mtoto umri miezi 6)</th>
        <th colspan="1" rowspan="2">Maziwa ya mbadala (N H)</th>
        <th colspan="1" rowspan="2">Andika kitua alikotoka mtoto</th>
        <th colspan="1" rowspan="2">Kituo alikopelekwa</th>
        <th colspan="1" rowspan="2">Sababu ya rufaa</th>
        <th colspan="1" rowspan="2">Mfano; hakuna taarifa, amehama au amefariki</th>
    </tr>
    <tr class="text-center">
        <th rowspan="1">1</th>
        <th rowspan="1">2</th>
        <th rowspan="1">3</th>
        <th rowspan="1">1</th>
        <th rowspan="1">2</th>
        <th rowspan="1">3</th>
        <th rowspan="1">1</th>
        <th rowspan="1">2</th>
        <th rowspan="1">1</th>
        <th rowspan="1">2</th>
        <th rowspan="1">N/H</th>
        <th rowspan="1">N/H</th>
        <th rowspan="1">N/H</th>
        <th rowspan="1">Uzito/umri</th>
        <th rowspan="1">Uzito/urefu</th>
        <th rowspan="1">Urefu/umri</th>
        <th rowspan="1">Uzito/umri</th>
        <th rowspan="1">Uzito/urefu</th>
        <th rowspan="1">Uzito/umri</th>
        <th rowspan="1">Uzito/urefu</th>
        <th rowspan="1">Urefu/umri</th>
        <th rowspan="1">Urefu/umri</th>
        <th rowspan="1">Uzito/umri</th>
        <th rowspan="1">Uzito/urefu</th>
        <th rowspan="1">Urefu/umri</th>
        <th rowspan="1">Miezi 12</th>
        <th rowspan="1">Miezi 18</th>
        <th rowspan="1">Miezi 24</th>
        <th rowspan="1">Miezi 30</th>
    </tr>
    @foreach (range(1,15) as $rows)
    <tr class="text-center">
        @foreach (range(1,36) as $columns)
        <td style="height:15px;"></td>
        @endforeach
    </tr>
    @endforeach
</table>