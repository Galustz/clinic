<div class="report-form-header text-center">
    <p>WIZARA YA AFYA, MAENDELEO YA JAMII, JINSIA, NA WATOTO FOMU <br>
        YA TAARIFA YA VIPIMO VYA MALARIA</p>
</div>
<div class="report-form-subheader text-center row">
    <div class="col-md-4 text-center">
        Jina la kituo: <strong style="text-decoration:underline;">{{$kituo}}</strong>
    </div>
    <div class="col-md-4 text-center">
        Wilaya:  <strong style="text-decoration:underline;">{{$wilaya}}</strong>
    </div>
    <div class="col-md-4 text-center"> 
        Mkoa:  <strong style="text-decoration:underline;">{{$mkoa}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwezi:   <strong style="text-decoration:underline;">{{$month_formatted}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwaka:   <strong style="text-decoration:underline;">{{$year}}</strong>
    </div> 
</div> 
<table class="reporttable1" style="width:90%;margin:auto;font-size:15px">
        <tr>
            <th rowspan="2">Kipimo</th> 
            <th colspan="2"> Umri chini <br>ya mwezi 1</th>
            <th colspan="2"> Umri mwezi <br>1 hadi 11</th>
            <th colspan="2"> Umri mwaka <br>1 hadi miaka 4</th>
            <th colspan="2"> Umri miaka  5<br>1 na zaidi</th>
            <th colspan="3"> Jumla</th>
        </tr>
        <tr> 
            <th>Me</th>
            <th>Ke</th>
            <th>Me</th>
            <th>Ke</th>
            <th>Me</th>
            <th>Ke</th>
            <th>Me</th>
            <th>Ke</th>
            <th>Me</th>
            <th>Ke</th>
            <th>Jumla</th>
        </tr>
        <tr>
            <td rowspan="2">mRDT</td>
            <td>Negative</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr> 
            <td>Positive</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="2">BS</td>
            <td>No MPS</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr> 
            <td>MPS seen</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr> 
            <td>Jumla ya<br> vipimo <br> vyote</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    
<div class="container p-5 m-1">
    <div class="row">
        <div class="col-md-7 mt-2">
            Jina la Mtayarishaji wa taarifa: <strong>{{Auth::user()->name}}</strong>
        </div>
        <div class="col-md-5 mt-2">
            Kada: .............................................................
        </div>
        <div class="col-md-12 mt-2">
            Wadhifa: ...........................................................
        </div>
        <div class="col-md-12 mt-2">
            Sahihi: ..........................................................
        </div>
        <div class="col-md-12 mt-2">
            Tarehe: <strong>{{\Carbon\Carbon::now()->format('d/m/Y')}}</strong>
        </div>
        <div class="col-md-12 mt-2">
            Imepitiwa na: .............................................................
        </div>
        <div class="col-md-12 mt-2">
            Namba ya simu ya Kituo/Wilaya/Mkoa: .....................................................
        </div>
        <div class="col-md-12 mt-2">
            Taarifa imepokelewa wilayani tarehe: ........................................................
        </div>
    </div>
</div>