<div class="report-form-header text-center">
    <p>WIZARA YA AFYA, MAENDELEO YA JAMII, JINSIA, WAZEE NA WATOTO <br> FOMU YA TAARIFA YA VIASHIRIA VYA UPATIKANAJI WA DAWA NCHINI <br>(TRACER MEDICINE)</p>
</div>
<div class="report-form-subheader text-center row">
    <div class="col-md-4 text-center">
        Jina la kituo: <strong style="text-decoration:underline;">{{$kituo}}</strong>
    </div>
    <div class="col-md-4 text-center">
        Wilaya:  <strong style="text-decoration:underline;">{{$wilaya}}</strong>
    </div>
    <div class="col-md-4 text-center"> 
        Mkoa:  <strong style="text-decoration:underline;">{{$mkoa}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwezi:   <strong style="text-decoration:underline;">{{$month_formatted}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwaka:   <strong style="text-decoration:underline;">{{$year}}</strong>
    </div> 
</div>
<div class="report-form-subheader">
    {{-- Jina la kituo <span>...........................................................................</span> 
    Aina ya kituo <span>...........................................................................</span> 
    Mmiliki wa kituo <span>...........................................................................</span> <br> 
    Wilaya <span>............................................................</span> 
    Mkoa <span>..............................................................</span> 
    Mwezi <span>.............................................................</span> 
    Mwaka <span>.............................................................</span> <br><br> --}}
    Chagua &nbsp; <strong>1</strong> = Kama kiashiria dawa ipo. &nbsp; <strong>0</strong>= Kama kiashiria/dawa haipo <br><br>
    Muda wa kiashiria/dawa kutokuwepo <br>
    Chagua &nbsp;&nbsp;&nbsp;<strong>A</strong> = Kama haipo kwa siku zisizozidi wiki moja &nbsp; 
    <strong>B</strong> = Kama haipo kwa muda wa wiki 1-4 <br>
    &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>C</strong> = Kama haipo zaidi ya mwezi mzima. <br>

</div>
<table class="reporttable1" style="width:90%;margin:auto;font-size:15px">
        <tr>
            <th>Na</th>
            <th>Maelezo</th>
            <th colspan="1">Je Kituo Kinato Huduma Hii? <br> 1 = ndio <br> 0 = Hapana</th>
            <th colspan="1">Hali ya kiashiria/dawa ipo/haipo? <br> 1 = ndio <br> 0 = Hapana </th>
            <th colspan="1">Kama haipo <br> A: chini ya wiki moja <br>B: wiki 1-4 <br>C: Zaidi ya mwezi mzima</th> 
        </tr>
        <tr>
            <td>1</td>
            <td>DPT + HepB/HiB vaccine for immunization</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Vidonge vya ALU vya kumeza</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Amoxycillin/Cotrimoxazole ya maji</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>4</td>
            <td>Amoxycillin/Cotrimoxazole ya vidonge</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>5</td>
            <td>Dawa za vidonge za minyoo Albendazole au Mebendazole</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Dawa ya kuhara ya kuchanganya na maji(ORS)</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Sindano ya Ergometrine au Oxytocin au Vidonge vya Misoprostol</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>8</td>
            <td>Dawa ya sindano ya uzazi wa mpango(Depo)</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>9</td>
            <td>Maji ya mishipa(Dextrose 6% au Sodium Chloride + Dextrose</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Mabomba ya sindano kwa matumizi ya mara moja(Disposable)</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>11</td>
            <td>Kipimo cha malaria cha haraka(MRDT) au vifaa vya kupimia katika Hadubini</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>12</td>
            <td>Magnesium Sulphate Sindano</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>13</td>
            <td>Zinc sulphate Vidonge</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>14</td>
            <td>Paracetamol Tablets</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>15</td>
            <td>Benzyl Penicilline Injection</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>16</td>
            <td>Ferrous + Folic Acid Tablets</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>17</td>
            <td>Metronidazole Tablets</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>18</td>
            <td>Combined Oral Contraceptives</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>19</td>
            <td>Catgut Sutures</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>20</td>
            <td>Navirapine Oral Solution</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>21</td>
            <td>Tenofovir 300mg + Lamivudine 30mg + Efavirenz 600mg</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>22</td>
            <td>Efavirenz 600mg Tablets</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>23</td>
            <td>Zidovudine 60mg + Lamivudine 30mg + Nevirapine 50mg Tablets</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>24</td>
            <td>UNIGOLD HIV 1/2</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>25</td>
            <td>Determine HIV 1&2</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>26</td>
            <td>FACS Count reagent</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>27</td>
            <td>DBS</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>28</td>
            <td>RHZE Rifampicin 150mg/Isoniazide 75mg/Pyrazinamide/Ethambutol Tablets</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>29</td>
            <td>RH Rifampicin 150 MG/Isonizide 75mg Tablets</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>30</td>
            <td>Sulphandoxine+pyrimethamine</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        
    </table>
    
<div class="container p-5 m-1">
    <div class="row">
        <div class="col-md-4 mt-2">
            Jina la Mtayarishaji wa taarifa: <strong>{{Auth::user()->name}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Kada: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Wadhifa: ...........................................................
        </div>
        <div class="col-md-4 mt-2">
            Sahihi: ..........................................................
        </div>
        <div class="col-md-4 mt-2">
            Tarehe: <strong>{{\Carbon\Carbon::now()->format('d/m/Y')}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Imepitiwa na: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Namba ya simu ya Kituo/Wilaya/Mkoa: .....................................................
        </div>
        <div class="col-md-4 mt-2">
            Taarifa imepokelewa wilayani tarehe: ........................................................
        </div>
    </div>
</div>