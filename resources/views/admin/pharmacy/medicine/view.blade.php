@extends('layouts.admin')
@section('content')
    <div class="container">
            @if(!empty($success_msg))
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Success</span>
                    {{$success_msg}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
            @php
                use App\Medicine;
                use App\MedicineCategory;
                $medicine = Medicine::find($id);
                $categories = MedicineCategory::all();
                $medicines = Medicine::all(); 
                $batch  = \App\MedicineStockBatch::where('medicine_id',$medicine->id)
                                        ->where('remaining_quantity','!=','0') 
                                        ->orderBy('expiry_date','asc')
                                        ->first();  
                $remaining_quantity = \App\MedicineStockBatch::where('medicine_id',$medicine->id)
                                        ->sum('remaining_quantity');
                $expiry_date = !empty($batch->expiry_date)?$batch->expiry_date:'';
                //$remaining_quantity = !empty($batch->remaining_quantity)?$batch->remaining_quantity:'';
                $disabled = $medicine->medicine_category_id==4?'':'disabled';
            @endphp
        <div class="card card-body">
            <p>Date: <strong>{{Carbon\Carbon::today()->format('d/m/Y')}}</strong></p>
            <p>Name: <strong>{{$medicine->name}}</strong></p>
            <p>Expiry date: <strong>{{$expiry_date}}</strong></p>
            <p>Stock used today:<strong></strong></p>
            <p>Remaining stock: <strong>{{$remaining_quantity.' '.$medicine->selling_unit_name.'s'}}</strong></p>
        </div>
        <a href="{{url()->previous()}}">Back</a>
        <div class="card card-body">
            <h4>Medicine Details</h4>
            <hr>
            <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/pharmacy/medicines/save')}}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">
                        <input type="text" name="id" id="service_id" hidden>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Medicine type:</label>
                                <div class="input-group">
                                    <select name="medicine_category" id="medicine_category" onchange="enable_mgs_per_tablet()" class="form-control">
                                        @foreach ($categories as $category)
                                            <option value="{{$category->id}}" {{$category->id==$medicine->medicine_category_id?'selected':''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Name:</label>
                                <div class="input-group"> 
                                    <input type="text" name="name" value="{{$medicine->name}}" id="name" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Code:</label>
                                <div class="input-group"> 
                                    <input type="text" name="code" value="{{$medicine->code}}" id="code" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Code 2:</label>
                                <div class="input-group"> 
                                    <input type="text" name="code_2" value="{{$medicine->code_2}}" id="code" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Description:</label>
                                <div class="input-group"> 
                                    <input type="text" name="description" value="{{$medicine->description}}" id="price" class="form-control">
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Unit purchasing price:</label>
                                <div class="input-group"> 
                                    <input type="number" name="unit_purchasing_price" value="{{$medicine->unit_purchasing_price}}" id="unit_purchasing_price" class="form-control" required>
                                    <span style="padding-top:10px;padding-left:10px;">. Tsh</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Unit selling price:</label>
                                <div class="input-group"> 
                                    <input type="number" name="price" value="{{$medicine->selling_price}}" id="price" class="form-control" required>
                                    <span style="padding-top:10px;padding-left:10px;">. Tsh</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Purchasing unit name:</label>
                                <div class="input-group"> 
                                    <input type="text" name="purchasing_unit_name" value="{{$medicine->purchasing_unit_name}}" placeholder="Boxes, Bottles.." id="purchasing_unit_name" class="form-control" required>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Selling unit name:</label>
                                <div class="input-group"> 
                                    <input type="text" name="selling_unit_name" value="{{$medicine->selling_unit_name}}" placeholder="Tablet, Bottle, Packet.." id="selling_unit_name" class="form-control" required>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Quantity per unit:</label>
                                <div class="input-group"> 
                                    <input type="number" name="quantity_per_unit" value="{{$medicine->quantity_per_unit}}" id="" class="form-control" required>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Formula:</label>
                                <div class="input-group"> 
                                    <input type="text" name="formula"  id="formula" class="form-control" disabled> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Predefined value:</label>
                                <div class="input-group"> 
                                    <input type="number" placeholder="0" value="{{$medicine->predefine_value}}" name="predefined_value" id="predefined" class="form-control"> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mgs per tablet:</label>
                                <div class="input-group"> 
                                    <input type="number" value="{{$medicine->mgs_per_tablet}}" name="mgs_per_tablet" id="mgs_per_tablet" class="form-control" {{$disabled}} required> 
                                </div>
                            </div>
                        </div> 
                        <input type="hidden" name="medicine_id" value="{{$id}}">
                        <div class="col-md-2">
                            <br>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card card-body">
            @php 
                use App\MedicineStockBatch;
                $batches = MedicineStockBatch::where('medicine_id',$medicine->id)->get();

                $i=1;
            @endphp
            <div class="row">
                <div class="col-md-10   "><h4>Medicine batches</h4></div>
                <div class="col-md-2"><a href="{{url('admin/pharmacy/medicines/newBatch',$medicine->id)}}" class="btn btn-primary">New Batch</a></div>
            </div>            
            <hr>   
            <div class="row form-group">
                <div class="table-responsive m-b-40">
                    <table id="patient-que-table" class="datatable mytable">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Date bought</th> 
                                <th>Unit purchasing <br> price</th>
                                <th>No of <br>Units</th>
                                <th>Total price</th>
                                <th>Batch no</th>
                                <th>Expiry date</th>
                                <th>Remaining quantity</th> 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($batches as $batch)
                            <tr class="table-row">
                                <td>{{$i}}</td>
                                <td>{{$batch->created_at->format('d/m/Y')}}</td> 
                                <td>{{$batch->unit_purchasing_price}}</td>
                                <td>{{$batch->total_quantity.' '.$batch->medicine->selling_unit_name.'s'}}</td>
                                <td>{{$batch->total_price}}</td> 
                                <td>{{$batch->batch_number}}</td> 
                                <td>{{$batch->expiry_date}}</td>
                                <td>{{$batch->remaining_quantity.' '.$batch->medicine->selling_unit_name.'s'}}</td>
                                <td>
                                    <a href="{{url('admin/pharmacy/medicines/editBatch',$batch->id)}}" class="btn btn-primary">Edit</a>
                                </td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection