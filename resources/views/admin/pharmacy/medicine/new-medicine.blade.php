@extends('layouts.admin')
@section('content')
    <div class="container">
            @if(!empty($success_msg))
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Success</span>
                    {{$success_msg}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
        <div class="card card-body">
            @php
                use App\Medicine;
                use App\MedicineCategory;
                $categories = MedicineCategory::all();
                $medicines = Medicine::all();
            @endphp
            <h4>Medicine Details</h4>
            <hr>
            <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/pharmacy/medicines/new')}}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">
                        <input type="text" name="id" id="service_id" hidden>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Medicine type:</label>
                                <div class="input-group">
                                    <select name="medicine_category" id="medicine_category" onchange="enable_mgs_per_tablet()" class="form-control">
                                        @foreach ($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Name:</label>
                                <div class="input-group"> 
                                    <input type="text" name="name" id="name" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Code:</label>
                                <div class="input-group"> 
                                    <input type="text" name="code" id="code" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Code 2:</label>
                                <div class="input-group"> 
                                    <input type="text" name="code_2" id="code" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Description:</label>
                                <div class="input-group"> 
                                    <input type="text" name="description" id="price" class="form-control">
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Unit purchasing price:</label>
                                <div class="input-group"> 
                                    <input type="number" name="unit_purchasing_price" id="unit_purchasing_price" class="form-control" required>
                                    <span style="padding-top:10px;padding-left:10px;">. Tsh</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Unit selling price:</label>
                                <div class="input-group"> 
                                    <input type="number" name="price" id="price" class="form-control" required>
                                    <span style="padding-top:10px;padding-left:10px;">. Tsh</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Purchasing unit name:</label>
                                <div class="input-group"> 
                                    <input type="text" name="purchasing_unit_name" placeholder="Boxes, Bottles.." id="purchasing_unit_name" class="form-control" required>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Selling unit name:</label>
                                <div class="input-group"> 
                                    <input type="text" name="selling_unit_name" placeholder="Tablet, Bottle, Packet.." id="selling_unit_name" class="form-control" required>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Quantity per unit:</label>
                                <div class="input-group"> 
                                    <input type="number" name="quantity_per_unit" id="" class="form-control" required>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Formula:</label>
                                <div class="input-group"> 
                                    <input type="text" name="formula" id="formula" class="form-control" disabled> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="display:none;">
                            <div class="form-group">
                                <label>Predefined value:</label>
                                <div class="input-group"> 
                                    <input type="number" placeholder="0" name="predefined_value" id="predefined" class="form-control"> 
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mgs per tablet:</label>
                                <div class="input-group"> 
                                    <input type="number" name="mgs_per_tablet" id="mgs_per_tablet" class="form-control" disabled required> 
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-2" style="padding-top:30px;"> 
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
 
@endsection