@extends('layouts.admin')
@section('content')
    <div class="container">
            @if(!empty($success_msg))
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Success</span>
                    {{$success_msg}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
        
            <div class="container-fluid">
                <div class="row" style="margin:auto;">
                    <div class="col-md-8">
                        <div class="card card-body">
                        <form action="{{ url('admin/pharmacy/suppliers/new') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Supplier Name</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="name"><Address>Address</label>
                                <input type="text" name="address" id="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="name">Contact Person</label>
                                <input type="text" name="contact_person" id="contact_person" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="name">Phone Number</label>
                                <input type="text" name="phone_no" id="phone_no" class="form-control">
                            </div>
                            <div class="form-group appendable">
                                <div class="row" id="medicine-price-container">
                                    <div class="col-md-6">
                                        <label for="medicines">Medicines To Be Supplied</label>
                                        <select class="form-control counter" name="medicines[]" id="medicines" style="height:30px;">
                                            @foreach($medicines as $medicine)
                                                <option value="{{$medicine->id}}">{{$medicine->name}} test</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="price">Price</label>
                                        <input type="number" name="price[]" id="price" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-outline-primary new-medicine-btn btn-block" style="margin-bottom:30px;">Add More Medicine</button>
                            <button type="submit" class="btn btn-success btn btn-block">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('delete-medicine-btn').css('display','none');
        $('.new-medicine-btn').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            $clone = $('#medicine-price-container').clone(true).appendTo('.appendable');
            $('delete-medicine-btn').css('display','block');
        });

        $('.delete-medicine-btn').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            if (count > 1) {
                $(this).parent().parent().remove();
                $('delete-medicine-btn').css('display','block');
            } else {
                $('delete-medicine-btn').prop('disabled',true);
            }
        });
    </script>
@endsection