@extends('layouts.admin') 
@section('content')
    <div class="container">
        <div class="row card">
            <h2 class="col-md-12 text-center">{{ $supplier->name }}</h2>
            <h4 class="col-md-12 text-center p-t-10 p-b-10">{{ $supplier->address }}</h4>
            <div class="row">
                <div class="col-md-12 text-center">Contact Person:  {{ $supplier->contact_person }}</div>
                <div class="col-md-12 text-center">Phone Number:  {{ $supplier->phone_no }}</div>
                <div class="col-md-12 text-center">Added On:  {{ $supplier->created_at }}</div>
            </div>
        </div> 
        
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive m-b-40">
                    <table id="patient-que-table" class="datatable table table-borderless table-data3">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Description</th>
                                <th>Selling Price</th>
                                <th>Purchasing Unit Name</th>
                                <th>Quantity Per Unit</th>
                                <th>Unit Purchasing Price</th>
                                <th>Code</th>
                                <th>Code 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($supplier->medicines as $medicine)
                            <tr class="table-row">
                            <td>{{$medicine->id}}</td>
                                <td>{{$medicine->description}}</td>
                                <td>{{$medicine->selling_price}}</td>
                                <td>{{$medicine->purchasing_unit_name}}</td>
                                <td>{{$medicine->quantity_per_unit}}</td>
                                <td>{{ $medicine->unit_purchasing_price }}</td>
                                <td>{{ $medicine->code }}</td>
                                <td>{{ $medicine->code_2 }}</td>
                                {{-- <td><a href="{{ url('admin/pharmacy/supplier/view',$supplier->id) }}" class="btn btn-primary">View</a></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection