@php
    use App\Medicine;
    use App\MedicineCategory;
    $categories = MedicineCategory::all();
    $medicines = Medicine::all();
@endphp
@extends('layouts.admin') @section('content')
<div class="container">
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="container">
            <a href="{{url('admin/pharmacy/lpo/new')}}" class="btn btn-primary">Add LPO</a>

            </div>
        </div><br><br>
        <!-- DATA TABLE-->
        <div class="table-responsive m-b-40">
            <table id="patient-que-table" class="datatable table table-borderless table-data3">
                <thead>
                    <tr>
                        <th>Number</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($medicines as $medicine)
                    <tr class="table-row">
                    <td>{{$medicine->id}}</td>
                        <td>{{$medicine->name}}</td>
                        <td>{{$medicine->category->name}}</td>
                        <td>{{$medicine->description}}</td>
                        <td>{{$medicine->selling_price}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE                  -->
    </div>
</div>

</div>
@endsection