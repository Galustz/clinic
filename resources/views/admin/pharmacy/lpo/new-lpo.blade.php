@extends('layouts.admin')
@section('content')
    <div class="container">
            @if(!empty($success_msg))
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Success</span>
                    {{$success_msg}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
        <div class="card card-body">
            
        </div>
    </div>
@endsection