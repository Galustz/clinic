<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="service-form" enctype="multipart/form-data" method="post" action="service/create" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">
                        <input type="text" name="id" id="service_id" hidden>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="service" class=" form-control-label">Service</label>
                                <div class="input-group">
                                    {{-- <div class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </div> --}}
                                    <input type="text" name="service" id="service" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="price" class=" form-control-label">Price</label>
                                <div class="input-group">
                                    {{-- <div class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </div> --}}
                                    <input type="text" name="price" id="price" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit Category -->
