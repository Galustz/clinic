@extends('layouts.admin')
@section('content')
    <div class="container">
            {{-- @include('components.alert-message') --}}
            <div class="row">
                <div class="col-md-12">
                    <h3>New Supplier</h3>
                    <hr>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row" style="margin:auto;">
                    <div class="col-md-12">
                        <form action="{{ url('admin/suppliers/add') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="name">Supplier Name</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="name">Address</label>
                                <input type="text" name="address" id="address" class="form-control">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="name">Contact Person</label>
                                <input type="text" name="contact_person" id="contact_person" class="form-control">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="name">Phone Number</label>
                                <input type="text" name="phone_no" id="phone_no" class="form-control">
                            </div>
                            <div class="col-md-6 form-group appendable">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-outline-primary new-medicine-btn btn-block" type="button" style="margin-bottom:30px;">Add More Medicine</button>
                                    </div>
                                </div>
                                <div class="row" id="medicine-price-container">
                                    <div class="col-md-6">
                                        <label for="medicines">Medicines To Be Supplied</label>
                                        <select class="form-control counter" name="medicines[]" id="medicines">
                                            @foreach($medicines as $medicine)
                                                <option value="{{$medicine->id}}">{{$medicine->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="price">Price</label>
                                        <input type="number" name="price[]" id="price" class="form-control">
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 form-group appendable-e">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-outline-primary new-medicine-btn-e btn-block" type="button" style="margin-bottom:30px;">Add More Equipment</button>
                                    </div>
                                </div>
                                <div class="row" id="equipment-price-container">
                                    <div class="col-md-6">
                                        <label for="medicines">Equipments To Be Supplied</label>
                                        <select class="form-control counter" name="equipments[]" id="medicines">
                                            @foreach($equipments as $equipment)
                                                <option value="{{$equipment->id}}">{{$equipment->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="price">Price</label>
                                        <input type="number" name="equipment_price[]" id="price" class="form-control">
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn btn-block">Submit</button>
                        </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-scripts')
    
<script>
    $('delete-medicine-btn').css('display','none');
    $('.new-medicine-btn').click(function(e){
        e.preventDefault();
        count = $('.counter').length;
        $clone = $('#medicine-price-container').clone(true).appendTo('.appendable');
        $('delete-medicine-btn').css('display','block');
    });
    $('.new-medicine-btn-e').click(function(e){
        e.preventDefault();
        count = $('.counter').length;
        $clone = $('#equipment-price-container').clone(true).appendTo('.appendable-e');
        $('delete-medicine-btn').css('display','block');
    });

    $('.delete-medicine-btn').click(function(e){
        e.preventDefault();
        count = $('.counter').length;
        if (count > 1) {
            $(this).parent().parent().remove();
            $('delete-medicine-btn').css('display','block');
        } else {
            $('delete-medicine-btn').prop('disabled',true);
        }
    });
</script>
@endsection