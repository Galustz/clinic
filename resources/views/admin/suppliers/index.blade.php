@extends('layouts.admin')
@section('content')
    <div class="container">
        @include('components.content-header',[
            'header'=>'Suppliers',
            "button"=>[
                'name'=>'Add supplier',
                'url'=>'/admin/suppliers/add',
                'color'=>'primary',
                ],
            ])
        @component('components.table',[
            "columns"=>[
                "No","Name","Phone","Address","Contact person","Action"
            ],
            "collection"=>$suppliers,
            "data_columns"=>[
                "iteration",
                "name",
                "phone_no",
                "address",
                "contact_person", 
                "buttons"=>[
                    ["url"=>$supplier_view_url,"id"=>true,"name"=>"View","color"=>"primary"],
                    ["url"=>$lpo_create_url,"id"=>true,"name"=>"Create LPO","color"=>"success"],
                    ["url"=>$supplier_edit_url,"id"=>true,"name"=>"Edit","color"=>"primary"],
                    ["url"=>$supplier_delete_url,"id"=>true,"name"=>"Remove","color"=>"danger"],
                ]
            ]
        ]) 
        @endcomponent
    </div>
@endsection