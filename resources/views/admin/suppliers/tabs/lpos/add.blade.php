@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Create LPO</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{url()->current()}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="">LPO terms</label>
                            <select name="terms" id="" class="form-control">
                                @foreach ($terms as $term)
                                    <option value="{{$term}}">{{$term}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group appendable">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-outline-primary new-medicine-btn btn-block" style="margin-bottom:30px;">Add More Medicine</button>
                                </div>
                            </div>
                            <div class="row" id="medicine-price-container">
                                <div class="col-md-6">
                                    <label for="medicines">Medicines To Be Supplied</label>
                                    <select class="form-control counter" name="medicines[]" id="medicines">
                                        @foreach($medicines as $medicine)
                                            <option value="{{$medicine->id}}">{{$medicine->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="quantity">Quantity</label>
                                    <input type="number" name="quantity[]" id="quantity" class="form-control" required>
                                </div>
                                <div class="col-md-2 text-right">
                                    <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group appendable-e">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-outline-primary new-medicine-btn-e btn-block" type="button" style="margin-bottom:30px;">Add More Equipment</button>
                                </div>
                            </div>
                            <div class="row" id="equipment-price-container">
                                <div class="col-md-6">
                                    <label for="medicines">Equipments To Be Supplied</label>
                                    <select class="form-control counter" name="equipments[]" id="medicines">
                                        @foreach($equipments as $equipment)
                                            <option value="{{$equipment->id}}">{{$equipment->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="price">Quantity</label>
                                    <input type="number" name="equipment_quantity[]" id="price" class="form-control">
                                </div>
                                <div class="col-md-2 text-right">
                                    <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" style="">Create</button>
                            <a href="{{url()->previous()}}" class="btn btn-danger">Back</a>
                        </div>
                    </div>                 
                </form>
            </div>         
        </div>
    </div>

    <script>
        $('delete-medicine-btn').css('display','none');
        $('.new-medicine-btn').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            $clone = $('#medicine-price-container').clone(true).appendTo('.appendable');
            $('delete-medicine-btn').css('display','block');
        });

        $('.new-medicine-btn-e').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            $clone = $('#equipment-price-container').clone(true).appendTo('.appendable-e');
            $('delete-medicine-btn').css('display','block');
        });

        $('.delete-medicine-btn').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            if (count > 1) {
                $(this).parent().parent().remove();
                $('delete-medicine-btn').css('display','block');
            } else {
                $('delete-medicine-btn').prop('disabled',true);
            }
        });
    </script>
@endsection
