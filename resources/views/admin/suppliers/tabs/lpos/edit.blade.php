@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Create LPO</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('admin/suppliers/lpo/edit')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
                    <input type="hidden" name="lpo_id" value="{{$lpo->id}}">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="">LPO terms</label>
                            <select name="terms" id="" class="form-control">
                                @foreach ($terms as $term)
                                    <option value="{{$term}}">{{$term}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group appendable">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-outline-primary new-medicine-btn btn-block" style="margin-bottom:30px;">Add More Medicine</button>
                                </div>
                            </div>
                            @if ($selected_medicines->count()>0)
                                @foreach ($selected_medicines as $selected_medicine)
                                    <div class="row" id="medicine-price-container">
                                        <div class="col-md-6">
                                            <label for="medicines">Medicines To Be Supplied</label>
                                            <select class="form-control counter" name="medicines[]" id="medicines">
                                                @foreach($medicines as $medicine)
                                                    <option value="{{$medicine->id}}"{{$medicine->id == $selected_medicine->medicine_id?'selected':''}}>{{$medicine->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="quantity">Quantity</label>
                                            <input type="number" name="quantity[]" id="quantity" value="{{$selected_medicine->quantity}}" class="form-control" required>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                        </div>
                                    </div>
                                @endforeach                               
                            @else
                                <div class="row" id="medicine-price-container">
                                    <div class="col-md-6">
                                        <label for="medicines">Medicines To Be Supplied</label>
                                        <select class="form-control counter" name="medicines[]" id="medicines">
                                            @foreach($medicines as $medicine)
                                                <option value="{{$medicine->id}}"{{$medicine->id == $selected_medicine->medicine_id?'selected':''}}>{{$medicine->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="quantity">Quantity</label>
                                        <input type="number" name="quantity[]" id="quantity" value="{{$selected_medicine->quantity}}" class="form-control" required>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="col-md-6 form-group appendable-e">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-outline-primary new-medicine-btn-e btn-block" type="button" style="margin-bottom:30px;">Add More Equipment</button>
                                </div>
                            </div>
                            @if ($selected_equipments->count()>0)
                                @foreach ($selected_equipments as $selected_equipment)
                                    <div class="row" id="equipment-price-container">
                                        <div class="col-md-6">
                                            <label for="medicines">Equipments To Be Supplied</label>
                                            <select class="form-control counter" name="equipments[]" id="medicines">
                                                @foreach($equipments as $equipment)
                                                    <option value="{{$equipment->id}}" {{$selected_equipment->medicine_id == $equipment->id?'selected':''}}>{{$equipment->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="price">Quantity</label>
                                            <input type="number" name="equipment_quantity[]" value="{{$selected_equipment->quantity}}" id="price" class="form-control" required>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                        </div>
                                    </div>
                                @endforeach                                 
                            @else
                                <div class="row" id="equipment-price-container">
                                    <div class="col-md-6">
                                        <label for="medicines">Equipments To Be Supplied</label>
                                        <select class="form-control counter" name="equipments[]" id="medicines">
                                            @foreach($equipments as $equipment)
                                                <option value="{{$equipment->id}}">{{$equipment->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="price">Quantity</label>
                                        <input type="number" name="equipment_quantity[]" id="price" class="form-control" required>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <button class="btn btn-danger delete-medicine-btn" style="margin-top:30px;">X</button>
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" style="">Save</button>
                            <a href="{{url()->previous()}}" class="btn btn-danger">Back</a>
                        </div>
                    </div>                 
                </form>
            </div>         
        </div>
    </div>

    <script>
        $('delete-medicine-btn').css('display','none');
        $('.new-medicine-btn').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            $clone = $('#medicine-price-container').clone(true).appendTo('.appendable');
            $('delete-medicine-btn').css('display','block');
        });

        $('.new-medicine-btn-e').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            $clone = $('#equipment-price-container').clone(true).appendTo('.appendable-e');
            $('delete-medicine-btn').css('display','block');
        });

        $('.delete-medicine-btn').click(function(e){
            e.preventDefault();
            count = $('.counter').length;
            if (count > 1) {
                $(this).parent().parent().remove();
                $('delete-medicine-btn').css('display','block');
            } else {
                $('delete-medicine-btn').prop('disabled',true);
            }
        });
    </script>
@endsection
