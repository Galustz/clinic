<h5 class="m-t-10">{{$is_editing?'Edit supplier medicine price':'Add medicine to supplier'}}</h5>
<form action="{{$add_medicine_url}}" class="form" method="POST">
    {{ csrf_field() }}
    <div class="row">
        <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
        @if ($is_editing)
            <input type="hidden" name="supplier_medicine_id" value="{{$supplier_medicine_id}}">            
            <input type="hidden" name="medicine_id" value="{{$edit_medicine_id}}">            
        @endif
        <div class="form-group col-md-5">
            <label for="">Medicine</label>
            <select class="form-control" name="medicine_id" id="" {{$is_editing?'disabled':''}}>
                @foreach ($medicines as $medicine)
                    <option value="{{$medicine->id}}" {{isset($edit_medicine_id)?($edit_medicine_id==$medicine->id?'selected':''):''}}>{{$medicine->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-5">
            <label for="">Price</label>
            <input type="number" class="form-control" name="price" id="" value="{{$edit_price??''}}">
        </div>
        <div class="col-md-2 text-right">
            <button class="btn btn-primary btn-sm" style="margin-top:32px;">{{$is_editing?'Edit medicine':'Add medicine'}}</button>
        </div>
    </div>
</form>
@if (!$is_editing) 
<h5 class="m-t-10">All supplier medicines</h5>
<table class="mytable m-t-10">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Price</th>
            <th>Code</th>
            <th>Code 2</th>
            <th>Action</th>
        </tr>        
    </thead>
    <tbody>
        @foreach ($supplier->medicines as $medicine)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$medicine->name}}</td>
            <td>{{$medicine->pivot->price}}</td>
            <td>{{$medicine->code}}</td>
            <td>{{$medicine->code_2}}</td>
            <td>
                <a href="{{Request::fullUrl().'&supplier_medicine_id='.$medicine->pivot->id}}" class="btn btn-sm btn-primary">Edit</a>
                <a href="{{$delete_url."/".$medicine->pivot->id}}" class="btn btn-sm btn-danger">Remove</a>
            </td>
        </tr>
        @endforeach       
    </tbody>
</table>
@endif