@if (isset($edit))
<h5 class="m-t-10">Edit supplier details:</h5>
<form action="{{url('admin/suppliers/edit')}}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
    <table class="table table-striped m-t-10">
        <tr>
            <td col="20%">Name</td>
            <td> <input type="text" class="form-control" name="name" value="{{$supplier->name}}"> </td> 
        </tr>
        <tr>
            <td>Addresss</td>
            <td> <input type="text" class="form-control" name="address" value="{{$supplier->address}}">  </td>
           
        </tr>
        <tr>
            <td>Contact person</td>
            <td><input type="text" class="form-control" name="contact_person" value="{{$supplier->contact_person}}"> </td>
             
        </tr>
        <tr>
            <td>Phone no</td> 
            <td> <input type="text" class="form-control" name="phone_no" value="{{$supplier->phone_no}}"> </td> 
        </tr> 
        <tr>
            <td></td> 
            <td class="text-right">
                <button class="btn btn-primary">Save</button>
            </td>  
        </tr> 
    </table>  
</form>
@else
<h5 class="m-t-10">Supplier details:</h5>
<table class="table table-striped m-t-10">
    <tr>
        <td col="20%">Name</td>
        <td>{{$supplier->name}}</td>
    </tr>
    <tr>
        <td>Addresss</td>
        <td>{{$supplier->address}}</td>
    </tr>
    <tr>
        <td>Contact person</td>
        <td>{{$supplier->contact_person}}</td>
    </tr>
    <tr>
        <td>Phone no</td>
        <td>{{$supplier->phone_no}}</td>
    </tr> 
</table>  
@endif