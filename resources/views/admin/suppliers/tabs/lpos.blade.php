<div class="row m-t-10">
    <div class="col-md-8">
        <h5>Local Purchase Orders:</h5>
    </div>
    <div class="col-md-4 text-right">
        <a href="{{$lpo_create_url."/$supplier->id"}}" class="btn btn-sm btn-primary">Create LPO</a>
    </div>
</div>
<table class="mytable m-t-10 mytable dataTable">
    <tbody>
        <tr>
            <th>No</th>
            <th>Date</th> 
            <th>Status</th>
            <th>Terms</th>
            <th>Action</th>
        </tr>       
    </tbody>
    <tbody>
        @foreach ($lpos as $lpo)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$lpo->created_at->format('d/m/Y')}}</td> 
            <td>{{$lpo->status}}</td>
            <td>{{$lpo->terms}}</td>
            <td>
                <a href="{{$view_lpo_url."/$lpo->id"}}" class="btn btn-primary btn-sm btn-outline">View</a>
                @if ($lpo->status == 'received')
                    {{-- <a href="{{$credit_note_url."/$lpo->id"}}" class="btn btn-primary btn-sm btn-outline">Credit note</a>
                    @if ($lpo->terms === "cash")
                        <a href="{{$attach_receipt_url."/$lpo->id"}}" class="btn btn-primary btn-sm btn-outline">Attach Receipt</a>                
                    @else
                        <a href="{{$attach_invoice_url."/$lpo->id"}}" class="btn btn-primary btn-sm btn-outline">Attach Invoice</a>
                    @endif                 --}}
                @else
                    <a href="{{$edit_lpo_url."/$lpo->id"}}" class="btn btn-primary btn-sm btn-outline">Edit</a>
                    <a href="{{$goods_received_url."/$lpo->id"}}" class="btn btn-success btn-sm btn-outline">Goods received</a>
                    <a href="{{$cancel_lpo_url."/$lpo->id"}}" class="btn btn-danger btn-sm btn-outline">Cancel</a>
                @endif
            </td>
        </tr>    
        @endforeach        
    </tbody>
</table>