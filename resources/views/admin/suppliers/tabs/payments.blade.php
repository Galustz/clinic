<h5 class="m-t-10">{{$is_editing?'Edit supplier equipment price':'Add payment to supplier'}}</h5>
<form action="{{$add_payment_url}}" class="form" method="POST">
    {{ csrf_field() }}
    <div class="row">
        <input type="hidden" name="supplier_id" value="{{$supplier->id}}"> 
        <div class="col-md-6 form-group">
            <label for="">Description</label>
            <textarea name="description" class="form-control" id="" cols="30" rows="3">{{$model->description??null}}</textarea>
        </div>
        <div class="col-md-6 form-group">
            <label for="">Amount</label>
            <input name="amount" type="number" class="form-control" value="{{$model->amount??null}}">
        </div>
        <div class="col-md-6 form-group">
            <label for="">Date</label>
            <input type="date" name="date" id="" class="form-control" value="{{$model->date??null}}">
        </div>
        <div class="col-md-6 text-right">
            <button class="btn btn-primary btn-sm" style="margin-top:32px;">Add payment</button>
        </div>
    </div>
</form>
@if (!$is_editing) 
<h5 class="m-t-10">All supplier payments</h5>
<table class="mytable m-t-10">
    <thead>
        <tr>
            <th>No</th>
            <th>Description</th>
            <th>amount</th>
            <th>date</th> 
            <th>Action</th>
        </tr>        
    </thead>
    <tbody>
        @foreach ($supplier->payments as $payment)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$payment->description}}</td>
            <td>{{$payment->amount}}</td>
            <td>{{$payment->date}}</td>   
            <td> 
                <a href="{{$payment_delete_url."/".$payment->pivot->id}}" class="btn btn-sm btn-danger">Remove</a>
            </td>
        </tr>
        @endforeach       
    </tbody>

</table>
@endif