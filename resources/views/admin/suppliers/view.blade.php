@extends('layouts.admin')
@section('content')
    <div class="container">
      {{-- @include('components.alert-message') --}}
        <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link {{$link=='details'?'active':''}}" href="{{url()->current().'?link=details'}}">Details</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{$link=='medicines'?'active':''}}" href="{{url()->current().'?link=medicines'}}">Medicines</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{$link=='equipments'?'active':''}}" href="{{url()->current().'?link=equipments'}}">Equipments</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{$link=='payments'?'active':''}}" href="{{url()->current().'?link=payments'}}">Payments</a>
            </li> 
            <li class="nav-item">
              <a class="nav-link {{$link=='lpos'?'active':''}}" href="{{url()->current().'?link=lpos'}}">Lpos</a>
            </li> 
        </ul>
        @if ($link=='details')
            @include($view_root.'.tabs.details')
        @endif
        @if ($link=='medicines')
            @include($view_root.'.tabs.medicines')
        @endif
        @if ($link=='equipments')
            @include($view_root.'.tabs.equipments')
        @endif
        @if ($link=='lpos')
            @include($view_root.'.tabs.lpos')
        @endif
        @if ($link=='payments')
            @include($view_root.'.tabs.payments')
        @endif
    </div>
@endsection