<div class="col-md-4">
    <div class="card text-white">
        <div class="card-header bg-success">
            {{$title??''}}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-10">
                    <h2 class="text-success">
                        {{$stat??''}}
                    </h2>
                </div>
                <div class="col-md-2">
                    <div class="">
                        <i class="fas {{$icon??''}} text-success" style="font-size: 20px; margin-top: 10px;"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>