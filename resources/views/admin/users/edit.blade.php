@extends('layouts.admin')
@section('content')

<div class="container">
    <div class="card card-body">

        <form class="" enctype="multipart/form-data" method="post" action="{{ url('/admin/users/edit') }}">
            {{ csrf_field() }}
            <input type="text" name="id" id="user-id" hidden>
            <div class="modal-body">

                <div class="row form-group">
                    <input type="hidden" name="id" value="{{$user->id}}">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="employee_id" class=" form-control-label">Employee No.</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div>
                                <input type="text" name="employee_id" id="employee_id" value="{{$user->employee_id}}" class="form-control" required>
                            </div>
                        </div>
                        @if ($errors->has('employee_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('employee_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name" class=" form-control-label">Name</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" name="name" id="name" value="{{$user->name}}" class="form-control" required>
                            </div>
                        </div>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" class=" form-control-label">Email</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-at"></i>
                                </div>
                                <input type="email" name="email" id="email" value="{{$user->email}}" class="form-control" required>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    {{-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="role" class=" form-control-label">User Role</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <select name="role" id="role" class="form-control" required>
                                    <option value="">Please select...</option>
                                    <option value="receptionist" {{$user->hasRole('receptionist')?'selected':''}}>Receptionist</option>
                                    <option value="laboratorist" {{$user->hasRole('laboratorist')?'selected':''}}>Laboratorist</option>
                                    <option value="pharmacist" {{$user->hasRole('pharmacist')?'selected':''}}>Pharmacist</option>
                                    <option value="doctor" {{$user->hasRole('doctor')?'selected':''}}>Doctor</option>
                                    <option value="admin" {{$user->hasRole('admin')?'selected':''}}>Administrator</option>
                                </select>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="password" class=" form-control-label">New Password</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="password" id="password" name="password" class="password-match form-control">
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="passconfrim" class="form-control-label">Confirm Password</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="password" id="password_confirmation" name="password_confirmation" data-equal-to="password" data-msg-error="Passwords do not match." class="confirm-password-match form-control">
                            </div>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
        </form>
    </div>
</div>
@endsection
