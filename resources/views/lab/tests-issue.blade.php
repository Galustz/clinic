@php
    use App\LabList;
    use App\LabTestRecord;
    use App\Patient;

    $labentry = LabList::find($id);
    $prn = $labentry->patient_registration_no;
    $pvn = $labentry->visit_no;

    $patient = Patient::find($prn); 
    $labrecords = LabTestRecord::where('patient_registration_no',$prn)->where('visit_no',$pvn)->get(); 

    $now = new DateTime;

    $age= $now->diff(date_create_from_format('Y-m-d', $patient->DOB))->y;

    $servicecost = 0; 
@endphp
<div class="card-header">Prescription Details</div>
<div class="card card-body" style="font-size:12px;"> 
    <div class="row">
        <div class="col-md-4">
            <P class="font-small">Name:</P>    
            <p><strong>{{$patient->name}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Reg No:</p>
            <p> <strong>{{$patient->reg_no}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Age:</p>
            <p><strong>{{$age}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Gender:</p>
            <p><strong>{{$patient->gender}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Occupation:</p>
            <p><strong>{{$patient->occupation}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Doctor:</p>
            <p><strong>Doctor 1</strong></p>
        </div>
    </div>  <br> 
    <form action="{{url('billing/payTestBill')}}" method="POST"> 
    {{ csrf_field() }}
    <div class="row"> 
            <div class="table-responsive">
                <table class="table col-lg-12 col-md-12 col-sm-12">
                    <tbody>
                        @foreach($labrecords as $labrecord)
                        <tr>
                        <td>{{$labrecord->lab_test->name}}</td>
                        <td>{{$labrecord->lab_test->price}} Tsh</td>
                        <td>
                            <input type="hidden" name="lab_record_id[]" value="{{$labrecord->id}}">
                            <select name="labtest_status[]" class="medstatus" onchange="newtestcost({{$labrecord->id}})" id="medstatus{{$labrecord->id}}">
                                    <option value="Testing">Testing</option>
                                    <option value="Refused">Refused</option>
                                </select>
                            </td>  
                        </tr>  
                        @php 
                            $servicecost = $servicecost + $labrecord->lab_test->price;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div> 
    </div> <hr>
    <input type="hidden" name="prn" value="{{$prn}}">
    <input type="hidden" name="pvn" value="{{$pvn}}">
    <input type="hidden" name="lab_list_id" value="{{$id}}">
    @include('layouts.billing-form-fields-sm')
    <br> 
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Submit
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>