@php
    use App\BillingList;
    use App\Patient;
    use Carbon\Carbon;
    use App\Vital;
    use App\NoteFromDoctor;
    use App\NoteFromLaboratory;

    $pts = BillingList::find($id); 
    $patient = Patient::find($pts->patient_registration_no);
    $date = date_create_from_format('Y-m-d', $patient->DOB);   
    $reference = Carbon::now();
    $diff = $reference->diff($date);
    $years = $diff->y;
    $nfdoctor = NoteFromDoctor::where('billing_id',$id)->first(); 
    $nflab = NoteFromLaboratory::where('billing_id',$id)->first(); 
    // dd($id);
    $nfd = !empty($nfdoctor)?$nfdoctor->content:'No content';
    $nfl = !empty($nflab)?$nflab->content:'';  
@endphp

@extends('home')
@section('section')
<div style="padding:40px;">
    <a  onclick="window.history.back()" class="btn btn-link">Back</a>
    @if((Auth::user()->hasRole('laboratorist')||Auth::user()->hasRole('admin'))&&Request::is('laboratory/*'))
    <div class="card card-body">
        <h4 class="">Patient Details</h4>
        <hr>
        <div class="row">
            <div class="col-lg-2">
                Date: <br> <strong>{{$pts->created_at->format('d/m/Y')}}</strong> 
            </div>
            <div class="col-lg-1">
                Reg no:<br> <strong>{{$patient->reg_no}}</strong></strong>
            </div>
            <div class="col-lg-2">
                Name:<br> <strong>{{$patient->name}}</strong>
            </div>
            <div class="col-lg-1">
                Age:<br> <strong>{{$years}}</strong>
            </div>
            <div class="col-lg-1">
                Gender:<br> <strong>{{$patient->gender}}</strong>
            </div>
            {{-- <div class="col-lg-2">
                Occupation:<br> <strong>{{$patient->Occupation}}</strong>
            </div> --}}
        </div> 
    </div>
    @endif
    @if((Auth::user()->hasRole('laboratorist')||Auth::user()->hasRole('admin'))&&Request::is('laboratory/*'))
    <div class="card card-body">
        <label for="nfd">Note from doctor:</label>
        <textarea class="form-control" name="" id="nf" rows="3" readonly>{{$nfd}}</textarea>
    </div>
    @endif
    @include('lab.test-fields')
     
</div>

@endsection