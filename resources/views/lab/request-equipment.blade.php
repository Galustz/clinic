@php 
    use App\RequestEquipment;

    $requests = RequestEquipment::all();
@endphp
<div class=""> 
    <h4>Request new equipment</h4>
    <div class="card card-body">    
        <form action="{{url('laboratory/equipment/request?ew=w')}}" method="POST">
            {{csrf_field()}}
            <div class="row">
                <div class="col-lg-5 form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" class="form-control">
                </div> 
                <div class="col-lg-2">
                    <button class="btn btn-primary" style="margin-top:30px;">Request</button>
                </div>
            </div>
        </form>  
    </div>
    <h4>Request Gloves</h4>
    <div class="col-md-12 card card-body">
            <form action="{{url('billing/getgloves')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="">No of gloves:</label>
                    <input class="form-control" type="number" name="no_of_gloves" id="">
                </div>
                <button class="btn btn-primary" type="submit">Request</button>
            </form>
    </div>
</div>
<br>
<table id="patient-que-table" class="datatable table table-borderless table-data3">
    <thead>
        <tr>
            {{-- <th>Number</th> --}}
            <th>Date</th> 
            <th>Name</th>   
            <th>Status</th>  
            <th></th> 
        </tr>
    </thead>
    <tbody>
        @foreach($requests as $request)
            <tr>
                <td>{{$request->created_at->format('d/m/Y')}}</td>
                <td>{{$request->name}}</td>
                <td>{{$request->status}}</td>
                <td><a class="btn btn-primary" href="{{url('/laboratory/equipment/edit',$request->id)}}">Edit</a></td>
            </tr>
        @endforeach
    </tbody>
</table>