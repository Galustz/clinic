@php
    use App\LabList;
    use App\Patient;
    use App\MedicineRecords;
    
    $lablist = LabList::where('billing_id',0)->get();
@endphp
@if(!empty($success))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
        <span class="badge badge-pill badge-success">Success</span>
        {{$success}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif
  <section class="p-t-20"> 
            <div class="row">
                <div class="col-lg-7">
                    <h3 class="">Patients List</h3>                  
                      <div class="row">
                          <div class="col-md-12">
                              <!-- DATA TABLE-->
                              <div class="table-responsive m-b-40">
                                  <table id="patient-que-table" class="table table-borderless table-data3">
                                      <thead>
                                          <tr>
                                              <th>Number</th></th>
                                              <th>Reg no</th>
                                              <th>Name</th>
                                              <th>Type</th>
                                              <th>Doctor</th>
                                              <th></th> 
                                          </tr>
                                      </thead>
                                      <tbody>
                                          @foreach ($lablist as $list)   
                                          @php
                                            $patient = Patient::find($list->patient_registration_no);
                                            $name = $patient->name;
                                            if($billingitem->family_help_id!=0)
                                                $name = $name.'\'s '.$billingitem->familyHelp->relationship;
                                          @endphp
                                            <tr class="">                                          
                                                <td>{{$list->id}}</td>
                                                <td>{{$list->patient_registration_no}}</td>
                                                <td>{{$patient->name}}</td>
                                                <td>{{$patient->type}}</td>
                                                <td>Doctor 1</td>
                                                <td><a class="btn btn-primary" href="{{route('test-details',$list->id)}}">Bill</a></td>                                        
                                            </tr> 
                                          @endforeach
                                      </tbody>
                                  </table>
                              </div>
                              <!-- END DATA TABLE                  -->
                          </div>
                      </div>
                </div>
                @if(!empty($id))
                <div class="col-lg-5" style="padding-top:27px; padding-right:40px;">
                    @include('lab.tests-issue')
                </div>
                @endif
            </div> 
    </section>
    <style>
        .table-row:hover td{
            cursor: pointer; 
            background-color: grey;
            color:white;
        }
        
        .active-row td{
            background-color: grey;
            color:white;
        }
    </style>