@php
    use App\BillingList;
    use App\Patient;
    use App\LabTestRecord;
    $queue = BillingList::where('status','taken sample')->where('created_at','>=',Carbon\Carbon::today())->get(); 
@endphp
<div class="container">
<div class="row">
        <div class="col-md-12">
                <!-- DATA TABLE-->
                <div class="table-responsive m-b-40">
                    <table id="patient-que-table" class="table datatable mytable">
                        <thead>
                            <tr>
                                <th>Lab Que #</th>
                                <th>Time</th>
                                <th>Reg. no</th>
                                <th>Name</th>
                                <th>From</th>
                                <th>Tests</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($queue as $q)
                            @php
                                $patient = Patient::find($q->patient_registration_no);
                                $tests = LabTestRecord::where('patient_registration_no',$q->patient_registration_no)->where('visit_no',$q->visit_no)->where('family_help_id',$q->family_help_id)->get();
                            
                                $name = $patient->name;
                                if($q->family_help_id!=0)
                                    $name = $name.'\'s '.$q->familyHelp->relationship;
                            @endphp
                                <tr>
                                    <td>{{$q->queue_no}}</td>
                                    <td>{{$q->updated_at->format('h:i a')}}</td>
                                    <td>{{$q->patient_registration_no}}</td>
                                    <td>{{$name}}</td>
                                    <td>Doctor</td>
                                    <td>
                                        @foreach($tests as $t)
                                            {{$t->lab_test->name}},
                                        @endforeach
                                    </td>
                                    <td><a class="btn btn-primary" href="{{url('laboratory/test',$q->id)}}">Add results</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- END DATA TABLE                  -->
            </div>
</div>
</div>