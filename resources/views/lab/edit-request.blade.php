@php
    use App\RequestEquipment;
    $request = RequestEquipment::find($id);
@endphp
@extends('home')
@section('section')
<div class="container" style="padding-top:55px;">
    
<div class="card">
        <div class="card-header">
                <h3 class="col-lg-12">Edit equipment request</h3>
        </div>
        <div class="card-body">    
            <form action="{{url('laboratory/equipment/edit/save')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <input type="hidden" value="{{$request->id}}" name="id">
                    <div class="col-lg-5 form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" value="{{$request->name}}" class="form-control">
                    </div>
                    <div class="col-lg-2 form-group">
                        <label for="">Status</label><br>
                        <select name="status" id="">
                            <option value="Recieved">Recieved</option>
                            <option value="Not Recieved">Not Recieved</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-primary" style="margin-top:30px;">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection