@extends('layouts.admin')
@section('content')
@php
    use App\LabTest;
    $labtest = LabTest::find($id);
@endphp
<div class="container">
    <div class="card card-body">
        <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/laboratory/tests/edit')}}">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="row form-group">
                    <input type="text" name="id" id="service_id" hidden>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                @php
                                    if($labtest->name!="Stool"&&$labtest->name!="FBP"&&$labtest->name!="Urinal"){
                                        $readonly= "";
                                    } 
                                    else {
                                        $readonly = "readonly";
                                    }
                                @endphp
                                
                                <input type="text" placeholder="Name" name="name" value="{{$labtest->name}}" id="service" class="form-control" {{$readonly}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                <input type="number" name="price" placeholder="Price" value="{{$labtest->price}}" id="price" class="form-control">
                                <span style="padding-top:10px;padding-left:10px;">. Tsh</span>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$labtest->id}}">
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                {{--
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div> --}}
                                <input type="text" placeholder="Code" name="code" value="{{$labtest->code}}" id="service" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div> 
</div>
@endsection