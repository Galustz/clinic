@php
    use App\PharmacyList;
    use App\MedicineRecord;
    use App\Patient;

    if(!empty($medicine_id)){
        $id = $medicine_id;
        $pharmacyentry = PharmacyList::find($id); 
        $prn = $pharmacyentry->patient_registration_no;
        $pvn = $pharmacyentry->visit_no;

        $patient = Patient::find($prn); 
        $medicinerecords = MedicineRecord::where('patient_registration_no',$prn)->where('visit_no',$pvn)->get(); 

        $now = new DateTime;

        $age= $now->diff(date_create_from_format('Y-m-d', $patient->DOB))->y;

        $servicecost = 0;
    }
@endphp
@if(!empty($medicine_id))
<div class="card-header">Prescription Details</div>
<div class="card card-body" style="font-size:12px;"> 
    <div class="row">
        <div class="col-md-4">
            <P class="font-small">Name:</P>    
            <p><strong>{{$patient->name}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Reg No:</p>
            <p> <strong>{{$patient->reg_no}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Age:</p>
            <p><strong>{{$age}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Gender:</p>
            <p><strong>{{$patient->gender}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Occupation:</p>
            <p><strong>{{$patient->occupation}}</strong></p>
        </div>
        <div class="col-md-4">
            <p>Doctor:</p>
            <p><strong>Doctor 1</strong></p>
        </div>
    </div>  <br> 
    <form action="{{url('billing/payMedicineBill')}}" method="POST"> 
    {{ csrf_field() }}
    <div class="row"> 
            <div class="table-responsive">
                <table class="table col-lg-12 col-md-12 col-sm-12">
                    <tbody>
                        @foreach($medicinerecords as $medicinerecord)
                        <tr>
                        <td>{{$medicinerecord->medicine->name}}</td>
                        <td>{{$medicinerecord->quantity}}</td>
                            <td>{{$medicinerecord->perday}} per day</td>
                            <td>{{$medicinerecord->days}} days</td>
                            <td>
                            <input type="hidden" name="medicine_record_id[]" value="{{$medicinerecord->id}}">
                            <select name="medicine_status[]" class="medstatus" onchange="newservicecost({{$medicinerecord->id}})" id="medstatus{{$medicinerecord->id}}">
                                    <option value="Taken">Taken</option>
                                    <option value="Refused">Refused</option>
                                </select>
                            </td> 
                        </tr>  
                        @php 
                            $servicecost = $servicecost + ($medicinerecord->medicine->selling_price * $medicinerecord->days);
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div> 
    </div> <hr>
    <input type="hidden" name="prn" value="{{$prn}}">
    <input type="hidden" name="pvn" value="{{$pvn}}">
    <input type="hidden" name="pharmacy_list_id" value="{{$id}}">
    @include('layouts.billing-form-fields-sm')
    <br> 
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Submit
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>
@endif