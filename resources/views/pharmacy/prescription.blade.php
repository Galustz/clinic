@extends('home')
@section('section')
<div class="default-tab">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="prescription-list-tab" data-toggle="tab" href="#prescription-list" role="tab" aria-controls="prescription-list"
        aria-selected="true" ><i class="fas fa-list"></i> Medicine Prescriptions</a>
      </div>
    </nav>
    <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                                    
      <div class="tab-pane fade show active" id="prescription-list" role="tabpanel" aria-labelledby="prescription-list-tab">
        @include('billings.prescription')
      </div>
    </div>
  </div>
  @include('medicine.med-list')
  @endsection
  