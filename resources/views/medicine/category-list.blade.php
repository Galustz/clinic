  <!-- DATA TABLE-->
  <section class="p-t-20">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <h3 class="title-5 m-b-35">Medicine Categories</h3>

                  <div class="row">
                      <div class="col-md-12">
                          <!-- DATA TABLE-->
                          <div class="table-responsive m-b-40">
                              <table id="patient-que" class="table datatable-1 table-borderless table-data3">
                                  <thead>
                                      <tr>
                                          <th>#</th>
                                          <th>Name</th>
                                          <th>description</th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>001</td>
                                          <td>Syrup</td>
                                          <td></td>
                                          <td>
                                              <div class="table-data-feature">
                                                  <a href="#" data-target="#editMedCategory" data-toggle="modal">
                                                    <button class="btn btn-success">
                                                        <i class="zmdi zmdi-edit"> Edit</i>
                                                    </button>
                                                  </a>
                                                  <button class="btn btn-danger">
                                                      <i class="zmdi zmdi-delete"> Delete</i>
                                                  </button>
                                              </div>
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                          <!-- END DATA TABLE                  -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- END DATA TABLE-->
