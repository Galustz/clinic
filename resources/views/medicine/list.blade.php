  <!-- DATA TABLE-->
  <section class="p-t-20">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <h3 class="title-5 m-b-35">Medicines</h3>

                  <div class="row">
                      <div class="col-md-12">
                          <!-- DATA TABLE-->
                          <div class="table-responsive m-b-40">
                              <table id="patient-que" class="table datatable-1 table-borderless table-data3">
                                  <thead>
                                      <tr>
                                          <th>#</th>
                                          <th>name</th>
                                          <th>category</th>
                                          <th>quantity</th>
                                          <th>buying price</th>
                                          <th>selling price</th>
                                          <th>description</th>
                                          <th>supplier</th>
                                          <th>expiry date</th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>001</td>
                                          <td>panadol</td>
                                          <td>Tablets</td>
                                          <td>500</td>
                                          <td>TZS 400</td>
                                          <td>TZS 500</td>
                                          <td>For headache</td>
                                          <td>ABC Pharmacists</td>
                                          <td>05-2020</td>
                                          <td>
                                              <div class="table-data-feature">
                                                  <a href="#" data-target="#editMedicine" data-toggle="modal">
                                                    <button class="btn btn-success">
                                                        <i class="zmdi zmdi-edit"> Edit</i>
                                                    </button>
                                                  </a>
                                                  <a href="#" data-target="#viewMedicine" data-toggle="modal">
                                                    <button class="btn btn-primary">
                                                        <i class="zmdi zmdi-eye"> View</i>
                                                    </button>
                                                  </a>
                                                  <button class="btn btn-danger">
                                                      <i class="zmdi zmdi-delete"> Delete</i>
                                                  </button>
                                              </div>
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                          <!-- END DATA TABLE                  -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- END DATA TABLE-->
