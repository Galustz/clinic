<!-- modal edit Category -->
<div class="modal fade" id="editMedCategory" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="largeModalLabel">Edit Medicine Category</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
            <form enctype="multipart/form-data" method="post" action="" >
                {{ csrf_field() }}
				<div class="modal-body">
					<div class="row form-group">

						<div class="col-md-12">
							<div class="form-group">
								<label for="name" class=" form-control-label">Name</label>
								<input type="text" name="name" id="name" class="form-control" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="description" class=" form-control-label">Description</label>
								<textarea type="text" id="description" name="description" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Confirm</button>
				</div>
            </form>
		</div>
	</div>
</div>
<!-- end edit Category -->
