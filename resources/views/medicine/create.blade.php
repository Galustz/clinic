
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong><i class="fa fa-medkit"></i>   New Medicine</strong>
                        <small> Form</small>
					</div>
					<form enctype="multipart/form-data" method="post" action="" >
						{{ csrf_field() }}
						<div class="modal-body">
							<div class="row form-group">

								<div class="col-md-4">
									<div class="form-group">
										<label for="name" class=" form-control-label">Name</label>
										<input type="text" name="name" id="name" class="form-control" required>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="medicine_category_id" class=" form-control-label">Category</label>
										<select name="medicine_category_id" id="medicine_category_id" class="form-control">
											<option value="0">Please select</option>
											<option value="1">Option #1</option>
											<option value="2">Option #2</option>
											<option value="3">Option #3</option>
										</select>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="quantity" class=" form-control-label">Quantity</label>
										<input type="number" name="quantity" id="quantity" class="form-control" required>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="buying_price" class=" form-control-label">Buying Price</label>
										<input type="number" name="buying_price" id="buying_price" class="form-control">
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="selling_price" class=" form-control-label">Selling Price</label>
										<input type="number" id="selling_price" name="selling_price" class="form-control" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="manufacturer" class=" form-control-label">Manufacturer</label>
										<input type="text" id="manufacturer" name="manufacturer" class="form-control">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="description" class=" form-control-label">Description</label>
										<textarea type="text" id="description" name="description" class="form-control"></textarea>
									</div>
								</div>
							</div>		
						</div>
						<div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>