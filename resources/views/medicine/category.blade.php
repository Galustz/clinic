
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong><i class="fa fa-medkit"></i>   New Medicine Category</strong>
					</div>
					<form enctype="multipart/form-data" method="post" action="" >
						{{ csrf_field() }}
						<div class="modal-body">
							<div class="row form-group">

								<div class="col-md-12">
									<div class="form-group">
										<label for="name" class=" form-control-label">Name</label>
										<input type="text" name="name" id="name" class="form-control" required>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="description" class=" form-control-label">Description</label>
										<textarea type="text" id="description" name="description" class="form-control"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
