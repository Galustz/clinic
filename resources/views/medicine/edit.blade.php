<!-- modal edit midicine -->
<div class="modal fade" id="editMedicine" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="largeModalLabel">Edit Medicine</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" method="post" action="" >
				{{ csrf_field() }}
				<div class="modal-body">
					<div class="row form-group">

						<div class="col-md-4">
							<div class="form-group">
								<label for="name" class=" form-control-label">Name</label>
								<input type="text" name="name" id="name" class="form-control" required>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label for="medicine_category_id" class=" form-control-label">Category</label>
								<select name="medicine_category_id" id="medicine_category_id" class="form-control">
									<option value="0">Please select</option>
									<option value="1">Option #1</option>
									<option value="2">Option #2</option>
									<option value="3">Option #3</option>
								</select>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label for="quantity" class=" form-control-label">Quantity</label>
								<input type="number" name="quantity" id="quantity" class="form-control" required>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label for="buying_price" class=" form-control-label">Buying Price</label>
								<input type="number" name="buying_price" id="buying_price" class="form-control">
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label for="selling_price" class=" form-control-label">Selling Price</label>
								<input type="number" id="selling_price" name="selling_price" class="form-control" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="manufacturer" class=" form-control-label">Manufacturer</label>
								<input type="text" id="manufacturer" name="manufacturer" class="form-control">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="description" class=" form-control-label">Description</label>
								<textarea type="text" id="description" name="description" class="form-control"></textarea>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Confirm</button>
				</div>
			</form>
		</div>
	</div>
</div>
	<!-- end edit medicine -->
