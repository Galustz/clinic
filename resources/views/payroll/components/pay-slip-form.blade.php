@php
    if(!isset($editable))
        $user = \App\User::find($item->id);
    else{
        $user = $item->user; 
    }
@endphp
@if ($user)
<div class="col-md-12">
    <hr>
    <div class="row">
        <div class="col-md-8">
            <table class="table borderless">
                <tr>
                    <th>Employee name</th>
                    <th>Joining date</th>
                    <th>Designation</th>
                    {{-- <th>Phone</th> --}}
                    {{-- <th>Address</th> --}}
                </tr>
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->form_joining_date}}</td>
                    <td>{{$user->role->name}}</td>
                    {{-- <td>{{$user->phone_no}}</td> --}}
                    {{-- <td>{{$user->address}}</td> --}}
                </tr>
            </table>
        </div>
        <div class="col-md-4" style="border-left: 1px solid gray;">
            <h4>Net Salary</h4><br>
            @if (isset($editable))
                <h5>{{number_format($item->net_salary)}} Tsh</h5>
            @else 
                <input type="number" class="form-control" value="{{$user->salary}}" name="net_salaries[]">
            @endif
            <input type="hidden" name="employee_ids[]" value="{{$user->id}}">
        </div>
    </div>
    <hr>
</div>     
@endif
