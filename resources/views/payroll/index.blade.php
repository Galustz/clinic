@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <form action="{{url()->current()}}" method="GET" class="col-md-12">
                <div class="row">
                    {{-- <div class="col-md-3">
                        <label for="">Employee</label>
                        <select name="employee_id" id="" class="form-control">
                            <option value="0" {{$employee_id == 0? 'selected':''}}>All</option>
                            @foreach ($employees as $employee)
                                <option value="{{$employee->id}}" {{$employee_id == $employee->id?'selected':''}}>{{$employee->name}}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    <div class="col-md-3">
                        <label for="">Month</label>
                        <select name="month" id="" class="form-control">
                            @foreach ($months as $key=>$m)
                                <option value="{{$key}}" {{$key==$month?'selected':''}}>{{$m}}</option>
                            @endforeach 
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="">Year</label>
                        <select name="year" id="" class="form-control">
                            @foreach ($years as $y)
                                <option value="{{$y}}" {{$year == $y? 'selected':''}}>{{$y}}</option>
                            @endforeach 
                        </select>
                    </div>
                    <input type="hidden" name="generate" value="sf">
                    <div class="col-md-3">
                        <button class="btn btn-outline-primary btn-sm" style="margin-top:35px">Generate</button>
                    </div>
                </div> 
            </form>
        </div>
    </div>
    <div class="container mt-2">
        @if (isset($payroll_items))
        <form action="{{url('payroll/save')}}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="month" value="{{$month}}">
            <input type="hidden" name="year" value="{{$year}}">
            @if(isset($editable))
                <div class="row">
                    {{-- <div class="col-md-2">
                        Date: <strong>{{$payroll->date}}</strong>
                    </div> --}}
                    <div class="col-md-3">
                        Total amount: <strong>{{$payroll->total_amount_formatted}}</strong>
                    </div>
                </div>
            @endif
            @foreach ($payroll_items as $item)
                <div class="row"> 
                    @include('payroll.components.pay-slip-form')
                </div>                
            @endforeach
            @if (!isset($editable))
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary">Save</button>
                    </div>
                </div>                
            @endif
        </form>
        @endif        
    </div>

@endsection