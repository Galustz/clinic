@extends('components.add-form')
@section('form')
<div class="col-md-6 form-group">
    <label for="">Description</label>
    <textarea name="description" id="" cols="30" rows="2" class="form-control">{{$model->description??null}}</textarea>
</div>
<div class="col-md-6 form-group">
    <label for="">Amount</label>
    <input name="amount" id="" type="number" class="form-control" value="{{$model->amount??null}}"/>
</div> 
<div class="col-md-6 form-group">
    <label for="">Date</label>
    <input name="date" id="" type="date" class="form-control" value="{{$model->date??null}}"/>
</div> 
@endsection