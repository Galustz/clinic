@extends('components.add-form')

@section('form')
<div class="col-md-6 form-group">
    <label for="">Company name</label>
    <input type="text" name="name" id="" value="{{$model->name??null}}" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Company code</label>
    <input type="text" name="code" id="" value="{{$model->code??null}}" class="form-control">
</div>
<div class="col-md-6 form-group"> 
    <label for="">Contact person name</label>
    <input type="text" name="contact_person" id="" value="{{$model->contact_person??null}}" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Contact person phone number</label>
    <input type="number" name="contact_person_phone_no" id="" value="{{$model->contact_person_phone_no??null}}" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Email</label>
    <input type="email" name="email" id="" value="{{$model->email??null}}" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Address</label>
    <textarea name="address" id="" cols="30" rows="3" class="form-control">{{$model->address??null}}</textarea>
</div>
@endsection