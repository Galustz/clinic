@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Corporate companies",
            "button"=>[
                "name"=>"Add new corporate company",
                "color"=>"primary",
                "url"=>$new_cc_url
            ]
        ])
        @include('components.table',[
            "columns"=>["No","Name","Code","Contact person","Phone","Email","Address",""],
            "collection"=>$corporate_companies,
            "data_columns"=>[
                "iteration",
                "name",
                "code",
                "contact_person",
                "contact_person_phone_no",
                "email",
                "address",
                "buttons"=>[
                    ["name"=>"Edit","color"=>"primary","url"=>$edit_cc_url,"id"=>true],
                    ["name"=>"Delete","color"=>"danger","url"=>$delete_cc_url,"id"=>true,"should_confirm"=>true,"action"=>"delete"]
                ]
            ]
        ])
    </div>
@endsection