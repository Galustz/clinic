@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            'header'=>'Inventory',
            "button"=>[
                'name'=>'Add item',
                'url'=>$add_item_url,
                'color'=>'primary',
                ],
            ])

        @include('components.table',[
            "columns"=>['No','Name',"Unit","Quantity","Action"],
            "collection"=>$inventory_records,
            "data_columns"=>[
                "iteration",
                "name",
                "unit",
                "quantity",
                "buttons"=>[
                    ["url"=>$edit_url,"id"=>true,"name"=>"Edit","color"=>"primary"],
                    ["url"=>$delete_url,"id"=>true,"name"=>"Delete","color"=>"danger","should_confirm"=>true,"action"=>"delete"],
                ]
            ]
        ])
    </div>
@endsection