@extends('components.add-form')
@section('form') 
<div class="col-md-6 form-group">
    <label for="">Name</label>
    <input type="text" name="name" value="{{$model->name??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Unit</label>
    <input type="text" name="unit" value="{{$model->unit??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Quantity</label>
    <input type="number" name="quantity" value="{{$model->quantity??''}}" id="" class="form-control">
</div> 
@endsection