@extends('layouts.admin')
@section('content')
<div class="container">
    @include('components.content-header',[
        "header"=>"Cash In"
    ]) 

    @include('components.table',[
        "columns"=>['No','Date Created',"Time","Amount","Type"],
        "collection"=>$data,
        "data_columns"=>[
            "iteration",
            "transaction_date",
            "time",
            "amount",
            "type"  
        ]
    ])
</div>    
@endsection