@extends('components.add-form')
@section('form')
    <div class="col-md-6">
        <label for="">Designation</label>
        <select name="designation_id" id="" class="form-control">
            <option value="0">All</option>
            @foreach ($designations as $designation)
                <option value="{{$designation->id}}">{{$designation->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-6">
        <label for="">Description <small>(optional)</small></label>
        <textarea name="description" id="" cols="30" rows="2" class="form-control">{{$model->description}}</textarea>
    </div>
    <div class="col-md-6">
        <label for="">Select file <small>{{$file_edit_warning}}</small></label>
        <input type="file" name="document" id="" class="form-control" {{$is_editing?'':'required'}}>
    </div>
@endsection