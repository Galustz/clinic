@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Insurance Companies", 
            'button'=>[
                "url"=>$new_ic_url,
                "name"=>"Add new insurance company",
                "color"=>"primary"
            ]
        ])
        @include('components.table',[
            "columns"=>["No","Name","Address","Code","Contact person","Phone","Email",""],
            "collection"=>$insurance_companies,
            "data_columns"=>[
                "iteration",
                "name",
                "address",
                "code",
                "contact_person",
                "contact_person_phone_no",
                "email",
                "buttons"=>[
                    ["name"=>"Edit","url"=>$edit_ic_url,"id"=>true,"color"=>"primary"],
                    ["name"=>"Delete","url"=>$delete_ic_url,"id"=>true,"color"=>"danger","should_confirm"=>true,"action"=>"delete"]
                ]
            ]
        ])
    </div>
@endsection