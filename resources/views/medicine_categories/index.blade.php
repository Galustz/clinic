@extends('layouts.admin')
@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Medicine Types",
            "button"=>[
                "name"=>"Create new type",
                "url"=>$add,
                "color"=>"primary"
            ]
        ])

        @include('components.table',[
            "columns"=>["No","Name","Action"],
            "collection"=>$medicine_categories,
            "data_columns"=>[
                "iteration",
                "name", 
                "buttons"=>[
                    ["name"=>"Edit","id"=>true,"url"=>$edit,"color"=>"primary"],
                    [
                        "name"=>"Delete",
                        "id"=>true,
                        "url"=>$remove,
                        "should_confirm"=>true,
                        "additional_confirm_message"=>"All medicines associated with this type will be deleted too!",
                        "action"=>"delete",
                        "color"=>"danger"
                    ]
                ]
            ]
        ])
    </div>
@endsection