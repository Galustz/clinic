@extends('components.add-form')
@section('form')
    <div class="col-md-6 form-group">
        <label for="">Name</label>
        <input type="text" class="form-control" name="name" value="{{$model->name??null}}">
    </div> 
@endsection