@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Quality control members",
            "button"=>[
                "color"=>"primary",
                "name"=>"Manage members",
                "url"=>$new_member_url
            ]])

        @include('components.table',[
            "columns"=>["No","Name","Designation","Action"],
            "collection"=>$members,
            "data_columns"=>[
                "iteration",
                "name",
                "designation",
                "buttons"=>[ 
                    ["name"=>"Appointment letter","url"=>$show_appointment_letter_url,"color"=>"primary","id"=>true],
                ]
            ]
        ])
    </div>
@endsection