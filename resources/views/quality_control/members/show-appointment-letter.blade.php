@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="container" style="border:1px solid lightgray; padding: 30px;"> 
            {!!$letter!!}
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 text-right">
                <button onclick="window.history.back()" class="btn btn-danger btn-sm">Back</button>
            </div>
        </div>
    </div>
@endsection