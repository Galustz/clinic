@extends('components.add-form')
@section('form')
    <div class="col-md-6 form-group">
        <label for="">Description</label>
        <textarea name="description" class="form-control" id="" cols="30" rows="3">{{$model->description??null}}</textarea>
    </div>
    <div class="col-md-6 form-group">
        <label for="">Amount</label>
        <input name="amount" type="number" class="form-control" value="{{$model->amount??null}}">
    </div>
    <div class="col-md-6 form-group">
        <label for="">Date</label>
        <input type="date" name="date" id="" class="form-control" value="{{$model->date??null}}">
    </div>
@endsection