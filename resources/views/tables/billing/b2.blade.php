@php
    //use App\PharmacyList;
    use App\BillingList;
    use App\Patient;
    use App\MedicineRecord;
    use App\LabTestRecord; 
    $billinglist = BillingList::where('billing_id',0)->get();
@endphp
@if(!empty($success))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
        <span class="badge badge-pill badge-success">Success</span>
        {{$success}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif
  <section class="p-t-20"> 
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="">Patients List</h3>                  
                      <div class="row">
                          <div class="col-md-12">
                              <!-- DATA TABLE-->
                              <div class="table-responsive m-b-40">
                                  <table id="patient-que-table" class="datatable table table-borderless table-data3">
                                      <thead>
                                          <tr>
                                              {{-- <th>Number</th> --}}
                                              <th>Reg no</th>
                                              <th>Name</th> 
                                              <th>Type</th>
                                              <th>Doctor</th>
                                              <th>Test/<br>Medicine</th>
                                              <th>Accepted/<br>Refused</th>
                                              <th>Bill</th>
                                              <th></th> 
                                          </tr>
                                      </thead>
                                      <tbody>
                                          @foreach ($billinglist as $billingitem)   
                                            @php
                                                $patient = Patient::find($billingitem->patient_registration_no);
                                                $prn = $billingitem->patient_registration_no;
                                                $pvn = $billingitem->visit_no;
                                                if($billingitem->type == "Pharmacy"){
                                                        $servs = MedicineRecord::where('patient_registration_no',$prn)->where('visit_no',$pvn)->get();
                                                }else if($billingitem->type == "Laboratory"){                                                        
                                                        $servs = LabTestRecord::where('patient_registration_no',$prn)->where('visit_no',$pvn)->get();                                                       
                                                }
                                                $sum=0;
                                                $laburl  = url('/billing/payTestBill');
                                                $medurl = url('/billing/payMedicineBill');
                                                $url = $billingitem->type=="Laboratory"?$laburl:$medurl;
                                            @endphp
                                            <tr>                                          
                                                {{-- <td>{{$billingitem->id}}</td> --}}
                                                <td>{{$billingitem->patient_registration_no}}</td>
                                                <td>{{$patient->name}}</td>
                                                <td>{{$billingitem->type}}</td> 
                                                <td>Doctor 1</td>
                                                <form action="{{$url}}" method="post">
                                                {{csrf_field()}}
                                                <td>
                                                    @if(!empty($servs))
                                                        @foreach($servs as $serv) 
                                                            {{$serv->item->name}} <br>
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!empty($servs))
                                                        @foreach($servs as $serv) 
                                                            @php
                                                                $price = $serv->item->price?$serv->item->price:$serv->item->selling_price;
                                                                $sum = $sum + $price;
                                                            @endphp
                                                            <input type="hidden" value="{{$serv->id}}" name="{{$billingitem->type=='Laboratory'?'lab_record_id[]':'medicine_record_id[]'}}">
                                                            <input type="checkbox" name="status[]" value="Taken" onchange="newSum({{$billingitem->id}},{{$serv->id}})" id="checkbox{{$billingitem->id.$serv->id}}" checked><span id="price{{$billingitem->id.$serv->id}}">{{$price}}</span><br>                                                           
                                                        @endforeach
                                                        Total: <span id="sum{{$billingitem->id}}">{{$sum}}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <span>Amount recieved:</span><input onkeyup="newDue({{$billingitem->id}})" style="margin-bottom:-20px;" type="number" class="form-control" name="amount_paid" id="recieved{{$billingitem->id}}" required><br>
                                                    <span>Due:</span><input style="margin-bottom:-20px;" type="number" class="form-control" name="amount_due" value="{{$sum}}" id="due{{$billingitem->id}}" readonly><br>
                                                    <span>Change:</span><input style="margin-bottom:-20px;" type="number" class="form-control" name="change" value="0"  id="change{{$billingitem->id}}" readonly><br>
                                                    <span>Change Returned:</span><input style="margin-bottom:-20px;" type="number" class="form-control" name="change_given"  id="" required><br>

                                                </td>
                                            <input type="hidden" name="list_id" value="{{$billingitem->id}}">
                                                <input type="hidden" name="service_cost" value="{{$sum}}" id="service_cost{{$billingitem->id}}">
                                                <input type="hidden" name="prn" value="{{$prn}}">
                                                <input type="hidden" name="pvn" value="{{$pvn}}">
                                                <td><button type="submit" href="" class="btn btn-primary">Bill</button></td>
                                                
                                            </form>
                                            </tr> 
                                          @endforeach
                                      </tbody>
                                  </table>
                              </div>
                              <!-- END DATA TABLE                  -->
                          </div>
                      </div>
                </div> 
            </div> 
    </section>
    <style>
        .table-row:hover td{
            cursor: pointer; 
            background-color: grey;
            color:white;
        }
        
        .active-row td{
            background-color: grey;
            color:white;
        }
    </style>