<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Auth::routes();
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('checkSession','HomeController@checkSession');

Route::middleware('auth')->group(function()
{
    Route::get('/',function()
    {
        if(Auth::user()->hasRole('admin'))
        {        
            return redirect('/admin');
        }
        if(Auth::user()->hasRole('doctor'))
        {        
            return redirect('/doctor');
        }
        if(Auth::user()->hasRole('consultant'))
        {        
            return redirect('/doctor');
        }
        if(Auth::user()->hasRole('pharmacist'))
        {        
            return redirect('/pharmacy');
        }
        if(Auth::user()->hasRole('laboratorist'))
        {        
            return redirect('/laboratory');
        }
        if(Auth::user()->hasRole('receptionist'))
        {         
            return redirect('/reception');
        }
        if(Auth::user()->hasRole('billing')){
            return redirect('/billing');
        } 
    });

    Route::prefix('billing')->namespace('Billing')->middleware(['App\Http\Middleware\BillingMiddleware'])->group(function(){
        Route::get('/','BillingController@index');
        Route::get('/testdetails/{id}','LaboratoryController@testDetails')->name('test-details');
        Route::post('/payTestBill','LaboratoryController@payBill');

        Route::get('/billings', 'BillingController@pharmacyBilling');
        Route::post('/payMedicineBill','PharmacyController@payBill');

        Route::post('/pay-for-appointment','BillingController@payForAppointment');
    });

    Route::prefix('admin')->namespace('Admin')->middleware(['App\Http\Middleware\AdminMiddleware'])->group(function()
    {
        Route::get('','AdminController@index');
        Route::get('/patients/list',function(){
            $title = 'Ádmininstrator :: Patients';
            return view('admin.patient',compact('title'));
        });
        Route::get('/appointment/list',function(){
            $title = 'Ádmininstrator :: Appointments';
            return view('admin.appointment',compact('title'));
        });
        Route::get('/appointment/upcoming',function(){
            $title = 'Ádmininstrator :: Upcoming Appoinment';
            return view('admin.upcoming',compact('title'));
        });
        Route::get('/medicine/list',function(){
            $title = 'Ádmininstrator :: Medicine';
            return view('admin.medicine',compact('title'));
        });
        Route::get('/billing/list','Reports\BillingController@list');
        Route::get('/services',function(){
            $title = 'Ádmininstrator :: Services';
            return view('admin.service',compact('title'));
        });
        Route::get('/servicemodes',function(){
            $title = 'Ádmininstrator :: Service Modes';
            return view('admin.servicemode',compact('title'));
        });
        Route::get('/users',function(){
            $title = 'Ádmininstrator :: Users';
            return view('admin.users',compact('title'));
        });
        Route::get('/settings',function(){
            $title = 'Ádmininstrator :: Settings';
            return view('admin.settings',compact('title'));
        });
        Route::get('/rooms',function(){
            $title = 'Ádmininstrator :: Rooms';
            return view('admin.room',compact('title'));
        });
        Route::get('/units',function(){
            $title = 'Ádmininstrator :: Unit Allocations';
            return view('admin.units',compact('title'));
        });

        Route::post('/user/create','AdminController@addUser');
        Route::post('/user/update','AdminController@updateUser');
        
        Route::post('/room/create','AdminController@addRoom');
        Route::post('/room/update','AdminController@updateRoom');
        Route::post('/service/create','AdminController@addService');
        Route::post('/service/update','AdminController@updateService');
        Route::post('/servicemode/create','AdminController@addServiceMode');
        Route::post('/servicemode/update','AdminController@updateServiceMode');
        Route::post('/unit/create','AdminController@addUnit');
        Route::post('/unit/update','AdminController@updateUnit');

        //new routes
        Route::prefix('reception')->group(function(){
            Route::get('/services','ReceptionController@services');
            Route::get('/services/delete/{id}','ReceptionController@deleteService');
            Route::post('/services/new','ReceptionController@newService');
            Route::get('/services/edit/{id}','ReceptionController@editServiceForm');
            Route::post('/services/edit/','ReceptionController@editService');

            Route::get('/patienttypes','ReceptionController@patientType');
            Route::get('/patienttypes/delete/{id}','ReceptionController@deletePatientType');
            Route::post('/patienttypes/new','ReceptionController@newPatientType');
            Route::get('/patienttypes/edit/{id}','ReceptionController@editPatientTypeForm');
            Route::post('/patienttypes/edit/','ReceptionController@editPatientType');

            Route::get('/servicemodes','ReceptionController@serviceMode');
            Route::get('/servicemodes/delete/{id}','ReceptionController@deleteServiceMode');
            Route::post('/servicemodes/new','ReceptionController@newServiceMode');
            Route::get('/servicemodes/edit/{id}','ReceptionController@editServiceModeForm');
            Route::post('/servicemodes/edit/','ReceptionController@editServiceMode');
        });

        Route::prefix('patients')->group(function(){
            Route::get('/view/{id}','PatientController@viewPatient'); 
            Route::get('/edit/{id}','PatientController@editPatientShow'); 
            Route::post('/edit/','PatientController@editPatient');
            Route::get('/history','\App\Http\Controllers\DoctorController@history');

            // Route::get('/patienttype','ReceptionController@patienttype');
            // Route::get('/patienttype/delete/{id}','ReceptionController@deletePatientType');
            // Route::post('/patienttype/new','ReceptionController@newPatientType');
        });


        Route::prefix('laboratory')->group(function(){
            Route::get('/tests','LaboratoryController@tests');
            Route::get('/tests/delete/{id}','LaboratoryController@deleteTest');
            Route::post('/tests/new','LaboratoryController@newTest');
            Route::get('/tests/edit/{id}','LaboratoryController@testEditForm');
            Route::post('/tests/edit/','LaboratoryController@testEdit');

            Route::get('/queue','LaboratoryController@queue');
            Route::get('/results','LaboratoryController@results');
        });

        Route::prefix('pharmacy')->group(function(){
            Route::get('/categories','PharmacyController@categories');
            Route::get('/categories/delete/{id}','PharmacyController@deleteCategory');
            Route::post('/categories/new','PharmacyController@newCategory');



            Route::get('/medicines','PharmacyController@medicines');
            Route::get('/medicines/delete/{id}','PharmacyController@deleteMedicine');
            Route::get('/medicines/new','PharmacyController@newMedicineForm');
            Route::post('/medicines/new','PharmacyController@newMedicine');
            Route::post('/medicines/save','PharmacyController@saveMedicine');
            Route::get('/medicines/view/{id}','PharmacyController@viewMedicine');
            
            Route::get('/medicines/newBatch/{id}','PharmacyController@newBatchForm');

            Route::get('/medicines/editBatch/{id}','PharmacyController@editBatchForm');
            Route::post('/medicines/editBatch','PharmacyController@editBatch');

            Route::post('/medicines/newBatch','PharmacyController@newBatch');

            Route::get('/test','PharmacyController@getMd');

            Route::get('/suppliers','PharmacyController@suppliers');
            Route::get('/suppliers/new','PharmacyController@newSupplierForm');
            Route::post('/suppliers/new','PharmacyController@newSupplier');
            Route::get('/supplier/view/{id}','PharmacyController@viewSupplier');

            Route::get('/lpo','PharmacyController@lpo');
            Route::get('/lpo/new','PharmacyController@newLpoForm');
            Route::post('/lpo/new','PharmacyController@newLpo');


        });

        Route::prefix('users')->group(function(){
            Route::get('/','UsersController@index');

            Route::get('/units','UsersController@units');
            Route::get('/units/delete/{id}','UsersController@deleteUnit');
            Route::post('/units/new','UsersController@newUnit');

            Route::get('/rooms','UsersController@rooms');
            Route::get('/rooms/delete/{id}','UsersController@deleteRoom');
            Route::post('/rooms/new','UsersController@newRoom');
            
            Route::get('/delete/{id}','UsersController@deleteUser');
            Route::get('/edit/{id}','UsersController@editUserForm');
            Route::post('/edit','UsersController@editUser');
        });

        Route::prefix('suppliers')->group(function(){
            Route::get('/','SuppliersController@index');
            Route::get('/add','SuppliersController@addForm');
            Route::get('/edit/{supplier_id}','SuppliersController@editForm');
            Route::get('/delete/{supplier_id}','SuppliersController@delete');
            Route::post('/add','SuppliersController@add');
            Route::post('/edit','SuppliersController@edit');
            Route::get('/view/{id}','SuppliersController@view');

            Route::get('/medicine/delete/{supplier_medicine_id}','SuppliersController@deleteMedicine');
            Route::get('/medicine/edit/{supplier_medicine_id}','SuppliersController@view');
            Route::post('/medicine/add','SuppliersController@addMedicine');
            
            Route::get('/equipment/delete/{supplier_medicine_id}','SuppliersController@deleteEquipment');
            Route::get('/equipment/edit/{supplier_medicine_id}','SuppliersController@view');
            Route::post('/equipment/add','SuppliersController@addEquipment');

            Route::get('/payment/delete/{supplier_medicine_id}','SuppliersController@deletePayment'); 
            Route::post('/payment/add','SuppliersController@addPayment');

            Route::prefix('lpo')->group(function(){
                Route::get('add/{supplier_id}','SupplierLpoController@addLpoForm');
                Route::post('add/{supplier_id?}','SupplierLpoController@addLpo');
                Route::get('received/{lpo_id}','SupplierLpoController@goodsReceivedForm');
                Route::post('received','SupplierLpoController@goodsReceived');
                Route::get('cancel/{lpo_id}','SupplierLpoController@cancelLpo');
                Route::get('attachinvoice/{lpo_id}','SupplierLpoController@attachLpoInvoice');
                Route::get('attachreceipt/{lpo_id}','SupplierLpoController@attachReceipt');
                Route::get('creditnote/{lpo_id}','SupplierLpoController@creditNote');
                Route::get('view/{lpo_id}','SupplierLpoController@viewLpo');
                Route::get('edit/{lpo_id}','SupplierLpoController@editLpoForm');
                Route::post('edit','SupplierLpoController@editLpo');
            });
        });
        Route::prefix('reports')->namespace('Reports')->group(function(){
            Route::prefix('mtuha')->group(function(){
                Route::get('/','MtuhaController@index');
            });
            Route::prefix('/')->group(function(){
                Route::get('new-patients','BillingController@newPatients');
                Route::get('new-patients/filter','BillingController@newPatientsFilter');

                Route::get('express-patients','BillingController@newPatients');
                Route::get('patients-per-disease','BillingController@patientsPerDisease');
                Route::post('filter-patients-per-disease','BillingController@filterPatientPerDiseaseByDate');
                Route::get('/billing','BillingController@index');
            });
            Route::prefix('reception')->group(function(){
                Route::get('/','ReceptionController@index');
            });
            Route::prefix('/')->group(function(){
                Route::get('equipments','LaboratoryController@equipments');
                Route::get('equipments/filter','LaboratoryController@equipmentsFilter');
                Route::get('/billing','LaboratoryController@index');
            });
            Route::prefix('/')->group(function(){
                Route::get('/medicine-sold','PharmacyController@medicineSold');
                Route::post('/medicine-sold/filter','PharmacyController@filterMedicineSold');
                Route::get('/pharmacy','PharmacyController@index');
            });
            Route::prefix('/')->group(function(){
                Route::get('consultations','DoctorController@consultations');
                Route::get('consultations/filter','DoctorController@consultationsFilter');
                Route::get('/doctor','DoctorController@index');
            });
            Route::prefix('stock')->group(function(){
                Route::get('/','StockController@index');
                Route::get('/filter','StockController@filter');
            });
        });

        Route::prefix('trash')->group(function(){
            Route::get('services','TrashController@services');
            Route::get('patienttypes','TrashController@patienttypes');
            Route::get('servicemodes','TrashController@servicemodes');
            Route::get('tests','TrashController@tests');
            Route::get('medicines','TrashController@medicine');
        });

        Route::prefix('settings')->group(function(){
            Route::get('/','SettingsController@index');
            Route::post('/set','SettingsController@set');
            Route::post('/update','SettingsController@updateSettings');



        }); 
    });

    Route::prefix('doctor')->middleware(['middleware' => 'App\Http\Middleware\DoctorMiddleware'])->group(function()
    {
        Route::get('/', 'DoctorController@index')->name('doctor');
        Route::get('/patient/detail/{id}','DoctorController@patientDetails')->name('patient-details');
        Route::post('/patient/diagnosis','DoctporController@patientDiagnosis')->name('patient-diagnosis');
        Route::post('/patient/familyhelp','DoctorController@familyHelp');
        Route::get('/patient/visit/{id}','DoctorController@patientVisit')->name('patient-visit');
        Route::get('/history','DoctorController@history');
        Route::get('/history/{id}','DoctorController@getVisit');
        Route::get('/updateVisit','DoctorController@updateVisit');
        Route::get('/patient/edit/{id}','DoctorController@editPatientForm');
        Route::post('/patient/edit','DoctorController@editPatient');
        Route::post('/prescribeMedicine','DoctorController@prescribeMedicine');
        Route::post('/giveLabTest','DoctorController@giveLabTest');
        Route::post('/add-diagnosis','DoctorController@giveDiagnosis');
        Route::post('/prescribeMedicineFh','DoctorController@prescribeMedicineFh');
        Route::post('/giveLabTestFh','DoctorController@giveLabTestFh');
        Route::get('results/{id}','Laboratory\LaboratoryController@testView');
        Route::get('/closeFile/{id}','DoctorController@closeFile');
        Route::get('/getMedicines','DoctorController@getMedicines');
        Route::get('/getMgPerTablet','DoctorController@getMgPerTablet');
        //Route::get('/back','DoctorController@back')->name('back');
        Route::get('/familyhelp/add','DoctorController@familyHelpAdd');
        Route::get('/familyhelp/view/','DoctorController@familyHelpView');
    });

    Route::get('/patient/visit/{id}','DoctorController@patientVisit')->name('patient-visit');

    Route::group(['middleware' => 'App\Http\Middleware\PharmacyMiddleware'], function()
    {
        Route::get('/pharmacy','PharmacyController@index');
        Route::post('/pharmacy/medicineGiven','Billing\PharmacyController@medicineGiven');
        //billings
    });

    Route::prefix('laboratory')->namespace('Laboratory')->middleware(['App\Http\Middleware\LabMiddleware'])->group(function()
    {
        Route::get('/','LaboratoryController@index');  
        Route::get('/takesample/{id}','LaboratoryController@takeSample');
    });

    Route::group(['middleware' => 'App\Http\Middleware\LabMiddleware2'], function()
    {
        Route::get('/laboratory2','LaboratoryController2@index');
    });

    Route::group(['middleware' => 'App\Http\Middleware\ReceptionMiddleware'], function()
    {
        Route::get('/reception', 'PatientsController@index'); 
        Route::get('/reception/patients','PatientsController@index');
        Route::get('/reception/patient/edit/{reg_no}', 'PatientsController@edit');
        Route::get('/reception/patient/view/{reg_no}', 'PatientsController@view');

        Route::get('/reception/appointments','AppointmentController@index');        
        Route::get('/reception/appointment/edit/{id}', 'AppointmentController@edit');

        Route::get('/reception/billing', 'BillingController@index');       

        Route::get('/reception/patientOut/{id}','ReceptionController@patientOut');
        Route::get('/reception/patientEntered/{id}','ReceptionController@patientEntered');
        Route::get('/reception/labPatientEntered/{id}','ReceptionController@labPatientEntered');
        Route::get('/reception/labPatientOut/{id}','ReceptionController@labPatientOut');
    });
    

    Route::get('/photo','PatientsController@photo');
    
    Route::resource('patients', 'PatientsController');
    Route::post('reception/patient/store','ReceptionController@store');
    Route::post('/patient/visit','ReceptionController@store');
    Route::get('/patient-que/delete/{id}','PatientsController@delete_que');

    Route::get('/patientQue','DatatableController@patient_que');
    Route::get('/patientDocQue','DatatableController@doc_patient_que');
    Route::get('/patientHistory/{id}','DatatableController@patient_history');
    Route::get('/patientList','DatatableController@patient_list');
    Route::get('/patientPayment/{id}','DatatableController@patient_payment');

    Route::resource('appointments', 'AppointmentController');
    Route::get('/appointment/edit/{id}','AppointmentController@edit');

    Route::get('/appointmentUpcoming','DatatableController@upcoming_appointment');
    Route::get('/appointmentList','DatatableController@appointment_list');
    Route::get('/appointmentDoc','DatatableController@doc_appointment');

    Route::resource('billings','BillingController');
    Route::get('/billing/list','DatatableController@billingList');
    Route::get('/billing/medicine','DatatableController@billingMedicine');
    Route::get('/billing/lab','DatatableController@billingLab');
    Route::get('/billing/patient','DatatableController@billingPatient');
    Route::get('/billing/reception','DatatableController@billingReception');
    Route::post('/billing/getgloves','Billing\BillingController@requestGloves');
    
    Route::get('laboratory/test/{id}','Laboratory\LaboratoryController@testView');  
    Route::post('laboratory/testresults','Laboratory\LaboratoryController@submitTestResults');
    Route::post('laboratory/savetestresults','Laboratory\LaboratoryController@saveTestResults');
    Route::get('laboratory/testresults','Laboratory\LaboratoryController@index');
    Route::get('laboratory/results/view/{id}','Laboratory\LaboratoryController@viewResults');
    Route::post('laboratory/equipment/request','Laboratory\LaboratoryController@requestEquipment');
    Route::get('laboratory/equipment/edit/{id}','Laboratory\LaboratoryController@editRequestForm');
    Route::post('laboratory/equipment/edit/save','Laboratory\LaboratoryController@editRequest');

    Route::get('/autocomplete/{id}','PatientsController@autocomplete_ajax');
    Route::get('/name-ajax','PatientsController@name_ajax');
    Route::get('/reg_no-ajax','PatientsController@reg_no_ajax');
    Route::get('/getRegNo','PatientsController@generatePatientRegNo');
    Route::get('/getAge','PatientsController@getAge');

    Route::get('/getServicePrice/{val}','ReceptionController@getServicePrice');
    Route::get('/getPatienttypePrice/{val}','ReceptionController@getPatientTypePrice');
    Route::get('/getCharges/{id}','AppointmentController@getCharges');

    
    Route::get('/getMedCost/{id}','PharmacyController@getMedCost');
    Route::get('/getTestCost/{id}','Billing\LaboratoryController@getTestCost');

    Route::get('/users','DatatableController@users');
    Route::get('/rooms','DatatableController@rooms');
    Route::get('/services','DatatableController@services');
    Route::get('/servicemodes','DatatableController@servicemodes');
    Route::get('/units','DatatableController@units');
    Route::get('/checkMedicineStock','Admin\PharmacyController@checkForEnoughMedicinesInStock');

});
    Route::get('/medicineDescription','Billing\PharmacyController@getMedDescription');

    Route::get('/getServicePrice','ReceptionController@getServicePrice');

// new routes, feb 2020 refactor.
Route::group(['middleware' => 'auth'], function () {

    // equipments
    Route::group(['prefix' => 'equipments'], function () {
        Route::get('/', 'EquipmentController@index');
        Route::get('/add/{id?}', 'EquipmentController@addForm');
        Route::post('/add', 'EquipmentController@add');
        Route::get('/order/{id}', 'EquipmentController@order');
        Route::get('/associate', 'EquipmentController@associateForm');
        Route::post('/associate', 'EquipmentController@associate');
        Route::get('/view/{id}','EquipmentController@view');

        Route::group(['prefix'=>'batches'], function(){
            Route::get('/restock/{equipment_id}','EquipmentBatchController@restock');
            Route::post('/add','EquipmentBatchController@add');
            Route::get('/delete/{id}','EquipmentBatchController@delete');
        });
    });

    // procurement
    Route::group(['prefix' => 'procurement'], function () {
        Route::get('/','ProcurementController@index');     
    });

    // inventory
    Route::group(['prefix' => 'inventory'], function (){
       Route::get('/',"InventoryController@index"); 
       Route::get('/add/{id?}','InventoryController@addForm');
       Route::post('/add','InventoryController@add');
       Route::get('/view/{id}','InventoryController@view');
       Route::get('/remove/{id}','InventoryController@remove');
    });

    // cash-control
    Route::group(['prefix' => 'cash-control'], function () {
       Route::get('/','CashControlController@index');
       Route::post('/set-session','CashControlController@setSessionData');
       Route::get('/cash-in','CashControlController@cashIn');
       Route::get('/cash-out','CashControlController@cashOut');
    });

    // credit
    Route::group(['prefix' => 'credit'], function () {
       Route::get('/','CreditController@index');
       Route::get('/add/{id?}','CreditController@addForm');
       Route::post('/add','CreditController@add');
       Route::get('/pay/{id}','CreditController@pay');
       Route::get('/view/{id}','CreditController@view');
       Route::get('/remove/{id}','CreditController@remove');
    });

    // payment
    Route::group(['prefix' => 'payments'], function () {
       Route::get('/','PaymentController@index');
       Route::get('/add/{id?}','PaymentController@addForm');
       Route::post('/add','PaymentController@add');
       Route::get('/remove/{id}','PaymentController@remove');
       Route::get('/attach-invoice/{id}','PaymentController@attachInvoiceForm');
       Route::post('/attach-invoice','PaymentController@attachInvoice');
    });

    // standard operating procedures
    Route::group(['prefix' => 'standard-operating-procedures'], function () {
        Route::get('/','StandardOperatingProcedureController@index');
        Route::get('/add/{id?}','StandardOperatingProcedureController@addForm');
        Route::post('/add','StandardOperatingProcedureController@add');
        Route::get('/delete/{id}','StandardOperatingProcedureController@delete');
        Route::get('/download/{id}','StandardOperatingProcedureController@download');
        Route::get('/department','StandardOperatingProcedureController@department');
    });

    // consultants 
    Route::group(['prefix' => 'consultants'], function () {
        Route::get('/','ConsultantController@index');
        Route::get('/add/{id?}','ConsultantController@addForm');
        Route::post('/add','ConsultantController@add');
        Route::get('/remove/{id}','ConsultantController@remove');
        Route::get('/view/{id}','ConsultantController@view');
    });

    // appointments 
    Route::group(['prefix' => 'appointments'], function () {
        Route::get('/','AppointmentController@index');
        Route::get('/add/{id?}','AppointmentController@addForm');
        Route::get('/modify/{id}','AppointmentController@modifyForm');
        Route::post('/add','AppointmentController@add');
        Route::post('/modify','AppointmentController@modify');
        Route::get('/remove/{id}','AppointmentController@remove');
    });

    // announcements 
    Route::group(['prefix' => 'announcements'], function () {
        Route::get('/','AnnouncementController@index');
        Route::get('/add/{id?}','AnnouncementController@addForm');
        Route::post('/add','AnnouncementController@add');
        Route::get('/dismiss/{id}','AnnouncementController@dismiss');
        Route::get('/seen','AnnouncementController@seen');
    });

    // organisation-structure 
    Route::group(['prefix' => 'organisation-structure'], function () {
        Route::get('/','OrganisationStructureController@index');
        Route::get('/add/{id?}','OrganisationStructureController@addForm');
        Route::post('/add','OrganisationStructureController@add');
        Route::get('/remove/{id}','OrganisationStructureController@remove');
        Route::get('/download/{id}','OrganisationStructureController@download');
    });

    // quality-control 
    Route::group(['prefix' => 'quality-control'], function () {
        Route::group(['prefix' => 'members'], function () {
            Route::get('/','QualityControlMemberController@index');
            Route::get('/add/{id?}','QualityControlMemberController@addForm');
            Route::post('/add','QualityControlMemberController@add');
            Route::get('/appointment-letter','AppointmentLetterController@appointmentLetterForm');
            Route::post('/appointment-letter','AppointmentLetterController@appointmentLetter');
            Route::get('/appointment-letter/show/{user_id}','AppointmentLetterController@show');
            Route::get('/view/{id}','QualityControlMemberController@view');
            Route::get('/remove/{id}','QualityControlMemberController@remove');
        });
    });

    // meeting-minutes 
    Route::group(['prefix' => 'meeting-minutes'], function () {
        Route::get('/','MeetingMinuteController@index');
        Route::get('/add/{id?}','MeetingMinuteController@addForm');
        Route::post('/add','MeetingMinuteController@add');
        Route::get('/remove/{id}','MeetingMinuteController@remove');
        Route::get('/view/{id}','MeetingMinuteController@view');
    });

    // schedules 
    Route::group(['prefix' => 'schedules'], function () {
        Route::get('/','ScheduleController@index');
        Route::get('/add/{id?}','ScheduleController@addForm');
        Route::post('/add','ScheduleController@add');
        Route::get('/remove/{id}','ScheduleController@remove');
        Route::get('/view/{id}','ScheduleController@view');
    });

    // organisation structure meeting-minutes 
    Route::group(['prefix' => 'og-meeting-minutes'], function () {
        Route::get('/','MeetingMinuteController@index');
        Route::get('/add/{id?}','MeetingMinuteController@addForm');
        Route::post('/add','MeetingMinuteController@add');
        Route::get('/remove/{id}','MeetingMinuteController@remove');
        Route::get('/view/{id}','MeetingMinuteController@view');
    });

    // organisation strutcture schedules 
    Route::group(['prefix' => 'og-schedules'], function () {
        Route::get('/','ScheduleController@index');
        Route::get('/add/{id?}','ScheduleController@addForm');
        Route::post('/add','ScheduleController@add');
        Route::get('/remove/{id}','ScheduleController@remove');
        Route::get('/view/{id}','ScheduleController@view');
    });

    // feature management 
    Route::group(['prefix' => 'feature-management'], function () {
        Route::get('/','FeatureManagementController@index'); 
        Route::get('/hide/{id}','FeatureManagementController@hide');
        Route::get('/show/{id}','FeatureManagementController@show');
    });

    // human -resource 
    Route::group(['prefix' => 'human-resource'], function () { 
        Route::get('/','HumanResourceController@index'); 
    });

    // employee
    Route::group(['prefix' => 'employees'], function () {
        Route::get('/','EmployeeController@index');
        Route::get('/add/{id?}','EmployeeController@addForm');
        Route::post('/add','EmployeeController@add');
        Route::get('/remove/{id}','EmployeeController@remove');
        Route::get('/view/{id}','EmployeeController@view');
    });

    // attendance 
    Route::group(['prefix' => 'attendance'], function () { 
        Route::get('/','AttendanceController@index');
        Route::get('/take','AttendanceController@takeForm');
        Route::post('/take','AttendanceController@take');
        Route::get('/fill','AttendanceController@fill'); 
        Route::get('/async-fill','AttendanceController@asyncFill'); 
    });

    // employee-files
    Route::group(['prefix' => 'employee-files'], function () {
        Route::get('/files/{id}','EmployeeFilesController@index');
        Route::get('/upload/{id}','EmployeeFilesController@uploadForm');
        Route::post('/upload','EmployeeFilesController@upload');
        Route::get('/remove/{id}','EmployeeFilesController@remove'); 
        Route::get('/download/{id}','EmployeeFilesController@download'); 
    });

    // employee-appraisal
    Route::group(['prefix' => 'employee-appraisals'], function () {
        Route::get('/files/{id}','EmployeeAppraisalController@index');
        Route::get('/upload/{id}','EmployeeAppraisalController@uploadForm');
        Route::post('/upload','EmployeeAppraisalController@upload');
        Route::get('/remove/{id}','EmployeeAppraisalController@remove'); 
        Route::get('/download/{id}','EmployeeAppraisalController@download'); 
    });

    // leaves 
    Route::group(['prefix' => 'leaves'], function () {
        Route::get('/','LeaveController@index');
        Route::get('/add/{id?}','LeaveController@addForm');
        Route::post('/add','LeaveController@add');
        Route::get('/remove/{id}','LeaveController@remove');
        Route::get('/modify/{id}','LeaveController@modify');
    });

    // payroll 
    Route::group(['prefix' => 'payroll'], function () {
        Route::get('/','PayrollController@index');
        Route::get('/generate','PayrollController@generate');
        Route::post('/save','PayrollController@save');
        // Route::get('/pay-salary/{id}','PayrollController@paySalary');
        // Route::get('/download-payment-slip/{id}','PayrollController@downloadPaymentSlip');
        // Route::get('/print/{id}','PayrollController@print'); 
        // Route::get('/edit-payment-slip/{id}','PayrollController@editPaymentSlipForm');
        // Route::post('/edit-payment-slip','PayrollController@editPaymentSlip');
    });

    // settings for payroll
    Route::group(['prefix' => 'settings/deductions'], function () {
        Route::group(['prefix' => 'deductions'], function () {
            Route::get('/','DeductionController@index');
            Route::get('/add/{id?}','DeductionController@addForm');
            Route::post('/add','DeductionController@add');
            Route::get('/remove/{id}','DeductionController@remove');           
        });   
        Route::group(['prefix' => 'additions'], function () {
            Route::get('/','AdditionController@index');
            Route::get('/add/{id?}','AdditionController@addForm');
            Route::post('/add','AdditionController@add');
            Route::get('/remove/{id}','AdditionController@remove'); 
        });
    });

    // medicine category settings
    Route::group(['prefix' => 'settings/medicine-categories'], function () {
        Route::get('/','MedicineCategoryController@index');
        Route::get('/add/{id?}','MedicineCategoryController@addForm');
        Route::post('/add','MedicineCategoryController@add');
        Route::get('/remove/{id}','MedicineCategoryController@remove'); 
    });

    // additions
    Route::group(['prefix' => 'additions'], function () {
        Route::post('assign','AdditionController@assign');
        Route::get('/remove/{id}','AdditionController@remove'); 
    });

    // deduction
    Route::group(['prefix' => 'dedutions'], function () {
        Route::post('assign','DeductionController@assign');
        Route::get('/remove/{id}','DeductionController@remove'); 
    });

    // receipt-generation
    Route::group(['prefix'=>'receipt-genaration'],function(){
        Route::prefix('/generate/{id]','ReceiptGenerationController@generate');
    });

    // insurance-companies
    Route::group(['prefix' => 'insurance-companies'], function () {
        Route::get('/','InsuranceCompanyController@index');
        Route::get('/add/{id?}','InsuranceCompanyController@addForm');
        Route::post('/add','InsuranceCompanyController@add');
        Route::get('/remove/{id}','InsuranceCompanyController@remove');
        Route::get('/view/{id}','InsuranceCompanyController@view');
        
        Route::get('/assign-disease-codes','InsuranceCompanyController@assignDiseaseCodesForm');
        Route::post('/assign-disease-codes','InsuranceCompanyController@assignDiseaseCodes');
                
        Route::get('/assign-patient/{id}','InsuranceCompanyController@assignPatient');
        Route::get('/remove-patient/{id}','InsuranceCompanyController@removePatient');

        Route::get('/monthly-report','InsuranceCompanyController@index');
        Route::get('/generate-invoice','InsuranceCompanyController@generateInvoice');
    });

    // corporate-companies
    Route::group(['prefix' => 'corporate-companies'], function () {
        Route::get('/','CorporateCompanyController@index');
        Route::get('/add/{id?}','CorporateCompanyController@addForm');
        Route::post('/add','CorporateCompanyController@add');
        Route::get('/remove/{id}','CorporateCompanyController@remove');
        Route::get('/view/{id}','CorporateCompanyController@view');
         
        Route::get('/assign-patient/{id}','CorporateCompanyController@assignPatient');
        Route::get('/remove-patient/{id}','CorporateCompanyController@removePatient');

        Route::get('/monthly-report','CorporateCompanyController@index');
        Route::get('/generate-invoice','CorporateCompanyController@generateInvoice');
    });    

    // reports-graphs
    Route::group(['prefix' => 'reports/graphs'], function () {
        Route::get('/','GraphController@default');
    });

    Route::group(['prefix'=>'reception-service-details'],function(){
        Route::get('/services','ReceptionServiceDetailsApiController@getServices');
        Route::get('/service-modes','ReceptionServiceDetailsApiController@getServiceModes');
        Route::get('/patient-types','ReceptionServiceDetailsApiController@getPatientTypes');
        Route::get('/insurance-companies','ReceptionServiceDetailsApiController@getInsuranceCompanies');
        Route::get('/corporate-companies','ReceptionServiceDetailsApiController@getCorporateCompanies');

        Route::get('/reg-no-suggestions','ReceptionServiceDetailsApiController@getRegNoSuggestions');
        Route::get('/name-suggestions','ReceptionServiceDetailsApiController@getNameSuggestions');

        Route::get('/que-no','ReceptionServiceDetailsApiController@getQueNumber');
        Route::get('/doctors','ReceptionServiceDetailsApiController@getDoctors');
        Route::get('/consultants','ReceptionServiceDetailsApiController@getConsultants');
        Route::get('/tests','ReceptionServiceDetailsApiController@getTests');
        Route::get('/medicine-types','ReceptionServiceDetailsApiController@getMedicineTypes');
        Route::get('/medicines/{id}','ReceptionServiceDetailsApiController@getMedicines');
        Route::get('/medicine-units','ReceptionServiceDetailsApiController@getMedicineUnits');
    });
    Route::group(['prefix'=>'reception-investigation-results'],function(){ 
        Route::get('/results/{billing_list_id}','ReceptionInvestigationResultsApiController@showResults');
        Route::get('/close-results/{billing_list_id}','ReceptionInvestigationResultsApiController@closeResults');
    });
});
