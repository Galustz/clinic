<?php

namespace App;


use \App\BaseModel as Model;
use Carbon\Carbon;

class MeetingMinute extends Model
{
    protected $guarded = [];

    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function getFormDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getFileNameAttribute($value)
    {
        return $value != null?$value:"No file!";
    }

    public function hasFile()
    {
        return $this->url != null;
    }
}
