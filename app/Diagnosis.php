<?php

namespace App;


use \App\BaseModel as Model;
use Carbon\Carbon;

class Diagnosis extends Model
{
    public function records()
    {
        return $this->hasMany(DiagnosisRecord::class, 'diagnosis_id', 'id');
    }

    public function getSummary()
    {
        return (object)[
            "under_one_month"=>$this->underOneMonth(),
            "one_month_to_one_year"=> $this->oneMonthToOneYear(),
            "one_year_to_five_years"=>$this->oneYearToFiveYears(),
            "five_years_to_sixty_years"=>$this->fiveYearsToSixtyYears(),
            "sixty_years_and_above"=>$this->sixtyYearsAndAbove(),
            "total"=>$this->total()
        ];
    }

    public function underOneMonth()
    { 
        $month_limit = $this->getDateFromMonth(1);
        $diagnoses_women = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','>',$month_limit)
                            ->where('patients.gender','female')
                            ->count();
        $diagnoses_men = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','>',$month_limit)
                            ->where('patients.gender','male')
                            ->count();
        $diagnoses_jumla = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','>',$month_limit) 
                            ->count();
        
        return $this->values($diagnoses_women,$diagnoses_men,$diagnoses_jumla);
    }

    public function oneMonthToOneYear()
    { 
        $month_limit = $this->getDateFromMonth(1);
        $year_limit = $this->getDateFromYear(1);
        $diagnoses_women = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$month_limit)
                            ->where('patients.DOB','>',$year_limit)
                            ->where('patients.gender','female')
                            ->count();
        $diagnoses_men = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$month_limit)
                            ->where('patients.DOB','>',$year_limit)
                            ->where('patients.gender','male')
                            ->count();
        $diagnoses_jumla = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$month_limit)
                            ->where('patients.DOB','>',$year_limit)
                            ->count();
        
        return $this->values($diagnoses_women,$diagnoses_men,$diagnoses_jumla);
    }

    public function oneYearToFiveYears()
    {   
        $year_limit = $this->getDateFromYear(1);
        $years_limit = $this->getDateFromYear(5);
        $diagnoses_women = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit)
                            ->where('patients.DOB','>',$years_limit)
                            ->where('patients.gender','female')
                            ->count();
        $diagnoses_men = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit)
                            ->where('patients.DOB','>',$years_limit)
                            ->where('patients.gender','male')
                            ->count();
        $diagnoses_jumla = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit)
                            ->where('patients.DOB','>',$years_limit)
                            ->count();
        
        return $this->values($diagnoses_women,$diagnoses_men,$diagnoses_jumla);
    }

    public function fiveYearsToSixtyYears()
    { 
        $year_limit = $this->getDateFromYear(5);
        $years_limit = $this->getDateFromYear(60);
        $diagnoses_women = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit)
                            ->where('patients.DOB','>',$years_limit)
                            ->where('patients.gender','female')
                            ->count();
        $diagnoses_men = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit)
                            ->where('patients.DOB','>',$years_limit)
                            ->where('patients.gender','male')
                            ->count();
        $diagnoses_jumla = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit)
                            ->where('patients.DOB','>',$years_limit)
                            ->count();
        
        return $this->values($diagnoses_women,$diagnoses_men,$diagnoses_jumla);
    }

    public function sixtyYearsAndAbove()
    {  
        $year_limit = $this->getDateFromYear(60);
        $diagnoses_women = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit) 
                            ->where('patients.gender','female')
                            ->count();
        $diagnoses_men = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit) 
                            ->where('patients.gender','male')
                            ->count();
        $diagnoses_jumla = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no')
                            ->where('patients.DOB','<=',$year_limit) 
                            ->count();
        
        return $this->values($diagnoses_women,$diagnoses_men,$diagnoses_jumla);
    }

    public function total()
    { 
        $year_limit = $this->getDateFromYear(60);
        $diagnoses_women = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no') 
                            ->where('patients.gender','female')
                            ->count();
        $diagnoses_men = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no') 
                            ->where('patients.gender','male')
                            ->count();
        $diagnoses_jumla = $this->records()
                            ->join('patients','patients.reg_no','=','diagnosis_records.patient_registration_no') 
                            ->count();
        
        return $this->values($diagnoses_women,$diagnoses_men,$diagnoses_jumla);
    }

    public function values( $women, $men, $total)
    {
        $women = $women==0?null:$women;
        $men = $men==0?null:$men;
        $total = $total==0?null:$total;
        return (object)[
            "women" => $women,
            "men" => $men,
            "total" => $total
        ];
    }

    public function getMonthAndYearFromRequest()
    {
        return (object)[
            "month"=>request()->month,
            "year"=>request()->year
        ];
    }

    public function getDateFromMonth($value)
    {
        return Carbon::now()->subMonth($value);
    }

    public function getDateFromYear($years)
    {
        return Carbon::now()->subYear($years);
    }
}
