<?php

namespace App;
use DateTime;


use \App\BaseModel as Model;
use Carbon\Carbon;

class Patient extends Model
{
    protected $date = ['DOB','created_at'];
    protected $table = 'patients';

    // public function patientRegNo()
	// {
	// 	return 'P-'.sprintf("%04d", $this->id);
    // }
    
    public function billing(){
        return $this->hasMany('App\Billing');
    }
    
    public function getAgeObjectAttribute()
    {
        $date = date_create_from_format('Y-m-d', $this->DOB);   
        $reference = Carbon::now();
        $diff = $reference->diff($date);
        return (object)[
            "years"=>$diff->y,
            "months"=>$diff->m,
            "days"=>$diff->d
        ];
    }

    public function getAgeAttribute()
    {
        $date = date_create_from_format('Y-m-d', $this->DOB);
        $reference = Carbon::now();
        $diff = $reference->diff($date);
        return "$diff->y years, $diff->m     months, $diff->d days";
    }
}
