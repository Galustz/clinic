<?php

namespace App;


use \App\BaseModel as Model;


class MedicineCategory extends Model
{
    //
    protected $table = 'medicine_category';
    protected $guarded = [];
    
    public function medicines()
    {
        return $this->hasMany(Medicine::class, 'medicine_category_id', 'id');
    }
}
