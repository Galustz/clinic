<?php

namespace App;


use \App\BaseModel as Model;


class EmployeeLeave extends Model
{
    protected $guarded = [];

    public function employee()
    {
        return $this->hasOne(User::class, 'id', 'employee_id');
    }

    public function getNameAttribute()
    {
        return $this->employee->name;
    }

    public function getNumberOfDaysAttribute()
    {
        return count(explode(",",$this->date));
    }
}
