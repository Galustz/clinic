<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentLetter extends Model
{
    protected $guarded = [];
    public static $NAME_TEMPLATE_STRING = "**name**";

    public static function setLetter($content)
    {
        $letter = AppointmentLetter::first();
        if($letter==null){
            AppointmentLetter::create(['content'=>$content]);
        }else{
           $letter->update(['content'=>$content]);
        }
    }

    public static function getLetter()
    {
        return AppointmentLetter::first()->content??null;
    }
}
