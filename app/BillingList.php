<?php

namespace App;


use \App\BaseModel as Model;


class BillingList extends Model
{
    protected $guarded = [];
    public function familyHelp()
    {
        return $this->belongsTo('App\FamilyHelp','family_help_id');
    }

    public function patient()
    {
        return $this->hasOne(Patient::class, 'reg_no', 'patient_registration_no');
    }

    public function getPatientQueAttribute()
    { 
        $que = PatientQueueTemporary::whereVisitNo($this->visit_no)
                                      ->wherePatientRegNo($this->patient_registration_no)
                                      ->first();
        return $que;
    }
}
