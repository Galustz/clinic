<?php

namespace App;


use \App\BaseModel as Model;


class Medicine extends Model
{
    //
    public function category(){
        return $this->belongsTo('App\MedicineCategory','medicine_category_id','id');
    }

    public function suppliers(){
        return $this->belongsToMany('App\Supplier','supplier_medicines','medicine_id','supplier_id')->withPivot('price','id');
    }

    public function medicineBatches()
    {
        return $this->hasMany('App\MedicineStockBatch','medicine_id');
    }

    public function medicineRecords()
    {
        return $this->hasMany('App\MedicineRecord','medicine_id');
    }

    public function remainingQuantity()
    {
        return $this->medicineBatches()->sum('remaining_quantity');
    }

    public function addNewBatch( $medicine_id, $number_of_units_bought, $expiry_date, $batch_number)
    {  
        $medicine = self::find($medicine_id); 

        $total_price = $medicine->unit_purchasing_price * $number_of_units_bought;
        $total_quantity = $medicine->quantity_per_unit * $number_of_units_bought;
        $remaining_quantity = $total_quantity;

        $batch = new MedicineStockBatch;        
        $batch->medicine_id=$medicine_id;
        $batch->number_of_units_bought=$number_of_units_bought;
        $batch->total_price= $total_price;
        $batch->total_quantity= $total_quantity;
        $batch->expiry_date=$expiry_date;
        $batch->batch_number=$batch_number;
        $batch->remaining_quantity= $remaining_quantity;
        $batch->unit_purchasing_price=$medicine->unit_purchasing_price;
        $batch->save();

        return (object)[
            "total_cost"=>$total_price
        ];
    }
}
