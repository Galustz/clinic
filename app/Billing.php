<?php

namespace App;

use \App\BaseModel as Model;

// use Illuminate\Database\Eloquent\Model;


class Billing extends Model
{
    protected $date = ['created_at']; 

    public function change()
    {
        return $this->hasOne('App\BillingChange','billing_id');
    }
    public function patient(){
        return $this->belongsTo('App\Patient');
    }

    public function patienttype()
    {
        return $this->patient->patient_type;
    }

    public static function getCashInRecords($filters=null)
    {
        $records = Billing::whereStatus('paid')->canFilter($filters)->get();

        $records = $records->map(function($item){
            $item["amount"] = "$item->service_cost Tsh";
            $item["transaction_date"] = $item->created_at->format('d/m/Y');
            $item["time"] = $item->created_at->format('H:m');
            $item["type"] = $item->transaction_type;
            return $item;
        });
        return $records;
    }
}
