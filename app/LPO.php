<?php

namespace App;


use \App\BaseModel as Model;

use Illuminate\Support\Facades\DB;

class LPO extends Model
{
    protected $table = 'l_p_os'; 

    const SALE_TYPES = [
        "in"=>"Invoice",
        "cr"=>"Credit",
        "cs"=>"Cash Sale",
    ];

    const ITEM_STATI = [
        "rc"=>"Received",
        // "rb"=>"Returned Back",
        "nyr"=>"Not Yet Received"
    ];

    public function items()
    {
        return $this->hasMany(LPOItem::class, 'l_p_o_id', 'id');
    }

    public function medicines()
    {
        return $this->items()->whereType('medicine')->get();
    }

    public function equipments()
    {
        return $this->items()->whereType('equipment')->get();
    }

    public function supplier()
    {
        return $this->hasOne(Supplier::class, 'id', 'supplier_id');
    }

    public function getTotalAmountAttribute()
    {
        $sum = $this->items()->sum(DB::raw('price*quantity'));
        return number_format($sum).'Tsh';
    }
}
