<?php

namespace App;


use \App\BaseModel as Model;


class MedicineRecord extends Model
{
    //
    public function medicine(){
        return $this->belongsTo('App\Medicine','medicine_id');
    }

    
    public function item(){
        return $this->belongsTo('App\Medicine','medicine_id');
    }

    public function medicineStockUses()
    {
        return $this->hasMany('App\MedicineStockUse','medicine_record_id');
    }

    public function getPerdayAttribute($value)
    {
        return intval($value);
    }

    public function getDaysAttribute($value)
    {
        $arr = explode('/',$value);
        $arr[0] = intval($arr[0]);
        $value = join('/',$arr);
        return $value;
    }
}
