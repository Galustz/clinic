<?php

namespace App;


use \App\BaseModel as Model;


class Supplier extends Model
{ 
    public function medicines(){
        return $this->belongsToMany('App\Medicine','supplier_medicines','supplier_id','medicine_id')->withPivot('price','id');
    }

    public function equipments(){
        return $this->belongsToMany('App\Equipment','supplier_equipments','supplier_id','equipment_id')->withPivot('price','id');
    }

    public function payments()
    {
        return $this->belongsToMany('App\Payment','supplier_payments','supplier_id','payment_id');
    }
    
    public function lpos()
    {
        return $this->hasMany(LPO::class, 'supplier_id', 'id');
    }
}
