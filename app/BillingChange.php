<?php

namespace App;


use \App\BaseModel as Model;


class BillingChange extends Model
{
    public function billing()
    {
        return $this->belongsTo('App\Billing','billing_id');
    }
}
