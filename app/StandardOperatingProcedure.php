<?php

namespace App;


use \App\BaseModel as Model;


class StandardOperatingProcedure extends Model
{
    protected $guarded = [];
    
    public function department()
    {
        return $this->hasOne( Role::class, 'id', 'designation_id');
    }

    public function getDepartmentNameAttribute()
    {
        return $this->department()->name;
    }
    
    public function designation()
    {
        return $this->hasOne( Role::class, 'id', 'designation_id');
    }

    public function getDesignationNameAttribute()
    { 
        return $this->designation_id == 0? "All": $this->designation->name;
    }

    public function getDescriptionAttribute( $value)
    {
        $value = (empty($value)) ? "-" : $value ;
        return $value;
    }
}
