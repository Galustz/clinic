<?php

namespace App;


use \App\BaseModel as Model;


class QualityControlMember extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getNameAttribute()
    {
        return $this->user->name;
    }

    public function getIdAttribute()
    {
        return $this->user->id;
    }

    public function getDesignationAttribute()
    {
        return $this->user->role->name;
    }

    public static function getSelectedMembers()
    {
        $members = self::all();
        $members = $members->map(function($member){
            return $member->user_id;
        })->toArray();
        return $members;
    }
}
