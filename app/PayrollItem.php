<?php

namespace App;


use \App\BaseModel as Model;


class PayrollItem extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'employee_id');
    }
}
