<?php

namespace App;


use \App\BaseModel as Model;


class PatientQueueTemporary extends Model
{
    const PATIENT_QUEUE_TYPES = ["seeing_doctor"=>"sd","appointment"=>"ap"];
    
    public function patient()
    {
        return $this->hasOne('App\Patient', 'patient_reg_no', 'reg_no');
    }

    public function getDoctorAttribute($value)
    {
        return User::find($value);
    }
    
    public function serviceMode()
    {
        return $this->hasOne(ServiceMode::class, 'id', 'service_mode');
    }
}
