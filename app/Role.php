<?php

namespace App;


use \App\BaseModel as Model;


class Role extends Model
{

	public static $STAFF_ROLES = ['laboratorist','doctor','receptionist','laboratorist_2','billing',
								  'pharmacist'];

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function permissions(){
		return $this->belongsToMany('Permission');
	}
	
	public function hasPermission($permission){
		$permissions = $this->permissions()->lists('id');

		if(in_array($permission, $permissions)){
			return true;
		}
		return false;
	}
}
