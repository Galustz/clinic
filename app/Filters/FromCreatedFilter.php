<?php 

namespace App\Filters;

use Carbon\Carbon;

class FromCreatedFilter 
{ 
    public function filter($builder, $value)
    {
        $date = Carbon::parse($value);

        return $builder->where('created_at',">=", $date);
    }
}
