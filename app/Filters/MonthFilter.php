<?php 

namespace App\Filters;

use Carbon\Carbon;

class MonthFilter 
{ 
    public static $MONTHS = [
        "1"=>"January",
        "2"=>"February",
        "3"=>"March",
        "4"=>"April",
        "5"=>"May",
        "6"=>"June",
        "7"=>"July",
        "8"=>"August",
        "9"=>"September",
        "10"=>"October",
        "11"=>"November",
        "12"=>"December",
    ];

    public static function getMonths(){
        return self::$MONTHS;
    }

    public function filter($builder, $value)
    { 
        return $builder->whereMonth('date',"=", $value);
    }
}
