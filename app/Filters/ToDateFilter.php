<?php 

namespace App\Filters;

use Carbon\Carbon;

class ToDateFilter
{
    public function filter($builder, $value)
    {
        $date = Carbon::parse($value)->addHours(23)->addMinutes(59)->addSeconds(59);
        return $builder->where('date',"<=", $date);
    }
}
