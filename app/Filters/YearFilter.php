<?php 

namespace App\Filters;

use Carbon\Carbon;

class YearFilter 
{ 
    public static $YEARS_BACK = 10;

    public static function getYears(){

        for($i=0;$i<=self::$YEARS_BACK;$i++) $years[] = Carbon::today()->year - $i;

        return $years;
    }
    public function filter($builder, $value)
    { 
        return $builder->whereYear('date',"=", $value);
    }
}
