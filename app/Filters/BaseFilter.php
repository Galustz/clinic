<?php
 

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class BaseFilter
{
    protected $declared_filters;

    public function __construct($filters)
    {
        $this->request = request();
        $this->declared_filters = $filters;
    }

    public function filter(Builder $builder)
    { 
        foreach ($this->getRequestedFilters() as $filter => $value) {
            $this->resolveFilterClass($filter)->filter( $builder, $value);
        }
        return $builder;
    }

    public function getRequestedFilters(){

        // sort through request to obtain the key value pairs using the declared filters

        // return array_filter($this->request->only(array_keys($this->declared_filters)));
        return array_filter($this->request->only($this->declared_filters));
    }

    public function resolveFilterClass($declared_filter_key)
    {
        $class_name = "\\App\Filters\\".studly_case($declared_filter_key)."Filter";
        $class_instance = App::make($class_name); 
        // return new $this->declared_filters[$declared_filter_key]; 
        return $class_instance;
    }
}