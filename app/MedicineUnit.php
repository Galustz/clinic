<?php

namespace App;


use \App\BaseModel as Model;


class MedicineUnit extends Model
{
    //
    protected $fillable = ['name'];
}
