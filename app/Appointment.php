<?php

namespace App;


use \App\BaseModel as Model;


class Appointment extends Model
{
    protected $date = ['appointment_date','created_at'];
    protected $primaryKey = 'appointment_id';
}
