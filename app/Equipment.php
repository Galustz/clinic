<?php

namespace App;


use \App\BaseModel as Model;


class Equipment extends Model
{ 
    protected $guarded = [];

    public function batches()
    {
        return $this->hasMany(EquipmentBatch::class, 'equipment_id', 'id');
    }

    public function getQuantityInStockAttribute()
    {
        $quantity = $this->getQuantityInStock($this->id);
        return $quantity;
    }

    public function getQuantityInStock($equipment_id)
    { 
        $equipment = self::find($equipment_id);
        $sum = $equipment->batches()->sum('remaining_quantity');
        $sum = $sum * $equipment->quantity_per_unit;
        return $sum;
    }

    public function addNewBatch($equipment_id, $quantity, $date, $code)
    {
        $equipment = self::find($equipment_id);

        $equipment_batch = new EquipmentBatch();
        $equipment_batch->equipment_id = $equipment_id;
        $equipment_batch->quantity = $quantity;
        $equipment_batch->remaining_quantity = $quantity;
        $equipment_batch->date = $date;
        $equipment_batch->code = $code;
        $equipment_batch->save();

        $total_cost = $equipment->unit_purchasing_price * $quantity;

        return (object)[
            "total_cost"=>$total_cost
        ]; 
    }
}
