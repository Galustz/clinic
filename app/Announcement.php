<?php

namespace App;


use \App\BaseModel as Model;
use Carbon\Carbon;

class Announcement extends Model
{
    protected $guarded = [];
    public static $TARGET_TYPES = ['all'=>'All','select'=>'Select'];

    
    public function users()
    {
        return $this->belongsToMany(User::class, 'announcement_targets', 'announcement_id', 'user_id');
    }
    
    public function targets()
    {
        return $this->hasMany(AnnouncementTarget::class, 'announcement_id', 'id');
    }

    public function getTargetAttribute()
    {
        if($this->target_type==self::$TARGET_TYPES['all']){
            return $this->target_type;
        }else{
            $targets_array = $this->users->map(function($target){
                return $target->name;
            })->toArray(); 
            $targets_string = implode(",  ", $targets_array);
            return $targets_string;
        }
    }
    
    public function syncTargets($announcement_targets)
    {
        if($this->target_type == self::$TARGET_TYPES['select']){
            $this->users()->sync($announcement_targets); 
        }else{
            $this->users()->sync(User::staffs());
        }
    }

    public function getCreatedAtAttribute($created_at)
    {
        return Carbon::parse($created_at)->format('d/m/Y');
    }

    public static function getUserAnnouncements()
    { 
        return request()->user()->announcements()->latest()->take(10)->get();
    }

    public static function getUserNewAnnouncements()
    {  
        return request()->user()->announcements()->where('announcement_targets.status','created')->orderBy('created_at','desc')->get();
    }
}
