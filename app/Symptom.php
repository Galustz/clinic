<?php

namespace App;


use \App\BaseModel as Model;


class Symptom extends Model
{
    //
    protected $fillable = ['name'];
}
