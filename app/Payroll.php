<?php

namespace App;


use \App\BaseModel as Model;


class Payroll extends Model
{
    protected $guarded = [];

    public function items()
    {
        return $this->hasMany(PayrollItem::class, 'payroll_id', 'id');
    }

    public function getTotalAmountFormattedAttribute()
    {
        return number_format($this->total_amount)." Tsh";
    }
}
