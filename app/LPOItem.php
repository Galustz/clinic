<?php

namespace App;


use \App\BaseModel as Model;


class LPOItem extends Model
{
    public function getNameAttribute()
    {
        // TODO the name should come depending on if it is medicine or equipment
        return $this->medicine->name;
    }

    public function data()
    {
        if($this->type == "medicine"){
            return $this->hasOne(Medicine::class, 'id', 'medicine_id');
        }else{
            return $this->hasOne(Equipment::class, 'id', 'medicine_id');
        }
    }
    public function getTotalAmountAttribute()
    {
        return number_format($this->price * $this->quantity). 'Tsh';
    }
}
