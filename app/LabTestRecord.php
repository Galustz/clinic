<?php

namespace App;


use \App\BaseModel as Model;


class LabTestRecord extends Model
{
    //
    public function lab_test(){
        return $this->belongsTo('App\LabTest');
    }

    public function item(){
        return $this->belongsTo('App\LabTest','lab_test_id');
    }

    public function getResultsAttribute()
    {
        switch ($this->lab_test->name){
            case 'Urinal':
                $result = UrinalResult::whereLabTestRecordId($this->id)->first();
                $type = "urinal";
                break;
            case 'Stool':
                $result = StoolResult::whereLabTestRecordId($this->id)->first();
                $type = "stool";
                break;
            case 'FBP':
                $result = FbpResult::whereLabTestRecordId($this->id)->first();
                $type = "fbp";
                break;
            
            default: 
                $result = $this->result;
                $type = "default";
                break;
        }
        return (object)["object"=>$result,"type"=>$type];
    }
} 
