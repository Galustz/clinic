<?php

namespace App;


use \App\BaseModel as Model;


class DiagnosisRecord extends Model
{
    public function diagnosis(){
        return $this->belongsTo('App\Diagnosis');
    }
}
