<?php

namespace App;

use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model{
    public function scopeCanFilter(Builder $builder, $filters)
    {
        return (new BaseFilter($filters))->filter($builder);
    }
}