<?php

namespace App;

use \App\BaseModel as Model;


class SymptomRecord extends Model
{
    //
    public function symptom(){
        return $this->belongsTo('App\Symptom','symptom_id','id');
    }
}
