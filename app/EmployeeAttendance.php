<?php

namespace App;


use \App\BaseModel as Model;
use Carbon\Carbon;

class EmployeeAttendance extends Model
{
    protected $guarded = [];

    public static function getDatesOfMonth( $month = null, $year = null)
    {

        if(request()->has('month'))$month = request()->month;
        if($month==null)$month = Carbon::today()->month;

        if(request()->has('year'))$year = request()->year;
        if($year==null)$year = Carbon::today()->year;

        $date_object = Carbon::createFromDate($year,$month);

        for( $i=1; $i <= $date_object->daysInMonth ; $i++ ) $days_in_month[] = Carbon::createFromDate($year,$month,$i);

        return $days_in_month;
    }
}
