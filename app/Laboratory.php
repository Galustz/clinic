<?php

namespace App;


use \App\BaseModel as Model;


class Laboratory extends Model
{
    protected $table = 'laboratory';
    protected $primaryKey = 'lab_id';
}
