<?php

namespace App;


use \App\BaseModel as Model;


class Setting extends Model
{
    //
    protected $fillable = ['name'];

    public static function getSettingValue( $value)
    { 
        $result = \App\Setting::where('name',$value)->first();
        return $result->value;
    }
}
