<?php

namespace App\Http\Controllers;

use App\AppointmentLetter;
use App\User;
use Illuminate\Http\Request;

class AppointmentLetterController extends Controller
{

    public function __construct()
    {        
        $this->initialise(
            '/quality-control/members/appointment-letter',
            'quality_control.members.',
            ['role:admin'],
            [ 
                "form_url"=>"/", 
            ],
            AppointmentLetter::class
        );
    }

    public function appointmentLetterForm()
    { 
        $this->header = "Modify Appointment Letter";
        $this->letter = $this->defaultModel::getLetter();
        $this->name_template_string = $this->defaultModel::$NAME_TEMPLATE_STRING;
        return $this->cView('appointment-letter');
    }

    public function appointmentLetter()
    { 
        $this->defaultModel::setLetter($this->request->letter);
        return redirect($this->root_url)->with('success_msg','Modified successsfully');
    }

    public function show()
    { 
        $name = User::find($this->request->user_id)->name;
        $letter = $this->defaultModel::getLetter();
        $this->letter = str_replace( $this->defaultModel::$NAME_TEMPLATE_STRING, $name, $letter);

        return $this->cView('show-appointment-letter');
    }
}
