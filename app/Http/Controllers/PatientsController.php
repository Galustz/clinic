<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use App\PatientQueue;
use App\Vital;
use App\Billing;
use App\Diagnosis;
use App\Service;
use App\ServiceMode;
use App\Room;
use App\Unit;
use App\Doctor;
use App\Prescription;
use App\Laboratory;
use App\PatientQueueTemporary;
use App\PatientVisit; 
use App\BillingChange;
use App\BillingTransaction;

use Auth;
use DB;
use DateTime;

class PatientsController extends Controller
{    
    public function index()
    { 
        $user = Auth::user();
        $title = "Reception";
        if($user->hasRole('doctor'))
        {
            $title = 'Doctor :: Patients';
        }
        if($user->hasRole('receptionist'))
        {
            $title = 'Reception :: Patients';
        }

        $pqns = PatientQueueTemporary::where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('created_at','desc')->first();
        if($pqns==null){
            $pqn = 1;
        }else{
            $pqn = $pqns->patient_que_no+1;
        }
        $rooms = Room::pluck('name','id');
        $services = Service::pluck('name','id');
        $doctors = Doctor::pluck('name','doc_id');
        $service_modes = ServiceMode::pluck('name','id');
        $units = Unit::pluck('name','id');

        return view('reception.index',compact('title','rooms','services','doctors','service_modes','units','pqn'));
    }

    public function store(Request $request)
    {
        $pqns = PatientQueueTemporary::where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('created_at','desc')->first();
        if($pqns==null){
            $pqn = 1;
        }else{
            $pqn = $pqns->patient_que_no+1;
        }

        $visit_no = PatientVisit::where('patient_registration_no','=',$request->reg_no)->orderBy('created_at','desc')->first();
        if($visit_no==null){
            $vn = 1;
        }else{
            $vn = $visit_no->visit_no+1;
        }
        

        $billing = new Billing;
        $billing->patient_registration_no = $request->reg_no;
        $billing->visit_no = $vn;
        $billing->service_cost = $request->service_cost;
        $billing->amount_due = $request->amount_due;
        $billing->transaction_type = $request->transaction_type;
        $billing->discount = 0;
        $billing->save();

        $patient = new Patient;
        $patient->reg_no = $request->reg_no;
        $patient->name = $request->name;
        $patient->gender = $request->gender;
        $patient->blood_group = $request->blood_group;
        $patient->type = $request->type;
        $patient->nationality = $request->nationality;
        $patient->DOB = $request->dob;
        $patient->occupation = $request->occupation;

        $patient->email = $request->email;
        $patient->phone = $request->phone;
        $patient->telephone = $request->landline;
        $patient->address = $request->address;

        $patient->patient_type = $request->patient_type;

        if($request->type == 'child')
        {
            $patient->mother_name = $request->mother_name;
            $patient->mother_occupation = $request->mother_occupation;
            $patient->father_name = $request->father_name;
            $patient->father_occupation = $request->father_occupation;
        }

        $image = $request->photo;
        if($image)
        {
            $imageName = $image->getClientOriginalName();
            $image->move('assets/images/patients',$imageName);
            $patient->photo = $imageName;
        }

        $patient->save();



        $vital = new Vital;
        $vital->patient_reg_no = $request->reg_no;
        $vital->weight = $request->weight;
        $vital->height = $request->height;
        $vital->blood_pressure = $request->blood_presure;
        $vital->pulse_rate = $request->pulse_rate;
        $vital->respiratory_rate = $request->respiratory_rate;
        $vital->temperature = $request->temperature;
        $vital->other = $request->other;
        $vital->nurse = $request->nurse;
        $vital->visit_no = $vn;

        $vital->save();

        $queue = new PatientQueueTemporary;
        $queue->patient_que_no = $pqn;
        $queue->patient_reg_no = $request->reg_no;
        $queue->service_mode = $request->service_mode;
        $queue->requested_service = $request->service;
        $queue->doctor = $request->doctor;
        $queue->unit_allocation = $request->unit_allocation; 
        $queue->room_no = $request->room_no;
        $queue->visit_no = $vn;
        $queue->status = 'waiting';
        $queue->billing_id = $billing->id;
        $queue->save();

        $visit = new PatientVisit;
        $visit->patient_registration_no = $request->reg_no;
        $visit->visit_no = $vn;
        $visit->doctor_id = 1;
        $visit->save();


        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $request->amount_paid;
        $billingtransaction->amount_due = $request->amount_due;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();

        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $request->amount_paid;
        $billingchange->change = $request->change;
        $billingchange->change_given = $request->change_given;
        $billingchange->change_due = $request->change_remaining;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();

        return redirect('/reception');
    }

    public function view($id)
    {
        $user = Auth::user();
        $p = Patient::whereId($id)->first();

        if($user->hasRole('doctor'))
        {
            $title = 'Doctor :: Patient View';
        }
        if($user->hasRole('receptionist'))
        {
            $title = 'Reception :: Patient View';
        }

        $patient = Patient::findOrFail($id);
        return view('patients.details.view',compact('title','patient'));
    }

    public function edit($id)
    {
        $patient = Patient::where('patient_reg_no',$id)->first();
        return view('patients.edit',compact('patient'));
    }

    public function update($id, Request $request)
    {
        $patient = Patient::find($id);
        $formInput = $request->except('photo');

        $image = $request->photo;
        if($image)
        {
            $imageName = $image->getClientOriginalName();
            $image->move('assets/images/patients',$imageName);
            $formInput['photo'] = $imageName;
        }

        $patient->update($formInput);
    }

    public function delete($id)
    {
        Patient::destroy($id);
    }

    public function delete_que($id)
    {
        PatientQueue::destroy($id);
    }

    public function history($reg_no)
    {
        $title = 'Doctor :: Patient History';
        return view('patients.history',compact('title'));
    }

    public function history_view($reg_no,$id)
    {
        $title = 'Doctor :: History View';
        $patient = Patient::where('reg_no',$reg_no)->first();
        $diagnosis = Diagnosis::where('diagnosis_id',$id)->first();
        $prescription = Prescription::where('prescription_id',$diagnosis->prescripton_id)->first();
        $vitals = Vital::where('patient_reg_no',$reg_no)->where('created_at',$diagnosis->created_at)->first();
        $lab = Laboratory::where('patient_reg_no',$reg_no)->where('created_at',$diagnosis->created_at)->first();

        $doctor = Doctor::where('doc_id',$diagnosis->doc_id)->first();
        $service = Service::where('id',$diagnosis->service_id)->first();
        $service_mode = ServiceMode::where('service_mode_id',$diagnosis->service_mode_id)->first();


        return view('patients.details.history-view',compact([
            'title' => $title,
            'patient' => $patient,
            'date' => $diagnosis->created_at->format('d-M-Y'),
            'doctor' => $doctor->name,
            'service' => $service->service_name,
            'service_mode' => $service_mode->name,
            'vitals' => $vitals,
            'diagnosis' => $diagnosis,
            'prescription' => $prescription,
            'lab_result' => $lab
        ]));
    }    

    public function visit(Request $request)
    {         
        $pqns = PatientQueueTemporary::where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('created_at','desc')->first();
        if($pqns==null){
            $pqn = 1;
        }else{
            $pqn = $pqns->patient_que_no+1;
        }

        $visit_no = PatientVisit::where('patient_registration_no','=',$request->reg_no)->orderBy('created_at','desc')->first();
        if($visit_no==null){
            $vn = 1;
        }else{
            $vn = $visit_no->visit_no+1;
        }
        
        $billing = new Billing;
        $billing->patient_registration_no = $request->reg_no;
        $billing->visit_no = $vn;
        $billing->service_cost = $request->service_cost;
        $billing->amount_due = $request->amount_due;
        $billing->transaction_type = $request->transaction_type;
        $billing->discount = 0;
        $billing->save();

        // $queue = new PatientQueue;
        // $queue->patient_reg_no = $request->reg_no;
        // $queue->service_mode_id = $request->service_mode;
        // $queue->service_id = $request->service;
        // $queue->doc_id = $request->doctor;
        // $queue->id = $request->unit_allocation;
        // $queue->date = date('d-m-Y h:mm:ss');
        // $queue->room_no = $request->room_no;

        // $queue->save();

        $queue = new PatientQueueTemporary;
        $queue->patient_que_no = $pqn;
        $queue->patient_reg_no = $request->reg_no;
        $queue->service_mode = $request->service_mode;
        $queue->requested_service = $request->service;
        $queue->doctor = $request->doctor;
        $queue->unit_allocation = $request->unit_allocation; 
        $queue->room_no = $request->room_no;
        $queue->visit_no = $vn;
        $queue->billing_id = $billing->id;
        $queue->status = 'waiting';


        $queue->save();


        $vital = new Vital;
        $vital->patient_reg_no = $request->reg_no;
        $vital->visit_no = $vn;
        $vital->weight = $request->weight;
        $vital->height = $request->height;
        $vital->blood_pressure = $request->blood_presure;
        $vital->pulse_rate = $request->pulse_rate;
        $vital->respiratory_rate = $request->respiratory_rate;
        $vital->temperature = $request->temperature;
        $vital->other = $request->other;
        $vital->nurse = $request->nurse;

        $vital->save();

        $visit = new PatientVisit;
        $visit->patient_registration_no = $request->reg_no;
        $visit->visit_no = $vn;
        $visit->doctor_id = 1;
        $visit->save();


        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $request->amount_paid;
        $billingtransaction->amount_due = $request->amount_due;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();

        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $request->amount_paid;
        $billingchange->change = $request->change;
        $billingchange->change_given = $request->change_given;
        $billingchange->change_due = $request->change_remaining;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();

        return redirect('/reception');
    }    

    public function name_ajax(Request $request)
    {
        $query = $request->get('term','');
            
        $patients = Patient::where('name','LIKE','%'.$query.'%')->get();
        
        $data = array();

        foreach ($patients as $patient) {
            $data[] = array('value'=>$patient->name,'id'=>$patient->id);
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
    public function reg_no_ajax(Request $request)
    {
        $query = $request->get('term','');
            
        $patients = Patient::where('reg_no','LIKE','%'.$query.'%')->get();
        
        $data = array();

        foreach ($patients as $patient) {
            $data[] = array('value'=>$patient->reg_no,'id'=>$patient->id);
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function autocomplete_ajax($id)
    {
        $patient = Patient::whereId($id)->first();
        
        return response()->json([
            'reg_no' => $patient->reg_no,
            'name'=> $patient->name,
            'p_type' => $patient->type,
            'photo' => $patient->photo,
            'email' => $patient->email,
            'gender' => $patient->gender,
            'phone' => $patient->phone,
            'address' => $patient->address
        ]);

    }    

    public function photo(Request $request)
    {
        $type = $request->type;
        if($type == 'child')
        {
            return view('patients.child-photo');
        }
        return view('patients.photo');
    }

    public function getNextPatientQueNo() 
    {
        $nextId = DB::select("show table status like 'patient_daily_que'");
        return $que_no = $nextId[0]->Auto_increment;
    }

    public function generatePatientRegNo() 
    {
        $nextId = DB::select("show table status like 'patients'");
        $reg_no = $nextId[0]->Auto_increment;
        return response()->json(['reg_no' => $reg_no]);
    }

    public function getAge(Request $request)
    {
        $dob = $request->dob;
        $date = date_create_from_format('Y-m-d', $dob);       

        $reference = new DateTime;

        $diff = $reference->diff($date);

        $years = $diff->y;
        $months = $diff->m;
        $days = $diff->d;
        
        return response()->json(['years' => $years, 'months' => $months, 'days' => $days]);
    }
}
