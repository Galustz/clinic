<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LPO;
use App\LPOItem;
use App\Medicine;
use App\Payment;
use App\SupplerPayment;
use App\Supplier;
use App\SupplierEquipment;
use App\SupplierMedicine;
use App\SupplierPayment;
use Carbon\Carbon;

class SuppliersController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            'admin/suppliers',
            'admin.suppliers',
            [],
            [
                "delete_url"=>"/medicine/delete/",
                "equipment_delete_url"=>"/equipment/delete/",
                "payment_delete_url"=>"/payment/delete/",
                "supplier_view_url"=>"/view",
                "supplier_delete_url"=>"/delete",
                "supplier_edit_url"=>"/edit",
                "edit_url"=>"/medicine/edit/",
                "add_medicine_url"=>"/medicine/add",
                "add_equipment_url"=>"/equipment/add",
                "add_payment_url"=>"/payment/add",
                "lpo_create_url"=>"/lpo/add",
                "suppliers_list_url"=>"",
                "cancel_lpo_url"=>"/lpo/cancel",
                "credit_note_url"=>"/lpo/creditnote",
                "goods_received_url"=>"/lpo/received",
                "attach_receipt_url"=>"/lpo/attachreceipt",
                "attach_invoice_url"=>"/lpo/attachinvoice",
                "view_lpo_url"=>"/lpo/view",
                "edit_lpo_url"=>"/lpo/edit"
            ],
            null
        ); 
    }

    public function index(Request $request)
    {
        $this->suppliers = Supplier::all();
        return $this->cView('index');
    }
    
    public function addForm(Request $request)
    {
        $this->medicines = Medicine::all();
        $this->equipments = Equipment::all();
        return $this->cView('add');
    }
    
    public function editForm(Request $request)
    {
        $this->link = request()->has('link')?request('link'):'details'; 
        $this->medicines = Medicine::all();
        $this->equipments = Equipment::all();
        $this->edit = true; 
        $this->supplier = Supplier::find($request->supplier_id);

        return $this->cView('view');
    }
    
    public function edit(Request $request)
    {
        $supplier = Supplier::find($request->supplier_id);
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->contact_person = $request->contact_person;
        $supplier->phone_no = $request->phone_no;
        $supplier->save();

        return redirect($this->root_url)->with("success_msg","Supplier updated successfully");
    }

    public function delete(Request $request)
    {
        $id = $request->supplier_id;
        try {
            SupplerPayment::whereSupplierId($id)->delete();
            SupplierEquipment::whereSupplierId($id)->delete();
            SupplierMedicine::whereSupplierId($id)->delete();
            $lpos = LPO::whereSupplierId($id)->get();
            foreach ($lpos as $key => $lpo) {
                foreach ($lpo->items as $key => $item) {
                    $item->delete();
                }
                $lpo->delete();
            } 
            Supplier::find($id)->delete();            
        } catch (\Throwable $e) {
            
        } 
        return redirect($this->root_url)->with("success_msg","Supplier removed successfully");
    }

    public function add(Request $request){ 
        // dd($request->medicines);
        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->contact_person = $request->contact_person;
        $supplier->phone_no = $request->phone_no;
        $supplier->save();
         
        foreach($request->medicines as $index=>$medicine_id){
            // dd($medicine_id);
            $suppliermedicine = new SupplierMedicine();
            $suppliermedicine->medicine_id = $medicine_id;
            $suppliermedicine->supplier_id = $supplier->id;
            $suppliermedicine->price = $request->price[$index];
            $suppliermedicine->save(); 
        } 
        foreach($request->equipments as $key=>$equipment_id){
            // dd($equipment_id);
            $supplierequipment = new SupplierEquipment();
            $supplierequipment->equipment_id = $equipment_id;
            $supplierequipment->supplier_id = $supplier->id;
            $supplierequipment->price = $request->equipment_price[$key];
            $supplierequipment->save(); 
        }
        return redirect()->back()->with('success_msg','Supplier added successfully'); 
    }

    public function view(Request $request){
        $this->link = request()->has('link')?request('link'):'details';
        $this->supplier = Supplier::with('medicines')->find($request->id); 
        $this->medicines = Medicine::all();
        $this->equipments = Equipment::all();
        $this->lpos = $this->supplier->lpos()->where('status','!=','cancelled')->get();

        $this->is_editing = false;
        if($request->has('supplier_medicine_id')){
            $supplier_medicine = SupplierMedicine::find($request->supplier_medicine_id);
            $this->is_editing = true;
            $this->edit_medicine_id = $supplier_medicine->medicine_id;
            $this->edit_price = $supplier_medicine->price;
            $this->supplier_medicine_id = $supplier_medicine->id;
        }
        if($request->has('supplier_equipment_id')){
            $supplier_medicine = SupplierEquipment::find($request->supplier_equipment_id);
            $this->is_editing = true;
            $this->edit_equipment_id = $supplier_medicine->equipment_id;
            $this->edit_price = $supplier_medicine->price;
            $this->supplier_equipment_id = $supplier_medicine->id;
        }

        return $this->cView('view');
    }

    public function deleteMedicine($supplier_medicine_id)
    {
        SupplierMedicine::find($supplier_medicine_id)->delete();
        return redirect()->back()->with('success_msg','Deleted successfully');
    }
    public function deleteEquipment($supplier_equipment_id)
    {
        SupplierEquipment::find($supplier_equipment_id)->delete();
        return redirect()->back()->with('success_msg','Deleted successfully');
    }
    public function deletePayment($supplier_payment_id)
    {
        SupplierPayment::find($supplier_payment_id)->delete();
        return redirect()->back()->with('success_msg','Deleted successfully');
    }

    public function addMedicine(Request $request)
    {
        $is_editing = $request->has('supplier_medicine_id');
        if($is_editing)
            $suppliermedicine = SupplierMedicine::find($request->supplier_medicine_id);
        else
            $suppliermedicine = new SupplierMedicine();
        $suppliermedicine->medicine_id = $request->medicine_id;
        $suppliermedicine->supplier_id = $request->supplier_id;
        $suppliermedicine->price = $request->price;
        $suppliermedicine->save();

        if($is_editing){
            return redirect('admin/suppliers/view/'.$suppliermedicine->supplier_id.'?link=medicines')->with('success_msg','Medicine added successfully');
        }
        return redirect()->back()->with('success_msg','Medicine added successfully');
    }

    public function addEquipment(Request $request)
    {
        $is_editing = $request->has('supplier_equipment_id');
        if($is_editing)
            $supplierequipment = SupplierEquipment::find($request->supplier_equipment_id);
        else
            $supplierequipment = new SupplierEquipment();
        $supplierequipment->equipment_id = $request->equipment_id;
        $supplierequipment->supplier_id = $request->supplier_id;
        $supplierequipment->price = $request->price;
        $supplierequipment->save();

        if($is_editing){
            return redirect('admin/suppliers/view/'.$supplierequipment->supplier_id.'?link=equipments')->with('success_msg','Equipment added successfully');
        }
        return redirect()->back()->with('success_msg','Medicine added successfully');
    }

    public function addPayment(Request $request)
    {    
        $payment = Payment::create([
            "description"=>$request->description,
            "date"=>Carbon::parse($request->date),
            "amount"=>$request->amount
        ]);
        $supplierpayment = new SupplierPayment;
        $supplierpayment->payment_id = $payment->id;
        $supplierpayment->supplier_id = $request->supplier_id; 
        $supplierpayment->save(); 
        return redirect()->back()->with('success_msg','Medicine added successfully');
    }
}
