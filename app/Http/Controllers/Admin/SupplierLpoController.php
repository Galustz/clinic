<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\LPO;
use App\LPOItem;
use App\Supplier;
use App\SupplierEquipment;
use App\SupplierMedicine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Medicine;
use App\Payment;
use App\SupplierPayment;
use Carbon\Carbon;

class SupplierLpoController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            'admin/suppliers',
            'admin.suppliers.tabs.lpos',
            [],
            [ 
                "lpo_create_url"=>"/lpo/add",
                "suppliers_list_url"=>"",
                "cancel_lpo_url"=>"/lpo/cancel",
                "credit_note_url"=>"/lpo/creditnote",
                "goods_received_url"=>"/lpo/received",
                "attach_receipt_url"=>"/lpo/attachreceipt",
                "attach_invoice_url"=>"/lpo/attachinvoice",
                "view_lpo_url"=>"/lpo/view",
                "edit_lpo_url"=>"/lpo/edit",
                "form_url"=>"/lpo/received"
            ],
            null
        ); 
    }



    public function viewLpo($lpo_id)
    {
        $this->lpo = Lpo::find($lpo_id);
        return $this->cView('view');
    }

    public function addLpoForm(Request $request)
    { 
        $this->supplier = Supplier::find($request->supplier_id);
        $this->medicines = $this->supplier->medicines;
        $this->equipments = $this->supplier->equipments;
        $this->terms = ["cash","credit"];
        return $this->cView('add');   
    }

    public function editLpoForm($lpo_id)
    { 
        $this->lpo = Lpo::find($lpo_id);
        $this->supplier = $this->lpo->supplier;
        $this->medicines = $this->supplier->medicines;
        $this->equipments = $this->supplier->equipments;

        $this->selected_medicines = $this->lpo->medicines();
        $this->selected_equipments = $this->lpo->equipments();
 
        $this->terms = ["cash","credit"];
        return $this->cView('edit');   
    }

    public function editLpo(Request $request)
    {
        $lpo = LPO::find($request->lpo_id); 
        $lpo->terms = $request->terms; 
        $lpo->save();

        $lpo->items()->delete();
        
        foreach($request->medicines as $key => $medicine){
            $lpo_item = new LPOItem(); 
            $lpo_item->medicine_id = $request->medicines[$key];
            $lpo_item->l_p_o_id = $lpo->id;
            
            $price = SupplierMedicine::whereSupplierId($request->supplier_id)
                                        ->whereMedicineId($request->medicines[$key])
                                        ->first()
                                        ->price;

            $lpo_item->price = $price * $request->quantity[$key];
            $lpo_item->type = "medicine";
            $lpo_item->quantity = $request->quantity[$key];
            $lpo_item->save();
        }
        
        foreach($request->equipments as $key => $equipment_id){
            $lpo_item = new LPOItem; 
            $lpo_item->medicine_id = $request->equipments[$key];
            $lpo_item->l_p_o_id = $lpo->id;
            
            $price = SupplierEquipment::whereSupplierId($request->supplier_id)
                                        ->whereEquipmentId($request->equipments[$key])
                                        ->first()
                                        ->price;

            $lpo_item->price = $price * $request->equipment_quantity[$key];
            $lpo_item->type = "equipment";
            $lpo_item->quantity = $request->equipment_quantity[$key];
            $lpo_item->save();
        }

        return redirect($this->suppliers_list_url."/view/$request->supplier_id?link=lpos")->with('success_msg','Successfully created LPO');
    }

    public function addLpo(Request $request)
    {
        $lpo = new LPO();
        $lpo->user_id = $request->user()->id;
        $lpo->terms = $request->terms;
        $lpo->supplier_id = $request->supplier_id;
        $lpo->save();
        
        foreach($request->medicines as $key => $medicine){
            $lpo_item = new LPOItem(); 
            $lpo_item->medicine_id = $request->medicines[$key];
            $lpo_item->l_p_o_id = $lpo->id;
            
            $price = SupplierMedicine::whereSupplierId($request->supplier_id)
                                        ->whereMedicineId($request->medicines[$key])
                                        ->first()
                                        ->price;

            $lpo_item->price = $price * $request->quantity[$key];
            $lpo_item->type = "medicine";
            $lpo_item->quantity = $request->quantity[$key];
            $lpo_item->save();
        }
        
        foreach($request->equipments as $key => $equipment_id){
            $lpo_item = new LPOItem; 
            $lpo_item->medicine_id = $request->equipments[$key];
            $lpo_item->l_p_o_id = $lpo->id;
            
            $price = SupplierEquipment::whereSupplierId($request->supplier_id)
                                        ->whereEquipmentId($request->equipments[$key])
                                        ->first()
                                        ->price;

            $lpo_item->price = $price * $request->equipment_quantity[$key];
            $lpo_item->type = "equipment";
            $lpo_item->quantity = $request->equipment_quantity[$key];
            $lpo_item->save();
        }

        return redirect($this->suppliers_list_url."/view/$request->supplier_id?link=lpos")->with('success_msg','Successfully created LPO');
    }

    public function goodsReceivedForm($lpo_id)
    { 
        $this->lpo = Lpo::find($lpo_id);
        $this->header = "Goods received from ".$this->lpo->supplier->name;
        $this->sale_types = LPO::SALE_TYPES;
        $this->item_stati = LPO::ITEM_STATI;
        return $this->cView('goods-received');
    }

    public function goodsReceived(Request $request, Medicine $_medicine, Equipment $_equipment)
    {
        $lpo = LPO::find($request->lpo_id);

        $is_complete = true;
        $is_all_received = true;

        foreach ($request->medicine_lpo_item_stati as $key => $value) {
            if($is_complete && $value == LPO::ITEM_STATI["nyr"])
                $is_complete = false;

            $lpo_item_id = $request->medicine_lpo_item_ids[$key];
            $lpo_item = LPOItem::find($lpo_item_id);
            if($lpo_item->status == 'rc'){
                $lpo_item->reason = $request->medicine_lpo_item_reasons[$key];
                $lpo_item->sale_status = $value;
                
                if($is_all_received && $lpo_item->quantity != $request->medicine_lpo_item_quantities[$key])
                    $is_all_received = false;
    
                $lpo_item->received_quantity = $request->medicine_lpo_item_quantities[$key];
                $lpo_item->save();
                

                $batch_number = 'MD'.Carbon::now()->format('YmdHis');
                $expiry_date = $request->medicine_expiry_dates[$key];
                $received_quantity = $request->medicine_lpo_item_quantities[$key];

                // batch_number, medicine_id, expiry_date, quantity
                $paid_amount = $_medicine->addNewBatch($lpo_item->medicine_id, $received_quantity, $expiry_date, $batch_number);

                if($request->sale_type == "in" || $request->sale_type == "cs"){
                    $payment = new Payment; 
                    $payment->description = "Payment for medicines in LPO id: ".$lpo->id;
                    $payment->amount = $paid_amount->total_cost;
                    $payment->date = Carbon::today();
                    $payment->save();

                    $supplier_payment = new SupplierPayment();
                    $supplier_payment->supplier_id = $lpo->supplier->id;
                    $supplier_payment->payment_id = $payment->id;
                    $supplier_payment->save();
                }
            }
        }

        foreach ($request->equipment_lpo_item_stati as $key => $value) {
            if($is_complete && $value == LPO::ITEM_STATI["nyr"])
                $is_complete = false;

            $lpo_item_id = $request->equipment_lpo_item_ids[$key];
            $lpo_item = LPOItem::find($lpo_item_id);

            if($lpo_item->status == "rc"){
                $lpo_item->reason = $request->equipment_lpo_item_reasons[$key];
                $lpo_item->sale_status = $value;
                
                if($is_all_received && $lpo_item->quantity != $request->equipment_lpo_item_quantities[$key])
                    $is_all_received = false;

                $lpo_item->received_quantity = $request->equipment_lpo_item_quantities[$key];
                $lpo_item->save();


                $date = Carbon::today();
                $batch_code = 'EQ'.Carbon::now()->format('YmdHis');
                // batch_number, medicine_id, expiry_date, quantity
                $paid_amount = $_equipment->addNewBatch(
                    $lpo_item->medicine_id,
                    $request->equipment_lpo_item_quantities[$key],
                    $date,$batch_code
                );

                if($request->sale_type == "in" || $request->sale_type == "cs"){
                    $payment = new Payment; 
                    $payment->description = "Payment for equipments in LPO id: ".$lpo->id;
                    $payment->amount = $paid_amount->total_cost;
                    $payment->date = Carbon::today();
                    $payment->save();

                    $supplier_payment = new SupplierPayment();
                    $supplier_payment->supplier_id = $lpo->supplier->id;
                    $supplier_payment->payment_id = $payment->id;
                    $supplier_payment->save();
                }
            }
        }

        if(!$is_complete)
            $lpo->update([
                "status"=>"incomplete",
                "sale_type"=>$request->sale_type,
                "received_date"=>$request->received_date,
                "sale_number"=>$request->sale_number
            ]);
        else if(!$is_all_received)
            $lpo->update([
                "status"=>"has_credit",
                "sale_type"=>$request->sale_type,
                "received_date"=>$request->received_date,
                "sale_number"=>$request->sale_number
            ]);

        return redirect($this->suppliers_list_url."/view/".$lpo->supplier->id."?link=lpos")->with('success_msg','Successfully received LPO');
    }

    public function cancelLpo($lpo_id)
    {
        $lpo = Lpo::find($lpo_id);
        $lpo->status = "cancelled";
        $lpo->save();
        return redirect()->back()->with('success_msg','Succesffully cancelled LPO');
    }

}
