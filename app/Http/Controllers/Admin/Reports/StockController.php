<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockController extends Controller
{
    public function index()
    {
        return view('admin.reports.stock.index');
    }

    public function filter(Request $request)
    {
        return view('admin.reports.stock.index',['from'=>$request->from,'to'=>$request->to]);
    }
}
