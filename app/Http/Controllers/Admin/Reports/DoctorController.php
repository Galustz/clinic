<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorController extends Controller
{
    
    public $layout_root = 'admin.reports.doctor.';
    
    function consultations()
    {
        return view($this->layout_root.'consultations');
    }

    
    public function consultationsFilter(Request $request)
    {
        return view($this->layout_root.'consultations',['from'=>$request->from,'to'=>$request->to]);
    }
}
