<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Diagnosis;
use App\DiagnosisRecord;
use App\BillingList;
use App\Billing;

class BillingController extends Controller
{
    public $layout_root = 'admin.reports.billing.';
    public function newPatients()
    {
        return view($this->layout_root.'new-patients');
    }

    public function newPatientsFilter(Request $request)
    {
        return view($this->layout_root.'new-patients',['from'=>$request->from,'to'=>$request->to]);
    }

    public function expressPatients()
    {
        return view($this->layout_root.'express-patients');
    }

    public function patientsPerDisease()
    {
        $diagnosis = Diagnosis::all();
        $records = DiagnosisRecord::with('diagnosis')->get();
        $patient_per_disease = collect([]);
        $keys = $patient_per_disease->keys();
        $values = $patient_per_disease->values();
        
        $grouped = $records->groupBy('diagnosis.name');

        return view($this->layout_root.'patients-per-disease', ['diseases'=>$grouped]);
    }

    public function filterPatientPerDiseaseByDate(Request $request){
        $from = $request->from;
        $to = $request->to;

        $records = DiagnosisRecord::with('diagnosis')
                    ->where('created_at','>',$from)
                    ->where('created_at','<',$to)
                    ->get();

        $grouped = $records->groupBy('diagnosis.name');
        return view($this->layout_root.'patients-per-disease', ['diseases'=>$grouped]);
    }

    public function list(){
        $list = Billing::with('patient')->get();
        $title = "Billing::list";
        return view('admin.billing',['title'=>$title,'list'=>$list]);
    }
}
