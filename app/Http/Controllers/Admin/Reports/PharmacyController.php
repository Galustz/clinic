<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Medicine;
use carbon\Carbon;

class PharmacyController extends Controller
{
    
    public $layout_root = 'admin.reports.pharmacy.';
    public function medicineSold()
    {
        $medicines = Medicine::all();
        $sum = $medicines->sum('selling_price');

        // dd($medicines);
        return view($this->layout_root.'medicine-sold',['medicines'=>$medicines,'total_amount_sold'=>$sum]);
    }

    public function filterMedicineSold(Request $request){
        $from = $request->from;
        $to = $request->to;

        $date = new Carbon($from);
        
        $medicines = Medicine::where('created_at','>', $from)->where('created_at','<',$to)->get();

        return view($this->layout_root.'medicine-sold',['medicines'=>$medicines]);
    }
}
