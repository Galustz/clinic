<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LaboratoryController extends Controller
{
    public $layout_root = 'admin.reports.laboratory.';

    public function equipments()
    {
        return view($this->layout_root.'equipments');
    }

    public function equipmentsFilter(Request $request)
    {
        return view($this->layout_root.'equipments',['from'=>$request->from,'to'=>$request->to]);
    }
}
