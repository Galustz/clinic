<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Filters\MonthFilter;
use App\Filters\YearFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;

class MtuhaController extends Controller
{
    //
    public function index(Request $request)
    {
        $form_no = $request->has('form_no')?$request->form_no:5;
        $years = YearFilter::getYears();
        $months = MonthFilter::getMonths();
        $month = $request->has('month')?$request->month:1;
        $year = $request->has('year')?$request->year:2020;

        $kituo = Setting::getSettingValue("clinic_name");
        $wilaya = Setting::getSettingValue('wilaya');
        $mkoa = Setting::getSettingValue('mkoa');

        $month_formatted = $months[$month];

        $data = [
            'title'=>'Mtuha Reports',
            'form_no'=>$form_no,
            'years'=>$years,
            'months'=>$months,
            'month'=>$month,
            'year'=>$year,
            'kituo'=>$kituo,
            'wilaya'=>$wilaya,
            'mkoa'=>$mkoa,
            'month_formatted'=>$month_formatted
        ]; 
        return view('admin.reports.mtuha.index',$data); 
    }
}
