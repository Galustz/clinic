<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\PatientType;
use App\ServiceMode;

class ReceptionController extends Controller
{
    //
    public function services(){
        
        return view('reception.services.index',['title'=>'Services']);

    }

    public function deleteService($id){
        $service = Service::find($id);
        $service->trashed = "true";
        $service->save();
        return view('reception.services.index',['title'=>'Services']);
    }

    public function editServiceForm($id){
        return view('reception.services.edit-service',['title'=>'Edit Service','id'=>$id]);
    }

    public function editService(Request $request){
        $service = Service::find($request->service_id);
        $service->name = $request->name;
        $service->price = $request->price;
        $service->save();        
        return view('reception.services.index',['title'=>'Services']);
    }

    public function newService(Request $request){
        $service = new Service;
        $service->name = $request->name;
        $service->price = $request->price;
        $service->save();
        return redirect()->back();
    }

    public function patientType(){        
        return view('reception.patienttypes.index',['title'=>'Patient Types']);
    }

    public function deletePatientType($id){
        $patienttype = PatientType::find($id);
        $patienttype->trashed = "true";
        $patienttype->save();
        return view('reception.patienttypes.index',['title'=>'Patient Type']);
    }

    public function editPatientTypeForm($id){
        return view('reception.patienttypes.edit-patienttype',['title'=>'Edit Patient Type','id'=>$id]);
    }

    public function editPatientType(Request $request){
        $patienttype = PatientType::find($request->patienttype_id);
        $patienttype->name = $request->name;
        $patienttype->price = $request->price;
        $patienttype->save();        
        return view('reception.patienttypes.index',['title'=>'Patient Type']);
    }

    public function newPatientType(Request $request){
        $patienttype = new PatientType;
        $patienttype->name = $request->name;
        $patienttype->price = $request->price;
        $patienttype->save();
        return redirect()->back();
    }

    
    public function serviceMode(){        
        return view('reception.servicemodes.index',['title'=>'Service modes']);
    }

    public function deleteServiceMode($id){
        $servicemode = ServiceMode::find($id);
        $servicemode->trashed = "true";
        $servicemode->save();
        return view('reception.servicemodes.index',['title'=>'Service Mode']);
    }

    public function editServiceModeForm($id){
        return view('reception.servicemodes.edit-servicemode',['title'=>'Edit Service Mode','id'=>$id]);
    }

    public function editServiceMode(Request $request){
        $servicemode = ServiceMode::find($request->servicemode_id);
        $servicemode->name = $request->name;
        $servicemode->price = $request->price;
        $servicemode->save();        
        return view('reception.servicemodes.index',['title'=>'Service Mode']);
    }

    public function newServiceMode(Request $request){
        $servicemode = new ServiceMode;
        $servicemode->name = $request->name;
        $servicemode->price = $request->price;
        $servicemode->save();
        return redirect()->back();
    }

}
