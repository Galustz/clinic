<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Patient;

class PatientController extends Controller
{
    //
    public function viewPatient($id){
        return view('patients.details.view',['id'=>$id,'title'=>'Patient Details']);
    }

    public function editPatientShow
    ($id){
        return view('patients.details.edit',['id'=>$id,'title'=>'Edit Patient']);
    }

    public function editPatient(Request $request){
        $patient = Patient::find($request->id);
        $patient->name = $request->name;
        $patient->gender = $request->gender;
        $patient->blood_group = $request->blood_group;
        $patient->patient_type = $request->patient_type;
        $patient->nationality = $request->nationality;
        $patient->email = $request->email;
        $patient->address = $request->address;
        $patient->telephone = $request->landline;
        $patient->phone = $request->phone;
        $patient->DOB = $request->dob;
        $patient->save();
        return view('patients.details.view',['id'=>$request->id,'title'=>'Patient Details']);
    }

    
    public function history($id){
        return view('doctor.history',['id'=>$id,'title'=>'Patient History']);
    }
}
