<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrashController extends Controller
{
    //
    public function services()
    {
        return view('trash.services.index',['title'=>'Trash']);  
    }
    public function patienttypes()
    {
        return view('trash.patienttypes.index',['title'=>'Trash']);
    }
    public function servicemodes()
    {
        return view('trash.servicemodes.index',['title'=>'Trash']);
    }
    public function tests()
    {
        return view('trash.tests.index',['title'=>'Trash']);
    }
    public function medicine()
    {
        return view('trash.medicine.index',['title'=>'Trash']);
    }
}
