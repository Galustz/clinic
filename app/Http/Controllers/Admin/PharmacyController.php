<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Medicine;
use App\MedicineCategory;
use App\Supplier;
use App\SupplierMedicine;
use App\MedicineStockBatch;

class PharmacyController extends Controller
{
    //
    public function categories(){
        return view('admin.pharmacy.medicine-categories.index',['title'=>'Categories']);
    }

    public function deleteCategory($id){

    }

    public function newCategory(Request $request){
        $cat = new MedicineCategory;
        $cat->name = $request->name;
        $cat->description = $request->description;
        $cat->save();
        return redirect()->back();
    }

    public function medicines(){
        return view('admin.pharmacy.medicine.index',['title'=>'Medicines']);
    }

    public function deleteMedicine($id){
        $medicine = Medicine::find($id);
        $medicine->trashed = "true";
        $medicine->save();
        return view('admin.pharmacy.medicine.index',['title'=>'Medicines']);
    }

    public function newMedicineForm(){
        return view('admin.pharmacy.medicine.new-medicine',['title'=>'Medicines']);
    }

    public function newMedicine(Request $request){  
        $med = new Medicine;
        $med->name = $request->name;
        $med->selling_price = $request->price;
        $med->description = $request->description;
        $med->medicine_category_id = $request->medicine_category;
        $med->selling_unit_name = $request->selling_unit_name;
        $med->purchasing_unit_name = $request->purchasing_unit_name;
        $med->predefined_value = $request->predefined_value;
        $med->code = $request->code;
        $med->code_2 = $request->code_2;
        $med->unit_purchasing_price = $request->unit_purchasing_price;
        $med->quantity_per_unit = $request->quantity_per_unit;
        if($request->has('mgs_per_tablet'))
            $med->mgs_per_tablet = $request->mgs_per_tablet;
        $med->save();
        return view('admin.pharmacy.medicine.new-medicine',['title'=>'Medicines','success_msg'=>'Medicine Added Successfuly']);
    }

    public function saveMedicine(Request $request){  
        $med = Medicine::find($request->medicine_id);
        $med->name = $request->name;
        $med->selling_price = $request->price;
        $med->description = $request->description;
        $med->medicine_category_id = $request->medicine_category;
        $med->selling_unit_name = $request->selling_unit_name;
        $med->purchasing_unit_name = $request->purchasing_unit_name;
        $med->predefined_value = $request->predefined_value;
        $med->code = $request->code;
        $med->code_2 = $request->code_2;
        $med->unit_purchasing_price = $request->unit_purchasing_price;
        $med->quantity_per_unit = $request->quantity_per_unit;
        if($request->has('mgs_per_tablet'))
            $med->mgs_per_tablet = $request->mgs_per_tablet;
        $med->save();
        return view('admin.pharmacy.medicine.view',['title'=>'Medicines','success_msg'=>'Medicine Edited Successfuly','id'=>$med->id]);
    }

    public function suppliers(){
        return view('admin.pharmacy.suppliers.index',['title'=>'Suppliers']);
    }

    public function newSupplierForm(){
        $medicines = Medicine::all();
        return view('admin.pharmacy.suppliers.new-supplier',['title'=>'Suppliers','medicines'=>$medicines]);
    }

    public function newSupplier(Request $request){ 
        // dd($request->medicines);
        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->contact_person = $request->contact_person;
        $supplier->phone_no = $request->phone_no;
        $supplier->save();
        
        $index = 0;
        foreach($request->medicines as $medicine_id){
            // dd($medicine_id);
            $suppliermedicine = new SupplierMedicine;
            $suppliermedicine->medicine_id = $medicine_id;
            $suppliermedicine->supplier_id = $supplier->id;
            $suppliermedicine->price = $request->price[$index];
            $suppliermedicine->save();
            $index++;
        }
        $medicines = Medicine::all();
        return view('admin.pharmacy.suppliers.new-supplier',['title'=>'Suppliers','success_msg'=>'Supplier Added Successfuly'
                        ,'medicines'=>$medicines]);
    }

    public function viewSupplier($id){
        $supplier = Supplier::with('medicines')->find($id);

        // dd($supplier->medicines);

        return view('admin.pharmacy.suppliers.view-supplier',['supplier'=>$supplier]);
    }

    public function lpo(){
        return view('admin.pharmacy.lpo.index',['title'=>'LPO']);
    }

    public function newLpoForm(){
        return view('admin.pharmacy.lpo.new-lpo',['title'=>'LPO']);
    }

    public function newLpo(Request $request){
        dd($request);
    }

    public function viewMedicine($id){
        return view('admin.pharmacy.medicine.view',['id'=>$id]);
    }

    public function newBatchForm($id)
    {
        return view('admin.pharmacy.medicine.new-batch',['id'=>$id]);
    }
    
    public function newBatch(Request $request, Medicine $medicine)
    { 
        $medicine->addNewBatch(
            $request->medicine_id,
            $request->number_of_units_bought,
            $request->expiry_date,
            $request->batch_number
        );
        return view('admin.pharmacy.medicine.view',['id'=>$request->medicine_id,'success_msg'=>'batch added succesfully']);
    }

    public function editBatchForm($id)
    {
        return view('admin.pharmacy.medicine.edit-batch',['id'=>$id]);
    }
    
    public function editBatch(Request $request)
    { 
        $batch = MedicineStockBatch::find($request->id); 
        $medicine = Medicine::find($batch->medicine_id); 

        $total_price = $medicine->unit_purchasing_price * $request->number_of_units_bought;
        $total_quantity = $medicine->quantity_per_unit * $request->number_of_units_bought;
        $remaining_quantity = $total_quantity;
         
        $batch->number_of_units_bought=$request->number_of_units_bought;
        $batch->total_price= $total_price;
        $batch->total_quantity= $total_quantity;
        $batch->expiry_date=$request->expiry_date;
        $batch->batch_number=$request->batch_number;
        $batch->remaining_quantity= $remaining_quantity;
        $batch->unit_purchasing_price=$medicine->unit_purchasing_price;
        $batch->save();        

        return view('admin.pharmacy.medicine.view',['id'=>$batch->medicine_id,'success_msg'=>'batch edited succesfully']);
    }

public function getMd()
{ 
    $medicine_id=1;
    $requested_medicine_amount = 53;//remaining 400
    $continue_iterations = true;
    $batch_ids[]=0; 
    $remaining_quantity = $requested_medicine_amount; 
    if($this->checkForEnoughMedicinesInStock($requested_medicine_amount,$medicine_id)){
        while($continue_iterations){       
            $batch  = \App\MedicineStockBatch::where('medicine_id',$medicine_id)
                                        ->where('remaining_quantity','!=','0')
                                        ->whereNotIn('id',$batch_ids)
                                        ->orderBy('expiry_date','asc')
                                        ->first();         
            $batch_ids[] = $batch->id;
            if($requested_medicine_amount<=$batch->remaining_quantity){  
                //good there's enough....
                $remaining_quantity = $batch->remaining_quantity - $requested_medicine_amount;
                $batch->remaining_quantity = $remaining_quantity;
                $batch->save();
                //continue with the storage procedure
                $continue_iterations = false; 
            }
            else{
                $requested_medicine_amount = $requested_medicine_amount - $batch->remaining_quantity;
                    
                $batch->remaining_quantity = 0;
                $batch->save();  
            } 
        }     
    }
    else{
        echo 'there arent enough meds';
    }   
}

public function checkForEnoughMedicinesInStock(Request $request)
{ 
    $requested_medicine_amount = $request->total_quantity;
    $medicine_id = $request->medicine_id;
    $continue_iterations = true;
    $batch_ids[]=0; 
    $remaining_quantity = $requested_medicine_amount;
    $there_are_enough_medicines = "false";
    $i = 1;
    $medicine = Medicine::find($medicine_id);
    while($continue_iterations){       
        $batch  = \App\MedicineStockBatch::where('medicine_id',$medicine_id)
                                    ->where('remaining_quantity','!=','0')
                                    ->whereNotIn('id',$batch_ids)
                                    ->orderBy('expiry_date','asc')
                                    ->first();
        
        if($batch!=null){
            $batch_ids[] = $batch->id;
            if($requested_medicine_amount<=$batch->remaining_quantity){  
                //good there's enough....
                $remaining_quantity = $batch->remaining_quantity - $requested_medicine_amount;
                $batch->remaining_quantity = $remaining_quantity; 
                //continue with the storage procedure
                $requested_medicine_amount = 0;
                $continue_iterations = false; 
                $there_are_enough_medicines = "true";
            }
            else{
                $requested_medicine_amount = $requested_medicine_amount - $batch->remaining_quantity;
                $batch->remaining_quantity = 0;  
            }
        }
        else{
            $continue_iterations = false; 
            $there_are_enough_medicines = "false";
        } 
        $i++;
    }
    $remaining = $request->total_quantity-$requested_medicine_amount;
    return ['available'=>$there_are_enough_medicines,'medicine'=>$medicine->name,'remaining_quantity'=>$remaining]; 
}

public function p($message)
{
    echo $message.'</br>';
}
}