<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Unit;
use App\Room;

class UsersController extends Controller
{
    //
    public function index(){
        return view('admin.users.users',['title'=>'Users']);
    }

    public function units(){
        return view('users.units.index',['title'=>'Units']);

    }

    public function deleteUnit($id){

    }

    public function newUnit(Request $request){
        $unit = new Unit;
        $unit->name = $request->name;
        $unit->description = $request->description;
        $unit->save();
        return redirect()->back();
    }

    public function rooms(){
        return view('users.rooms.index',['title'=>'Rooms']);

    }

    public function deleteRoom($id){

    }

    public function newRoom(Request $request){
        $room = new Room;
        $room->name = $request->name;
        $room->use = $request->use;
        $room->save();
        return redirect()->back();
    }

    public function deleteUser($id)
    {
        $user = \App\User::find($id);
        $user->delete();
        return redirect()->back();
    }

    public function editUserForm($id)
    {
        $user = \App\User::find($id);
        return view('admin.users.edit',['user'=>$user]);
    }

    public function editUser(Request $request){
        
        $this->validate($request, [
            'password' => 'confirmed' 
        ]);
    
        $user = \App\User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->employee_id = $request->employee_id;
        $user->save();          
        
        return redirect('admin/users');
    }
}
