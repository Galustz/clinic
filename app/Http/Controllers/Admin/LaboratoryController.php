<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LabTest;

class LaboratoryController extends Controller
{
    //
    public function tests(){
        return view('lab.tests.index',['title'=>'Lab Tests']);
    }

    public function deleteTest($id){
        $test = LabTest::find($id);
        $test->trashed = "true";
        $test->save();
        return view('lab.tests.index',['title'=>'Lab Tests']);
    }

    public function newTest(Request $request){
        $test = new LabTest();
        $test->name = $request->name;
        $test->price = $request->price;
        $test->code = $request->code;
        $test->save();
        return redirect()->back();
    }

    public function testEditForm($id)
    {
        return view('lab.tests.edit',['title'=>'Edit lab test','id'=>$id]);
    }

    public function testEdit(Request $request)
    { 
        $test = LabTest::find($request->id);
        $test->name = $request->name;
        $test->price  = $request->price;
        $test->code = $request->code;
        $test->save();
        return view('lab.tests.index',['title'=>'Lab Tests']);
    }
    
    public function queue(){
        return view('admin.labqueue',['title'=>'Laboratory Queue']);
    }

    public function results(){
        return view('admin.labresults',['title'=>'Laboratory Results']);
    }
}
