<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index()
    {
        return view('admin.settings.index');
    }

    // public function update(){ 
    //     exec('git status',$result[]);
    //     exec('git log --oneline',$result[]);
    //     exec('php artisan migrate:refresh',$result[]);
    //     exec('php artisan db:seed',$result[]);
    //     return redirect('/');
    // }

    public function set(Request $request)
    {   
        $settings = new \App\Setting;
        $settings->name = "clinic_name";
        $settings->value = $request->clinic_name;
        $settings->save();
 
        $settings = new \App\Setting;
        $settings->name = "service_charge";
        $settings->value = $request->service_charge;
        $settings->save();
        
        $settings = new \App\Setting;
        $settings->name = "discount";
        $settings->value = $request->discount;
        $settings->save();
        return redirect()->back();
    }

    public function updateSettings(Request $request)
    {    
        $settings = \App\Setting::firstOrNew(['name' => 'clinic_name']);
        $settings->value = $request->clinic_name==null?'':$request->clinic_name;
        $settings->save();
        $settings = \App\Setting::firstOrNew(['name' => 'service_charge']);
        $settings->value = $request->service_charge==null?'':$request->service_charge;
        $settings->save();
        $settings = \App\Setting::firstOrNew(['name' => 'mkoa']);
        $settings->value = $request->mkoa==null?'':$request->mkoa;
        $settings->save();
        $settings = \App\Setting::firstOrNew(['name' => 'wilaya']);
        $settings->value = $request->wilaya==null?'':$request->wilaya;
        $settings->save();
        // $settings = \App\Setting::where('name','discount')->first();
        // $settings->value = $request->discount;
        // $settings->save();
        return redirect()->back();
    }
}
