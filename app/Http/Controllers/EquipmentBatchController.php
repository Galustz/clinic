<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\EquipmentBatch;
use Illuminate\Http\Request;

class EquipmentBatchController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/equipments/batches',
            'equipments.batches',
            ['role:admin'],
            [
                "add_equipment_url"=>'/add',
                "form_url"=>'/add',
                "edit_url"=>'/add',
                "delete_url"=>'/remove'
            ],
            EquipmentBatch::class
        ); 
    }

    public function restock($equipment_id)
    {
        $this->equipment_id = $equipment_id; 
        $this->model =  Equipment::find($equipment_id);
        $this->header = "Restock ".$this->model->name;
        $this->is_editing=true;
        return $this->cView('form');
    }

    public function add(Request $request, Equipment $equipment)
    {  
        $data = $request->except(['_token','id']);
        $data["equipment_id"] = $request->id;
        $data["remaining_quantity"] = $request->quantity;
        EquipmentBatch::create($data);
        return redirect('equipments');
    }

    public function delete(Request $request)
    {
        $this->getModel()->delete();
        return redirect()->back()->with('success_msg','Batch removed successfully');
    }
}
