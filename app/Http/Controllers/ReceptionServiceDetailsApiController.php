<?php

namespace App\Http\Controllers;

use App\CorporateCompany;
use App\InsuranceCompany;
use App\LabTest;
use App\Medicine;
use App\MedicineCategory;
use App\MedicineUnit;
use App\Patient;
use App\PatientQueueTemporary;
use App\PatientType;
use App\Service;
use App\ServiceMode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReceptionServiceDetailsApiController extends Controller
{
    public function getServices()
    {
        return Service::get(['id','name','price']);
    }

    public function getServiceModes()
    {
        return ServiceMode::get(['id','name','price']);
    }

    public function getPatientTypes()
    {
        return PatientType::get(['id','name']);
    }

    public function getInsuranceCompanies()
    {
        return InsuranceCompany::get(['id','name']);
    }

    public function getCorporateCompanies()
    {
        return CorporateCompany::get(['id','name']);
    }

    public function getRegNoSuggestions(Request $request)
    {
        $query = $request->query('query');
        if(!empty($query))
            return Patient::where('reg_no','LIKE',"%$query%")->get(['id','reg_no','name','patient_type']);
        return [];
    }

    public function getNameSuggestions(Request $request)
    {
        $query = $request->query('query');
        if(!empty($query))
            return Patient::where('name','LIKE',"%$query%")->get(['id','reg_no','name','patient_type']);
        return [];
    }

    public function getQueNumber()
    { 
        $patient_que = PatientQueueTemporary::where('created_at', '>=', date('Y-m-d').' 00:00:00')
                                            ->orderBy('created_at','desc')
                                            ->first();
        if($patient_que)
            return $patient_que->patient_que_no;
        else
            return 1;
    }

    public function getDoctors()
    {
        return User::getStaff("doctor")->get(['users.id','users.name']);
    }

    public function getConsultants()
    {
        return User::getStaff("consultant")->get(['users.id','users.name','users.fee']);
    }

    public function getTests()
    {
        return LabTest::get(['id','name','price']);
    }

    public function getMedicineTypes()
    {
        $types = MedicineCategory::get(['id','name']);
        $nt = $types->map(function( $item, $key){
            $item["key"] = str_random(20);
            return $item;
        });
        return $nt;
    }

    public function getMedicines($id)
    {
        $meds = Medicine::whereMedicineCategoryId($id)->get(['id','name','selling_price']);
        $nt = $meds->map(function( $item, $key){
            $item["key"] = str_random(20);
            $item["remaining_quantity"] = $item->remainingQuantity();
            return $item;
        });
        return $nt;
    }

    public function getMedicineUnits()
    {
        $units = MedicineUnit::get(['id','name']);
        $unts = $units->map(function($item,$key){
            $item["text"] = $item->name;
            return $item;
        });
        return $unts;
    }
}
