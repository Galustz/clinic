<?php

namespace App\Http\Controllers;

use App\Equipment;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{  
    public function __construct()
    {
        $this->initialise(
            '/equipments',
            'equipments.',
            ['role:admin'],
            [
                "add_equipment_url"=>'/add',
                "form_url"=>'/add',
                "edit_url"=>'/add',
                "delete_url"=>'/remove',
                "restock_url"=>'/batches/restock',
                "batch_delete_url"=>'/batches/delete',
                "view_url"=>'/view'
            ],
            Equipment::class
        );
    }

    public function addForm()
    {   
        $this->header = $this->hasId()?"Edit Equipment":"Add Equipment";
        $this->model = $this->getModel(Equipment::class); 
        return $this->cView('add');
    }


    public function add()
    {
        $this->equipment = $this->getModel(Equipment::class);
        if($this->hasId()){ 
            $this->equipment->update($this->request->except('_token'));
        }
        else{ 
            $equipment = Equipment::create($this->request->except('_token')); 
        }
        return redirect($this->root_url)->with('success_msg','Equipment updated successfully'); 
    }

    public function index()
    { 
        $this->equipments = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function remove()
    {
        $this->defaultModel::find($this->request->id)->delete();
        return redirect($this->root_url)->with('success_msg','Equipment removed successfully');
    }

    public function view($equipment_id)
    {
        $this->model = $this->getModel();
        return $this->cView('view');
    }

}
