<?php

namespace App\Http\Controllers;

use App\EmployeeLeave;
use App\User;
use Illuminate\Http\Request;

class LeaveController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/leaves',
            'leaves.',
            ['role:admin'],
            [
                "new_employee_leave_url"=>"/add",
                "form_url"=>"/add",
                "edit_employee_leave_url"=>"/add",
                "remove_employee_leave_url"=>"/remove"
            ],
            EmployeeLeave::class
        );
    }

    public function index()
    {
        $this->leaves = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function addForm()
    {   
        $this->header = $this->hasId()?"Modify leave":"Assign leave";
        $this->employees = User::staffs();
        $this->model = $this->getModel();
        return $this->cView('add');
    }

    public function add()
    {
        $form_data = $this->request->except(['_token']);

        $leave = $this->getModel();

        if($this->hasId()){
            $leave->update($form_data);
        }else{
            $leave::create($form_data);
        }
        
        return redirect($this->root_url)->with('success_msg','Successfully update leave');
    }

    public function remove()
    {
        $this->getModel()->delete();
        return redirect($this->root_url)->with('success_msg','Successfully removed leave');
    }
}
