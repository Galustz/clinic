<?php

namespace App\Http\Controllers;

use App\StandardOperatingProcedure;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StandardOperatingProcedureController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/standard-operating-procedures',
            'standard_operating_procedures.',
            ['role:admin'],
            [
                "new_standard_operating_procedure_url"=>"/add",
                "form_url"=>"/add",
                "edit_standard_operating_procedure_url"=>"/add",
                "remove_standard_operating_procedure_url"=>"/remove",
                "download_standard_operating_procedure_url"=>"/download"
            ],
            StandardOperatingProcedure::class
        );
    }

    public function index()
    {
        $this->departments = User::departments();
        $this->standard_operating_procedures = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->header = $this->hasId()?"Edit standard operating procedure":"Add new standard operating procedure";
        $this->model = $this->getModel();
        $this->file_edit_warning = $this->hasId()?"(The file you upload will replace the old one)":"";
        $this->designations = User::departmentModels(); 
        return $this->cView('add');
    }

    public function add()
    { 
        $standard_operating_procedure = $this->getModel();


        $form_data = [ 
            "description" => $this->request->description, 
            "designation_id" => $this->request->designation_id
        ];

        if($this->request->hasFile('document')){
            $file = $this->request->file('document');
            $file_name = $file->getClientOriginalName();
            $file_url = $file->store('standard_operating_procedures');
            
            $form_data["url"] = $file_url;
            $form_data["file_name"] = $file_name;
        }

        if($this->hasId()){
            if($this->request->hasFile('document'))
                Storage::delete($standard_operating_procedure->url);
                
            $standard_operating_procedure->update($form_data);
        }else{
            $standard_operating_procedure::create($form_data);
        }

        return redirect($this->root_url)->with('success_msg','Successfully uploaded the standard operating procedure');
    }

    public function download()
    {
        return response()->download('storage/'.$this->getModel()->url);
    }

    public function delete()
    {
        $standard_operating_procedure = $this->getModel();
        Storage::delete($standard_operating_procedure->url);
        $standard_operating_procedure->delete();
        return redirect($this->root_url)->with('success_msg','Successfully deleted the file');
    }
}
