<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\DiagnosisRecord;
use App\Filters\MonthFilter;
use App\Filters\YearFilter;
use Carbon\Carbon;
use Fx3costa\LaravelChartJs\Builder;
use Illuminate\Http\Request;

class GraphController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            "/reports/graphs/",
            "admin.reports.graphs",
            ["role:admin"],
            [

            ],
            DiagnosisRecord::class,
        );
    }

    public function default()
    {
        $this->diagnoses = Diagnosis::all();
        $this->diagnosis_id = $this->request->has('diagnosis_id')?$this->request->diagnosis_id:1;
        $this->from = ($this->request->has('from')) ? $this->request->from : Carbon::now()->subDay(7)->format('Y-m-d') ;;
        $this->to = ($this->request->has('to')) ? $this->request->to : Carbon::now()->format('Y-m-d');

        $this->diagnosis = Diagnosis::find($this->diagnosis_id);

        $summary = $this->diagnosis->getSummary();
        $labels = [
            "Under one month",
            "1 month to 1 year",
            "1 year to 5 years",
            "5 years to 60 yers",
            "60 years and above",
            "Totals"
        ];

        $data_labels = [
            "women",
            "men",
            "total"
        ];

        $data_colors = [
            ["yellow","yellow","yellow","yellow","yellow","yellow"],
            ["green","green","green","green","green","green"],
            ["blue","blue","blue","blue","blue","blue"]
        ];

        $datasets = [];
        // foreach ($data_labels as $key => $value) {
            $datasets[] = [
                "label"=> "KE",
                "backgroundColor"=>$data_colors[0],
                "data"=>[
                        $summary->under_one_month->women,
                        $summary->one_month_to_one_year->women,
                        $summary->one_year_to_five_years->women,
                        $summary->five_years_to_sixty_years->women,
                        $summary->sixty_years_and_above->women,
                        $summary->total->women
                    ]
                ];
            $datasets[] = [
                "label"=>"ME",
                "backgroundColor"=>$data_colors[1],
                "data"=>[
                        $summary->under_one_month->men,
                        $summary->one_month_to_one_year->men,
                        $summary->one_year_to_five_years->men,
                        $summary->five_years_to_sixty_years->men,
                        $summary->sixty_years_and_above->men,
                        $summary->total->men
                    ]
                ];
            $datasets[] = [
                "label"=> "Jumla",
                "backgroundColor"=>$data_colors[2],
                "data"=>[
                        $summary->under_one_month->total,
                        $summary->one_month_to_one_year->total,
                        $summary->one_year_to_five_years->total,
                        $summary->five_years_to_sixty_years->total,
                        $summary->sixty_years_and_above->total,
                        $summary->total->total
                    ]
                ];
        // }

        $this->chartjs = (new Builder())
                        ->name('barChartTest')
                        ->type('bar')
                        ->size(['width' => 400, 'height' => 200])
                        ->labels($labels)
                        ->datasets($datasets)
                        ->options([]); 

        return $this->cView('default');
    }
}
