<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/employees',
            'employees.',
            ['role:admin'],
            [
                "new_employee_url"=>'/add',
                "edit_employee_url"=>'/add',
                "form_url"=>'/add',
                "remove_employee_url"=>'/remove',
                "view_employee_url"=>"/view"
            ],
            User::class
        );
    }

    public function index()
    {
        $this->employees = $this->defaultModel::staffs();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->model = $this->getModel();
        $this->header = $this->hasId()?"Edit employee":"Add new employee";
        $this->edit_password_info = $this->hasId()?"(Enter new password to change, leave blank to keep the old one)":"";
        $this->departments = $this->defaultModel::departmentModels();
        $this->genders = $this->defaultModel::$GENDERS;
        return $this->cView('add');
    }

    public function add()
    {
        $employee = $this->getModel();

        $form_data = $this->request->except('_token','password');
        $password = $this->request->password;

        if ($this->hasId()) { 

            if($password!=""&&$password!=null) $form_data["password"] = bcrypt($password);

            $employee->update($form_data);
            $employee->roles()->sync($form_data["department_id"]);
        }else{

            $form_data["password"] = bcrypt($password);

            $employee = $employee::create($form_data);
            $employee->roles()->attach($form_data["department_id"]);
        }

        return redirect($this->root_url)->with("success_msg","Successfully updated employee information");
    }

    public function view()
    {
        $this->link = request()->has('link')?request('link'):'details';

        $this->is_editing = false; 
        $this->id = $this->request->id;
        $this->model = $this->getModel();
        return $this->cView('tabs.details.index');
    }

    public function remove()
    {
        $model = $this->getModel();
        $model->roles()->detach();
        $model->delete();

        return redirect($this->root_url)->with('success_msg',"Removed successfully");
    }
}
