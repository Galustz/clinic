<?php

namespace App\Http\Controllers;

use App\EmployeeAttendance;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/attendance',
            'attendance.',
            ['role:admin'],
            [
                "take_attendance_url"=>"/take",
                "form_url"=>"/take",
            ],
            EmployeeAttendance::class
        );
    }

    public function index()
    {
        $this->filters = ['month','year'];

        $this->dates_in_month = $this->defaultModel::getDatesOfMonth();

        $this->employees = User::staffs();

        return $this->cView('index');
    }

    public function takeForm()
    {
        $this->header = "Take attendance";
        $this->employees = User::staffs();
        $this->date = Carbon::today()->format('Y-m-d');
        return $this->cView('take');
    }

    public function take()
    {  
        $employee_ids = $this->request->employee_id;
        $stati = $this->request->status;
        $date = $this->request->date;

        foreach ($employee_ids as $key => $value) { 
            $employee_attendance = $this->defaultModel::whereEmployeeId($value)->whereDate('date',$date)->first();
            $form_data = ["employee_id"=>$value,"status"=>$stati[$value],"date"=>$date];
            if($employee_attendance)
                $employee_attendance->update($form_data);
            else    
                $this->defaultModel::create($form_data);
        }

        return redirect($this->root_url)->with('success_msg','Attendance updated successfully');
    }
}
