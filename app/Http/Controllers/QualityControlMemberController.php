<?php

namespace App\Http\Controllers;

use App\QualityControlMember;
use App\User;
use Illuminate\Http\Request;

class QualityControlMemberController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/quality-control/members',
            'quality_control.members.',
            ['role:admin'],
            [
                "new_member_url"=>'/add',
                "edit_member_url"=>'/add',
                "form_url"=>"/add",
                "view_member_url"=>"/view",
                "remove_member_url"=>"/remove",
                "show_appointment_letter_url"=>"/appointment-letter/show"
            ],
            QualityControlMember::class
        );
    }

    public function index()
    {
        $this->members = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->model = $this->getModel();
        $this->header='Select members';
        $this->staffs = User::staffs();
        $this->selected_members = $this->defaultModel::getSelectedMembers();
        return $this->cView('add');
    }

    public function add()
    {
        $this->defaultModel::truncate();
        foreach ($this->request->members as $key => $member_id)
            $this->defaultModel::create(["user_id"=>$member_id]);
        return redirect($this->root_url)->with('success_msg','Members updated successfully');
    }
}
