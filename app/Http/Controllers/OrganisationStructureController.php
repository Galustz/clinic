<?php

namespace App\Http\Controllers;

use App\OrganisationStructure;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrganisationStructureController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/organisation-structure',
            'organisation_structures.',
            ['role:admin'],
            [
                "new_organisation_structure_url"=>"/add",
                "form_url"=>"/add",
                "edit_organisation_structure_url"=>"/add",
                "remove_organisation_structure_url"=>"/remove",
                "download_organisation_structure_url"=>"/download"
            ],
            OrganisationStructure::class
        );
    }

    public function index()
    {
        $this->departments = User::departments();
        $this->organisation_structure = $this->defaultModel::orderBy('id','desc')->first();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->header = "Update organisation structure";
        $this->model = $this->getModel();
        $this->file_edit_warning = $this->hasId()?"(The file you upload will replace the old one)":"";
        return $this->cView('add');
    }

    public function add()
    { 
        $organisation_structure = $this->defaultModel::orderBy('id','desc')->first();

        $form_data = [ 
            "description" => $this->request->description 
        ];

        if($this->request->hasFile('document')){
            $file = $this->request->file('document');
            $file_name = $file->getClientOriginalName();
            $file_url = $file->store('organisation_structures');
            
            $form_data["url"] = $file_url;
            $form_data["name"] = $file_name;
        }

        if($organisation_structure){
            if($this->request->hasFile('document'))
                Storage::delete($organisation_structure->url);
            $organisation_structure->update($form_data);
        }else{
            $organisation_structure::create($form_data);
        }

        return redirect($this->root_url)->with('success_msg','Successfully updated the organisation structure');
    }

    public function download()
    {
        return response()->download('storage/'.$this->getModel()->url);
    }

    public function remove()
    {
        $organisation_structure = $this->getModel();
        Storage::delete($organisation_structure->url);
        $organisation_structure->delete();
        return redirect($this->root_url)->with('success_msg','Successfully deleted the file');
    }
}
