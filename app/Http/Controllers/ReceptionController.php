<?php
namespace App\Http\Controllers;

use App\Billing;
use App\BillingChange;
use Illuminate\Http\Request;
use App\Service;
use App\ServiceMode;
use App\PatientType;
use App\PatientQueueTemporary;
use App\BillingList;
use App\BillingTransaction;
use App\FbpResult;
use App\LabTestRecord;
use App\MedicineRecord;
use App\MedicineUnit;
use App\Patient;
use App\PatientVisit;
use App\StoolResult;
use App\UrinalResult;
use App\Vital;
use Carbon\Carbon;

class ReceptionController extends Controller
{  
    public function store(Request $request)
    {
        
        if ($request->has('create_patient')) {
            $this->createPatient($request);
        }

        switch ($request->service) {
            case 1:
                // Seeing a doctor
                $this->seeingADoctor(request());
                break;
            case 2:
                // Only getting medicine
                $this->gettingMedicine(request());
                break;
            case 3:
                // Only doing vital measurements
                $this->doingVitalMeasurements(request());
                break;
            case 4:
                // Only doing investigation
                $this->doingInvestigation(request());
                break;
            case 5:
                // Getting appointment
                $this->gettingAppointment(request());
                break;
            
            default: 
                break;
        }
        return redirect('/reception');
    }

    public function seeingADoctor($request)
    { 
        $vn = $this->getVisitNumber();
        $billing = $this->billPatient( $request, $vn);
        $this->setToQueue( $request, $vn, $billing->id);
        $this->setVisit( $request, $vn); 
    }
    
    public function gettingAppointment($request)
    {  
        $vn = $this->getVisitNumber();

        $billing = $this->billPatient( $request, $vn);

        $billing_list = $this->setToQueue( $request, $vn, $billing->id, true, $billing);

        $this->setVisit( $request, $vn);
    }

    public function gettingMedicine($request)
    {   
        $prn = $request->reg_no;
        $pvn = $this->getVisitNumber();
        if($request->medicine_id!=null){  
            $billing = $this->billPatient($request,$pvn);

             $billinglist = new BillingList;
             $billinglist->patient_registration_no = $prn;
             $billinglist->visit_no = $pvn;
             $billinglist->billing_id = $billing->id;
             $billinglist->type = "Pharmacy";
             $billinglist->status = "paid";
             $billinglist->save();  

             foreach($request->medicine_id as $array_key=>$med){
                 $unit = MedicineUnit::firstOrCreate(['name' => $request->medicine_unit[$array_key] ]);
                 $medicine_record = new MedicineRecord();
                 $medicine_record->medicine_id = $med;
                 $medicine_record->patient_registration_no = $prn;
                 $medicine_record->visit_no = $pvn;
                 $medicine_record->quantity = $request->dose[$array_key];
                 $medicine_record->perday = $request->medicine_per_day[$array_key];
                 $medicine_record->days = $request->days[$array_key].$request->medicine_period[$array_key];
                 $medicine_record->total = $request->medicine_quantity[$array_key];
                 $medicine_record->dose_with_unit = $request->dose[$array_key].':'.$request->medicine_unit[$array_key];
                 $medicine_record->billing_id = $billinglist->id;
                 $medicine_record->status = "Paid";
                 $medicine_record->save(); 
            }       
         }
    }

    public function doingVitalMeasurements($request)
    {
         $vn = $this->getVisitNumber();
         $billing = $this->billPatient($request, $vn);
         $this->setVitals($request, $vn);
    }

    public function doingInvestigation($request)
    { 
        $prn = $request->reg_no;
        $pvn = $this->getVisitNumber();
        $pqn = $this->getLabQueueNumber();

        if($request->has('lab_tests')){

            $billing = $this->billPatient( $request, $pvn);

            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $prn;
            $billinglist->visit_no = $pvn;
            $billinglist->billing_id = $billing->id;
            $billinglist->type = "Laboratory_R";
            $billinglist->status = 'Paid';
            $billinglist->queue_no = $pqn;
            $billinglist->save();  
            
            foreach($request->lab_tests as $l_test){
                $lab_test = new LabTestRecord();
                $lab_test->patient_registration_no = $prn;
                $lab_test->visit_no = $pvn;
                $lab_test->lab_test_id = $l_test;
                $lab_test->status = "Paid";
                $lab_test->billing_id = $billinglist->id;
                $lab_test->save();
                
                if($lab_test->lab_test->name=="Urinal"){
                    $urinal = new UrinalResult();
                    $urinal->lab_test_record_id = $lab_test->id;
                    $urinal->save();
                }
                else if($lab_test->lab_test->name=="FBP"){
                    $fbp = new FbpResult();
                    $fbp->lab_test_record_id = $lab_test->id;
                    $fbp->save();
                }
                else if($lab_test->lab_test->name=="Stool"){
                    $stool = new StoolResult();
                    $stool->lab_test_record_id = $lab_test->id;
                    $stool->save();
                }  
            }   
       }
    }


    public function createPatient(Request $request)
    { 
        $patient = new Patient();
        $patient->reg_no = $request->reg_no;
        $patient->name = $request->name;
        $patient->gender = $request->gender;
        $patient->blood_group = $request->blood_group;
        $patient->type = $request->type;
        $patient->nationality = $request->nationality;
        $patient->DOB = $request->dob;
        $patient->occupation = $request->occupation;

        $patient->email = $request->email;
        $patient->phone = $request->phone;
        $patient->telephone = $request->landline;
        $patient->address = $request->address;

        $patient->patient_type = $request->patient_type;

        if($request->type == 'child')
        {
            $patient->mother_name = $request->mother_name;
            $patient->mother_occupation = $request->mother_occupation;
            $patient->father_name = $request->father_name;
            $patient->father_occupation = $request->father_occupation;
        }

        $image = $request->photo;
        if($image)
        {
            $imageName = $image->getClientOriginalName();
            $image->move('assets/images/patients',$imageName);
            $patient->photo = $imageName;
        } 
        $patient->save();
    }

    public function setVitals(Request $request, int $visit_no)
    {
        $vital = new Vital;
        $vital->patient_reg_no = $request->reg_no;
        $vital->weight = $request->weight;
        $vital->height = $request->height;
        $vital->blood_pressure = $request->blood_presure;
        $vital->pulse_rate = $request->pulse_rate;
        $vital->respiratory_rate = $request->respiratory_rate;
        $vital->temperature = $request->temperature;
        $vital->other = $request->other;
        $vital->nurse = $request->nurse;
        $vital->visit_no = $visit_no; 
        $vital->save();
    }

    public function setVisit(Request $request, $visit_no)
    {
        $visit = new PatientVisit;
        $visit->patient_registration_no = $request->reg_no;
        $visit->visit_no = $visit_no;
        $visit->doctor_id = 1;
        $visit->save();
    }

    public function setToQueue(Request $request, $visit_no, $billing_id, $is_appointment = false, $billing=null)
    {        
        $patient_que_number = $this->getQueueNumber();

        if ($is_appointment) {
            $queue = new PatientQueueTemporary;
            $queue->patient_que_no = $patient_que_number;
            $queue->patient_reg_no = $request->reg_no;
            $queue->service_mode = 1;
            $queue->requested_service = $request->service;
            $queue->doctor = $request->consultant_id;
            $queue->unit_allocation = "Unit 1"; 
            $queue->room_no = "Room 1";
            $queue->visit_no = $visit_no;
            $queue->status =  $billing->amount_due == "0"?"waiting":"partial_payment"; 
            $queue->billing_id = $billing_id;
            $queue->created_at = $date = Carbon::parse("$request->appointment_date $request->appointment_time")->format("Y-m-d H:i:s");
            $queue->type = PatientQueueTemporary::PATIENT_QUEUE_TYPES["appointment"];
            $queue->save();

            

            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $request->reg_no;
            $billinglist->visit_no = $visit_no;
            $billinglist->type = "Appointment";
            $billinglist->queue_no = $patient_que_number;
            $billinglist->billing_id = $billing_id;
            $billinglist->status = $billing->amount_due == "0"?"paid":null; 
            $billinglist->save();
            
            
        
            return $billinglist;
            // Y-m-d H:i:s         
        } else {
            $queue = new PatientQueueTemporary;
            $queue->patient_que_no = $patient_que_number;
            $queue->patient_reg_no = $request->reg_no;
            $queue->service_mode = $request->service_mode;
            $queue->requested_service = $request->service;
            $queue->doctor = $request->doctor;
            $queue->unit_allocation = $request->unit_allocation; 
            $queue->room_no = $request->room_no;
            $queue->visit_no = $visit_no;
            $queue->status = 'waiting';
            $queue->billing_id = $billing_id;
            $queue->save();    
        }

    }

    public function billPatient(Request $request, $visit_no)
    { 
        $billing = new Billing;
        $billing->patient_registration_no = $request->reg_no;
        $billing->visit_no = $visit_no;
        $billing->service_cost = $request->service_cost;
        $billing->amount_due = $request->amount_due;
        $billing->transaction_type = $request->transaction_type;
        $billing->discount = 0;
        $billing->save();

        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $request->amount_paid;
        $billingtransaction->amount_due = $request->amount_due;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();

        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $request->amount_paid;
        $billingchange->change = $request->change;
        $billingchange->change_given = $request->change_given;
        $billingchange->change_due = $request->change_remaining;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();
        return $billing;
    }

    public function getVisitNumber()
    { 
        $request = request();
        $visit_no = PatientVisit::where('patient_registration_no','=',$request->reg_no)->orderBy('created_at','desc')->first();
        if($visit_no==null){
            $vn = 1;
        }else{
            $vn = $visit_no->visit_no+1;
        }
        return $vn;
    }

    public function getLabQueueNumber()
    {   
        $pqns = BillingList::where('type','Laboratory')->where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('queue_no','desc')->first();
        if($pqns==null){
            $pqn = 1;
        }else{
            $pqn = $pqns->queue_no+1;
        }
        return $pqn;
    }

    public function getQueueNumber()
    {   
        $pqns = PatientQueueTemporary::where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('id','desc')->first();
        if($pqns==null){
            $patient_que_number = 1;
        }else{
            $patient_que_number = $pqns->patient_que_no+1;
        }
        return $patient_que_number;
    }

    public function getServicePrice(Request $request){ 
        $s1 = Service::where('name','=',$request->service_id)->first();
        $s2 = ServiceMode::where('name','=',$request->servicemode_id)->first();
        $s3 = PatientType::where('name','=',$request->patienttype_id)->first();
        $sum = $s1->price+ $s2->price+ $s3->price; 
         
        return $sum;
    } 

    public function patientEntered($id){
        $pqt = PatientQueueTemporary::find($id);
        $pqt->status = "Entered";
        $pqt->save();
        return redirect('/reception');
    }

    public function patientOut($id){
        $pqt = PatientQueueTemporary::find($id);
        $pqt->status = "Out";
        $pqt->save();
        return redirect('/reception');
    }

    public function labPatientEntered($id){
        $pqt = BillingList::find($id);
        $pqt->status = "Seen";
        $pqt->save();
        return redirect('/reception');
    }

    public function labPatientOut($id){
        $pqt = BillingList::find($id);
        $pqt->status = "Left";
        $pqt->save();
        return redirect('/reception');
    }

}
