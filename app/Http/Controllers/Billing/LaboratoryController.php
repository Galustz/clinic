<?php

namespace App\Http\Controllers\Billing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\LabTestRecord;
use App\BillingList;
use App\Billing;
use App\BillingTransaction;
use App\BillingChange; 


class LaboratoryController extends Controller
{
    //   
    public function testDetails($id){
        return view('billings.index',['id'=>$id,'title'=>'Billing']);
    }
    
    public function getTestCost($id){
        $labrecord = LabTestRecord::find($id);
        $labcost = $labrecord->lab_test->price;
        return $labcost;
    }

    // public function payBill(Request $request){  
    //     if(!empty($request->status)){
    //         $billing = new Billing;
    //         $billing->patient_registration_no = $request->prn;
    //         $billing->visit_no = $request->pvn;
    //         $billing->service_cost = $request->service_cost;
    //         if( $request->amount_paid>$request->service_cost)
    //             $due =0;
    //         else
    //             $due = $request->amount_paid - $request->service_cost;

    //         $billing->amount_due = $due;
    //         $billing->transaction_type = 'Laboratory';
    //         $billing->discount = 0;
    //         $billing->save();
            
    //         $billingtransaction = new BillingTransaction;
    //         $billingtransaction->amount_paid = $request->amount_paid;
    //         $billingtransaction->amount_due = $due;
    //         $billingtransaction->billing_id = $billing->id;
    //         $billingtransaction->save();

    //         $billingchange = new BillingChange;
    //         $billingchange->amount_recieved = $request->amount_paid;
    //         $billingchange->change = $request->change;
    //         $billingchange->change_given = $request->change;
    //         $billingchange->change_due = 0;//$request->change_remaining;
    //         $billingchange->billing_id = $billing->id;
    //         $billingchange->save();

    //         $pqns = BillingList::where('type','Laboratory')->where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('queue_no','desc')->first();
    //         if($pqns==null){
    //             $pqn = 1;
    //         }else{
    //             $pqn = $pqns->queue_no+1;
    //         }

    //         $pharmacylist = BillingList::find($request->list_id);
    //         $pharmacylist->billing_id = $billing->id;
    //         $pharmacylist->status = "paid";
    //         $pharmacylist->queue_no = $pqn;
    //         $pharmacylist->save();
    //     }else{
    //         $pharmacylist = BillingList::find($request->list_id);
    //         $pharmacylist->billing_id = $billing->id;
    //         $pharmacylist->status = "refused";
    //         $pharmacylist->save();
    //     } 
    //     return redirect('/billing')->with('success','Patient billed');
    // }
    public function payBill(Request $request)
    {
        switch ($request->action) {
            case 'bill':
                return $this->bill($request);
                break;
            case 'discount':
                return $this->discount($request);
                break;
            case 'refused':
                return $this->refused($request);
                break;
            case 'cashless':
                return $this->cashless($request);
                break;
            default:
                # code...
                break;
        }
    }

    public function bill($request){   
        $billing = new Billing;
        $billing->status = 'paid';
        $billing->patient_registration_no = $request->prn;
        $billing->visit_no = $request->pvn;
        $billing->service_cost = $request->total_amount;
        $billing->amount_due = $request->amount_due;
        $billing->transaction_type = 'laboratory';
        $billing->discount = 0;
        $billing->save();
        
        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $request->amount_recieved?$request->amount_recieved:0;
        $billingtransaction->amount_due = $request->amount_due;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();

        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $request->amount_recieved;
        $billingchange->change = $request->change_remaining;
        $billingchange->change_given = $request->change_returned; 
        $billingchange->change_due = $this->a($request->change_remaining) - $this->a($request->change_returned);//$request->change_remaining;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();
 
        $index = 0;
        if($request->has('lab_record_id'))
            foreach($request->lab_record_id as $ms){
                $mr = LabTestRecord::find($request->lab_record_id[$index]);
                if($request->has('status'.$request->lab_record_id[$index]))
                    $mr->status = "Paid";
                else
                    $mr->status = "Refused";
                $mr->save();
                $index++;
            } 
        // if($request->has('status'))
        // foreach($request->status as $ls){
        //     $lr = LabTestRecord::find($request->lab_record_id[$index]);
        //     $lr->status = 'paid';
        //     $lr->save();
        //     $index++;
        // }
        $pqns = BillingList::where('type','Laboratory')->where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('queue_no','desc')->first();
        if($pqns==null){
            $pqn = 1;
        }else{
            $pqn = $pqns->queue_no+1;
        }
        $lablist = BillingList::find($request->list_id);
        $lablist->billing_id = $billing->id;
        $lablist->queue_no = $pqn;
        $lablist->status = 'paid';
        $lablist->save();
        return redirect('/billing')->with('success','Patient billed');
    }

    public function cashless($request){
        $billing = new Billing;
        $billing->status = 'paid';
        $billing->patient_registration_no = $request->prn;
        $billing->visit_no = $request->pvn;
        $billing->service_cost = $request->total_amount;
        $billing->amount_due = 0;
        $billing->transaction_type = 'laboratory';
        $billing->discount = 0;
        $billing->save();
        
        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $request->total_amount;
        $billingtransaction->amount_due = 0;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();

        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $request->total_amount;
        $billingchange->change = 0;
        $billingchange->change_given = 0; 
        $billingchange->change_due = 0;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();
 
        $index = 0;
        if($request->has('lab_record_id'))
            foreach($request->lab_record_id as $ms){
                $mr = LabTestRecord::find($request->lab_record_id[$index]);
                if($request->has('status'.$request->lab_record_id[$index]))
                    $mr->status = "Paid";
                else
                    $mr->status = "Refused";
                $mr->save();
                $index++;
            } 
        // if($request->has('status'))
        // foreach($request->status as $ls){
        //     $lr = LabTestRecord::find($request->lab_record_id[$index]);
        //     $lr->status = 'paid';
        //     $lr->save();
        //     $index++;
        // }
        $pqns = BillingList::where('type','Laboratory')->where('created_at', '>=', date('Y-m-d').' 00:00:00')->orderBy('queue_no','desc')->first();
        if($pqns==null){
            $pqn = 1;
        }else{
            $pqn = $pqns->queue_no+1;
        }
        $lablist = BillingList::find($request->list_id);
        $lablist->billing_id = $billing->id;
        $lablist->queue_no = $pqn;
        $lablist->status = 'paid';
        $lablist->save();
        return redirect('/billing')->with('success','Patient billed');
    }

    public function refused($request)
    {     
        $lablist = BillingList::find($request->list_id);
        $lablist->billing_id = 0;
        $lablist->queue_no = 0;
        $lablist->status = 'refused';
        $lablist->save();
        return redirect('/billing');
    }
    
    public function a($a)
    { 
        $a = str_replace(',', '', $a);
        return $a;
    }

}
