<?php

namespace App\Http\Controllers;

use App\Billing;
use Illuminate\Http\Request;

class CashControlController extends Controller
{ 
    public function __construct()
    {
        $this->initialise(
            '/cash-control',
            'cash_control',
            ['role:admin'],
            [

            ],
            null
        );
    }

    public function cashIn()
    { 
        $this->filters = ['from_created','to_created'];
        $this->data = Billing::getCashInRecords($this->filters);
        return $this->cView('cash_in');
    }
}
