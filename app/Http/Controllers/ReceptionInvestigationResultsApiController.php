<?php

namespace App\Http\Controllers;

use App\BillingList;
use App\LabTestRecord;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReceptionInvestigationResultsApiController extends Controller
{
    public function showResults($billing_list_id)
    {
        $this->billing_list = BillingList::find($billing_list_id);
        $this->patient = $this->billing_list->patient;
        $this->tests =  LabTestRecord::where('patient_registration_no',$this->patient->reg_no)
                                     ->where('visit_no',$this->billing_list->visit_no)
                                     ->where('family_help_id',$this->billing_list->family_help_id)
                                     ->get();
        return view('reception.api.results',$this->view_data);
    }

    public function closeResults($billing_list_id)
    {
        $billing_list = BillingList::find($billing_list_id);
        $billing_list->update(["status"=>"CompleteCycle"]);
    }
}
