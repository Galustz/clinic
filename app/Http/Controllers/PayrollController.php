<?php

namespace App\Http\Controllers;

use App\Filters\MonthFilter;
use App\Filters\YearFilter;
use App\Payment;
use App\Payroll;
use App\PayrollItem;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    public function __construct() {
        $this->initialise(
            "/payroll",
            "payroll.",
            ['role:admin'],
            [],
            Payroll::class
        );
    }

    public function index()
    {
        $this->month = $this->request->has('month')?$this->request->month:Carbon::now()->month;
        $this->year = $this->request->has('year')?$this->request->year:Carbon::now()->year;
        $this->employee_id = $this->request->has('employee_id')?$this->request->employee_id:0;

        $this->employees  = User::staffs();
        $this->years = YearFilter::getYears();
        $this->months = MonthFilter::getMonths();

        if ($this->request->has('generate')) {
 
            if($this->employee_id == 0){
                $this->payroll_items = $this->employees;
            }else{
                $this->payroll_items = User::whereId($this->employee_id)->get();
            }           

            $payroll = Payroll::where('month',$this->month)
                                ->where('year',$this->year)
                                ->orderBy('id','desc')
                                ->first();
            if($payroll){
                $this->payroll_items = $payroll->items;
                $this->payroll = $payroll;
                $this->editable = false;
            }
        }
        return $this->cView("index");
    }

    public function save()
    {
        $date = Carbon::createFromDate( $this->request->year, $this->request->month)
                        ->endOfMonth();

        $total_amount = array_sum($this->request->net_salaries);
        
        $payroll = Payroll::create([
            "month"=>$this->request->month,
            "year"=>$this->request->year,
            "total_amount"=>$total_amount
        ]);

        foreach ($this->request->employee_ids as $key => $employee_id) {
           PayrollItem::create([
               "employee_id"=>$employee_id,
               "net_salary"=>$this->request->net_salaries[$key],
               "payroll_id"=>$payroll->id
           ]);
        } 

        Payment::create([
            "description"=>"Salary payment",
            "date"=>$date,
            "amount"=>$total_amount
        ]);

        return redirect($this->root_url)->with("message_success","Saved successfully");
    }
}
