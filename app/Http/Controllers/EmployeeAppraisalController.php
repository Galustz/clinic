<?php

namespace App\Http\Controllers;

use App\EmployeePerfomanceAppraisal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmployeeAppraisalController extends Controller
{
    public function __construct()
    {
        $this->link = "";
        $this->id = request()->id; 
        $this->initialise(
            '/employee-appraisals',
            'employees.tabs.appraisal.',
            ['role:admin'],
            [
                "new_employee_appraisal_url"=>"/upload/$this->id",
                "form_url"=>"/upload",
                // "edit_employee_appraisal_url"=>"/add",
                "remove_employee_appraisal_url"=>"/remove",
                "download_employee_appraisal_url"=>"/download"
            ],
            EmployeePerfomanceAppraisal::class
        );
    }

    public function index()
    {
        $this->employee_appraisals = $this->defaultModel::whereEmployeeId($this->request->id)->get(); 
        return $this->cView('index');
    }

    public function uploadForm()
    { 
        $this->header = "Upload appraisal";
        $this->model = $this->getModel();
        return $this->cView('add');
    }

    public function upload()
    { 
        $employee_file = $this->defaultModel;

        $form_data = [ 
            "employee_id" => $this->request->employee_id,
            "description" => $this->request->description,
            "date" => $this->request->date
        ];

        if($this->request->hasFile('document')){
            $file = $this->request->file('document');
            $file_name = $file->getClientOriginalName();
            $file_url = $file->store('employee_files');
            
            $form_data["url"] = $file_url;
            $form_data["name"] = $file_name;
        }
 
        $employee_file::create($form_data); 
        $employee_id  =  $this->request->employee_id;
        return redirect("$this->root_url/files/$employee_id")->with('success_msg','Successfully uploaded the employee file');
    }

    public function download()
    {
        return response()->download('storage/'.$this->getModel()->url);
    }

    public function remove()
    {
        $employee_appraisal = $this->getModel();
        Storage::delete($employee_appraisal->url);
        $employee_appraisal->delete();
        return redirect()->back()->with('success_msg','Successfully deleted the file');
    }
}
