<?php

namespace App\Http\Controllers;

use App\MedicineCategory;
use Illuminate\Http\Request;

class MedicineCategoryController extends Controller
{
    public function __construct() {
        $this->initialise(
            "/settings/medicine-categories",
            "medicine_categories.",
            ["auth"],
            [
                "add"=>"/add",
                "edit"=>"/add",
                "remove"=>"/remove"
            ],
            MedicineCategory::class
        );
    }

    public function index()
    {
        $this->medicine_categories = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function addForm(Request $request)
    {
        $this->header = $this->hasId()?"Edit new medicine type":"Add medicine type";
        $this->model = $this->getModel();
        return $this->cView('add');
    }

    public function add(Request $request)
    { 
        $this->category_data = $this->request->only("name");
        $this->medicine_category = $this->getModel();

        if($this->hasId()){
            $this->medicine_category->update($this->category_data);
        }else{
            $this->medicine_category = $this->medicine_category::create($this->category_data);
        }
        return redirect($this->root_url)->with('success_msg','Medicine type updated successfully');
    }

    public function remove(Request $request)
    {
        $this->getModel()->medicines()->delete();
        $this->getModel()->delete();

        return redirect($this->root_url)->with('success_msg','Announcement deleted successfully');
    }
}
