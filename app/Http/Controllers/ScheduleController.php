<?php

namespace App\Http\Controllers;

use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/schedules',
            'schedules.',
            ['role:admin'],
            [
                "form_url"=>"/add",
                "new_schedule_url"=>"/add",
                "edit_schedule_url"=>"/add",
                "remove_schedule_url"=>"/remove",
                "view_schedule_url"=>"/view"
            ],
            Schedule::class
        );

        $this->is_og= $this->request->is("og-*");
    }

    public function index()
    {
            $this->filters = ['year','month'];

            if($this->is_og)
                $this->schedules = $this->defaultModel::canFilter($this->filters)->whereType('og')->latest()->get();
            else
                $this->schedules = $this->defaultModel::canFilter($this->filters)->whereType('qc')->latest()->get();
            
            return $this->cView('index');
    }

    public function addForm()
    {
            $this->header = $this->hasId()?"Edit schedule":"Add new schedule";
            $this->file_edit_message = $this->hasId()?"(The file you upload will automatically replace the old one":"";
            $this->model = $this->getModel(); 
            return $this->cView('add');
    }

    public function add()
    {
        $schedule = $this->getModel();
        $form_data = [];

        if($this->request->hasFile('document')){
            $file = $this->request->file('document');
            $form_data["file_name"] = $file->getClientOriginalName();
            $form_data["url"] = $file->store('meeting_minutes');            
        }

        $form_data = array_merge($this->request->only('content','date','time'),$form_data);
        $form_data["type"] = $this->is_og?"og":"qc";

        if($this->hasId()){
            if( $this->request->should_remove_file){
                Storage::delete($schedule->url);
                $form_data["file_name"] = null;
                $form_data["url"] = null;
            }
            $schedule->update($form_data);
        }else{
            $form_data["url"] = $file_url??null;
            $form_data["file_name"] = $file_name??null;
            $schedule::create($form_data);
        }

        return redirect($this->root_url)->with('success_msg','Successfully update the schedule');
    }

    public function remove()
    {
        $model = $this->getModel();
        if($model->hasFile())
            Storage::delete($model->url);
        $model->delete();
        return redirect($this->root_url)->with('success_msg','Minute deleted successfully');
    }

    public function view()
    {
        $this->model = $this->getModel();
        return $this->cView('view');
    }
}
