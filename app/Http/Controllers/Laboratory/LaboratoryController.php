<?php

namespace App\Http\Controllers\Laboratory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LabTestRecord;
use App\BillingList;
use App\FbpResult;
use App\StoolResult;
use App\UrinalResult;
use App\NoteFromLaboratory;
use App\RequestEquipment;


class LaboratoryController extends Controller
{
    //
    
    public function index()
    {
        $title = 'Laboratory';
        return view('lab.index',compact('title'));
    }

    public function testView($id)
    {
        return view('lab.test',['id'=>$id,'title'=>'Laboratory']);
    }

    public function viewResults($id){
        return view('lab.results',['id'=>$id,'title'=>'Laboratory']);
    }

    public function takeSample($id){
        $lablist = BillingList::find($id);
        $lablist->status = "taken sample";
        $lablist->save();
        return redirect('laboratory');
    }

    public function saveTestResults(Request $request){  
        if($request->fbp_result_id){
            $fbp = FbpResult::find($request->fbp_result_id);
            $fbp->wbc = $request->wbc;
            $fbp->gra = $request->gra;
            $fbp->Hb = $request->Hb;
            $fbp->from_machine=$request->machine_fbp;
            $fbp->save();    
        }
        

        if($request->urinal_result_id){
            $urinal = UrinalResult::find($request->urinal_result_id);
            $urinal->macro = $request->macro_urinal;
            $urinal->blood = $request->blood_urinal;
            $urinal->color = $request->color_urinal;
            $urinal->pus = $request->pus_urinal;
            $urinal->ova = $request->ova_urinal;
            $urinal->other = $request->other_urinal;
            $urinal->from_machine = $request->machine_urinal;
            $urinal->save(); 
        }
        
        if($request->stool_result_id){
            $stool = StoolResult::find($request->stool_result_id);
            $stool->macro = $request->macro_stool;
            $stool->blood = $request->blood_stool;
            $stool->pus = $request->pus_stool;
            $stool->ova = $request->ova_stool;
            $stool->other = $request->other_stool;
            $stool->from_machine = $request->machine_stool;
            $stool->save();    
        }
        

        $index = 0;
        if(!empty($request->result))
        foreach($request->result as $result){
            $lab_record = LabTestRecord::find($request->test_id[$index]);
            $lab_record->result = $result;
            $lab_record->status = "done";
            $lab_record->save();
            $index++;
        }
        
        $nfl = NoteFromLaboratory::find($request->note_from_laboratory_id);
        if($nfl){
            $nfl->content = $request->note_from_laboratory;
            $nfl->save();
        }
        

        return redirect()->back();
    }
    public function submitTestResults(Request $request){ 
        
        if($request->has('fbp_result_id')){
            $fbp = FbpResult::find($request->fbp_result_id);
            $fbp->wbc = $request->wbc;
            $fbp->gra = $request->gra;
            $fbp->Hb = $request->Hb;
            $fbp->from_machine=$request->machine_fbp;
            $fbp->save(); 
        }
       
 
        if($request->has('urinal_result_id')){
            $urinal = UrinalResult::find($request->urinal_result_id);
            $urinal->macro = $request->macro_urinal;
            $urinal->blood = $request->blood_urinal;
            $urinal->color = $request->color_urinal;
            $urinal->pus = $request->pus_urinal;
            $urinal->ova = $request->ova_urinal;
            $urinal->other = $request->other_urinal;
            $urinal->from_machine = $request->machine_urinal;
            $urinal->save();
        }
        
        if($request->has('stool_result_id')){
            $stool = StoolResult::find($request->stool_result_id);
            $stool->macro = $request->macro_stool;
            $stool->blood = $request->blood_stool;
            $stool->pus = $request->pus_stool;
            $stool->ova = $request->ova_stool;
            $stool->other = $request->other_stool;
            $stool->from_machine = $request->machine_stool;
            $stool->save();  
        }
        

        $index = 0;
        if(!empty($reqeust->result))
        foreach($request->result as $result){
            $lab_record = LabTestRecord::find($request->test_id[$index]);
            $lab_record->result = $result;
            if(empty($result)||$result=="")
                $lab_record->status = "testing";
            else
                $lab_record->status = "done";

            $lab_record->save();
            $index++;
        }
        
        $nfl = NoteFromLaboratory::find($request->note_from_laboratory_id);
        if($nfl){
            $nfl->content = $request->note_from_laboratory;
            $nfl->save();
        }
        $lablist = BillingList::find($request->lablist_id);
        $lablist->status = "done";
        $lablist->save();
        return view('lab.index',['title'=>'Laboratory','success_lab'=>'Lab test results submitted']);
    }

    public function requestEquipment(Request $request){
        $req = new RequestEquipment;
        $req->name = $request->name;
        $req->status = 'requested';// $request->status;
        $req->save();
        return redirect('/laboratory')->with('equipment',true);
    }

    public function editRequestForm($id){
        return view('lab.edit-request',['id'=>$id,'title'=>'Edit Request']);
    }

    public function editRequest(Request $request){ 
        $req = RequestEquipment::find($request->id);
        $req->name  = $request->name;
        $req->status = $request->status;
        $req->save();
        return redirect('laboratory');
    }
}
