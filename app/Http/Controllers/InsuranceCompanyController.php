<?php

namespace App\Http\Controllers;

use App\InsuranceCompany;
use Illuminate\Http\Request;

class InsuranceCompanyController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            "/insurance-companies",
            "insurance-companies",
            ["role:admin"],
            [
                "new_ic_url"=>'/add',
                "edit_ic_url"=> '/add',
                "delete_ic_url"=> '/remove'
            ],
            InsuranceCompany::class
        );
    }

    public function index()
    {
        $this->insurance_companies = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->model = $this->getModel();
        $this->header = $this->hasId()?"Edit insurance company":"Add new insurance company";
        return $this->cView('add');
    }

    public function add()
    { 
        $insurance_company = $this->getModel();
        $form_data = $this->request->except('_token');
        if($this->is_editing){
            $insurance_company->update($form_data);    
        }else{
            $insurance_company::create($form_data);
        }
        return redirect($this->root_url)->with('success_msg','Successfully update the insurance company details');
    }

    public function remove()
    {
        $this->getModel()->delete();
        return redirect($this->root_url)->with('success_msg','Insurance company removed successfully!');
    }
}
