<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/announcements',
            'announcements.',
            ['auth'],
            [
                'new_announcement_url'=>'/add',
                'edit_announcement_url'=>'/add',
                'dismiss_announcement_url'=>'/dismiss',
                'form_url'=>'/add'
            ],
            Announcement::class
        );
    }

    public function index()
    {
        $this->filters = ['from_created','to_created'];
        $this->announcements = $this->defaultModel::canFilter($this->filters)->get();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->header = $this->hasId()?"Edit new announcement":"Add announcement";
        $this->model = $this->getModel();
        $this->staffs = $this->request->user()::staffs();
        $this->target_types = Announcement::$TARGET_TYPES;
        $this->selected_target_types = $this->model->target_type==$this->target_types['select']?$this->model->targets:[];

        if($this->selected_target_types!=[])
            $this->selected_target_types = $this->selected_target_types->map(function($value){
                return $value->user_id;
            })->toArray(); 
            
        return $this->cView('add');
    }

    public function add()
    {
        $this->announcement_data = $this->request->only('description','target_type');
        $this->announcement = $this->getModel();

        if ($this->hasId()) {
            $this->announcement->update($this->announcement_data); 
        }else{
            $this->announcement = $this->announcement::create($this->announcement_data);
        }

        $this->announcement->syncTargets($this->request->targets);

        return redirect($this->root_url)->with('success_msg','Announcement updated successfully');
    }

    public function dismiss()
    {
        $this->getModel()->delete();
        return redirect($this->root_url)->with('success_msg','Announcement dismissed successfully');
    }

    public function seen()
    {
        $this->request->user()->announcementTargets()->update(["status"=>"seen"]);
    }
}
