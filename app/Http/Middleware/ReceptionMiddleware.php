<?php

namespace App\Http\Middleware;

use Closure;

class ReceptionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->hasRole('receptionist')||$request->user()->hasRole('admin'))
		{
            return $next($request);
		}
		return redirect('/');
    }
}
