<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone_no', 100)->nullable();
            $table->string('employee_code', 100)->nullable();
            $table->string('joining_date', 100)->nullable();
            $table->string('gender', 100)->nullable();
            $table->text('address')->nullable();
            $table->string('department_id', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_no');
            $table->dropColumn('employee_code');
            $table->dropColumn('joining_date');
            $table->dropColumn('gender');
            $table->dropColumn('address');
            $table->dropColumn('department_id');
        });
    }
}
