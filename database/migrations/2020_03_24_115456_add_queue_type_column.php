<?php

use App\PatientQueueTemporary;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQueueTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patient_queue_temporaries', function (Blueprint $table) {
            $table->string('type', 100)->default(PatientQueueTemporary::PATIENT_QUEUE_TYPES["seeing_doctor"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patient_queue_temporaries', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
