<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMeetingMinutesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_minutes', function (Blueprint $table) {
            $table->dropColumn('schedule_id');
            $table->string('date', 100)->nullable();
            $table->string('file_name', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_minutes', function (Blueprint $table) {
            $table->dropColumn('date');
            $table->dropColumn('file_name');
            $table->integer('schedule_id')->nullable();
        });
    }
}
