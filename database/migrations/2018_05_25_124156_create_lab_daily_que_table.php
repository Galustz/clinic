<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabDailyQueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Lab_daily_que', function (Blueprint $table) 
        {
            $table->increments('lab_que_id');
            $table->string('que_no');
            $table->string('patient_reg_no');
            $table->string('status');
            $table->date('date');
            $table->timestamps();
  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Lab_daily_que');
    }
}
