<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrashedColumns2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_modes', function (Blueprint $table) {
            //
            $table->string('trashed', 100)->nullable()->default('false');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_modes', function (Blueprint $table) {
            //
            $table->dropColumn('trashed');
        });
    }
}
