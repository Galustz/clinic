<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicineStockBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_stock_batches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medicine_id');
            $table->string('number_of_units_bought', 100);
            $table->string('total_price', 100);
            $table->string('total_quantity', 100); 
            $table->date('expiry_date')->nullable();
            $table->string('batch_number', 100);
            $table->string('remaining_quantity', 100);
            $table->string('unit_purchasing_price', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_stock_batches');
    }
}
