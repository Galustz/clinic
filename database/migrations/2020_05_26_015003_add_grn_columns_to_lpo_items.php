<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGrnColumnsToLpoItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('l_p_o_items', function (Blueprint $table) {
            $table->string('received_quantity', 100)->nullable()->after('type');
            $table->string('sale_status', 100)->nullable()->after('type');
            $table->string('reason', 100)->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('l_p_o_items', function (Blueprint $table) {
            $table->dropColumn('quantity');
            $table->dropColumn('status');
            $table->dropColumn('reason');
        });
    }
}
