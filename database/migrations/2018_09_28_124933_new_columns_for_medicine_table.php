<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewColumnsForMedicineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicines', function (Blueprint $table) {
            //
            $table->string('quantity_per_unit', 100);
            $table->string('unit_purchasing_price', 100);
            $table->string('code_2', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicines', function (Blueprint $table) {
            //
            $table->dropColumn('quantity_per_unit');
            $table->dropColumn('unit_purchasing_price');
            $table->dropColumn('code_2');
        });
    }
}
