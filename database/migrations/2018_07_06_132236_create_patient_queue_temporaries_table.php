<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientQueueTemporariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_queue_temporaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patient_que_no');
            $table->string('service_mode');
            $table->string('type_of_patient');
            $table->string('room_no');
            $table->string('doctor');
            $table->string('requested_service');
            $table->string('unit_allocation');
            $table->string('visit_no');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_queue_temporaries');
    }
}
