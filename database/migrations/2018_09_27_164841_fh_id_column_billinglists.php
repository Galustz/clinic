<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FhIdColumnBillinglists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_lists', function (Blueprint $table) {
            //
            $table->integer('family_help_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_lists', function (Blueprint $table) {
            //
            $table->dropColumn('family_help_id');
        });
    }
}
