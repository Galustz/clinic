<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardOperatingProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_operating_procedures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 100)->nullable();
            $table->string('file_name', 100)->nullable();
            $table->text('description')->nullable();
            $table->integer('designation_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_operating_procedures');
    }
}
