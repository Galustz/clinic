<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LpoColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('l_p_os', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('supplier_id');
            $table->string('status')->default('created');
            $table->string('terms')->default('cash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('l_p_os', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('supplier_id');
            $table->dropColumn('status');
            $table->dropColumn('terms');
        });
    }
}
