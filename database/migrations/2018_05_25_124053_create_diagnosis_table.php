<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosis', function (Blueprint $table) 
        {
            $table->increments('diagnosis_id');
            $table->mediumText('examination');
            $table->mediumText('diff_diagnosis');
            $table->string('file_name');
            $table->string('prescription_id');
            $table->mediumText('description');
            $table->string('lab_id');
            $table->string('doc_id');
            $table->string('patient_reg_no');
            $table->date('date');
            $table->string('service_id');
            $table->string('service_mode_id');
            $table->string('unit_id');
            $table->timestamps();
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosis');
    }
}
