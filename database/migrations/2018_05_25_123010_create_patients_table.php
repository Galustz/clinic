<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('reg_no')->unique();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('telephone')->nullable();
            $table->string('email')->unigue()->nullable();
            $table->date('DOB');
            $table->string('blood_group');
            $table->string('gender');
            $table->string('nationality')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('mother_occupation')->nullable();
            $table->string('father_name')->nullable();
            $table->string('father_occupation')->nullable();
            $table->string('occupation')->nullable();
            $table->string('photo')->nullable();
            $table->string('type');
            $table->string('patient_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
