<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patient_registration_no', 100);
            $table->integer('equipment_id')->nullable();
            $table->string('quantity', 100)->nullable();
            $table->integer('billing_id')->nullable()->default(0);
            $table->string('status', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment_records');
    }
}
