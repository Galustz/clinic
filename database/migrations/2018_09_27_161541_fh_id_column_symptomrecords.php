<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FhIdColumnSymptomrecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('symptom_records', function (Blueprint $table) {
            //
            $table->integer('family_help_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('symptom_records', function (Blueprint $table) {
            //
            $table->dropColumn('family_help_id');
        });
    }
}
