<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_batches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quantity')->nullable();
            $table->string('remaining_quantity')->nullable();
            $table->string('date')->nullable();
            $table->string('code')->nullable();
            $table->integer('equipment_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment_batches');
    }
}
