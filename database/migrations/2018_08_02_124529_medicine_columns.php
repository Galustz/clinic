<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MedicineColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicines', function (Blueprint $table) {
            //
            $table->string('purchasing_unit_name');
            $table->string('selling_unit_name');
            $table->string('predefined_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicines', function (Blueprint $table) {
            //
            $table->dropColumn('purchasing_unit_name');
            $table->dropColumn('selling_unit_name');
            $table->dropColumn('predefined_value');
        });
    }
}
