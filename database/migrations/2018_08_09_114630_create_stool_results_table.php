<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoolResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stool_results', function (Blueprint $table) {
            $table->increments('id');
            $table->text('macro')->nullable();
            $table->string('blood')->nullable();
            $table->string('pus')->nullable();
            $table->string('ova')->nullable();
            $table->string('other')->nullable();
            $table->text('from_machine')->nullable();
            $table->string('lab_test_record_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stool_results');
    }
}
