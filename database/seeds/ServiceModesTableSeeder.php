<?php

use Illuminate\Database\Seeder;
use App\ServiceMode;

class ServiceModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $serviceMode = new ServiceMode;
        $serviceMode->name = "Express";
        $serviceMode->price = "1";
        $serviceMode->save();
        $serviceMode = new ServiceMode;
        $serviceMode->name = "Emergency";
        $serviceMode->price = "1";
        $serviceMode->save();
        $serviceMode = new ServiceMode;
        $serviceMode->name = "Normal";
        $serviceMode->price = "1";
        $serviceMode->save();
    }
}
