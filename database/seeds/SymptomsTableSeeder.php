<?php

use Illuminate\Database\Seeder;
use App\Symptom;

class SymptomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $symptom = new Symptom;
        $symptom->name = "Headache";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Stomachache";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Chest pain";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Neck cramp";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Painfull joints";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "General weaknes";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Swells";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Swollen feet";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Rash";
        $symptom->save();
        $symptom = new Symptom;
        $symptom->name = "Migraine";
        $symptom->save();
    }
}
