<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        $this->call(RoleTableSeeder::class); 
        $this->call(UserTableSeeder::class);
 
        $this->call(LabTestsTableSeeder::class); 
        $this->call(RoomsTableSeeder::class);
        $this->call(ServicesTableSeeder::class); 
        $this->call(UnitsTableSeeder::class);
        $this->call(MedicineCategoryTableSeeder::class);
        $this->call(PatientTypesTableSeeder::class);
        $this->call(ServiceModesTableSeeder::class);
        $this->call(DiagnosisTableSeeder::class);
    }
}
