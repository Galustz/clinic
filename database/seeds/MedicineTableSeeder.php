<?php

use Illuminate\Database\Seeder;
use App\Medicine;

class MedicineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $med = new Medicine;
        $med->name = 'Aspirin';
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '500';
        $med->description = 'Good for pain';
        $med->medicine_category_id = 3;
        $med->save();

        $med = new Medicine;
        $med->name = 'Paracetamol';
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '200';
        $med->description = 'Good for pain';
        $med->medicine_category_id = 3;
        $med->save();
        $med = new Medicine;
        $med->name = 'Penincillin';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '1500';
        $med->description = 'Good for infection';
        $med->medicine_category_id = 4;
        $med->save();
        $med = new Medicine;
        $med->name = 'Vitamin C';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '600';
        $med->description = 'Good for health';
        $med->medicine_category_id = 5;
        $med->save();
        $med = new Medicine;
        $med->name = 'Chloroquin';
        $med->selling_price = '500';
        $med->description = 'Good for malaria';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->medicine_category_id = 3;
        $med->save();
        $med = new Medicine;
        $med->name = 'Amoxillin';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '700';
        $med->description = 'Good for infection';
        $med->medicine_category_id = 3;
        $med->save();
        $med = new Medicine;
        $med->name = 'Cristapen';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '5500';
        $med->description = 'Good for infection';
        $med->medicine_category_id = 3;
        $med->save();
        $med = new Medicine;
        $med->name = 'Tramadol';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '2500';
        $med->description = 'Good for pain';
        $med->medicine_category_id = 3;
        $med->save();
        $med = new Medicine;
        $med->name = 'Dormadol';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '5000';
        $med->description = 'Good for pain';
        $med->medicine_category_id = 3;
        $med->save();
        $med = new Medicine;
        $med->name = 'ALU';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '3000';
        $med->description = 'Good for malaria';
        $med->medicine_category_id = 1;
        $med->save();
        $med = new Medicine;
        $med->name = 'Anti-Acids';
        
        $med->code = 'm';
        $med->medicine_category_id = 1;
        $med->purchasing_unit_name = "s";
        $med->selling_unit_name = "asdf";
        $med->predefined_value = "art";
        $med->selling_price = '500';
        $med->description = 'Good for acid excess';
        $med->medicine_category_id = 1;
        $med->save();


    }
}
