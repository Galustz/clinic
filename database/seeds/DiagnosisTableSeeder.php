<?php

use Illuminate\Database\Seeder;

class DiagnosisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    $diseases_array = [
        'Acute Flacid Paralysis',
        'Cholera',
        'Dysentery',
        'Measles',
        'Meningitis',
        'Neonatal Tetanus',
        'Plague',
        'Relapsing Fever(Louse borne typhus)',
        'Yellow Fever',
        'Influenza',
        'Typhoid',
        'Rabies',
        'Onchocerciasis',
        'Trypanosomiasis',
        'Viral haemorrhagic fevers',
        'Diarrhea with some dehydration',
        'Diarrhea with severe dehydration',
        'Schistosomiasis',
        'Malaria blood slide positive',
        'Malaria mRDT positive',
        'Malaria, clinical(No test)',
        'Malaria in pregnancy',
        'STI Genital Discharge Syndrome',
        'STI Genital Ulcer Disease',
        'STI Pelvic Inflamatory Diseases',
        'Sexually Transmitted Infections, Other',
        'Tuberculosis',
        'Leprosy',
        'Intestinal Worms',
        'Anaemia, Mild/Moderate',
        'Anaemia, Severe',
        'Sickle cell Disease',
        'Ear Infection, Acute',
        'Ear Infection, Chronic',
        'Eye diseases, Infectious',
        'Eye diseases, Non-infectious',
        'Eye diseases, Injuries',
        'Skin Infection, Non-Fungal',
        'Skin Infection, Fungal',
        'Skin Infection, Non-infectious',
        'Fungal Infection, Non-skin',
        'Osteomyelitis',
        'Nenonatal sepsis',
        'Low birth weight and Prematurity comoplications',
        'Birth asphyxia',
        'Pneumonia, Non-Severe',
        'Pneumonia, Severe',
        'Upper Respiratory Infections(Pharyngitis, Tonsillitis, Rhinitis)',
        'Cerebral palsy',
        'Urinal Tract Infections(UTI)',
        'Gynaecological diseases, Other',
        'Kwashiokor',
        'Marasmus',
        'Marasmic Kwashiakor',
        'Moderate Malnutrition',
        'Vitamin A Deficiency',
        'Other Nutritional Disorders',
        'Caries',
        'Periodntal Diseases',
        'Dental Emergency Care',
        'Dental Conditions, Other',
        'Fractures/Dislocations',
        'Burn',
        'Poisonig',
        'Road Traffic Accidents',
        'Pregnancy complications',
        'Abortion',
        'Snake and Insect Bites',
        'Animal bite(suspected rabies)',
        'Animal bite(No suspected rabies',
        'Emergencies, Other',
        'Surgical conditions, Other',
        'Epilepsy',
        'Psychoses',
        'Neurosis',
        'Substance abuse',
        'Hypertension',
        'Rheumatic Fever',
        'Cardiovascular Diseases,Other',
        'Bronchial Asthma',
        'Peptic Ulcers',
        'GIT Diseases, Other Non-infectious',
        'Diabetes',
        'Rheumatoid and Joint Diseases',
        'Thyroid Diseases',
        'Neoplasma/Cancer',
        'Ill Defined Symptoms(no Diagnosis)',
        'Diagnosis, Other',
        'Waliopewa rufaa'
    ];

        foreach ($diseases_array as $key => $value) {
            $diagnosis = new \App\Diagnosis;
            $diagnosis->name = $value;
            $diagnosis->save();
        }
    }
}
