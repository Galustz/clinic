<?php

use Illuminate\Database\Seeder;
use App\LabTest;

class LabTestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $test = new LabTest();
        $test->name = 'FBP';
        $test->code = 'c';
        $test->price = '0';
        $test->save();
        
        $test = new LabTest();
        $test->name = 'Urinal';
        $test->code = 'c';
        $test->price = '0';
        $test->save();

        $test = new LabTest();
        $test->name = 'Stool';
        $test->code = 'c';
        $test->price = '0';
        $test->save(); 
    }
}
