<?php

use Illuminate\Database\Seeder;
use App\PatientType;

class PatientTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $patientType = new PatientType;
        $patientType->name = "Insurance";
        $patientType->price = "1";
        $patientType->save();
        $patientType = new PatientType;
        $patientType->name = "Corporate";
        $patientType->price = "1";
        $patientType->save();
        $patientType = new PatientType;
        $patientType->name = "Cash";
        $patientType->price = "1";
        $patientType->save();
    }
}
