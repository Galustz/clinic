<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $service = new Service;
        $service->name = "To see a doctor";
        $service->price = '11000';
        $service->save();
        $service = new Service;
        $service->name = "To get medicine only";
        $service->price = '2000';
        $service->save();
        $service = new Service;
        $service->name = "For vital measurements only";
        $service->price = '1000';
        $service->save();
        $service = new Service;
        $service->name = "For investigation only";
        $service->price = '7000';
        $service->save();
    }
}
