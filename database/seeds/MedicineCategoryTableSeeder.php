<?php

use Illuminate\Database\Seeder;
use App\MedicineCategory;

class MedicineCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $med =new Medicinecategory;
        $med->name = "Syrup";
        $med->save();

        $med =new Medicinecategory;
        $med->name = "Injection";
        $med->save();

        $med =new Medicinecategory;
        $med->name = "Cream";
        $med->save();
        
        $med =new Medicinecategory;
        $med->name = "Tablet";
        $med->save();
    }
}
