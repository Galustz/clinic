@echo off

if exist .git (
	git pull origin master
	php artisan migrate  
	
) else (
	git status
	git init 
	git clone https://adaptrix@bitbucket.org/adaptrix/clinic.git 
	cd clinic 
)

set /p Var1="Press enter to exit %Var1% "